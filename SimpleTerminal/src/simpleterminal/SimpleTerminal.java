/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal;

import simpleterminal.frame.TerminalFrame;
import java.util.LinkedList;

/**
 *
 * @author Ricardo
 */
public final class SimpleTerminal {

    private int xSize;
    private int ySize;
    private int topPadding;
    private int bottomPadding;
    private String leftPaddingString;
    private int xCursorPos;
    private int yCursorPos;
    private boolean hasFrame;
    private TerminalFrame frame;
    private LinkedList<char[]> charBuffer;
    private int bufferOffset;
    private char cursor;

    public SimpleTerminal(int xSize, int ySize, int topPadding, int bottomPadding, int leftPadding, char cursor) {

        this.xSize = xSize;
        this.ySize = ySize;
        this.topPadding = topPadding;
        this.bottomPadding = bottomPadding;
        this.hasFrame = false;
        this.frame = null;
        this.charBuffer = new LinkedList<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < leftPadding; i++) {
            sb.append(' ');
        }
        this.leftPaddingString = sb.toString();
        this.cursor = cursor;
        this.clearBuffer();
    }

    private char[] createNewLine() {
        char[] line = new char[xSize];
        for (int i = 0; i < line.length; i++) {
            line[i] = ' ';
        }
        return line;
    }

    public void clearBuffer() {
        this.xCursorPos = 0;
        this.yCursorPos = 0;
        this.bufferOffset = 0;
        this.charBuffer.clear();
        for (int i = 0; i < ySize; i++) {
            this.charBuffer.add(this.createNewLine());
        }
    }

    public void clearTerminal() {
        this.xCursorPos = 0;
        this.yCursorPos = 0;
        for (int i = 0; i < ySize; i++) {
            this.charBuffer.remove(this.bufferOffset);
        }
        for (int i = 0; i < ySize; i++) {
            this.charBuffer.add(this.bufferOffset, this.createNewLine());
        }
    }

    public void moveCursorUp() {
        if (this.yCursorPos > 0) {
            this.yCursorPos--;
        }
    }

    public void moveCursorDown() {
        if (this.yCursorPos == this.ySize - 1) {
            this.scrollDown();
        } else {
            this.yCursorPos++;
        }
    }

    public void moveCursorLeft() {
        if (xCursorPos == 0) {
            this.moveCursorUp();
            this.moveCursorToLineEnd();
        } else {
            xCursorPos--;
        }
    }

    public void moveCursorRight() {
        if (xCursorPos == this.xSize - 1) {
            this.moveCursorDown();
            this.moveCursorToLineStart();
        } else {
            xCursorPos++;
        }
    }

    public void moveCursorToLineStart() {
        this.xCursorPos = 0;
    }

    public void moveCursorToLineEnd() {
        this.xCursorPos = this.xSize - 1;
    }

    public void moveCursorToTerminalTop() {
        this.yCursorPos = 0;
    }

    public void moveCursorToTerminalBottom() {
        this.yCursorPos = this.ySize - 1;
    }

    public void scrollUp() {
        if (this.bufferOffset != 0) {
            this.bufferOffset--;
        }
    }

    public void scrollDown() {
        this.bufferOffset++;
        if (this.charBuffer.size() - this.bufferOffset < this.ySize) {
            this.charBuffer.add(this.createNewLine());
        }
    }

    public void pageUp() {
        for (int i = 0; i < this.ySize; i++) {
            this.scrollUp();
        }
    }

    public void pageDown() {
        for (int i = 0; i < this.ySize; i++) {
            this.scrollDown();
        }
    }

    public void newLine() {
        this.moveCursorDown();
        this.moveCursorToLineStart();
    }

    public void backspace() {
        this.moveCursorLeft();
        this.charBuffer.get(this.yCursorPos)[this.xCursorPos] = ' ';
    }

    public void writeChar(char c) {
        switch (c) {
            case '\n':
                this.newLine();
                break;
            case '\t':
                this.writeStr("    ");
                break;
            case '\r':
                this.moveCursorToLineStart();
                break;
            case '\b':
                this.backspace();
            case '\f':
                break;
            default:
                this.charBuffer.get(this.yCursorPos + this.bufferOffset)[this.xCursorPos] = c;
                this.moveCursorRight();
        }
    }

    public void writeStr(String str) {
        for (int i = 0; i < str.length(); i++) {
            this.writeChar(str.charAt(i));
        }
    }

    public void setCursorXPos(int xPos) {
        if (xPos >= 0 && xPos < this.xSize) {
            this.xCursorPos = xPos;
        }
    }

    public void setCursorYPos(int yPos) {
        if (yPos >= 0 && yPos < this.ySize) {
            this.yCursorPos = yPos;
        }
    }

    public void setCursorLine(int line) {
        this.setCursorYPos(line - 1);
    }

    public void setCursorColumn(int column) {
        this.setCursorXPos(column - 1);
    }

    public void setCursorPos(int xCursorPos, int yCursorPos) {
        this.setCursorXPos(xCursorPos);
        this.setCursorYPos(yCursorPos);
    }

    public void setCursorLineColumn(int line, int column) {
        this.setCursorLine(line);
        this.setCursorColumn(column);
    }

    private String generateLineStr(int line) {
        char[] aux;
        if (line == this.yCursorPos + this.bufferOffset) {
            aux = this.charBuffer.get(line).clone();
            aux[this.xCursorPos] = this.cursor;
        } else {
            aux = this.charBuffer.get(line);
        }
        return new String(aux);
    }

    public String leftPaddingString() {
        return this.leftPaddingString;
    }

    public int offset() {
        return this.bufferOffset;
    }

    public int[] cursorPos() {
        return new int[]{this.xCursorPos, this.yCursorPos};
    }

    public int[] cursorLineColumn() {
        return new int[]{this.yCursorPos + 1, this.xCursorPos + 1};
    }

    public void registerFrame(TerminalFrame frame) {
        this.hasFrame = true;
        this.frame = frame;
    }

    public void unregisterFrame() {
        this.hasFrame = false;
        this.frame = null;
    }

    public int[] terminalSize() {
        return new int[]{this.xSize, this.ySize};
    }

    public int[] requiredMainTerminalSize() {
        int x = this.xSize + this.leftPaddingString.length();
        int y = this.ySize + this.topPadding + this.bottomPadding;
        if (this.hasFrame) {
            x += 2;
            y += 2;
        }
        return new int[]{x, y};
    }

    public int[] requiredCenteredMainTerminalSize() {
        int x = this.xSize + this.leftPaddingString.length() * 2;
        int y = this.ySize + this.topPadding + this.bottomPadding;
        if (this.hasFrame) {
            x += 2;
            y += 2;
        }
        return new int[]{x, y};
    }

    public void renderTerminal() {
        for (int i = 0; i < topPadding; i++) {
            System.out.println();
        }
        if (this.hasFrame) {
            System.out.printf("%s%s\n", this.leftPaddingString, this.frame.generateTopLine(xSize));
        }
        if (this.hasFrame) {
            for (int i = 0; i < this.ySize; i++) {
                System.out.printf("%s%s\n", this.leftPaddingString, this.frame.generateMiddleLine(this.generateLineStr(i + this.bufferOffset)));
            }
        } else {
            for (int i = 0; i < this.ySize; i++) {
                System.out.printf("%s%s\n", this.leftPaddingString, this.generateLineStr(i + this.bufferOffset));
            }
        }
        if (this.hasFrame) {
            System.out.printf("%s%s\n", this.leftPaddingString, this.frame.generateBottomLine(xSize));
        }
        for (int i = 0; i < bottomPadding; i++) {
            System.out.println();
        }
    }

}
