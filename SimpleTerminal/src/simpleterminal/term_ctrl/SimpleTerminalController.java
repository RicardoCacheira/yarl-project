/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.term_ctrl;

import simpleterminal.SimpleTerminal;

/**
 *
 * @author Ricardo
 */
public class SimpleTerminalController implements TerminalController {

    private SimpleTerminal terminal;

    public SimpleTerminalController(SimpleTerminal terminal) {
        this.terminal = terminal;
    }

    @Override
    public void clearTerminal() {
        this.terminal.clearTerminal();
    }

    @Override
    public void clearBuffer() {
        this.terminal.clearBuffer();
    }

    @Override
    public void moveCursorUp() {
        this.terminal.moveCursorUp();
    }

    @Override
    public void moveCursorDown() {
        this.terminal.moveCursorDown();
    }

    @Override
    public void moveCursorLeft() {
        this.terminal.moveCursorLeft();
    }

    @Override
    public void moveCursorRight() {
        this.terminal.moveCursorRight();
    }

    @Override
    public void moveCursorTopLeft() {
        this.terminal.moveCursorToTerminalTop();
        this.terminal.moveCursorToLineStart();
    }

    @Override
    public void moveCursorTopRight() {
        this.terminal.moveCursorToTerminalTop();
        this.terminal.moveCursorToLineEnd();
    }

    @Override
    public void moveCursorBottomLeft() {
        this.terminal.moveCursorToTerminalBottom();
        this.terminal.moveCursorToLineStart();
    }

    @Override
    public void moveCursorBottomRight() {
        this.terminal.moveCursorToTerminalBottom();
        this.terminal.moveCursorToLineEnd();
    }

    @Override
    public void moveCursorTerminalTop() {
        this.terminal.moveCursorToTerminalTop();
    }

    @Override
    public void moveCursorTerminalBottom() {
        this.terminal.moveCursorToTerminalBottom();
    }

    @Override
    public void moveCursorToLineStart() {
        this.terminal.moveCursorToLineStart();
    }

    @Override
    public void moveCursorToLineEnd() {
        this.terminal.moveCursorToLineEnd();
    }

    @Override
    public void scrollUp() {
        this.terminal.scrollUp();
    }

    @Override
    public void scrollDown() {
        this.terminal.scrollDown();
    }

    @Override
    public void pageUp() {
        this.terminal.pageUp();
    }

    @Override
    public void pageDown() {
        this.terminal.pageDown();
    }

    @Override
    public void setCursorLine(int line) {
        this.terminal.setCursorLine(line);
    }

    @Override
    public void setCursorColumn(int column) {
        this.terminal.setCursorColumn(column);
    }

    @Override
    public void setCursorPos(int xCursorPos, int yCursorPos) {
        this.terminal.setCursorPos(xCursorPos, yCursorPos);
    }

    @Override
    public void setCursorLineColumn(int line, int column) {
        this.terminal.setCursorLineColumn(line, column);
    }

    @Override
    public void newLine() {
        this.terminal.newLine();
    }

    @Override
    public void backspace() {
        this.terminal.backspace();
    }

    @Override
    public int[] terminalSize() {
        return this.terminal.terminalSize();
    }

    @Override
    public int[] cursorPos() {
        return this.terminal.cursorPos();
    }

}
