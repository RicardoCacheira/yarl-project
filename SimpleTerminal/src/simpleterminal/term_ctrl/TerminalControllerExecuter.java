/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.term_ctrl;

/**
 * ^ - Move Cursor Up
 * V - Move Cursor Down
 * < - Move Cursor Left
 * > - Move Cursor Right
 * S - Move Cursor To Line Start
 * E - Move Cursor To Line End
 *
 * N - New Line
 * L n; - Set Cursor at Line n
 * C n; - Set Cursor at Column n
 * P x,y; - Set Cursor at Position x, y
 * LC l,c; - Set Cursor at Line l and Column c
 * BK - Backspace
 * CT - Clear Terminal
 * CB - Clear Buffer
 * SU - Scroll Up
 * SD - Scroll Down
 * PU - Page Up
 * PD - Page Down
 * TL - Move Cursor To Top Left
 * TR - Move Cursor To Top Right
 * TT - Move Cursor To Terminal Top
 * BL - Move Cursor To Bottom Left
 * BR - Move Cursor To Bottom Right
 * BT - Move Cursor To Terminal Bottom
 *
 * @author Ricardo
 */
public class TerminalControllerExecuter {

    TerminalController controller;

    public TerminalControllerExecuter(TerminalController controller) {
        this.controller = controller;
    }

    public void execute(String exec) {
        int pos;
        for (int i = 0; i < exec.length(); i++) {
            switch (exec.charAt(i)) {
                case '^':
                    this.controller.moveCursorUp();
                    break;
                case 'V':
                    this.controller.moveCursorDown();
                    break;
                case '<':
                    this.controller.moveCursorLeft();
                    break;
                case '>':
                    this.controller.moveCursorRight();
                    break;
                case 'S':
                    if (i != exec.length() - 1) {
                        switch (exec.charAt(i + 1)) {
                            case 'U':
                                this.controller.scrollUp();
                                i++;
                                break;
                            case 'D':
                                this.controller.scrollDown();
                                i++;
                                break;
                            default:
                                this.controller.moveCursorToLineStart();
                                break;
                        }
                    } else {
                        this.controller.moveCursorToLineStart();
                    }
                    break;
                case 'E':
                    this.controller.moveCursorToLineEnd();
                    break;
                case 'U':
                    this.controller.scrollUp();
                    break;
                case 'D':
                    this.controller.scrollDown();
                    break;
                case 'N':
                    this.controller.newLine();
                    break;
                case 'R':
                    this.controller.clearTerminal();
                    break;
                case 'L':
                    if (i != exec.length() - 1) {
                        switch (exec.charAt(i + 1)) {
                            case 'C':
                                pos = exec.indexOf(';', i);
                                if (pos != -1) {
                                    try {
                                        String[] cursor = exec.substring(i + 2, pos).split(",");
                                        int line = Integer.parseInt(cursor[0]);
                                        int column = Integer.parseInt(cursor[1]);
                                        this.controller.setCursorLineColumn(line, column);
                                    } catch (NumberFormatException ex) {
                                    }
                                    i = pos;
                                }
                                break;
                            default:
                                pos = exec.indexOf(';', i);
                                if (pos != -1) {
                                    try {
                                        int line = Integer.parseInt(exec.substring(i + 1, pos));
                                        this.controller.setCursorLine(line);
                                    } catch (NumberFormatException ex) {
                                    }
                                    i = pos;
                                }
                                break;
                        }
                    }
                    break;
                case 'C':
                    if (i != exec.length() - 1) {
                        switch (exec.charAt(i + 1)) {
                            case 'T':
                                this.controller.clearTerminal();
                                i++;
                                break;
                            case 'B':
                                this.controller.clearBuffer();
                                i++;
                                break;
                            default:
                                pos = exec.indexOf(';', i);
                                if (pos != -1) {
                                    try {
                                        int column = Integer.parseInt(exec.substring(i + 1, pos));
                                        this.controller.setCursorColumn(column);
                                    } catch (NumberFormatException ex) {
                                    }
                                    i = pos;
                                }
                                break;
                        }
                    }
                    break;
                case 'P':
                    if (i != exec.length() - 1) {
                        switch (exec.charAt(i + 1)) {
                            case 'U':
                                this.controller.pageUp();
                                i++;
                                break;
                            case 'D':
                                this.controller.pageDown();
                                i++;
                                break;
                            default:
                                pos = exec.indexOf(';', i);
                                if (pos != -1) {
                                    try {
                                        String[] cursor = exec.substring(i + 1, pos).split(",");
                                        int xPos = Integer.parseInt(cursor[0]);
                                        int yPos = Integer.parseInt(cursor[1]);
                                        this.controller.setCursorPos(xPos, yPos);
                                    } catch (NumberFormatException ex) {
                                    }
                                    i = pos;
                                }
                                break;
                        }
                    }
                    break;
                case 'T':
                    if (i != exec.length() - 1) {
                        switch (exec.charAt(i + 1)) {
                            case 'L':
                                this.controller.moveCursorTopLeft();
                                i++;
                                break;
                            case 'R':
                                this.controller.moveCursorTopRight();
                                i++;
                                break;
                            case 'T':
                                this.controller.moveCursorTerminalTop();
                                i++;
                                break;
                        }
                    }
                    break;
                case 'B':
                    if (i != exec.length() - 1) {
                        switch (exec.charAt(i + 1)) {
                            case 'L':
                                this.controller.moveCursorBottomLeft();
                                i++;
                                break;
                            case 'R':
                                this.controller.moveCursorBottomRight();
                                i++;
                                break;
                            case 'T':
                                this.controller.moveCursorTerminalBottom();
                                i++;
                                break;
                            case 'K':
                                this.controller.backspace();
                                i++;
                                break;
                        }
                    }
                    break;
            }
        }
    }

    public static String help() {
        return " ^ - Move Cursor Up\n"
                + " V - Move Cursor Down\n"
                + " < - Move Cursor Left\n"
                + " > - Move Cursor Right\n"
                + " S - Move Cursor To Line Start\n"
                + " E - Move Cursor To Line End\n"
                + " N - New Line\n"
                + " L<n>; - Set Cursor at Line n\n"
                + " C<n>; - Set Cursor at Column n\n"
                + " P<x>,<y>; - Set Cursor at Position x, y Note: the first char is 0,0 the second is 1,0\n"
                + " LC<l>,<c>; - Set Cursor at Line l and Column c Note: the first char is 1,1 the second is 1,2"
                + " BK - Backspace\n"
                + " CT - Clear Terminal\n"
                + " CB - Clear Buffer\n"
                + " SU - Scroll Up\n"
                + " SD - Scroll Down\n"
                + " PU - Page Up\n"
                + " PD - Page Down\n"
                + " TL - Move Cursor To Top Left\n"
                + " TR - Move Cursor To Top Right\n"
                + " TT - Move Cursor To Terminal Top\n"
                + " BL - Move Cursor To Bottom Left\n"
                + " BR - Move Cursor To Bottom Right\n"
                + " BT - Move Cursor To Terminal Bottom\n";
    }
}
