/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.term_ctrl;

/**
 *
 * @author Ricardo
 */
public interface TerminalController {

    public void clearTerminal();

    public void clearBuffer();

    public void moveCursorUp();

    public void moveCursorDown();

    public void moveCursorLeft();

    public void moveCursorRight();

    public void moveCursorTopLeft();

    public void moveCursorTopRight();

    public void moveCursorBottomLeft();

    public void moveCursorBottomRight();

    public void moveCursorTerminalTop();

    public void moveCursorTerminalBottom();

    public void moveCursorToLineStart();

    public void moveCursorToLineEnd();

    public void scrollUp();

    public void scrollDown();

    public void pageUp();

    public void pageDown();

    public void setCursorLine(int line);

    public void setCursorColumn(int column);

    public void setCursorPos(int xCursorPos, int yCursorPos);

    public void setCursorLineColumn(int line, int column);

    public void newLine();

    public void backspace();

    public int[] terminalSize();

    public int[] cursorPos();

}
