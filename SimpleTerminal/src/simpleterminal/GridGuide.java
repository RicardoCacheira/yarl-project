/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal;

/**
 *
 * @author Ricardo
 */
public class GridGuide {

    private static void printTopLine(int xSize) {
        int num;
        for (int i = 0; i < xSize; i++) {
            num = (i + 1) % 10;
            switch (num) {
                case 0:
                    System.out.print('|');
                    break;
                case 5:
                    System.out.print(':');
                    break;
                default:
                    System.out.print(num);
                    break;
            }
        }
        System.out.println();
    }

    private static void printDecGridLine(int xSize) {
        System.out.print('0');
        int num;
        for (int i = 1; i < xSize-1; i++) {
            if ((i + 1) % 10 == 0) {
                System.out.print('#');
            } else if ((i + 1) % 5 == 0) {
                System.out.print('+');
            } else {
                System.out.print('=');
            }
        }
         System.out.print('#');
        System.out.println();
    }

    private static void printGridLine(int xSize) {
        System.out.print('5');
        for (int i = 1; i < xSize-1; i++) {
            if ((i + 1) % 10 == 0) {
                System.out.print('#');
            } else if ((i + 1) % 5 == 0) {
                System.out.print('+');
            } else {
                System.out.print('-');
            }
        }
         System.out.print('#');
        System.out.println();
    }

    private static void printNonGridLine(int num, int xSize) {
        System.out.print((num + 1) % 10);
        for (int i = 1; i < xSize-1; i++) {
            if ((i + 1) % 10 == 0) {
                System.out.print('|');
            } else if ((i + 1) % 5 == 0) {
                System.out.print(':');
            } else {
                System.out.print(' ');
            }
        }
         System.out.print('|');
        System.out.println();
    }

    public static void printGrid(int xSize, int ySize) {
        GridGuide.printTopLine(xSize);
        for (int i = 1; i < ySize; i++) {
            if ((i + 1) % 10 == 0) {
                GridGuide.printDecGridLine(xSize);
            } else if ((i + 1) % 5 == 0) {
                GridGuide.printGridLine(xSize);
            } else {
                GridGuide.printNonGridLine(i, xSize);
            }
        }
    }

    public static void main(String[] args) {
        printGrid(80, 20);
    }

}
