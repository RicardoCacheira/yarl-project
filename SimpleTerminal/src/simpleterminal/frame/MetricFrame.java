/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.frame;

/**
 *
 * @author Ricardo
 */
public class MetricFrame implements TerminalFrame {

    private int yPos;

    MetricFrame() {
        yPos = 0;
    }

    @Override
    public String generateTopLine(int lineSize) {
        StringBuilder sb = new StringBuilder();
        sb.append('@');
        for (int i = 0; i < lineSize; i++) {
            if (i % 10 == 0) {
                sb.append('|');
            } else {
                sb.append('.');
            }
        }
        sb.append('@');
        return sb.toString();
    }

    @Override
    public String generateBottomLine(int lineSize) {
        this.yPos = 0;
        return generateTopLine(lineSize);
    }

    @Override
    public String generateMiddleLine(String str) {
        StringBuilder sb = new StringBuilder();
        char c;
        if (this.yPos % 10 == 0) {
            c = '+';
        } else {
            c = '|';
        }
        this.yPos++;
        sb.append(c);
        sb.append(str);
        sb.append(c);
        return sb.toString();
    }

}
