/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.frame;

/**
 *
 * @author Ricardo
 */
public interface TerminalFrame {

    public String generateTopLine(int lineSize);

    public String generateMiddleLine(String str);

    public String generateBottomLine(int lineSize);

}
