/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.frame.decorator;

import simpleterminal.frame.TerminalFrame;

/**
 *
 * @author Ricardo
 */
public class StringFrameDecorator extends TerminalFrameDecorator {

    public enum Line {
        TOP, BOTTOM
    };

    public enum LinePos {
        LEFT, CENTER, RIGHT
    };

    private String str;

    private Line line;

    private LinePos pos;

    public StringFrameDecorator(TerminalFrame frame, String str, Line line, LinePos pos) {
        super(frame);
        this.str = str;
        this.line = line;
        this.pos = pos;
    }

    private String generateLine(String line) {
        if (line.length() - 2 - this.str.length() < 0) {
            return line;
        } else {
            int firstBreakPoint = 0;
            int SecondBreakPoint = 0;
            switch (this.pos) {
                case LEFT:
                    firstBreakPoint = 1;
                    SecondBreakPoint = this.str.length() + 1;
                    break;
                case RIGHT:
                    firstBreakPoint = line.length() - this.str.length() - 1;
                    SecondBreakPoint = line.length() - 1;
                    break;
                case CENTER:
                    firstBreakPoint = line.length() / 2 - this.str.length() / 2;
                    SecondBreakPoint = firstBreakPoint + this.str.length();
                    break;
            }
            return String.format("%s%s%s", line.substring(0, firstBreakPoint), this.str, line.substring(SecondBreakPoint));
        }
    }

    public void setString(String str) {
        this.str = str;
    }

    @Override
    public String generateTopLine(int lineSize) {
        String topLine = super.generateTopLine(lineSize);
        if (this.line == Line.TOP) {
            return this.generateLine(topLine);
        } else {
            return topLine;
        }
    }

    @Override
    public String generateBottomLine(int lineSize) {
        String bottomLine = super.generateBottomLine(lineSize);
        if (this.line == Line.BOTTOM) {
            return this.generateLine(bottomLine);
        } else {
            return bottomLine;
        }
    }

}
