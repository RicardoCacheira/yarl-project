/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.frame.decorator;

import simpleterminal.SimpleTerminal;
import simpleterminal.frame.TerminalFrame;

/**
 *
 * @author Ricardo
 */
public class CursorPosFrameDecorator extends StringFrameDecorator {

    private SimpleTerminal terminal;
    private boolean showLegend;

    public CursorPosFrameDecorator(TerminalFrame frame, SimpleTerminal terminal, Line line, LinePos pos, boolean showLegend) {
        super(frame, "", line, pos);
        this.terminal = terminal;
        this.showLegend = showLegend;
    }

    private void setCursorStr() {
        int[] pos = this.terminal.cursorPos();
        if (this.showLegend) {
            super.setString(String.format("|X: %d, Y: %d|", pos[0], pos[1]));
        } else {
            super.setString(String.format("|%d, %d|", pos[0], pos[1]));
        }
    }

    @Override
    public String generateTopLine(int lineSize) {
        this.setCursorStr();
        return super.generateTopLine(lineSize);
    }

    @Override
    public String generateBottomLine(int lineSize) {
        this.setCursorStr();
        return super.generateBottomLine(lineSize);
    }

}
