/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.frame.decorator;

import simpleterminal.frame.TerminalFrame;

/**
 *
 * @author Ricardo
 */
public class TerminalFrameDecorator implements TerminalFrame {

    private TerminalFrame frame;

    public TerminalFrameDecorator(TerminalFrame frame) {
        this.frame = frame;
    }

    @Override
    public String generateTopLine(int lineSize) {
        return this.frame.generateTopLine(lineSize);
    }

    @Override
    public String generateMiddleLine(String str) {
        return this.frame.generateMiddleLine(str);
    }

    @Override
    public String generateBottomLine(int lineSize) {
        return this.frame.generateBottomLine(lineSize);
    }

}
