/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.frame.decorator;

import simpleterminal.SimpleTerminal;
import simpleterminal.frame.TerminalFrame;

/**
 *
 * @author Ricardo
 */
public class CursorLineColumnFrameDecorator extends StringFrameDecorator {

    private SimpleTerminal terminal;
    private boolean showLinesWithOffset;
    private boolean showLegend;

    public CursorLineColumnFrameDecorator(TerminalFrame frame, SimpleTerminal terminal, Line line, LinePos pos, boolean showLinesWithOffset, boolean showLegend) {
        super(frame, "", line, pos);
        this.terminal = terminal;
        this.showLinesWithOffset = showLinesWithOffset;
        this.showLegend = showLegend;
    }

    private void setCursorStr() {
        int[] pos = this.terminal.cursorLineColumn();
        if (this.showLinesWithOffset) {
            pos[0] += this.terminal.offset();
        }
        if (this.showLegend) {
            super.setString(String.format("|Line: %d, Column: %d|", pos[0], pos[1]));
        } else {
            super.setString(String.format("|%d, %d|", pos[0], pos[1]));
        }

    }

    @Override
    public String generateTopLine(int lineSize) {
        this.setCursorStr();
        return super.generateTopLine(lineSize);
    }

    @Override
    public String generateBottomLine(int lineSize) {
        this.setCursorStr();
        return super.generateBottomLine(lineSize);
    }

}
