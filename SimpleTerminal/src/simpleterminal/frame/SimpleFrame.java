/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal.frame;

/**
 *
 * @author Ricardo
 */
public class SimpleFrame implements TerminalFrame {

    private char leftTopCornerChar;
    private char rightTopCornerChar;
    private char leftBottomCornerChar;
    private char rightBottomCornerChar;
    private char topLineChar;
    private char bottomLineChar;
    private char leftLineChar;
    private char rightLineChar;

    public SimpleFrame(char leftTopCornerChar,
            char rightTopCornerChar,
            char leftBottomCornerChar,
            char rightBottomCornerChar,
            char topLineChar,
            char bottomLineChar,
            char leftLineChar,
            char rightLineChar
    ) {
        this.leftTopCornerChar = leftTopCornerChar;
        this.rightTopCornerChar = rightTopCornerChar;
        this.leftBottomCornerChar = leftBottomCornerChar;
        this.rightBottomCornerChar = rightBottomCornerChar;
        this.topLineChar = topLineChar;
        this.bottomLineChar = bottomLineChar;
        this.leftLineChar = leftLineChar;
        this.rightLineChar = rightLineChar;
    }

    public SimpleFrame(char corner, char verticalLine, char horizontalLine) {
        this(corner, corner, corner, corner, horizontalLine, horizontalLine, verticalLine, verticalLine);
    }

    public SimpleFrame(char corner, char line) {
        this(corner, corner, corner, corner, line, line, line, line);
    }

    public SimpleFrame(char character) {
        this(character, character, character, character, character, character, character, character);
    }

    @Override
    public String generateTopLine(int lineSize) {
        StringBuilder sb = new StringBuilder();
        sb.append(leftTopCornerChar);
        for (int i = 0; i < lineSize; i++) {
            sb.append(topLineChar);
        }
        sb.append(rightTopCornerChar);
        return sb.toString();
    }

    @Override
    public String generateMiddleLine(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(leftLineChar);
        sb.append(str);
        sb.append(rightLineChar);
        return sb.toString();
    }

    @Override
    public String generateBottomLine(int lineSize) {
        StringBuilder sb = new StringBuilder();
        sb.append(leftBottomCornerChar);
        for (int i = 0; i < lineSize; i++) {
            sb.append(bottomLineChar);
        }
        sb.append(rightBottomCornerChar);
        return sb.toString();
    }

}
