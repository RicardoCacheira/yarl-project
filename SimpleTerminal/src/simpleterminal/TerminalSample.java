/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleterminal;

import simpleterminal.frame.decorator.StringFrameDecorator;
import simpleterminal.frame.decorator.CursorPosFrameDecorator;
import simpleterminal.frame.TerminalFrame;
import simpleterminal.frame.SimpleFrame;
import java.util.Scanner;
import static simpleterminal.GridGuide.printGrid;
import simpleterminal.frame.decorator.CursorLineColumnFrameDecorator;
import simpleterminal.frame.decorator.StringFrameDecorator.Line;
import simpleterminal.frame.decorator.StringFrameDecorator.LinePos;
import simpleterminal.term_ctrl.SimpleTerminalController;
import simpleterminal.term_ctrl.TerminalControllerExecuter;

/**
 *
 * @author Ricardo
 */
public class TerminalSample {

    public static void main(String[] args) {
        printGrid(102, 24);
        SimpleTerminal term = new SimpleTerminal(80, 18, 1, 2, 10, '_');
        //TerminalFrame main = new MetricFrame();
        TerminalFrame main = new SimpleFrame('=', '|', '=');
        TerminalFrame nameDecorator = new StringFrameDecorator(main, "Simple Editor", Line.TOP, LinePos.CENTER);
        TerminalFrame cursorLineColTermDecorator = new CursorLineColumnFrameDecorator(nameDecorator, term, Line.BOTTOM, LinePos.LEFT, false, true);
        TerminalFrame cursorLineColBufferDecorator = new CursorLineColumnFrameDecorator(cursorLineColTermDecorator, term, Line.BOTTOM, LinePos.RIGHT, true, true);
      TerminalFrame legendDecorator = new StringFrameDecorator(cursorLineColBufferDecorator, "<-Terminal Pos|========|Buffer Pos->", Line.BOTTOM, LinePos.CENTER);
        term.registerFrame(legendDecorator);
        TerminalControllerExecuter exec = new TerminalControllerExecuter(new SimpleTerminalController(term));
        System.out.println("X: " + term.requiredCenteredMainTerminalSize()[0] + ", Y: " + (term.requiredCenteredMainTerminalSize()[1] + 1));
        term.writeStr("Simple Terminal Editor Demo\nTo quit write ::q\nFor help write ::h\nTo clear the terminal ::ct");
        String str;
        Scanner in = new Scanner(System.in);
        do {
            term.renderTerminal();
            System.out.print(term.leftPaddingString() + "Input: ");
            str = in.nextLine();
            if (str.toLowerCase().startsWith("::h")) {
                term.clearTerminal();
                term.writeStr("\n" + TerminalControllerExecuter.help());
            } else if (str.startsWith("::")) {
                exec.execute(str.substring(2).toUpperCase());
            } else {
                term.writeStr(str);
            }
        } while (!str.toLowerCase().equals("::q"));
    }

}
