/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author Ricardo
 * @param <T>
 */
public interface ClonableObject<T> {

    public T cloneObject();
}
