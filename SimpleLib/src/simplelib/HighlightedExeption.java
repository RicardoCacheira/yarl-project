/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author Ricardo
 */
public class HighlightedExeption extends IllegalArgumentException {

    private String msn;
    private int startPos = 0;
    private int endPos = 0;

    public HighlightedExeption(String msn) {
        super(msn);
    }

    public HighlightedExeption(String msn, String source, int pos) {
        this(generateMessage(msn, source, pos, pos));
    }

    public HighlightedExeption(String msn, String source, int startPos, int endPos) {
        super(generateMessage(msn, source, startPos, endPos));
        this.msn = msn;
        this.startPos = startPos;
        this.endPos = endPos;
    }

    private static String generateMessage(String msn, String source, int startPos, int endPos) {
        StringBuilder sb = new StringBuilder(msn);
        if (startPos == endPos) {
            sb.append(String.format("\nPosition: %d\n", startPos));
            sb.append(highlightChar(source, startPos));
        } else {
            sb.append(String.format("\nStart Position: %d\n", startPos));
            sb.append(String.format("End Position:   %d\n", endPos));
            sb.append(highlightSubstring(source, startPos, endPos));
        }

        return sb.toString();
    }

    public static String highlightSubstring(String source, int startPos, int endPos) {
        StringBuilder sb = new StringBuilder(source);
        sb.append('\n');
        for (int i = 0; i < startPos; i++) {
            sb.append(' ');
        }
        for (int i = startPos; i <= endPos; i++) {
            sb.append('^');
        }
        sb.append('\n');
        return sb.toString();
    }

    public static String highlightChar(String source, int charPos) {
        StringBuilder sb = new StringBuilder(source);
        sb.append('\n');
        for (int i = 0; i < charPos; i++) {
            sb.append(' ');
        }
        sb.append("^\n");
        return sb.toString();
    }

    /**
     * @return the msn
     */
    public String getMsn() {
        return msn;
    }

    /**
     * @return the startPos
     */
    public int getStartPos() {
        return startPos;
    }

    /**
     * @return the endPos
     */
    public int getEndPos() {
        return endPos;
    }
}
