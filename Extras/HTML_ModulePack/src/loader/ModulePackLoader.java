/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loader;

import lib.YARLI_Module;
import modules.html_module.HTML_Module;
import yarli.YARL_Interpreter;

/**
 *
 * @author Ricardo
 */
public class ModulePackLoader {

    private static final YARLI_Module[] MODULE_LIST = {new HTML_Module()};

    public static YARLI_Module[] loadModules(YARL_Interpreter interpreter) {
        return MODULE_LIST;
    }
}
