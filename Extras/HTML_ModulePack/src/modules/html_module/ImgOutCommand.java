/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.html_module;

import java.io.File;
import lib.YARLI_Module;
import yarli.ModuleCommand;
import yarli.YARL_Interpreter;

/**
 *
 * @author Ricardo
 */
public class ImgOutCommand extends ModuleCommand {

    public static String COMMAND_NAME = "IMG_OUT";

    public ImgOutCommand(YARLI_Module module) {
        super(COMMAND_NAME, module);
    }

    @Override
    public void run(YARL_Interpreter interpreter) {
        if (interpreter != null) {
            Object[] parameters = interpreter.getParameters(1, COMMAND_NAME);
            if (parameters != null) {
                String str = parameters[0].toString();
                File file = new File(str);
                if (file.exists() && file.isFile()) {
                    interpreter.showOutput(String.format("<img src=\"%s\"/>", file.toURI().toString()), YARL_Interpreter.HTML_OUTPUT);
                } else {
                    interpreter.throwError(String.format("File \"%s\" not found.", str));
                }
            }
        }
    }

    @Override
    public String getInfo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
