/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.html_module;

import lib.YARLI_Module;
import yarli.ModuleCommand;
import yarli.YARL_Interpreter;

/**
 *
 * @author Ricardo
 */
public class HTML_OutCommand extends ModuleCommand {

    public static String COMMAND_NAME = "HTMLOUT";

    public HTML_OutCommand(YARLI_Module module) {
        super(COMMAND_NAME, module);
    }

    @Override
    public void run(YARL_Interpreter interpreter) {
        if (interpreter != null) {
            Object[] parameters = interpreter.getParameters(1, COMMAND_NAME);
            if (parameters != null) {
                interpreter.showOutput(parameters[0], YARL_Interpreter.HTML_OUTPUT);
            }
        }
    }

    @Override
    public String getInfo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
