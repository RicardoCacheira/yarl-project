/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.html_module;

import lib.YARLI_Module;

import yarli.ModuleCommand;

/**
 *
 * @author Ricardo
 */
public class HTML_Module implements YARLI_Module {

    @Override
    public ModuleCommand[] commandList() {
        return new ModuleCommand[]{new HTML_OutCommand(this),
            new HTML_PrintCommand(this),
            new ImgOutCommand(this),
            new ImgPrintCommand(this)};
    }

    @Override
    public String getModuleName() {
        return "HTML Module";
    }

    @Override
    public String help() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
