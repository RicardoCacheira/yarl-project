package modules.type_conversion_module;

import rationalnumber.RationalNumber;
import rationalnumber.parser.RationalNumberParser;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: StrToNum
 *
 * Module: Type Conversion Module
 *
 * Description: Converts the given string into the correspondent number if
 * possible.
 *
 * Return: The number represented by the string.
 *
 * Return Type: Number
 *
 * ---Attribute---
 *
 * Name: String to convert
 * Description: The string that will be converted into a number
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * The string will only be converted if its a valid number else it will throw an
 * error.
 *
 * ---Note---
 *
 * Usage: < String to convert > StrToNum -> Number
 *
 * @author Ricardo Cacheira
 */
public class StrToNumCommand extends ModuleCommand {

    public static String COMMAND_NAME = "StrToNum";

    public StrToNumCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public StrToNumCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String)) {
                    String str = (String) parameteres[0];
                    if (RationalNumberParser.isNumberValid(str)) {
                        interpreter.push(new RationalNumber(str));
                    } else {
                        interpreter.throwError("The given string isn't a valid number.");
                    }

                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("String to convert", "The string that will be converted into a number", "String")
        };
        String[] noteList = {
            "The string will only be converted if its a valid number else it will throw an error."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Converts the given string into the correspondent number if possible.", "The number represented by the string.", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new StrToNumCommand(this.getId(), this.getModule());
    }

}
