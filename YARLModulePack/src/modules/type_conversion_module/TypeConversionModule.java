package modules.type_conversion_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is responsible for the conversion of types of objects.
 *
 * @author Ricardo Cacheira
 */
public class TypeConversionModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new NumToStrCommand(this), 
			new ObjToStrCommand(this), 
			new StrToNumCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"StrToNum",
			"NumToStr",
			"ObjToStr"
		};
	}

	@Override
	public String moduleName() {
		return "Type Conversion Module";
	}

	@Override
	public String help() {
		return "This module is responsible for the conversion of types of objects.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new TypeConversionModule();
	}

}