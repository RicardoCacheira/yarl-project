package modules.type_conversion_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ObjToStr
 *
 * Module: Type Conversion Module
 *
 * Description: Converts the given object into the correspondent string.
 *
 * Return: The string representing the object.
 *
 * Return Type: String
 *
 * ---Attribute---
 *
 * Name: Object to convert
 * Description: The object that will be converted into a string
 * Type: Object
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * If the given object has a ObjStringHandler the generated string will be that
 * one, else it will use toString.
 *
 * e.g. Array - [obj1, obj2, ...]
 *
 * e.g. Number - 1442/86
 *
 * ---Notes---
 *
 * Usage: < Object to convert > ObjToStr -> String
 *
 * @author Ricardo Cacheira
 */
public class ObjToStrCommand extends ModuleCommand {
    
    public static String COMMAND_NAME = "ObjToStr";
    
    public ObjToStrCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }
    
    public ObjToStrCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }
    
    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                interpreter.push(interpreter.generateObjString(parameteres[0]));
            }
        }
    }
    
    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("Object to convert", "The object that will be converted into a string", "Object")
        };
        String[] noteList = {
            "If the given object has a ObjStringHandler the generated string will be that one, else it will use toString.",
            "e.g. Array - [obj1, obj2, ...]",
            "e.g. Number - 1442/86"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Converts the given object into the correspondent string.", "The string representing the object.", "String", attributeList, noteList);
    }
    
    @Override
    public IdDefinedCommand cloneObject() {
        return new ObjToStrCommand(this.getId(), this.getModule());
    }
    
}
