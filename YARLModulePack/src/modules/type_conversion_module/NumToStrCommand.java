package modules.type_conversion_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.obj_str_handler.NumberStringHandler;

/**
 * Name: NumToStr
 *
 * Module: Type Conversion Module
 *
 * Description: Converts the given number into the correspondent string using
 * the desired notation.
 *
 * Return: The string representing the number in the desired notation.
 *
 * Return Type: String
 *
 * ---Attributes---
 *
 * Name: Number to convert
 * Description: The number that will be converted into a string
 * Type: Number
 *
 * Name: Desired notation
 * Description: The notation that will be used to convert the number into a
 * string
 * Type: NotationHandler
 *
 * ---Attributes---
 *
 * Usage: < Number to convert > < Desired notation > NumToStr -> String
 *
 * @author Ricardo Cacheira
 */
public class NumToStrCommand extends ModuleCommand {

    public static String COMMAND_NAME = "NumToStr";

    public NumToStrCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public NumToStrCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof NumberStringHandler)) {
                    interpreter.push(((NumberStringHandler) parameteres[1]).generateString((RationalNumber) parameteres[0]));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("Number to convert", "The number that will be converted into a string", "Number"),
            new CommandInfoAttribute("Desired notation", "The notation that will be used to convert the number into a string", "NotationHandler")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Converts the given number into the correspondent string using the desired notation.", "The string representing the number in the desired notation.", "String", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new NumToStrCommand(this.getId(), this.getModule());
    }

}
