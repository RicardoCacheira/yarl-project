package modules.io_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: SendError
 *
 * Module: I/O Module
 *
 * Description: Sends an error message.
 *
 * ---Attribute---
 *
 * Name: ErrorMessage
 * Description: The string that will be outputted when the error is shown.
 * Type: String
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * While this command shows a message identified as an error message for the
 * interpreter is not considered as one, so the Error Policy will NOT be
 * activated.
 *
 * Even if the error message handler is set to not show the error message this
 * command will show because it isn't an actual error.
 *
 * ---Notes---
 *
 * Usage: < ErrorMessage > SendError
 *
 * @author Ricardo Cacheira
 */
public class SendErrorCommand extends ModuleCommand {

    public static String COMMAND_NAME = "SendError";

    public SendErrorCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SendErrorCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.showOutput((String) parameteres[0], YARLI.ERROR_OUTPUT);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("ErrorMessage", "The string that will be outputted when the error is shown.", "String")
        };
        String[] noteList = {
            "While this command shows a message identified as an error message for the interpreter is not considered as one, so the Error Policy will NOT be activated.",
            "Even if the error message handler is set to not show the error message this command will show because it isn't an actual error."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Sends an error message.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SendErrorCommand(this.getId(), this.getModule());
    }

}
