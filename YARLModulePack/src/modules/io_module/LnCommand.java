package modules.io_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Ln
 *
 * Module: I/O Module
 *
 * Description: Outputs a newline character to the shell.
 *
 * Usage: Ln
 *
 * @author Ricardo Cacheira
 */
public class LnCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Ln";

    public LnCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public LnCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.showOutput("\n", YARLI.PLAIN_OUTPUT);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Outputs a newline character to the shell.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new LnCommand(this.getId(), this.getModule());
    }

}
