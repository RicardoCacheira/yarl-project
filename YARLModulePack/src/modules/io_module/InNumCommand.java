package modules.io_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import rationalnumber.RationalNumber;
import rationalnumber.parser.RationalNumberParser;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: InNum
 *
 * Module: I/O Module
 *
 * Description: Requests an input of an number from the user and pushes the
 * inserted number to the stack
 *
 * Return: Input
 *
 * Return Type: Number
 *
 * ---Attribute---
 *
 * Name: WarningMessage
 * Description: The string that will be outputted if the input is not a valid
 * number.
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * If the input isn't a valid number the warning message will be shown and the
 * input will be requested until a valid number is inputted.
 *
 * ---Note---
 *
 * Usage: < WarningMessage > InNum -> Number
 *
 * @author Ricardo Cacheira
 */
public class InNumCommand extends ModuleCommand {

    public static String COMMAND_NAME = "InNum";

    public InNumCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public InNumCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    String warningMsn = (String) parameteres[0];
                    String input = interpreter.readInput();
                    while (!RationalNumberParser.isNumberValid(input)) {
                        interpreter.throwWarning(warningMsn);
                        input = interpreter.readInput();
                    }
                    interpreter.push(new RationalNumber(input));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("WarningMessage", "The string that will be outputted if the input is not a valid number.", "String")
        };
        String[] noteList = {
            "If the input isn't a valid number the warning message will be shown and the input will be requested until a valid number is inputted."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Requests an input of an number from the user and pushes the inserted number to the stack", "Input", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new InNumCommand(this.getId(), this.getModule());
    }

}
