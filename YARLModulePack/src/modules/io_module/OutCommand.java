package modules.io_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Out
 *
 * Module: I/O Module
 *
 * Description: Outputs a string representing the top value from the stack to
 * the shell.
 *
 * ---Attribute---
 *
 * Name: Output
 * Description: The object whoose string will be outputted
 * Type: Object
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * Diferent objects can have diferent strings that represent them.
 *
 * e.g. Array - [obj1, obj2, ...]
 *
 * e.g. Number - 1442/86
 *
 * ---Notes---
 *
 * Usage: < Output > Out
 *
 * @author Ricardo Cacheira
 */
public class OutCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Out";

    public OutCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public OutCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameters = interpreter.getParameters(1, COMMAND_NAME);
            if (parameters != null) {
                if (parameters[0] instanceof String) {
                    interpreter.showOutput(parameters[0], YARLI.PLAIN_OUTPUT);
                } else {
                    interpreter.showOutput(interpreter.generateObjString(parameters[0]), YARLI.PLAIN_OUTPUT);
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("Output", "The object whoose string will be outputted", "Object")
        };
        String[] noteList = {
            "Diferent objects can have diferent strings that represent them.",
            "e.g. Array - [obj1, obj2, ...]",
            "e.g. Number - 1442/86"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Outputs a string representing the top value from the stack to the shell.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new OutCommand(this.getId(), this.getModule());
    }

}
