package modules.io_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module contains the commands responsible for the interaction with the user.
 *
 * @author Ricardo Cacheira
 */
public class IOModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new ClearCommand(this), 
			new InCommand(this), 
			new InNumCommand(this), 
			new LnCommand(this), 
			new OutCommand(this), 
			new PrintCommand(this), 
			new SendErrorCommand(this), 
			new SendInfoCommand(this), 
			new SendWarningCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"In",
			"Out",
			"Print",
			"Ln",
			"Clear",
			"InNum",
			"SendError",
			"SendWarning",
			"SendInfo"
		};
	}

	@Override
	public String moduleName() {
		return "I/O Module";
	}

	@Override
	public String help() {
		return "This module contains the commands responsible for the interaction with the user.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new IOModule();
	}

}