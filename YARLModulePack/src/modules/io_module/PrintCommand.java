package modules.io_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Print
 *
 * Module: I/O Module
 *
 * Description: Prints a string representing the top value from the stack to the
 * shell.
 *
 * ---Attribute---
 *
 * Name: Output
 * Description: The object whoose string will be Printed
 * Type: Object
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * This command unlike the Out command doesn't remove the value from the stack.
 *
 * Diferent objects can have diferent strings that represent them.
 *
 * e.g. Array - [obj1, obj2, ...]
 *
 * e.g. Number - 1442/86
 *
 * ---Notes---
 *
 * Usage: < Output > Print
 *
 * @author Ricardo Cacheira
 */
public class PrintCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Print";

    public PrintCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public PrintCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameters = interpreter.getParametersCopy(1, COMMAND_NAME);
            if (parameters != null) {
                if (parameters[0] instanceof String) {
                    interpreter.showOutput(parameters[0], YARLI.PLAIN_OUTPUT);
                } else {
                    interpreter.showOutput(interpreter.generateObjString(parameters[0]), YARLI.PLAIN_OUTPUT);
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("Output", "The object whoose string will be Printed", "Object")
        };
        String[] noteList = {
            "This command unlike the Out command doesn't remove the value from the stack.",
            "Diferent objects can have diferent strings that represent them.",
            "e.g. Array - [obj1, obj2, ...]",
            "e.g. Number - 1442/86"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Prints a string representing the top value from the stack to the shell.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new PrintCommand(this.getId(), this.getModule());
    }

}
