package modules.io_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: SendInfo
 *
 * Module: I/O Module
 *
 * Description: Sends an info message.
 *
 * ---Attribute---
 *
 * Name: InfoMessage
 * Description: The string that will be outputted when the info is shown.
 * Type: String
 *
 * ---Attribute---
 *
 * Usage: < InfoMessage > SendInfo
 *
 * @author Ricardo Cacheira
 */
public class SendInfoCommand extends ModuleCommand {

    public static String COMMAND_NAME = "SendInfo";

    public SendInfoCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SendInfoCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.showOutput((String) parameteres[0], YARLI.INFO_OUTPUT);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("InfoMessage", "The string that will be outputted when the info is shown.", "String")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Sends an info message.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SendInfoCommand(this.getId(), this.getModule());
    }

}
