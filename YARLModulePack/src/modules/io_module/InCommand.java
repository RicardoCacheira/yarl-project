package modules.io_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: In
 *
 * Module: I/O Module
 *
 * Description: Requests an input from the user and pushes the inserted string
 * to the stack
 *
 * Return: Input
 *
 * Return Type: String
 *
 * Usage: In -> String
 *
 * @author Ricardo Cacheira
 */
public class InCommand extends ModuleCommand {

    public static String COMMAND_NAME = "In";

    public InCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public InCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(interpreter.readInput());
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Requests an input from the user and pushes the inserted string to the stack", "Input", "String", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new InCommand(this.getId(), this.getModule());
    }

}
