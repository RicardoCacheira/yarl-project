package modules.io_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Clear
 *
 * Module: I/O Module
 *
 * Description: Clears the shell.
 *
 * ---Note---
 *
 * This command will only work if the implementation of the shell allows it.
 *
 * ---Note---
 *
 * Usage: Clear
 *
 * @author Ricardo Cacheira
 */
public class ClearCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Clear";

    public ClearCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ClearCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.clearOutput();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "This command will only work if the implementation of the shell allows it."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Clears the shell.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ClearCommand(this.getId(), this.getModule());
    }

}
