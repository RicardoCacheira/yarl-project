package modules.io_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: SendWarning
 *
 * Module: I/O Module
 *
 * Description: Sends an warning message.
 *
 * ---Attribute---
 *
 * Name: WarningMessage
 * Description: The string that will be outputted when the warning is shown.
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * Even if the warning message handler is set to not show the warning message
 * this command will show because it isn't an actual warning.
 *
 * ---Note---
 *
 * Usage: < WarningMessage > SendWarning
 *
 * @author Ricardo Cacheira
 */
public class SendWarningCommand extends ModuleCommand {

    public static String COMMAND_NAME = "SendWarning";

    public SendWarningCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SendWarningCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.showOutput((String) parameteres[0], YARLI.WARNING_OUTPUT);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("WarningMessage", "The string that will be outputted when the warning is shown.", "String")
        };
        String[] noteList = {
            "Even if the warning message handler is set to not show the warning message this command will show because it isn't an actual warning."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Sends an warning message.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SendWarningCommand(this.getId(), this.getModule());
    }

}
