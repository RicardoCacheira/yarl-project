package modules.var_ctrl_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: NewMatrix
 *
 * Module: Variable Control Module
 *
 * Description: Creates a new Matrix with the given id and the given dimensions
 *
 * ---Attributes---
 *
 * Name: X dimension
 * Description: The X dimension of the Vector
 * Type: Number
 *
 * Name: Y dimension
 * Description: The Y dimension of the Vector
 * Type: Number
 *
 * Name: id
 * Description: The id that will identify this Vector
 * Type: String
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The dimensions must both be a positive integer.
 *
 * All of the positions in the vector will start with the value 0.
 *
 * If the given id is already being used by another variable, IDCom, array,
 * vector or matrix, an error will be thrown.
 *
 * ---Notes---
 *
 * Usage: < X dimension > < Y dimension > < id > NewMatrix
 *
 * @author Ricardo Cacheira
 */
public class NewMatrixCommand extends ModuleCommand {

    public static String COMMAND_NAME = "NewMatrix";

    public NewMatrixCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public NewMatrixCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(3, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof RationalNumber) && (parameteres[2] instanceof String)) {
                    int xDimension = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                    if (xDimension != -1) {
                        int yDimension = interpreter.generateNonNegativeInt((RationalNumber) parameteres[1]);
                        if (yDimension != -1) {
                            interpreter.newMatrix((String) parameteres[2], xDimension, yDimension);
                        } else {
                            interpreter.throwError("Received Y Dimension is not a valid dimension.");
                        }
                    } else {
                        interpreter.throwError("Received X Dimension is not a valid dimension.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("X dimension", "The X dimension of the Vector", "Number"),
            new CommandInfoAttribute("Y dimension", "The Y dimension of the Vector", "Number"),
            new CommandInfoAttribute("id", "The id that will identify this Vector", "String")
        };
        String[] noteList = {
            "The dimensions must both be a positive integer.",
            "All of the positions in the vector will start with the value 0.",
            "If the given id is already being used by another variable, IDCom, array, vector or matrix, an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Creates a new Matrix with the given id and the given dimensions", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new NewMatrixCommand(this.getId(), this.getModule());
    }

}
