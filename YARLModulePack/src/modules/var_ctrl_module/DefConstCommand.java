package modules.var_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: DefConst
 *
 * Module: Variable Control Module
 *
 * Description: Creates a new constant with the given id and value
 *
 * ---Attributes---
 *
 * Name: value
 * Description: The object that the constant will have as value
 * Type: Object
 *
 * Name: id
 * Description: The id will identify this constant
 * Type: String
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * A constant is funcionally equal to the variable with the exeption that is
 * value cannot be changed.
 *
 * If the given id is already being used by another variable, IDCom, array,
 * vector or matrix, an error will be thrown.
 *
 * If the object implements the ClonableObject interface, then the cloneObject()
 * method will be used to insert the value, else the reference of the object
 * will be inserted instead.
 *
 * ---Notes---
 *
 * Usage: < value > < id > DefConst
 *
 * @author Ricardo Cacheira
 */
public class DefConstCommand extends ModuleCommand {

    public static String COMMAND_NAME = "DefConst";

    public DefConstCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public DefConstCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[1] instanceof String) {
                    interpreter.defineConstant((String) parameteres[1], parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("value", "The object that the constant will have as value", "Object"),
            new CommandInfoAttribute("id", "The id will identify this constant", "String")
        };
        String[] noteList = {
            "A constant is funcionally equal to the variable with the exeption that is value cannot be changed.",
            "If the given id is already being used by another variable, IDCom, array, vector or matrix, an error will be thrown.",
            "If the object implements the ClonableObject interface, then the cloneObject() method will be used to insert the value, else the reference of the object will be inserted instead."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Creates a new constant with the given id and value", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new DefConstCommand(this.getId(), this.getModule());
    }

}
