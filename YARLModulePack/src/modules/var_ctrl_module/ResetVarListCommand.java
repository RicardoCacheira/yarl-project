package modules.var_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ResetVarList
 *
 * Module: Variable Control Module
 *
 * Description: Removes all the variables from the interpeter
 *
 * ---Note---
 *
 * Arrays, vectors and matrices are considered variables and will be removed by
 * this command.
 *
 * ---Note---
 *
 * Usage: ResetVarList
 *
 * @author Ricardo Cacheira
 */
public class ResetVarListCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ResetVarList";

    public ResetVarListCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ResetVarListCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.resetVariableMap();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "Arrays, vectors and matrices are considered variables and will be removed by this command."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Removes all the variables from the interpeter", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ResetVarListCommand(this.getId(), this.getModule());
    }

}
