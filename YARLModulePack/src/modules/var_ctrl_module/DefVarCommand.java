package modules.var_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: DefVar
 *
 * Module: Variable Control Module
 *
 * Description: Creates a new variable with the given id and value
 *
 * ---Attributes---
 *
 * Name: value
 * Description: The object that the variable will have as value
 * Type: Object
 *
 * Name: id
 * Description: The id that will identify this variable
 * Type: String
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * If the given id is already being used by another variable, IDCom, array,
 * vector or matrix, an error will be thrown.
 *
 * If the object implements the ClonableObject interface, then the cloneObject()
 * method will be used to insert the value, else the reference of the object
 * will be inserted instead.
 *
 * ---Notes---
 *
 * Usage: < value > < id > DefVar
 *
 * @author Ricardo Cacheira
 */
public class DefVarCommand extends ModuleCommand {

    public static String COMMAND_NAME = "DefVar";

    public DefVarCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public DefVarCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[1] instanceof String) {
                    interpreter.defineVariable((String) parameteres[1], parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("value", "The object that the variable will have as value", "Object"),
            new CommandInfoAttribute("id", "The id that will identify this variable", "String")
        };
        String[] noteList = {
            "If the given id is already being used by another variable, IDCom, array, vector or matrix, an error will be thrown.",
            "If the object implements the ClonableObject interface, then the cloneObject() method will be used to insert the value, else the reference of the object will be inserted instead."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Creates a new variable with the given id and value", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new DefVarCommand(this.getId(), this.getModule());
    }

}
