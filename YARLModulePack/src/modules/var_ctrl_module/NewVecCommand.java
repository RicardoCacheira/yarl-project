package modules.var_ctrl_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: NewVec
 *
 * Module: Variable Control Module
 *
 * Description: Creates a new Vector with the given id and the given length
 *
 * ---Attributes---
 *
 * Name: length
 * Description: The length of the Vector
 * Type: Number
 *
 * Name: id
 * Description: The id that will identify this Vector
 * Type: String
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The length must be a positive integer.
 *
 * All of the positions in the vector will start with the value 0.
 *
 * If the given id is already being used by another variable, IDCom, array,
 * vector or matrix, an error will be thrown.
 *
 * ---Notes---
 *
 * Usage: < length > < id > NewVec
 *
 * @author Ricardo Cacheira
 */
public class NewVecCommand extends ModuleCommand {

    public static String COMMAND_NAME = "NewVec";

    public NewVecCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public NewVecCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof String)) {
                    int lenght = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                    if (lenght != -1) {
                        interpreter.newVector((String) parameteres[1], lenght);
                    } else {
                        interpreter.throwError("Received Number is not a valid Lenght.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("length", "The length of the Vector", "Number"),
            new CommandInfoAttribute("id", "The id that will identify this Vector", "String")
        };
        String[] noteList = {
            "The length must be a positive integer.",
            "All of the positions in the vector will start with the value 0.",
            "If the given id is already being used by another variable, IDCom, array, vector or matrix, an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Creates a new Vector with the given id and the given length", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new NewVecCommand(this.getId(), this.getModule());
    }

}
