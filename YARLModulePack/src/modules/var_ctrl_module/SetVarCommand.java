package modules.var_ctrl_module;

import java.util.List;
import parser.ParsedCommand;
import parser.Token;
import parser.YARLLexer;
import parser.YARLParser;
import parser.parsedtoken.ParsedToken;
import parser.parsedtoken.VecPosToken;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: SetVar
 *
 * Module: Variable Control Module
 *
 * Description: Sets the recieved Object as the new value of the variable
 *
 * ---Attributes---
 *
 * Name: value
 * Description: The object that the variable will have as value
 * Type: Object
 *
 * Name: id/ArrayPos/VecPos/MatrixPos
 * Description: The id/ArrayPos/VecPos/MatrixPos of the variable that we wish to
 * change the value
 * Type: String
 *
 * ---Attributes---
 *
 * ---Note---
 *
 * If the object implements the ClonableObject interface, then the cloneObject()
 * method will be used to insert the value, else the reference of the object
 * will be inserted instead.
 *
 * ---Note---
 *
 * Usage: < value > < id/ArrayPos/VecPos/MatrixPos > SetVar
 *
 * @author Ricardo Cacheira
 */
public class SetVarCommand extends ModuleCommand {

    public static String COMMAND_NAME = "SetVar";

    private YARLLexer lexer;

    private YARLParser parser;

    public SetVarCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
        this.lexer = new YARLLexer();
        this.parser = new YARLParser();
    }

    public SetVarCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[1] instanceof String) {
                    String str = (String) parameteres[1];
                    List<Token> result = lexer.analizeCommand(str);
                    if ((result.size() == 1) && (result.get(0).type() == Token.TokenType.ID)) {
                        interpreter.setVariableValue((String) parameteres[1], parameteres[0]);
                    } else {
                        ParsedCommand com = parser.parseCommand(result);
                        if (com.type() == ParsedCommand.ParsedCommandType.RUNNABLE && com.runnableSection().size() == 1) {
                            ParsedToken parsedToken = com.runnableSection().get(0);
                            if (parsedToken.type().equals(VecPosToken.VEC_POS_TYPE)) {
                                VecPosToken vecPos = (VecPosToken) parsedToken;
                                int[] pos = vecPos.getPositions(interpreter);
                                if (pos.length != 0) {
                                    if (vecPos.isTwoDimension()) {
                                        interpreter.setMatrixPos(vecPos.name(), pos[0], pos[1], parameteres[0]);
                                    } else {
                                        interpreter.setArrayVectorPos(vecPos.name(), pos[0], parameteres[0]);
                                    }
                                }

                            } else {
                                interpreter.throwError("Invalid Id/ArrayPos/VecPos/MatrixPos.");
                            }
                        } else {
                            interpreter.throwError("Invalid Id/ArrayPos/VecPos/MatrixPos.");
                        }
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("value", "The object that the variable will have as value", "Object"),
            new CommandInfoAttribute("id/ArrayPos/VecPos/MatrixPos", "The id/ArrayPos/VecPos/MatrixPos of the variable that we wish to change the value", "String")
        };
        String[] noteList = {
            "If the object implements the ClonableObject interface, then the cloneObject() method will be used to insert the value, else the reference of the object will be inserted instead."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Sets the recieved Object as the new value of the variable", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SetVarCommand(this.getId(), this.getModule());
    }

}
