package modules.var_ctrl_module;

import java.util.Map;
import java.util.TreeMap;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.variable.YARLI_Variable;
import yarli.YARLI;

/**
 * Name: ShowVarList
 *
 * Module: Variable Control Module
 *
 * Description: Prints all of the variables onto the shell.
 *
 * ---Note---
 *
 * The variables will be printed line by line in the shell and will contain the
 * id and a string representing the variable.
 *
 * ---Note---
 *
 * Usage: ShowVarList
 *
 * @author Ricardo Cacheira
 */
public class ShowVarListCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ShowVarList";

    public ShowVarListCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ShowVarListCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            StringBuilder sb = new StringBuilder("Variable List: \n\n");
            Map<String, YARLI_Variable> map = new TreeMap<>(interpreter.variableMap());
            for (Map.Entry<String, YARLI_Variable> entry : map.entrySet()) {
                YARLI_Variable var = entry.getValue();
                sb.append(String.format("Id: %s, Value: %s\n", var.getId(), interpreter.generateObjString(var.getValue())));
            }
            interpreter.showOutput(sb.toString(), YARLI.PLAIN_OUTPUT);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "The variables will be printed line by line in the shell and will contain the id and a string representing the variable."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Prints all of the variables onto the shell.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ShowVarListCommand(this.getId(), this.getModule());
    }

}
