package modules.var_ctrl_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is responsible for the commands to control, manage and modify Variables, Arrays, Vectors and Matrices.
 *
 * @author Ricardo Cacheira
 */
public class VarCtrlModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new AddArrayValCommand(this), 
			new AddArrayValAtCommand(this), 
			new ArrayVecSizeCommand(this), 
			new ClearArrayCommand(this), 
			new DefConstCommand(this), 
			new DefVarCommand(this), 
			new IsConstCommand(this), 
			new MatrixSizesCommand(this), 
			new NewArrayCommand(this), 
			new NewMatrixCommand(this), 
			new NewVarCommand(this), 
			new NewVecCommand(this), 
			new RemArrayValCommand(this), 
			new RemVarCommand(this), 
			new ResetVarListCommand(this), 
			new SetAsConstCommand(this), 
			new SetVarCommand(this), 
			new ShowVarListCommand(this), 
			new VarDumpCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"DefVar",
			"DefConst",
			"NewVar",
			"SetAsConst",
			"SetVar",
			"RemVar",
			"ResetVarList",
			"VarDump",
			"ShowVarList",
			"NewVec",
			"NewMatrix",
			"NewArray",
			"AddArrayVal",
			"AddArrayValAt",
			"RemArrayVal",
			"ClearArray",
			"ArrayVecSize",
			"MatrixSizes",
			"IsConst"
		};
	}

	@Override
	public String moduleName() {
		return "Variable Control Module";
	}

	@Override
	public String help() {
		return "This module is responsible for the commands to control, manage and modify Variables, Arrays, Vectors and Matrices.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new VarCtrlModule();
	}

}