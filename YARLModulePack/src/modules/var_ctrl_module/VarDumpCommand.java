package modules.var_ctrl_module;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Map;
import java.util.TreeMap;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.variable.YARLI_Variable;
import yarli.YARLI;

/**
 * Name: VarDump
 *
 * Module: Variable Control Module
 *
 * Description: Dumps all of the variables onto the desired file.
 *
 * ---Attribute---
 *
 * Name: filepath
 * Description: The the path of the file to be created with the list of the
 * variables
 * Type: String
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * The variables will be written line by line in the file and will contain the
 * id and a string representing the variable.
 *
 * If the interpreter is unable to create the file an error will be thrown.
 *
 * ---Notes---
 *
 * Usage: < filepath > VarDump
 *
 * @author Ricardo Cacheira
 */
public class VarDumpCommand extends ModuleCommand {

    public static String COMMAND_NAME = "VarDump";

    public VarDumpCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public VarDumpCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String)) {
                    try {
                        Formatter out = new Formatter((String) parameteres[0]);
                        Map<String, YARLI_Variable> map = new TreeMap<>(interpreter.variableMap());
                        for (Map.Entry<String, YARLI_Variable> entry : map.entrySet()) {
                            YARLI_Variable var = entry.getValue();
                            out.format("Id: %s, Value: %s\n", var.getId(), interpreter.generateObjString(var.getValue()));
                        }
                        out.close();
                    } catch (FileNotFoundException ex) {
                        interpreter.throwError("Error creating the File.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("filepath", "The the path of the file to be created with the list of the variables", "String")
        };
        String[] noteList = {
            "The variables will be written line by line in the file and will contain the id and a string representing the variable.",
            "If the interpreter is unable to create the file an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Dumps all of the variables onto the desired file.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new VarDumpCommand(this.getId(), this.getModule());
    }

}
