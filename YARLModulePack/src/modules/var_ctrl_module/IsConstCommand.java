package modules.var_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.variable.YARLI_Variable;

/**
 * Name: IsConst
 *
 * Module: Variable Control Module
 *
 * Description: Returns if the variable is constant or not.
 *
 * Return: The boolean value representing if the variable is constant.
 *
 * Return Type: Boolean
 *
 * ---Attribute---
 *
 * Name: id
 * Description: The id that identifies this variable to ckeck
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * If no variable is found an error will be thrown.
 *
 * ---Note---
 *
 * Usage: < id > IsConst -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class IsConstCommand extends ModuleCommand {
    
    public static String COMMAND_NAME = "IsConst";
    
    public IsConstCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }
    
    public IsConstCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }
    
    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    String str = (String) parameteres[0];
                    YARLI_Variable var = interpreter.getVariable(str);
                    if (var != null) {
                        interpreter.push(var.isConstant());
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }
    
    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("id", "The id that identifies this variable to ckeck", "String")
        };
        String[] noteList = {
            "If no variable is found an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns if the variable is constant or not.", "The boolean value representing if the variable is constant.", "Boolean", attributeList, noteList);
    }
    
    @Override
    public IdDefinedCommand cloneObject() {
        return new IsConstCommand(this.getId(), this.getModule());
    }
    
}
