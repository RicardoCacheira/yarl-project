package modules.var_ctrl_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.variable.NoVariableFoundException;
import yarli.YARLI;

/**
 * Name: ArrayVecSize
 *
 * Module: Variable Control Module
 *
 * Description: Returns the size of the given array/vector
 *
 * Return: The size of the array/vector
 *
 * Return Type: Number
 *
 * ---Attribute---
 *
 * Name: id
 * Description: The id of the array/vector to return the size
 * Type: String
 *
 * ---Attribute---
 *
 * Usage: < id > ArrayVecSize -> Number
 *
 * @author Ricardo Cacheira
 */
public class ArrayVecSizeCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ArrayVecSize";

    public ArrayVecSizeCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ArrayVecSizeCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    try {
                        interpreter.push(new RationalNumber(interpreter.getArrayVecSize((String) parameteres[0])));
                    } catch (NoVariableFoundException ex) {
                        interpreter.throwError(ex.getMessage());
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("id", "The id of the array/vector to return the size", "String")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns the size of the given array/vector", "The size of the array/vector", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ArrayVecSizeCommand(this.getId(), this.getModule());
    }

}
