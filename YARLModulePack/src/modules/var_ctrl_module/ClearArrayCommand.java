package modules.var_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ClearArray
 *
 * Module: Variable Control Module
 *
 * Description: Removes all of the values from the array
 *
 * ---Attribute---
 *
 * Name: id
 * Description: The id of the array to clear
 * Type: String
 *
 * ---Attribute---
 *
 * Usage: < id > ClearArray
 *
 * @author Ricardo Cacheira
 */
public class ClearArrayCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ClearArray";

    public ClearArrayCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ClearArrayCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.clearArray((String) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("id", "The id of the array to clear", "String")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Removes all of the values from the array", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ClearArrayCommand(this.getId(), this.getModule());
    }

}
