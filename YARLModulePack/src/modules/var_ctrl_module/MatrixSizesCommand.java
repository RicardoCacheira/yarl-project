package modules.var_ctrl_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.variable.NoVariableFoundException;

/**
 * Name: MatrixSizes
 *
 * Module: Variable Control Module
 *
 * Description: Returns the sizes of the given matrix
 *
 * Return: The sizes of the matrix
 *
 * Return Type: Number
 *
 * ---Attribute---
 *
 * Name: id
 * Description: The id of the array to clear
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * The return is pushws two seperate values to the stack, first the X size and
 * secoond the Y size.
 *
 * ---Note---
 *
 * Usage: < id > MatrixSizes -> Number
 *
 * @author Ricardo Cacheira
 */
public class MatrixSizesCommand extends ModuleCommand {

    public static String COMMAND_NAME = "MatrixSizes";

    public MatrixSizesCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public MatrixSizesCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    try {
                        int[] aux = interpreter.getMatrixSizes((String) parameteres[0]);
                        interpreter.push(new RationalNumber(aux[0]));
                        interpreter.push(new RationalNumber(aux[1]));
                    } catch (NoVariableFoundException ex) {
                        interpreter.throwError(ex.getMessage());
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("id", "The id of the array to clear", "String")
        };
        String[] noteList = {
            "The return is pushws two seperate values to the stack, first the X size and secoond the Y size."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns the sizes of the given matrix", "The sizes of the matrix", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new MatrixSizesCommand(this.getId(), this.getModule());
    }

}
