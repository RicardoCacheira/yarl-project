package modules.var_ctrl_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: AddArrayValAt
 *
 * Module: Variable Control Module
 *
 * Description: Adds the recieved Object to the desired position of the array
 *
 * ---Attributes---
 *
 * Name: value
 * Description: The object to insert at desired position of the array
 * Type: Object
 *
 * Name: pos
 * Description: The position in witch we have to insert the object into the
 * array
 * Type: Number
 *
 * Name: id
 * Description: The id of the array to insert the value
 * Type: String
 *
 * ---Attributes---
 *
 * ---Note---
 *
 * If the object implements the ClonableObject interface, then the cloneObject()
 * method will be used to insert the value, else the reference of the object
 * will be inserted instead.
 *
 * ---Note---
 *
 * Usage: < value > < pos > < id > AddArrayValAt
 *
 * @author Ricardo Cacheira
 */
public class AddArrayValAtCommand extends ModuleCommand {

    public static String COMMAND_NAME = "AddArrayValAt";

    public AddArrayValAtCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public AddArrayValAtCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(3, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[1] instanceof RationalNumber) && (parameteres[2] instanceof String)) {
                    int pos = interpreter.generateNonNegativeInt((RationalNumber) parameteres[1]);
                    if (pos != -1) {
                        interpreter.addObjToArrayAt((String) parameteres[2], pos, parameteres[0]);
                    } else {
                        interpreter.throwError("Received Number is not a valid position.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("value", "The object to insert at desired position of the array", "Object"),
            new CommandInfoAttribute("pos", "The position in witch we have to insert the object into the array", "Number"),
            new CommandInfoAttribute("id", "The id of the array to insert the value", "String")
        };
        String[] noteList = {
            "If the object implements the ClonableObject interface, then the cloneObject() method will be used to insert the value, else the reference of the object will be inserted instead."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Adds the recieved Object to the desired position of the array", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new AddArrayValAtCommand(this.getId(), this.getModule());
    }

}
