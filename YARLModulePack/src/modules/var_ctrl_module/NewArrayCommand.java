package modules.var_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: NewArray
 *
 * Module: Variable Control Module
 *
 * Description: Creates a new Array with the given id
 *
 * ---Attribute---
 *
 * Name: id
 * Description: The id that will identify this Array
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * If the given id is already being used by another variable, IDCom, array,
 * vector or matrix, an error will be thrown.
 *
 * ---Note---
 *
 * Usage: < id > NewArray
 *
 * @author Ricardo Cacheira
 */
public class NewArrayCommand extends ModuleCommand {

    public static String COMMAND_NAME = "NewArray";

    public NewArrayCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public NewArrayCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.newArray((String) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("id", "The id that will identify this Array", "String")
        };
        String[] noteList = {
            "If the given id is already being used by another variable, IDCom, array, vector or matrix, an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Creates a new Array with the given id", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new NewArrayCommand(this.getId(), this.getModule());
    }

}
