package modules.var_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: SetAsConst
 *
 * Module: Variable Control Module
 *
 * Description: Sets the variable with the given id as a constant
 *
 * ---Attribute---
 *
 * Name: id
 * Description: The id of the variable that we wish to turn into a constant
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * This process is irreversible.
 *
 * ---Note---
 *
 * Usage: < id > SetAsConst
 *
 * @author Ricardo Cacheira
 */
public class SetAsConstCommand extends ModuleCommand {

    public static String COMMAND_NAME = "SetAsConst";

    public SetAsConstCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SetAsConstCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.setVariableAsConstant((String) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("id", "The id of the variable that we wish to turn into a constant", "String")
        };
        String[] noteList = {
            "This process is irreversible."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Sets the variable with the given id as a constant", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SetAsConstCommand(this.getId(), this.getModule());
    }

}
