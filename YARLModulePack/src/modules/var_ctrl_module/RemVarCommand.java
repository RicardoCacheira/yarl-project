package modules.var_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: RemVar
 *
 * Module: Variable Control Module
 *
 * Description: Removes the variable with the given id
 *
 * ---Attribute---
 *
 * Name: id
 * Description: The id that identifies this variable to be removed
 * Type: String
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * Arrays, vectors and matrices are considered variables and can be removed by
 * this command.
 *
 * If no variable is found an warning will be thrown.
 *
 * ---Notes---
 *
 * Usage: < id > RemVar
 *
 * @author Ricardo Cacheira
 */
public class RemVarCommand extends ModuleCommand {

    public static String COMMAND_NAME = "RemVar";

    public RemVarCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public RemVarCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.removeVariable((String) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("id", "The id that identifies this variable to be removed", "String")
        };
        String[] noteList = {
            "Arrays, vectors and matrices are considered variables and can be removed by this command.",
            "If no variable is found an warning will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Removes the variable with the given id", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new RemVarCommand(this.getId(), this.getModule());
    }

}
