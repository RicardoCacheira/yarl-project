package modules.var_ctrl_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.variable.NoVariableFoundException;
import yarli.YARLI;

/**
 * Name: RemArrayVal
 *
 * Module: Variable Control Module
 *
 * Description: Removes the Object from the desired position of the array
 *
 * ---Attributes---
 *
 * Name: pos
 * Description: The position of the object we wish to remove from the array
 * Type: Number
 *
 * Name: id
 * Description: The id of the array to remove the value
 * Type: String
 *
 * ---Attributes---
 *
 * Usage: < pos > < id > RemArrayVal
 *
 * @author Ricardo Cacheira
 */
public class RemArrayValCommand extends ModuleCommand {

    public static String COMMAND_NAME = "RemArrayVal";

    public RemArrayValCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public RemArrayValCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof String)) {
                    int pos = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                    if (pos != -1) {
                        try {
                            interpreter.removeObjFromArrayAt((String) parameteres[1], pos);
                        } catch (IndexOutOfBoundsException | NoVariableFoundException ex) {
                            interpreter.throwError(ex.getMessage());
                        }
                    } else {
                        interpreter.throwError("Received Number is not a valid position.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("pos", "The position of the object we wish to remove from the array", "Number"),
            new CommandInfoAttribute("id", "The id of the array to remove the value", "String")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Removes the Object from the desired position of the array", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new RemArrayValCommand(this.getId(), this.getModule());
    }

}
