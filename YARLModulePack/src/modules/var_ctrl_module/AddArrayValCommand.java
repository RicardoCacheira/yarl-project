package modules.var_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: AddArrayVal
 *
 * Module: Variable Control Module
 *
 * Description: Adds the recieved Object to the end of the array
 *
 * ---Attributes---
 *
 * Name: value
 * Description: The object to insert at the end of the array
 * Type: Object
 *
 * Name: id
 * Description: The id of the array to insert the value
 * Type: String
 *
 * ---Attributes---
 *
 * ---Note---
 *
 * If the object implements the ClonableObject interface, then the cloneObject()
 * method will be used to insert the value, else the reference of the object
 * will be inserted instead.
 *
 * ---Note---
 *
 * Usage: < value > < id > AddArrayVal
 *
 * @author Ricardo Cacheira
 */
public class AddArrayValCommand extends ModuleCommand {

    public static String COMMAND_NAME = "AddArrayVal";

    public AddArrayValCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public AddArrayValCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[1] instanceof String) {
                    interpreter.addObjToArray((String) parameteres[1], parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("value", "The object to insert at the end of the array", "Object"),
            new CommandInfoAttribute("id", "The id of the array to insert the value", "String")
        };
        String[] noteList = {
            "If the object implements the ClonableObject interface, then the cloneObject() method will be used to insert the value, else the reference of the object will be inserted instead."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Adds the recieved Object to the end of the array", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new AddArrayValCommand(this.getId(), this.getModule());
    }

}
