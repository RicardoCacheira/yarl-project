package modules.command_ctrl_module;

import java.util.Map;
import java.util.TreeMap;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ShowIDCom
 *
 * Module: Command Control Module
 *
 * Description: Shows all the commands in the command list(Module command and
 * UDCom).
 *
 * Usage: ShowIDCom
 *
 * @author Ricardo Cacheira
 */
public class ShowIDComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ShowIDCom";

    public ShowIDComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ShowIDComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            StringBuilder sb = new StringBuilder("Command List: \n\n");
            Map<String, IdDefinedCommand> aux = new TreeMap<>(interpreter.commandMap());
            for (Map.Entry<String, IdDefinedCommand> entry : aux.entrySet()) {
                IdDefinedCommand command = entry.getValue();
                if (command instanceof ModuleCommand) {
                    ModuleCommand mCommand = (ModuleCommand) command;
                    sb.append(String.format("Id: %s, Module: %s", mCommand.getId(), mCommand.getModule().moduleName()));
                } else {
                    sb.append(command.getInfo().toString());
                }
                sb.append('\n');
            }
            interpreter.showOutput(sb.toString(), YARLI.PLAIN_OUTPUT);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Shows all the commands in the command list(Module command and UDCom).", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ShowIDComCommand(this.getId(), this.getModule());
    }

}
