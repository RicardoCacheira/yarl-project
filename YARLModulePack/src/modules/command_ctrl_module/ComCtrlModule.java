package modules.command_ctrl_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is responsible for the commands to control, manage and modify the commands.
 *
 * @author Ricardo Cacheira
 */
public class ComCtrlModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new AutoComCommand(this), 
			new ComDumpCommand(this), 
			new NoAutoComCommand(this), 
			new RemComCommand(this), 
			new ResetComCommand(this), 
			new SetComIDCommand(this), 
			new ShowIDComCommand(this), 
			new ShowMComCommand(this), 
			new ShowUDComCommand(this), 
			new ShowUsageCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"ResetCom",
			"SetComID",
			"RemCom",
			"ComDump",
			"ShowUDCom",
			"ShowIDCom",
			"ShowMCom",
			"ShowUsage",
			"AutoCom",
			"NoAutoCom"
		};
	}

	@Override
	public String moduleName() {
		return "Command Control Module";
	}

	@Override
	public String help() {
		return "This module is responsible for the commands to control, manage and modify the commands.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new ComCtrlModule();
	}

}