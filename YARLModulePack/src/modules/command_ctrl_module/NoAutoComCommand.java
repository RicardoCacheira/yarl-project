package modules.command_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: NoAutoCom
 *
 * Module: Command Control Module
 *
 * Description: Disables the previously defined Auto Command.
 *
 * Usage: NoAutoCom
 *
 * @author Ricardo Cacheira
 */
public class NoAutoComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "NoAutoCom";

    public NoAutoComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public NoAutoComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.disableAutoCommand();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Disables the previously defined Auto Command.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new NoAutoComCommand(this.getId(), this.getModule());
    }

}
