package modules.command_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ShowUsage
 *
 * Module: Command Control Module
 *
 * Description: Prints to the shell the info regarding the usage of the desired
 * Module command.
 *
 * ---Attribute---
 *
 * Name: ModuleCommand
 * Description: The ModuleCommand of which the user desires information about
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * If the given string isn't the Id of a Module Command it will be thrown an
 * warning.
 *
 * ---Note---
 *
 * Usage: < ModuleCommand > ShowUsage
 *
 * @author Ricardo Cacheira
 */
public class ShowUsageCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ShowUsage";

    public ShowUsageCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ShowUsageCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    String id = ((String) parameteres[0]).toUpperCase();
                    if (interpreter.hasCommand(id)) {
                        IdDefinedCommand command = interpreter.commandMap().get(id);
                        if (command instanceof ModuleCommand) {
                            ModuleCommand mCom = (ModuleCommand) command;
                            interpreter.showOutput(String.format("\nUsage:\n%s\n", ((ModuleCommandInfo) mCom.getInfo()).showFullUsage()), YARLI.INFO_OUTPUT);
                        } else {
                            interpreter.throwError("Invalid Module Command ID.");
                        }
                    } else {
                        interpreter.throwError("Invalid Module Command ID.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("ModuleCommand", "The ModuleCommand of which the user desires information about", "String")
        };
        String[] noteList = {
            "If the given string isn't the Id of a Module Command it will be thrown an warning."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Prints to the shell the info regarding the usage of the desired Module command.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ShowUsageCommand(this.getId(), this.getModule());
    }

}
