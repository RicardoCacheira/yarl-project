package modules.command_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import parser.ParsedCommand;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: AutoCom
 *
 * Module: Command Control Module
 *
 * Description: Defines a command to be run after a command is inserted into the
 * interpeter.
 *
 * ---Attribute---
 *
 * Name: command
 * Description: The command to run after a command is inserted into the
 * interpeter
 * Type: ParsedCommand
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * The Auto Command must be a runnable(with var) command else an error will be
 * thrown.
 *
 * The Auto Command will be disabled if it breaks its execution.
 *
 * Warning, A command can fully run even if an error is thrown depending on the
 * error policy.
 *
 * ---Notes---
 *
 * Usage: < command > AutoCom
 *
 * @author Ricardo Cacheira
 */
public class AutoComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "AutoCom";

    public AutoComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public AutoComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof ParsedCommand)) {
                    interpreter.enableAutoCommand((ParsedCommand) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("command", "The command to run after a command is inserted into the interpeter", "ParsedCommand")
        };
        String[] noteList = {
            "The Auto Command must be a runnable(with var) command else an error will be thrown.",
            "The Auto Command will be disabled if it breaks its execution.",
            "Warning, A command can fully run even if an error is thrown depending on the error policy."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Defines a command to be run after a command is inserted into the interpeter.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new AutoComCommand(this.getId(), this.getModule());
    }

}
