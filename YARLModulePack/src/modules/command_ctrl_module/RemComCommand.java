package modules.command_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: RemCom
 *
 * Module: Command Control Module
 *
 * Description: Removes the command with the given id
 *
 * ---Attribute---
 *
 * Name: command id
 * Description: the id of the command which we want to remove
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * If a command with the given id doens't exist an error will be thrown.
 *
 * ---Note---
 *
 * Usage: < command id > RemCom
 *
 * @author Ricardo Cacheira
 */
public class RemComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "RemCom";

    public RemComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public RemComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String)) {
                    interpreter.removeCommand((String) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("command id", "the id of the command which we want to remove", "String")
        };
        String[] noteList = {
            "If a command with the given id doens't exist an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Removes the command with the given id", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new RemComCommand(this.getId(), this.getModule());
    }

}
