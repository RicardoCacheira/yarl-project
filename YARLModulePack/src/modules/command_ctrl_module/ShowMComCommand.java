package modules.command_ctrl_module;

import java.util.Map;
import java.util.TreeMap;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ShowMCom
 *
 * Module: Command Control Module
 *
 * Description: Shows all the Module commands in the command list.
 *
 * Usage: ShowMCom
 *
 * @author Ricardo Cacheira
 */
public class ShowMComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ShowMCom";

    public ShowMComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ShowMComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            StringBuilder sb = new StringBuilder("Module Command List: \n\n");
            Map<String, IdDefinedCommand> map = interpreter.commandMap();
            Map<String, IdDefinedCommand> aux = new TreeMap<>();
            for (Map.Entry<String, IdDefinedCommand> entry : map.entrySet()) {
                IdDefinedCommand command = entry.getValue();
                if (command instanceof ModuleCommand) {
                    aux.put(command.getId(), command);
                }
            }
            for (Map.Entry<String, IdDefinedCommand> entry : aux.entrySet()) {
                ModuleCommand mCommand = (ModuleCommand) entry.getValue();
                sb.append(String.format("Id: %s, Module: %s\n", mCommand.getId(), mCommand.getModule().moduleName()));
            }
            interpreter.showOutput(sb.toString(), YARLI.PLAIN_OUTPUT);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Shows all the Module commands in the command list.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ShowMComCommand(this.getId(), this.getModule());
    }

}
