package modules.command_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ResetCom
 *
 * Module: Command Control Module
 *
 * Description: Resets the Command list by clearing it and by reinserting all
 * the previously loaded Module commands .
 *
 * Usage: ResetCom
 *
 * @author Ricardo Cacheira
 */
public class ResetComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ResetCom";

    public ResetComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ResetComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.resetCommandMap();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Resets the Command list by clearing it and by reinserting all the previously loaded Module commands .", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ResetComCommand(this.getId(), this.getModule());
    }

}
