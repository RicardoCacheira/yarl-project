package modules.command_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: SetComID
 *
 * Module: Command Control Module
 *
 * Description: Sets a new id for the command with the previously given id
 *
 * ---Attributes---
 *
 * Name: current command id
 * Description: the current id of the command which we want to change the id
 * Type: String
 *
 * Name: new command id
 * Description: the new id that we want to define onto the command
 * Type: String
 *
 * ---Attributes---
 *
 * ---Note---
 *
 * If a command with the current id doens't exist or if a command with the new
 * id already exists, an error will be thrown.
 *
 * ---Note---
 *
 * Usage: < current command id > < new command id > SetComID
 *
 * @author Ricardo Cacheira
 */
public class SetComIDCommand extends ModuleCommand {

    public static String COMMAND_NAME = "SetComID";

    public SetComIDCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SetComIDCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String) && (parameteres[1] instanceof String)) {
                    interpreter.setNewCommandId((String) parameteres[0], (String) parameteres[1]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("current command id", "the current id of the command which we want to change the id", "String"),
            new CommandInfoAttribute("new command id", "the new id that we want to define onto the command", "String")
        };
        String[] noteList = {
            "If a command with the current id doens't exist or if a command with the new id already exists, an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Sets a new id for the command with the previously given id", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SetComIDCommand(this.getId(), this.getModule());
    }

}
