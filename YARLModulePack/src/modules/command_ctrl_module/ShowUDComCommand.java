package modules.command_ctrl_module;

import java.util.Map;
import java.util.TreeMap;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.command.UserDefinedCommand;
import yarli.YARLI;

/**
 * Name: ShowUDCom
 *
 * Module: Command Control Module
 *
 * Description: Shows all the UDCom in the command list.
 *
 * Usage: ShowUDCom
 *
 * @author Ricardo Cacheira
 */
public class ShowUDComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ShowUDCom";

    public ShowUDComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ShowUDComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            StringBuilder sb = new StringBuilder("User Defined Command List: \n\n");
            Map<String, IdDefinedCommand> map = interpreter.commandMap();
            Map<String, IdDefinedCommand> aux = new TreeMap<>();
            for (Map.Entry<String, IdDefinedCommand> entry : map.entrySet()) {
                IdDefinedCommand command = entry.getValue();
                if (command instanceof UserDefinedCommand) {
                    aux.put(command.getId(), command);
                }
            }
            for (Map.Entry<String, IdDefinedCommand> entry : aux.entrySet()) {
                sb.append(entry.getValue().getInfo().toString()).append('\n');
            }
            interpreter.showOutput(sb.toString(), YARLI.PLAIN_OUTPUT);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Shows all the UDCom in the command list.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ShowUDComCommand(this.getId(), this.getModule());
    }

}
