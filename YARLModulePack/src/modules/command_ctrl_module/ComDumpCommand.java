package modules.command_ctrl_module;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Map;
import java.util.TreeMap;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.command.UserDefinedCommand;
import yarli.YARLI;

/**
 * Name: ComDump
 *
 * Module: Command Control Module
 *
 * Description: Dumps all of the UDCom onto the desired file.
 *
 * ---Attribute---
 *
 * Name: filepath
 * Description: The the path of the file to be created with the list of the
 * UDCom
 * Type: String
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * Only UDCom will be put onto the file, any Module Command will not be put.
 *
 * If the interpreter is unable to create the file an error will be thrown.
 *
 * ---Notes---
 *
 * Usage: < filepath > ComDump
 *
 * @author Ricardo Cacheira
 */
public class ComDumpCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ComDump";

    public ComDumpCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ComDumpCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String)) {
                    try {
                        Formatter out = new Formatter((String) parameteres[0]);
                        Map<String, IdDefinedCommand> map = interpreter.commandMap();
                        Map<String, IdDefinedCommand> aux = new TreeMap<>();
                        for (Map.Entry<String, IdDefinedCommand> entry : map.entrySet()) {
                            IdDefinedCommand command = entry.getValue();
                            if (command instanceof UserDefinedCommand) {
                                aux.put(command.getId(), command);
                            }
                        }
                        for (Map.Entry<String, IdDefinedCommand> entry : aux.entrySet()) {
                            out.format("%s\n", entry.getValue().getInfo().toString());
                        }
                        out.close();
                    } catch (FileNotFoundException ex) {
                        interpreter.throwError("Error creating the File.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("filepath", "The the path of the file to be created with the list of the UDCom", "String")
        };
        String[] noteList = {
            "Only UDCom will be put onto the file, any Module Command will not be put.",
            "If the interpreter is unable to create the file an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Dumps all of the UDCom onto the desired file.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ComDumpCommand(this.getId(), this.getModule());
    }

}
