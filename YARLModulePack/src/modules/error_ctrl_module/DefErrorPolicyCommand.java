package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.error.ErrorPolicy;

/**
 * Name: DefErrorPolicy
 *
 * Module: Error Control Module
 *
 * Description: Defines the error policy to be used by the interpreter
 *
 * ---Attribute---
 *
 * Name: DesiredErrorPolicy
 * Description: The error policy to be used by the interpreter
 * Type: ErrorPolicy
 *
 * ---Attribute---
 *
 * Usage: < DesiredErrorPolicy > DefErrorPolicy
 *
 * @author Ricardo Cacheira
 */
public class DefErrorPolicyCommand extends ModuleCommand {

    public static String COMMAND_NAME = "DefErrorPolicy";

    public DefErrorPolicyCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public DefErrorPolicyCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof ErrorPolicy) {
                    interpreter.defineErrorPolicy((ErrorPolicy) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("DesiredErrorPolicy", "The error policy to be used by the interpreter", "ErrorPolicy")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Defines the error policy to be used by the interpreter", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new DefErrorPolicyCommand(this.getId(), this.getModule());
    }

}
