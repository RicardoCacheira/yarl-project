package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.error.ErrorMessageHandler;

/**
 * Name: DefErrorMsg
 *
 * Module: Error Control Module
 *
 * Description: Defines how the error message to be handled by the interpreter
 *
 * ---Attribute---
 *
 * Name: DesiredErrorMsgHandler
 * Description: The Error message handler to be used by the interpreter
 * Type: ErrorMsgHandler
 *
 * ---Attribute---
 *
 * Usage: < DesiredErrorMsgHandler > DefErrorMsg
 *
 * @author Ricardo Cacheira
 */
public class DefErrorMsgCommand extends ModuleCommand {

    public static String COMMAND_NAME = "DefErrorMsg";

    public DefErrorMsgCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public DefErrorMsgCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof ErrorMessageHandler) {
                    interpreter.defineErrorMessageHandler((ErrorMessageHandler) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("DesiredErrorMsgHandler", "The Error message handler to be used by the interpreter", "ErrorMsgHandler")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Defines how the error message to be handled by the interpreter", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new DefErrorMsgCommand(this.getId(), this.getModule());
    }

}
