package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.error.CurrentBreakPolicy;

/**
 * Name: CurrBreak
 *
 * Module: Error Control Module
 *
 * Description: Breaks the UDCom command that calls the module command that
 * throws the error.
 *
 * Return: CurrBreak Policy
 *
 * Return Type: ErrorPolicy
 *
 * ---Note---
 *
 * e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 "Hi"
 * Test 42 ' and the stack at the moment is [111, "world"], if when running the
 * command 'A 93', the 'Test' command throws an error, then the execution of the
 * command 'A' will stop and the stack will be ["Hi", 123, 111, "world"] and
 * then the value 93 will be pushed.
 *
 * ---Note---
 *
 * Usage: CurrBreak -> ErrorPolicy
 *
 * @author Ricardo Cacheira
 */
public class CurrBreakCommand extends ModuleCommand {

    public static String COMMAND_NAME = "CurrBreak";

    public CurrBreakCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public CurrBreakCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new CurrentBreakPolicy(interpreter));
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 \"Hi\" Test 42 ' and the stack at the moment is [111, \"world\"], if when running the command 'A 93', the 'Test' command throws an error, then the execution of the command 'A' will stop and the stack will be [\"Hi\", 123, 111, \"world\"] and then the value 93 will be pushed."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Breaks the UDCom command that calls the module command that throws the error.", "CurrBreak Policy", "ErrorPolicy", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new CurrBreakCommand(this.getId(), this.getModule());
    }

}
