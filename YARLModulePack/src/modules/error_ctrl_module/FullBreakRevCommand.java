package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.error.FullBreakRevertPolicy;

/**
 * Name: FullBreakRev
 *
 * Module: Error Control Module
 *
 * Description: Breaks the runnable command and reverts the state of the
 * interpreter to the one at the start of the command
 *
 * Return: FullBreakRev Policy
 *
 * Return Type: ErrorPolicy
 *
 * ---Note---
 *
 * e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 "Hi"
 * Test 42 ' and the stack at the moment is [111, "world"], if when running the
 * command 'A 93', the 'Test' command throws an error, then the execution of the
 * command 'A' will stop and the stack will be reverted to [111, "world"] and
 * then the value 93 will NOT be pushed because the execution was broken.
 *
 * ---Note---
 *
 * Usage: FullBreakRev -> ErrorPolicy
 *
 * @author Ricardo Cacheira
 */
public class FullBreakRevCommand extends ModuleCommand {

    public static String COMMAND_NAME = "FullBreakRev";

    public FullBreakRevCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public FullBreakRevCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new FullBreakRevertPolicy(interpreter));
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 \"Hi\" Test 42 ' and the stack at the moment is [111, \"world\"], if when running the command 'A 93', the 'Test' command throws an error, then the execution of the command 'A' will stop and the stack will be reverted to [111, \"world\"] and then the value 93 will NOT be pushed because the execution was broken."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Breaks the runnable command and reverts the state of the interpreter to the one at the start of the command", "FullBreakRev Policy", "ErrorPolicy", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new FullBreakRevCommand(this.getId(), this.getModule());
    }

}
