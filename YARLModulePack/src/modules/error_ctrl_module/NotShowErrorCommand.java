package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.error.NotShowErrorMessage;

/**
 * Name: NotShowError
 *
 * Module: Error Control Module
 *
 * Description: Hides the error message from the user
 *
 * Return: NotShowError ErrorMsgHandler
 *
 * Return Type: ErrorMsgHandler
 *
 * Usage: NotShowError -> ErrorMsgHandler
 *
 * @author Ricardo Cacheira
 */
public class NotShowErrorCommand extends ModuleCommand {

    public static String COMMAND_NAME = "NotShowError";

    public NotShowErrorCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public NotShowErrorCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new NotShowErrorMessage());
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Hides the error message from the user", "NotShowError ErrorMsgHandler", "ErrorMsgHandler", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new NotShowErrorCommand(this.getId(), this.getModule());
    }

}
