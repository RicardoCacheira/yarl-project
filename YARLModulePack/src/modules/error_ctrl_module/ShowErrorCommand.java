package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.error.ShowErrorMessage;

/**
 * Name: ShowError
 *
 * Module: Error Control Module
 *
 * Description: Shows the error message to the user
 *
 * Return: ShowError ErrorMsgHandler
 *
 * Return Type: ErrorMsgHandler
 *
 * Usage: ShowError -> ErrorMsgHandler
 *
 * @author Ricardo Cacheira
 */
public class ShowErrorCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ShowError";

    public ShowErrorCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ShowErrorCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new ShowErrorMessage(interpreter));
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Shows the error message to the user", "ShowError ErrorMsgHandler", "ErrorMsgHandler", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ShowErrorCommand(this.getId(), this.getModule());
    }

}
