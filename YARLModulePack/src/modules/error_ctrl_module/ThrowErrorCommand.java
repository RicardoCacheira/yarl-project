package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ThrowError
 *
 * Module: Error Control Module
 *
 * Description: Throws an error with the given message.
 *
 * ---Attribute---
 *
 * Name: ErrorMessage
 * Description: The string that will be outputted when the error is shown.
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * This command will always generate an error so depending on the Error Policy
 * it might have undesired effects on the execution.
 *
 * ---Note---
 *
 * Usage: < ErrorMessage > ThrowError
 *
 * @author Ricardo Cacheira
 */
public class ThrowErrorCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ThrowError";

    public ThrowErrorCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ThrowErrorCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.throwError((String) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("ErrorMessage", "The string that will be outputted when the error is shown.", "String")
        };
        String[] noteList = {
            "This command will always generate an error so depending on the Error Policy it might have undesired effects on the execution."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Throws an error with the given message.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ThrowErrorCommand(this.getId(), this.getModule());
    }

}
