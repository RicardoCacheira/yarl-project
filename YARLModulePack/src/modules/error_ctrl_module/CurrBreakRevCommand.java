package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.error.CurrentBreakRevertPolicy;

/**
 * Name: CurrBreakRev
 *
 * Module: Error Control Module
 *
 * Description: Breaks the UDCom command that calls the module command that
 * throws the error and reverts the state of the interpreter to the one at the
 * start of the UDCom command.
 *
 * Return: CurrBreakRev Policy
 *
 * Return Type: ErrorPolicy
 *
 * ---Note---
 *
 * e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 "Hi"
 * Test 42 ' and the stack at the moment is [111, "world"], if when running the
 * command 'A 93', the 'Test' command throws an error, then the execution of the
 * command 'A' will stop and the stack will be reverted to [111, "world"] and
 * then the value 93 will be pushed.
 *
 * ---Note---
 *
 * Usage: CurrBreakRev -> ErrorPolicy
 *
 * @author Ricardo Cacheira
 */
public class CurrBreakRevCommand extends ModuleCommand {

    public static String COMMAND_NAME = "CurrBreakRev";

    public CurrBreakRevCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public CurrBreakRevCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new CurrentBreakRevertPolicy(interpreter));
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 \"Hi\" Test 42 ' and the stack at the moment is [111, \"world\"], if when running the command 'A 93', the 'Test' command throws an error, then the execution of the command 'A' will stop and the stack will be reverted to [111, \"world\"] and then the value 93 will be pushed."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Breaks the UDCom command that calls the module command that throws the error and reverts the state of the interpreter to the one at the start of the UDCom command.", "CurrBreakRev Policy", "ErrorPolicy", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new CurrBreakRevCommand(this.getId(), this.getModule());
    }

}
