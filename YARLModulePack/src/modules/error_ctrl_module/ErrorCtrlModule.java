package modules.error_ctrl_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is responsible for defining how the errors should be handled by the system.
 *
 * @author Ricardo Cacheira
 */
public class ErrorCtrlModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new ContinueCommand(this), 
			new CurrBreakCommand(this), 
			new CurrBreakRevCommand(this), 
			new DefErrorMsgCommand(this), 
			new DefErrorPolicyCommand(this), 
			new FullBreakCommand(this), 
			new FullBreakRevCommand(this), 
			new NotShowErrorCommand(this), 
			new ShowErrorCommand(this), 
			new ThrowErrorCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"DefErrorPolicy",
			"CurrBreakRev",
			"CurrBreak",
			"FullBreakRev",
			"FullBreak",
			"Continue",
			"DefErrorMsg",
			"ShowError",
			"NotShowError",
			"ThrowError"
		};
	}

	@Override
	public String moduleName() {
		return "Error Control Module";
	}

	@Override
	public String help() {
		return "This module is responsible for defining how the errors should be handled by the system.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new ErrorCtrlModule();
	}

}