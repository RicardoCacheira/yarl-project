package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.error.ContinuePolicy;

/**
 * Name: Continue
 *
 * Module: Error Control Module
 *
 * Description: Continues the execution as if nothing had appended.
 *
 * Return: Continue Policy
 *
 * Return Type: ErrorPolicy
 *
 * ---Notes---
 *
 * e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 "Hi"
 * Test 42 ' and the stack at the moment is [111, "world"], if when running the
 * command 'A 93', the 'Test' command throws an error, then the execution of the
 * command 'A' continues and so the stack will be [42, "Hi", 123, 111, "world"]
 * and then the value 93 will be pushed.
 *
 * In the previous example, if the 'Test' command used the string "Hi" as an
 * attribute, then the stack after the execution of the command 'A' would be
 * [42, 123, 111, "world"] and not [42, "Hi", 123, 111, "world"].
 *
 * ---Notes---
 *
 * Usage: Continue -> ErrorPolicy
 *
 * @author Ricardo Cacheira
 */
public class ContinueCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Continue";

    public ContinueCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ContinueCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new ContinuePolicy(interpreter));
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 \"Hi\" Test 42 ' and the stack at the moment is [111, \"world\"], if when running the command 'A 93', the 'Test' command throws an error, then the execution of the command 'A' continues and so the stack will be [42, \"Hi\", 123, 111, \"world\"] and then the value 93 will be pushed.",
            "In the previous example, if the 'Test' command used the string \"Hi\" as an attribute, then the stack after the execution of the command 'A' would be [42, 123, 111, \"world\"] and not [42, \"Hi\", 123, 111, \"world\"]."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Continues the execution as if nothing had appended.", "Continue Policy", "ErrorPolicy", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ContinueCommand(this.getId(), this.getModule());
    }

}
