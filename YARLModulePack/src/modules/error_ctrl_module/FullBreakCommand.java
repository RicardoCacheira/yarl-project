package modules.error_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.error.FullBreakPolicy;

/**
 * Name: FullBreak
 *
 * Module: Error Control Module
 *
 * Description: Breaks the runnable command
 *
 * Return: FullBreak Policy
 *
 * Return Type: ErrorPolicy
 *
 * ---Note---
 *
 * e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 "Hi"
 * Test 42 ' and the stack at the moment is [111, "world"], if when running the
 * command 'A 93', the 'Test' command throws an error, then the execution of the
 * command 'A' will stop and the stack will be ["Hi", 123, 111, "world"] and
 * then the value 93 will NOT be pushed because the execution was broken.
 *
 * ---Note---
 *
 * Usage: FullBreak -> ErrorPolicy
 *
 * @author Ricardo Cacheira
 */
public class FullBreakCommand extends ModuleCommand {

    public static String COMMAND_NAME = "FullBreak";

    public FullBreakCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public FullBreakCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new FullBreakPolicy(interpreter));
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "e.g. We have the module command with the ID 'Test', the UDCom ' A : 123 \"Hi\" Test 42 ' and the stack at the moment is [111, \"world\"], if when running the command 'A 93', the 'Test' command throws an error, then the execution of the command 'A' will stop and the stack will be [\"Hi\", 123, 111, \"world\"] and then the value 93 will NOT be pushed because the execution was broken."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Breaks the runnable command", "FullBreak Policy", "ErrorPolicy", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new FullBreakCommand(this.getId(), this.getModule());
    }

}
