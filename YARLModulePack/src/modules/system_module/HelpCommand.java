package modules.system_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Help
 *
 * Module: System Module
 *
 * Description: Prints to the shell the info regarding the given IDCom/Variable.
 *
 * ---Attribute---
 *
 * Name: IDCom/Variable
 * Description: The IDCom/Variable of which the user desires information about
 * Type: String
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * If the given string is a Module Command, it will be printed all the info
 * regarding the command(Name, Description, Return, Return Type and Attributes).
 *
 * If the given string is a User Defined Command, it will show the full command.
 *
 * If the given string is a Variable, it will show the current value.
 *
 * If its none of the above it will thrown an warning.
 *
 * ---Notes---
 *
 * Usage: < IDCom/Variable > Help
 *
 * @author Ricardo Cacheira
 */
public class HelpCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Help";

    public HelpCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public HelpCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    String str = (String) parameteres[0];
                    if (interpreter.hasCommand(str)) {
                        interpreter.showOutput(interpreter.getCommand(str).getInfo(), YARLI.PLAIN_OUTPUT);
                    } else if (interpreter.hasVariable(str)) {
                        interpreter.showOutput(String.format("Id: %s, Value: %s\n", str, interpreter.generateObjString(interpreter.getVariableValue(str))), YARLI.PLAIN_OUTPUT);
                    } else {
                        interpreter.throwWarning("No command/variable found with the given id.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("IDCom/Variable", "The IDCom/Variable of which the user desires information about", "String")
        };
        String[] noteList = {
            "If the given string is a Module Command, it will be printed all the info regarding the command(Name, Description, Return, Return Type and Attributes).",
            "If the given string is a User Defined Command, it will show the full command.",
            "If the given string is a Variable, it will show the current value.",
            "If its none of the above it will thrown an warning."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Prints to the shell the info regarding the given IDCom/Variable.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new HelpCommand(this.getId(), this.getModule());
    }

}
