package modules.system_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module has commands that interact directly with the interpreter.
 *
 * @author Ricardo Cacheira
 */
public class SystemModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new ExitCommand(this), 
			new HelpCommand(this), 
			new LoadCommand(this), 
			new NewInstCommand(this), 
			new ResetCommand(this), 
			new RunCommand(this), 
			new RunRCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"Run",
			"NewInst",
			"RunR",
			"Load",
			"Exit",
			"Help",
			"Reset"
		};
	}

	@Override
	public String moduleName() {
		return "System Module";
	}

	@Override
	public String help() {
		return "This module has commands that interact directly with the interpreter.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new SystemModule();
	}

}