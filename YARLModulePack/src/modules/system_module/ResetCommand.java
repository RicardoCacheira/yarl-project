package modules.system_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Reset
 *
 * Module: System Module
 *
 * Description: Resets the current instance by loading the default values.
 *
 * Usage: Reset
 *
 * @author Ricardo Cacheira
 */
public class ResetCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Reset";

    public ResetCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ResetCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.loadDefaultValues();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Resets the current instance by loading the default values.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ResetCommand(this.getId(), this.getModule());
    }

}
