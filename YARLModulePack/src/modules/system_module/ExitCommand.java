package modules.system_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Exit
 *
 * Module: System Module
 *
 * Description: Exits the current instance.
 *
 * ---Note---
 *
 * In cases where the current instance was created by another instance only the
 * current instace will stop.
 *
 * ---Note---
 *
 * Usage: Exit
 *
 * @author Ricardo Cacheira
 */
public class ExitCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Exit";

    public ExitCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ExitCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.stop();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "In cases where the current instance was created by another instance only the current instace will stop."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Exits the current instance.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ExitCommand(this.getId(), this.getModule());
    }

}
