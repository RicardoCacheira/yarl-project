package modules.system_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: NewInst
 *
 * Module: System Module
 *
 * Description: Creates a new instance of the interpreter.
 *
 * ---Notes---
 *
 * The new instance is loaded with the default values from the main one.
 *
 * If the current interpreter is unable to run both instances concurrently(at
 * the same time) then the the main instance(the one that called the NewInst
 * command) will interrupt until the new one finishes(the Exit command is
 * called).
 *
 * ---Notes---
 *
 * Usage: NewInst
 *
 * @author Ricardo Cacheira
 */
public class NewInstCommand extends ModuleCommand {

    public static String COMMAND_NAME = "NewInst";

    public NewInstCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public NewInstCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.startNewInstance();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "The new instance is loaded with the default values from the main one.",
            "If the current interpreter is unable to run both instances concurrently(at the same time) then the the main instance(the one that called the NewInst command) will interrupt until the new one finishes(the Exit command is called)."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Creates a new instance of the interpreter.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new NewInstCommand(this.getId(), this.getModule());
    }

}
