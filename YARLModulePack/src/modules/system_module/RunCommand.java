package modules.system_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Run
 *
 * Module: System Module
 *
 * Description: This command creates a new instance of the interpreter in which
 * the given file will be executed.
 *
 * ---Attribute---
 *
 * Name: FilePath
 * Description: The path of the file to be executed by the new instance
 * Type: String
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * If the interpreter is unable to load the file an error will be thrown.
 *
 * The new instance is loaded with the default values from the main one.
 *
 * If the current interpreter is unable to run both instances concurrently(at
 * the same time) then the the main instance(the one that called the Run
 * command) will interrupt until the new one finishes executing the file.
 *
 * ---Notes---
 *
 * Usage: < FilePath > Run
 *
 * @author Ricardo Cacheira
 */
public class RunCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Run";

    public RunCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public RunCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.runFile((String) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("FilePath", "The path of the file to be executed by the new instance", "String")
        };
        String[] noteList = {
            "If the interpreter is unable to load the file an error will be thrown.",
            "The new instance is loaded with the default values from the main one.",
            "If the current interpreter is unable to run both instances concurrently(at the same time) then the the main instance(the one that called the Run command) will interrupt until the new one finishes executing the file."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command creates a new instance of the interpreter in which the given file will be executed.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new RunCommand(this.getId(), this.getModule());
    }

}
