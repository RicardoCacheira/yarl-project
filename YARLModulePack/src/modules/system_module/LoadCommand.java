package modules.system_module;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Load
 *
 * Module: System Module
 *
 * Description: This command loads the given file into the interpreter.
 *
 * ---Attribute---
 *
 * Name: FilePath
 * Description: The path of the file to be loaded into the interpreter
 * Type: String
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * If the interpreter is unable to load the file an error will be thrown.
 *
 * In this context "Loading" means interpreting all lines in the file in order.
 *
 * ---Notes---
 *
 * Usage: < FilePath > Load
 *
 * @author Ricardo Cacheira
 */
public class LoadCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Load";

    public LoadCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public LoadCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    ArrayList<String> commandList = new ArrayList<>();
                    try {
                        Scanner in = new Scanner(new File((String) parameteres[0]));
                        while (in.hasNextLine()) {
                            commandList.add(in.nextLine());
                        }
                        in.close();
                        interpreter.addPriorityCommandListToBuffer(commandList.toArray(new String[commandList.size()]));
                    } catch (FileNotFoundException ex) {
                        interpreter.throwError("File not found.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("FilePath", "The path of the file to be loaded into the interpreter", "String")
        };
        String[] noteList = {
            "If the interpreter is unable to load the file an error will be thrown.",
            "In this context \"Loading\" means interpreting all lines in the file in order."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command loads the given file into the interpreter.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new LoadCommand(this.getId(), this.getModule());
    }

}
