package modules.command_flow_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is responsible for the command that control the flow of a runnable command
 *
 * @author Ricardo Cacheira
 */
public class CommandFlowModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new CBreakCommand(this), 
			new CBreakTCommand(this), 
			new ComTypeCommand(this), 
			new DoCommand(this), 
			new FBreakCommand(this), 
			new FBreakTCommand(this), 
			new ForCommand(this), 
			new ForEachCommand(this), 
			new IfCommand(this), 
			new IfElseCommand(this), 
			new IfElseIfCommand(this), 
			new IsComCommand(this), 
			new ParseComCommand(this), 
			new RepeatCommand(this), 
			new RunComCommand(this), 
			new RunParsedComCommand(this), 
			new SwitchCommand(this), 
			new WhileCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"CBreak",
			"CBreakT",
			"FBreak",
			"FBreakT",
			"Repeat",
			"If",
			"IfElse",
			"IfElseIf",
			"Switch",
			"For",
			"ForEach",
			"Do",
			"While",
			"IsCom",
			"ParseCom",
			"ComType",
			"RunCom",
			"RunParsedCom"
		};
	}

	@Override
	public String moduleName() {
		return "Command Flow Module";
	}

	@Override
	public String help() {
		return "This module is responsible for the command that control the flow of a runnable command";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new CommandFlowModule();
	}

}