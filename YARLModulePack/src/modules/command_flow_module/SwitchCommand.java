package modules.command_flow_module;

import parser.ParsedCommand;
import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Switch
 *
 * Module: Command Flow Module
 *
 * Description: Runs the first CaseStatement whoose CaseValue equal the given
 * SwitchValue, if not, runs the DefaultStatement, if defined.
 *
 * ---Attributes---
 *
 * Name: SwitchValue
 * Description: The value that will be compared to the given options.
 * Type: Object
 *
 * Name: CaseValue
 * Description: The value to check if it's equal to the given SwitchValue.
 * Type: Object
 *
 * Name: CaseStatement
 * Description: The command that is executed if the correspondent CaseValue
 * equals the SwitchValue
 * Type: ParsedCommand
 *
 * Name: DefaultStatement
 * Description: The command that is executed if none of the CaseValues equals
 * the SwitchValue
 * Type: ParsedCommand
 *
 * Name: NumberOfCases
 * Description: The Number of CaseValue and CaseStatement pairs that the command
 * has
 * Type: Number
 *
 * Name: HasDefaultStatement
 * Description: Defines if there is an DefaultStatement in case none of the
 * CaseValues equals the SwitchValue
 * Type: Boolean
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * If the number of CaseValue and CaseStatement pairs dont match the
 * NumberOfCases an error will be thrown.
 *
 * If none of the CaseValues equals the SwitchValue and an DefaultStatement is
 * defined then the DefaultStatement is executed.
 *
 * e.g. The following command '12 5 ("hi") 12 ("hello") 2 false Switch' pushes
 * the value "hello" to the stack
 *
 * e.g. The following command '12 5 ("hi") 7 ("hello") ("alt") 2 true Switch'
 * pushes the value "alt" to the stack
 *
 * ---Notes---
 *
 * Usage: < SwitchValue > < CaseValue > < CaseStatement > < DefaultStatement >
 * < NumberOfCases > < HasDefaultStatement > Switch
 *
 * @author Ricardo Cacheira
 */
public class SwitchCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Switch";

    public SwitchCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SwitchCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);//Gets the number of Cases and if has an alternative
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof Boolean)) {
                    int numberOfCases = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                    if (numberOfCases != -1) {
                        boolean hasDefaultStatement = (Boolean) parameteres[1];
                        int parameterNum = numberOfCases * 2 + 1;
                        if (hasDefaultStatement) {
                            parameterNum++;
                        }
                        parameteres = interpreter.getParameters(parameterNum, COMMAND_NAME);
                        if (parameteres != null) {
                            boolean validParameters = true;
                            for (int i = 0; i < numberOfCases; i++) {
                                if (!(parameteres[i * 2 + 2] instanceof ParsedCommand)) {
                                    interpreter.throwError("Invalid type of parameters.");
                                    validParameters = false;
                                    break;
                                }
                            }
                            if (hasDefaultStatement) {
                                if (!(parameteres[parameteres.length - 1] instanceof ParsedCommand)) {
                                    interpreter.throwError("Invalid type of parameters.");
                                    validParameters = false;
                                }
                            }
                            Object value = parameteres[0];
                            if (validParameters) {
                                boolean foundCase = false;
                                for (int i = 0; i < numberOfCases; i++) {
                                    Object caseValue = parameteres[i * 2 + 1];
                                    ParsedCommand caseStatement = (ParsedCommand) parameteres[i * 2 + 2];
                                    if (value.equals(caseValue)) {
                                        interpreter.runParsedCommand(caseStatement);
                                        foundCase = true;
                                        break;
                                    }
                                }
                                if (!foundCase && hasDefaultStatement) {
                                    interpreter.runParsedCommand((ParsedCommand) parameteres[parameteres.length - 1]);
                                }
                            }
                        }
                    } else {
                        interpreter.throwError("Received Number is not a valid Lenght.");
                    }

                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("SwitchValue", "The value that will be compared to the given options.", "Object"),
            new CommandInfoAttribute("CaseValue", "The value to check if it's equal to the given SwitchValue.", "Object"),
            new CommandInfoAttribute("CaseStatement", "The command that is executed if the correspondent CaseValue equals the SwitchValue", "ParsedCommand"),
            new CommandInfoAttribute("DefaultStatement", "The command that is executed if none of the CaseValues equals the SwitchValue", "ParsedCommand"),
            new CommandInfoAttribute("NumberOfCases", "The Number of CaseValue and CaseStatement pairs that the command has", "Number"),
            new CommandInfoAttribute("HasDefaultStatement", "Defines if there is an DefaultStatement in case none of the CaseValues equals the SwitchValue", "Boolean")
        };
        String[] noteList = {
            "If the number of CaseValue and CaseStatement pairs dont match the NumberOfCases an error will be thrown.",
            "If none of the CaseValues equals the SwitchValue and an DefaultStatement is defined then the DefaultStatement is executed.",
            "e.g. The following command '12 5 (\"hi\") 12 (\"hello\") 2 false Switch' pushes the value \"hello\" to the stack",
            "e.g. The following command '12 5 (\"hi\") 7 (\"hello\") (\"alt\") 2 true Switch' pushes the value \"alt\" to the stack"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Runs the first CaseStatement whoose CaseValue equal the given SwitchValue, if not, runs the DefaultStatement, if defined.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SwitchCommand(this.getId(), this.getModule());
    }

}
