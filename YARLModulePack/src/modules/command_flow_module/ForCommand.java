package modules.command_flow_module;

import parser.ParsedCommand;
import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.variable.NoVariableFoundException;

/**
 * Name: For
 *
 * Module: Command Flow Module
 *
 * Description: Runs the statment until the couter reaches or supasses the end
 * value , and increments/decremented the value at each iteration.
 *
 * ---Attributes---
 *
 * Name: CounterId
 * Description: The Id of the variable to use as the counter.
 * Type: String
 *
 * Name: StartValue
 * Description: The start value of the counter.
 * Type: Number
 *
 * Name: EndValue
 * Description: The end value of the counter.
 * Type: Number
 *
 * Name: Step
 * Description: The vaule that the counter is incremented/decremented.
 * Type: Number
 *
 * Name: Statement
 * Description: The command that is executed if the counter remains in the
 * limits
 * Type: ParsedCommand
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The statement command must be a valid runnable command(with/without var).
 *
 * If the variable doesn't exist, it will be created.
 *
 * If the id already exists and it's a variable it will be overwritten, if it's
 * a command an error will be thrown.
 *
 * If the execution of the statement command is broken, the cycle will stop and
 * a warning will be thrown.
 *
 * If the end value is greater than the start value the Step must be positive.
 *
 * If the end value is less than the start value the Step must be negative.
 *
 * The Step value must be non-zero number, else an error will be thrown.
 *
 * The Start, End and Step values can be non-integer number, they only have to
 * obey to the rules above.
 *
 * e.g. The command ' "i" 1/3 6/3 2/3 ( i Out Ln ) For ', prints "1/3" then "1"
 * then "5/3".
 *
 * ---Notes---
 *
 * Usage: < CounterId > < StartValue > < EndValue > < Step > < Statement > For
 *
 * @author Ricardo Cacheira
 */
public class ForCommand extends ModuleCommand {

    public static String COMMAND_NAME = "For";

    public ForCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ForCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(5, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String) && (parameteres[1] instanceof RationalNumber) && (parameteres[2] instanceof RationalNumber) && (parameteres[3] instanceof RationalNumber) && (parameteres[4] instanceof ParsedCommand)) {
                    ParsedCommand statement = (ParsedCommand) parameteres[4];
                    if (statement.type() == ParsedCommand.ParsedCommandType.RUNNABLE || statement.type() == ParsedCommand.ParsedCommandType.RUNNABLE_WITH_VAR) {
                        RationalNumber start = (RationalNumber) parameteres[1];
                        RationalNumber end = (RationalNumber) parameteres[2];
                        RationalNumber step = (RationalNumber) parameteres[3];
                        if (step.compareTo(RationalNumber.ZERO) != 0) {
                            String id = (String) parameteres[0];
                            if (interpreter.isValidId(id)) {
                                if (!interpreter.hasCommand(id)) {
                                    if (interpreter.hasVariable(id)) {
                                        interpreter.removeVariable(id);
                                    }
                                    interpreter.defineVariable(id, start);
                                    RationalNumber aux = start;
                                    if (start.compareTo(end) == 0) {
                                        //Do nothing.
                                    } else if (start.compareTo(end) < 0 && !step.isNegative()) {
                                        while (aux.compareTo(end) < 0) {
                                            if (interpreter.runParsedCommand(statement)) {
                                                try {
                                                    Object value = interpreter.getVariableValue(id);
                                                    if (value instanceof RationalNumber) {
                                                        aux = ((RationalNumber) value).add(step);
                                                        interpreter.setVariableValue(id, aux);
                                                    } else {
                                                        interpreter.throwError("The current value of the counter was not a Number.");
                                                        break;
                                                    }
                                                } catch (NoVariableFoundException ex) {
                                                    interpreter.throwError("Unable to get the current value of the counter.");
                                                    break;
                                                }
                                            } else {
                                                interpreter.throwWarning("The For cycle was interrupted due to a break.");
                                                break;
                                            }
                                        }
                                    } else if (start.compareTo(end) > 0 && step.isNegative()) {
                                        while (aux.compareTo(end) > 0) {
                                            if (interpreter.runParsedCommand(statement)) {
                                                try {
                                                    Object value = interpreter.getVariableValue(id);
                                                    if (value instanceof RationalNumber) {
                                                        aux = ((RationalNumber) value).add(step);
                                                    } else {
                                                        interpreter.throwError("The current value of the counter was not a Number.");
                                                        break;
                                                    }
                                                } catch (NoVariableFoundException ex) {
                                                    interpreter.throwError("Unable to get the current value of the counter.");
                                                    break;
                                                }
                                            } else {
                                                interpreter.throwWarning("The For cycle was interrupted due to a break.");
                                                break;
                                            }
                                        }
                                    } else {
                                        interpreter.throwError("Invalid combination of Start, End and Step.");
                                    }
                                } else {
                                    interpreter.throwError("The given id is already a defined command.");
                                }
                            } else {
                                interpreter.throwError("The given id is not a valid id.");
                            }

                        } else {
                            interpreter.throwError("The step cannot be zero.");
                        }
                    } else {
                        interpreter.throwError("The statement must be of the type Runnable/Runnable With Var.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("CounterId", "The Id of the variable to use as the counter.", "String"),
            new CommandInfoAttribute("StartValue", "The start value of the counter.", "Number"),
            new CommandInfoAttribute("EndValue", "The end value of the counter.", "Number"),
            new CommandInfoAttribute("Step", "The vaule that the counter is incremented/decremented.", "Number"),
            new CommandInfoAttribute("Statement", "The command that is executed if the counter remains in the limits", "ParsedCommand")
        };
        String[] noteList = {
            "The statement command must be a valid runnable command(with/without var).",
            "If the variable doesn't exist, it will be created.",
            "If the id already exists and it's a variable it will be overwritten, if it's a command an error will be thrown.",
            "If the execution of the statement command is broken, the cycle will stop and a warning will be thrown.",
            "If the end value is greater than the start value the Step must be positive.",
            "If the end value is less than the start value the Step must be negative.",
            "The Step value must be non-zero number, else an error will be thrown.",
            "The Start, End and Step values can be non-integer number, they only have to obey to the rules above.",
            "e.g. The command ' \"i\" 1/3 6/3 2/3 ( i Out Ln ) For ', prints \"1/3\" then \"1\" then \"5/3\"."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Runs the statment until the couter reaches or supasses the end value , and increments/decremented the value at each iteration.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ForCommand(this.getId(), this.getModule());
    }

}
