package modules.command_flow_module;

import parser.ParsedCommand;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.variable.Array;
import yarli.variable.Matrix;
import yarli.variable.Vector;

/**
 * Name: ForEach
 *
 * Module: Command Flow Module
 *
 * Description: Runs the statment for all the values in the given array/vector.
 *
 * ---Attributes---
 *
 * Name: ValId
 * Description: The Id of the variable that will contain the current value of
 * the array/vector.
 * Type: String
 *
 * Name: ArrayToIterate
 * Description: The Array/Vector/Matrix that contains the values that we desire
 * to iterate.
 * Type: Array/Vector/Matrix
 *
 * Name: Statement
 * Description: The command that is executed at each iteration of the cycle
 * Type: ParsedCommand
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The statement command must be a valid runnable command(with/without var).
 *
 * If the variable doesn't exist, it will be created.
 *
 * If the id already exists and it's a variable it will be overwritten, if it's
 * a command an error will be thrown.
 *
 * If the execution of the statement command is broken, the cycle will stop and
 * a warning will be thrown.
 *
 * ---Notes---
 *
 * Usage: < ValId > < ArrayToIterate > < Statement > ForEach
 *
 * @author Ricardo Cacheira
 */
public class ForEachCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ForEach";

    public ForEachCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ForEachCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    private void iterateList(Object[] list, YARLI interpreter, String id, ParsedCommand statement) {
        for (int i = 0; i < list.length; i++) {
            if (!interpreter.setVariableValue(id, list[i])) {
                interpreter.throwError("Unable to set a new value to the variable.");
                break;
            }
            if (!interpreter.runParsedCommand(statement)) {
                interpreter.throwWarning("The ForEach cycle was interrupted due to a break.");
                break;
            }
        }
    }

    private void iterateMatrix(Object[][] matrix, YARLI interpreter, String id, ParsedCommand statement) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (!interpreter.setVariableValue(id, matrix[i][j])) {
                    interpreter.throwError("Unable to set a new value to the variable.");
                    break;
                }
                if (!interpreter.runParsedCommand(statement)) {
                    interpreter.throwWarning("The ForEach cycle was interrupted due to a break.");
                    break;
                }
            }
        }
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(3, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String) && (parameteres[2] instanceof ParsedCommand)) {
                    String id = (String) parameteres[0];
                    ParsedCommand statement = (ParsedCommand) parameteres[2];
                    if (statement.type() == ParsedCommand.ParsedCommandType.RUNNABLE || statement.type() == ParsedCommand.ParsedCommandType.RUNNABLE_WITH_VAR) {
                        if (interpreter.isValidId(id)) {
                            if (!interpreter.hasCommand(id)) {
                                if (interpreter.hasVariable(id)) {
                                    interpreter.removeVariable(id);
                                }
                                interpreter.newVariable(id);
                                if (parameteres[1] instanceof Vector) {
                                    Vector vec = (Vector) parameteres[1];
                                    this.iterateList(vec.getCopy(), interpreter, id, statement);
                                } else if (parameteres[1] instanceof Array) {
                                    Array array = (Array) parameteres[1];
                                    this.iterateList(array.getCopy(), interpreter, id, statement);
                                } else if (parameteres[1] instanceof Matrix) {
                                    Matrix matrix = (Matrix) parameteres[1];
                                    this.iterateMatrix(matrix.getCopy(), interpreter, id, statement);
                                } else {
                                    interpreter.throwError("Invalid type of parameters.");
                                }
                            } else {
                                interpreter.throwError("The given id is already a defined command.");
                            }
                        } else {
                            interpreter.throwError("The given id is not a valid id.");
                        }
                    } else {
                        interpreter.throwError("The statement must be of the type Runnable/Runnable With Var.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("ValId", "The Id of the variable that will contain the current value of the array/vector.", "String"),
            new CommandInfoAttribute("ArrayToIterate", "The Array/Vector/Matrix that contains the values that we desire to iterate.", "Array/Vector/Matrix"),
            new CommandInfoAttribute("Statement", "The command that is executed at each iteration of the cycle", "ParsedCommand")
        };
        String[] noteList = {
            "The statement command must be a valid runnable command(with/without var).",
            "If the variable doesn't exist, it will be created.",
            "If the id already exists and it's a variable it will be overwritten, if it's a command an error will be thrown.",
            "If the execution of the statement command is broken, the cycle will stop and a warning will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Runs the statment for all the values in the given array/vector.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ForEachCommand(this.getId(), this.getModule());
    }

}
