package modules.command_flow_module;

import parser.ParsedCommand;
import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Repeat
 *
 * Module: Command Flow Module
 *
 * Description: Runs the given command the desired amount of times.
 *
 * ---Attributes---
 *
 * Name: Command to repeat
 * Description: The command that will be runned the desired amout of times
 * Type: ParsedCommand
 *
 * Name: NumRepeats
 * Description: The amout of times the desired command will be runned
 * Type: Number
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The Command must be a runnable(with var) command else an error will be
 * thrown.
 *
 * The Repeat will be interrupted if it breaks its execution.
 *
 * ---Notes---
 *
 * Usage: < Command to repeat > < NumRepeats > Repeat
 *
 * @author Ricardo Cacheira
 */
public class RepeatCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Repeat";

    public RepeatCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public RepeatCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof ParsedCommand) && (parameteres[1] instanceof RationalNumber)) {
                    ParsedCommand command = (ParsedCommand) parameteres[0];
                    if (command.type() == ParsedCommand.ParsedCommandType.RUNNABLE || command.type() == ParsedCommand.ParsedCommandType.RUNNABLE_WITH_VAR) {
                        RationalNumber count = (RationalNumber) parameteres[1];
                        if (count.isAnInteger() && count.compareTo(RationalNumber.ZERO) > 0) {
                            while (count.compareTo(RationalNumber.ZERO) != 0) {
                                if (!interpreter.runParsedCommand(command)) {
                                    interpreter.throwWarning("The Repeat Command was interrupted due to a break.");
                                    break;
                                }
                                count = count.subtract(RationalNumber.ONE);
                            }
                        } else {
                            interpreter.throwError("Invalid number of repeats.");
                        }
                    } else {
                        interpreter.throwError("The received command must be of the type Runnable/Runnable With Var.");
                    }

                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("Command to repeat", "The command that will be runned the desired amout of times", "ParsedCommand"),
            new CommandInfoAttribute("NumRepeats", "The amout of times the desired command will be runned", "Number")
        };
        String[] noteList = {
            "The Command must be a runnable(with var) command else an error will be thrown.",
            "The Repeat will be interrupted if it breaks its execution."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Runs the given command the desired amount of times.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new RepeatCommand(this.getId(), this.getModule());
    }

}
