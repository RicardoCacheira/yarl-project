package modules.command_flow_module;

import parser.ParsedCommand;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ComType
 *
 * Module: Command Flow Module
 *
 * Description: Returns the type of the given parsed command.
 *
 * Return: The type of the given parsed command
 *
 * Return Type: String
 *
 * ---Attribute---
 *
 * Name: command
 * Description: The command whoose type will be returned.
 * Type: ParsedCommand
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * This command only returns 4 possible strings, "Runnable", "RunnableVar",
 * "Definition" and "DefinitionVar".
 *
 * ---Note---
 *
 * Usage: < command > ComType -> String
 *
 * @author Ricardo Cacheira
 */
public class ComTypeCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ComType";

    public ComTypeCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ComTypeCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof ParsedCommand) {
                    switch (((ParsedCommand) parameteres[0]).type()) {
                        case RUNNABLE:
                            interpreter.push("Runnable");
                            break;
                        case RUNNABLE_WITH_VAR:
                            interpreter.push("RunnableVar");
                            break;
                        case DEFINITION:
                            interpreter.push("Definition");
                            break;
                        case DEFINITION_WITH_VAR:
                            interpreter.push("DefinitionVar");
                            break;
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("command", "The command whoose type will be returned.", "ParsedCommand")
        };
        String[] noteList = {
            "This command only returns 4 possible strings, \"Runnable\", \"RunnableVar\", \"Definition\" and \"DefinitionVar\"."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns the type of the given parsed command.", "The type of the given parsed command", "String", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ComTypeCommand(this.getId(), this.getModule());
    }

}
