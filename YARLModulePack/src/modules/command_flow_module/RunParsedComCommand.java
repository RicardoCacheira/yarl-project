package modules.command_flow_module;

import parser.ParsedCommand;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: RunParsedCom
 *
 * Module: Command Flow Module
 *
 * Description: Runs the given parsed command.
 *
 * ---Attribute---
 *
 * Name: Command to run
 * Description: The command that will be run
 * Type: ParsedCommand
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * '(aCommand) RunParsedCom' is the same as using ' (aCommand) 1 Repeat'.
 *
 * ---Note---
 *
 * Usage: < Command to run > RunParsedCom
 *
 * @author Ricardo Cacheira
 */
public class RunParsedComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "RunParsedCom";

    public RunParsedComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public RunParsedComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof ParsedCommand) {
                    interpreter.runParsedCommand((ParsedCommand) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("Command to run", "The command that will be run", "ParsedCommand")
        };
        String[] noteList = {
            "'(aCommand) RunParsedCom' is the same as using ' (aCommand) 1 Repeat'."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Runs the given parsed command.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new RunParsedComCommand(this.getId(), this.getModule());
    }

}
