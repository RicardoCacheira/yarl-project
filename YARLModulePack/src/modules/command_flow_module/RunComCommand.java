package modules.command_flow_module;

import parser.YARLLexer;
import parser.YARLParser;
import parser.YARLYParserException;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: RunCom
 *
 * Module: Command Flow Module
 *
 * Description: Runs the correspondent command of the given string.
 *
 * ---Attribute---
 *
 * Name: CommandString
 * Description: The string representing the command that will be runned
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * '"aCommand" RunCom' is the same as using '(aCommand) RunParsedCom'.
 *
 * ---Note---
 *
 * Usage: < CommandString > RunCom
 *
 * @author Ricardo Cacheira
 */
public class RunComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "RunCom";

    public RunComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public RunComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    String str = (String) parameteres[0];
                    YARLParser parser = new YARLParser();
                    try {
                        interpreter.runParsedCommand(parser.parseCommand(new YARLLexer(), str));
                    } catch (YARLYParserException ex) {
                        interpreter.throwError("Invalid Command:\n" + ex.getMessage());
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("CommandString", "The string representing the command that will be runned", "String")
        };
        String[] noteList = {
            "'\"aCommand\" RunCom' is the same as using '(aCommand) RunParsedCom'."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Runs the correspondent command of the given string.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new RunComCommand(this.getId(), this.getModule());
    }

}
