package modules.command_flow_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: CBreak
 *
 * Module: Command Flow Module
 *
 * Description: Breaks the execution of the command that executes 'CBreak'.
 *
 * ---Notes---
 *
 * This command only breaks the command that explicitly executes 'CBreak', it
 * doesn't break a command that executes the command that explicitly executes
 * 'CBreak'.
 *
 * e.g. having defined the following command ' A : 123 CBreak "Test" ', if I now
 * insert the command ' 100 A "Hello" ', then the result on the stack will be
 * the following, ["Hello", 123, 100].
 *
 * ---Notes---
 *
 * Usage: CBreak
 *
 * @author Ricardo Cacheira
 */
public class CBreakCommand extends ModuleCommand {

    public static String COMMAND_NAME = "CBreak";

    public CBreakCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public CBreakCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.breakCurrentCommand();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "This command only breaks the command that explicitly executes 'CBreak', it doesn't break a command that executes the command that explicitly executes 'CBreak'.",
            "e.g. having defined the following command ' A : 123 CBreak \"Test\" ', if I now insert the command ' 100 A \"Hello\" ', then the result on the stack will be the following, [\"Hello\", 123, 100]."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Breaks the execution of the command that executes 'CBreak'.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new CBreakCommand(this.getId(), this.getModule());
    }

}
