package modules.command_flow_module;

import parser.ParsedCommand;
import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: IfElseIf
 *
 * Module: Command Flow Module
 *
 * Description: Runs the statment if after running the condition, the value on
 * the top of the stack is True, if not, then the next condition is executed
 * until there is no more conditions or one returns true.
 *
 * ---Attributes---
 *
 * Name: Condition
 * Description: The command to verify if the statement will be executed.
 * Type: ParsedCommand
 *
 * Name: Statement
 * Description: The command that is executed if the condition returns True
 * Type: ParsedCommand
 *
 * Name: Alternative
 * Description: The command that is executed if all the conditions returns False
 * Type: ParsedCommand
 *
 * Name: NumberOfIfs
 * Description: The Number of conditions and statements that the command has
 * Type: Number
 *
 * Name: HasAlternative
 * Description: Defines if there is an alternative in case all of the conditions
 * return false
 * Type: Boolean
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The condition command must be a valid runnable command(with/without var).
 *
 * If the execution of the condition command is broken, an error will be thrown.
 *
 * If after the condition command is run the value on the top of the stack isn't
 * a boolean, an error will be thrown.
 *
 * If the condition command is a runnable command with var, then, the value is
 * first put on the variable and then checked if is true.
 *
 * If the number of conditions and Statements dont match the NumberOfIfs an
 * error will be thrown.
 *
 * If the condition returns false, then the next condition is executed until
 * there is no more conditions or one returns true.
 *
 * If none of the conditions returns true and an Alternative is defined then the
 * Alternative is executed.
 *
 * e.g. The following command '(False) ("hi") (True) ("hello") 2 false IfElseIf'
 * pushes the value "hello" to the stack
 *
 * e.g. The following command '(False) ("hi") (False) ("hello") ("alt") 2 true
 * IfElseIf' pushes the value "alt" to the stack
 *
 * ---Notes---
 *
 * Usage: < Condition > < Statement > < Alternative > < NumberOfIfs >
 * < HasAlternative > IfElseIf
 *
 * @author Ricardo Cacheira
 */
public class IfElseIfCommand extends ModuleCommand {

    public static String COMMAND_NAME = "IfElseIf";

    public IfElseIfCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public IfElseIfCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);//Gets the number of Ifs and if has an alternative
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof Boolean)) {
                    int numberOfIfs = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                    if (numberOfIfs != -1) {
                        boolean hasAlternative = (Boolean) parameteres[1];
                        int parameterNum = numberOfIfs * 2;
                        if (hasAlternative) {
                            parameterNum++;
                        }
                        parameteres = interpreter.getParameters(parameterNum, COMMAND_NAME);
                        if (parameteres != null) {
                            boolean validParameters = true;
                            for (int i = 0; i < numberOfIfs; i++) {
                                if ((parameteres[i * 2] instanceof ParsedCommand) && (parameteres[i * 2 + 1] instanceof ParsedCommand)) {
                                    ParsedCommand condition = (ParsedCommand) parameteres[i * 2];
                                    if (condition.type() != ParsedCommand.ParsedCommandType.RUNNABLE && condition.type() != ParsedCommand.ParsedCommandType.RUNNABLE_WITH_VAR) {
                                        interpreter.throwError(String.format("The condition number %d must be of the type Runnable/Runnable With Var.", i + 1));
                                    }
                                } else {
                                    interpreter.throwError("Invalid type of parameters.");
                                    validParameters = false;
                                    break;
                                }
                            }
                            if (hasAlternative) {
                                if (!(parameteres[parameteres.length - 1] instanceof ParsedCommand)) {
                                    interpreter.throwError("Invalid type of parameters.");
                                    validParameters = false;
                                }
                            }
                            if (validParameters) {
                                boolean foundCondition = false;
                                for (int i = 0; i < numberOfIfs; i++) {
                                    ParsedCommand condition = (ParsedCommand) parameteres[i * 2];
                                    ParsedCommand statement = (ParsedCommand) parameteres[i * 2 + 1];
                                    if (interpreter.runParsedCommand(condition)) {
                                        Object[] conditionResult = interpreter.getParameters(1, COMMAND_NAME);
                                        if (conditionResult != null) {
                                            if (conditionResult[0] instanceof Boolean) {
                                                if ((Boolean) conditionResult[0]) {
                                                    interpreter.runParsedCommand(statement);
                                                    foundCondition = true;
                                                    break;
                                                }
                                            } else {
                                                interpreter.throwError(String.format("The condition command number %d returned a non boolean value.", i + 1));
                                            }
                                        } else {
                                            interpreter.throwError(String.format("The condition command number %d found an empty stack.", i + 1));
                                        }
                                    } else {
                                        interpreter.throwError(String.format("The condition command number %d was interrupted due to a break.", i + 1));
                                    }
                                }
                                if (!foundCondition && hasAlternative) {
                                    interpreter.runParsedCommand((ParsedCommand) parameteres[parameteres.length - 1]);
                                }
                            }
                        }
                    } else {
                        interpreter.throwError("Received Number is not a valid Lenght.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("Condition", "The command to verify if the statement will be executed.", "ParsedCommand"),
            new CommandInfoAttribute("Statement", "The command that is executed if the condition returns True", "ParsedCommand"),
            new CommandInfoAttribute("Alternative", "The command that is executed if all the conditions returns False", "ParsedCommand"),
            new CommandInfoAttribute("NumberOfIfs", "The Number of conditions and statements that the command has", "Number"),
            new CommandInfoAttribute("HasAlternative", "Defines if there is an alternative in case all of the conditions return false", "Boolean")
        };
        String[] noteList = {
            "The condition command must be a valid runnable command(with/without var).",
            "If the execution of the condition command is broken, an error will be thrown.",
            "If after the condition command is run the value on the top of the stack isn't a boolean, an error will be thrown.",
            "If the condition command is a runnable command with var, then, the value is first put on the variable and then checked if is true.",
            "If the number of conditions and Statements dont match the NumberOfIfs an error will be thrown.",
            "If the condition returns false, then the next condition is executed until there is no more conditions or one returns true.",
            "If none of the conditions returns true and an Alternative is defined then the Alternative is executed.",
            "e.g. The following command '(False) (\"hi\") (True) (\"hello\") 2 false IfElseIf' pushes the value \"hello\" to the stack",
            "e.g. The following command '(False) (\"hi\") (False) (\"hello\") (\"alt\") 2 true IfElseIf' pushes the value \"alt\" to the stack"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Runs the statment if after running the condition, the value on the top of the stack is True, if not, then the next condition is executed until there is no more conditions or one returns true.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new IfElseIfCommand(this.getId(), this.getModule());
    }

}
