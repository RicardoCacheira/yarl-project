package modules.command_flow_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: FBreak
 *
 * Module: Command Flow Module
 *
 * Description: Breaks the full execution of the interpreter.
 *
 * ---Note---
 *
 * e.g. having defined the following command ' A : 123 FBreak "Test" ', if I now
 * insert the command ' 100 A "Hello" ', then the result on the stack will be
 * the following, [123, 100].
 *
 * ---Note---
 *
 * Usage: FBreak
 *
 * @author Ricardo Cacheira
 */
public class FBreakCommand extends ModuleCommand {

    public static String COMMAND_NAME = "FBreak";

    public FBreakCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public FBreakCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.breakFullCommand();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "e.g. having defined the following command ' A : 123 FBreak \"Test\" ', if I now insert the command ' 100 A \"Hello\" ', then the result on the stack will be the following, [123, 100]."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Breaks the full execution of the interpreter.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new FBreakCommand(this.getId(), this.getModule());
    }

}
