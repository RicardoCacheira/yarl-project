package modules.command_flow_module;

import parser.ParsedCommand;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: While
 *
 * Module: Command Flow Module
 *
 * Description: Runs the condition and if the value on the top of the stack is
 * True, Runs the statment and iterates.
 *
 * ---Attributes---
 *
 * Name: Condition
 * Description: The command to verify if the statement will be executed.
 * Type: ParsedCommand
 *
 * Name: Statement
 * Description: The command that is executed if the condition returns True
 * Type: ParsedCommand
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The condition command must be a valid runnable command(with/without var).
 *
 * The statement command must be a valid runnable command(with/without var).
 *
 * If the execution of the condition command is broken, an error will be thrown.
 *
 * If the execution of the statement command is broken, the cycle will stop and
 * a warning will be thrown.
 *
 * If after the condition command is run the value on the top of the stack isn't
 * a boolean, an error will be thrown.
 *
 * If the condition command is a runnable command with var, then, the value is
 * first put on the variable and then checked if is true.
 *
 * e.g. In the following command '(:A: 123 False True) ("Hi") While ' the
 * statement will not be executed because, first pops the value 'True' and
 * inserts into the variable 'A', then pops the value 'False' and verifies.
 *
 * ---Notes---
 *
 * Usage: < Condition > < Statement > While
 *
 * @author Ricardo Cacheira
 */
public class WhileCommand extends ModuleCommand {

    public static String COMMAND_NAME = "While";

    public WhileCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public WhileCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    private boolean getConditionResult(ParsedCommand condition, YARLI interpreter) {
        if (interpreter.runParsedCommand(condition)) {
            Object[] conditionResult = interpreter.getParameters(1, COMMAND_NAME);
            if (conditionResult != null) {
                if (conditionResult[0] instanceof Boolean) {
                    return (Boolean) conditionResult[0];
                } else {
                    interpreter.throwError("The condition command returned a non boolean value.");
                }
            } else {
                interpreter.throwError("The condition command found an empty stack.");
            }
        } else {
            interpreter.throwError("The condition command was interrupted due to a break.");
        }
        return false;
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof ParsedCommand) && (parameteres[1] instanceof ParsedCommand)) {
                    ParsedCommand condition = (ParsedCommand) parameteres[0];
                    ParsedCommand statement = (ParsedCommand) parameteres[1];
                    if (condition.type() == ParsedCommand.ParsedCommandType.RUNNABLE || condition.type() == ParsedCommand.ParsedCommandType.RUNNABLE_WITH_VAR) {
                        if (statement.type() == ParsedCommand.ParsedCommandType.RUNNABLE || statement.type() == ParsedCommand.ParsedCommandType.RUNNABLE_WITH_VAR) {
                            while (this.getConditionResult(condition, interpreter)) {
                                if (!interpreter.runParsedCommand(statement)) {
                                    interpreter.throwWarning("The while cycle was interrupted due to a break.");
                                    break;
                                }
                            }
                        } else {
                            interpreter.throwError("The statement must be of the type Runnable/Runnable With Var.");
                        }
                    } else {
                        interpreter.throwError("The condition must be of the type Runnable/Runnable With Var.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("Condition", "The command to verify if the statement will be executed.", "ParsedCommand"),
            new CommandInfoAttribute("Statement", "The command that is executed if the condition returns True", "ParsedCommand")
        };
        String[] noteList = {
            "The condition command must be a valid runnable command(with/without var).",
            "The statement command must be a valid runnable command(with/without var).",
            "If the execution of the condition command is broken, an error will be thrown.",
            "If the execution of the statement command is broken, the cycle will stop and a warning will be thrown.",
            "If after the condition command is run the value on the top of the stack isn't a boolean, an error will be thrown.",
            "If the condition command is a runnable command with var, then, the value is first put on the variable and then checked if is true.",
            "e.g. In the following command '(:A: 123 False True) (\"Hi\") While ' the statement will not be executed because, first pops the value 'True' and inserts into the variable 'A', then pops the value 'False' and verifies."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Runs the condition and if the value on the top of the stack is True, Runs the statment and iterates.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new WhileCommand(this.getId(), this.getModule());
    }

}
