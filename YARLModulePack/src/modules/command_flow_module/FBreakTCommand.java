package modules.command_flow_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: FBreakT
 *
 * Module: Command Flow Module
 *
 * Description: Breaks the full execution of the interpreter if the boolean
 * value is true.
 *
 * ---Attribute---
 *
 * Name: Value
 * Description: The boolean value that is used to check if the command execution
 * should be broken
 * Type: Boolean
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * e.g. having defined the following command ' A : 123 True FBreakT "Test" ', if
 * I now insert the command ' 100 A "Hello" ', then the result on the stack will
 * be the following, [123, 100].
 *
 * ---Note---
 *
 * Usage: < Value > FBreakT
 *
 * @author Ricardo Cacheira
 */
public class FBreakTCommand extends ModuleCommand {

    public static String COMMAND_NAME = "FBreakT";

    public FBreakTCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public FBreakTCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof Boolean) {
                    if ((Boolean) parameteres[0]) {
                        interpreter.breakFullCommand();
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("Value", "The boolean value that is used to check if the command execution should be broken", "Boolean")
        };
        String[] noteList = {
            "e.g. having defined the following command ' A : 123 True FBreakT \"Test\" ', if I now insert the command ' 100 A \"Hello\" ', then the result on the stack will be the following, [123, 100]."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Breaks the full execution of the interpreter if the boolean value is true.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new FBreakTCommand(this.getId(), this.getModule());
    }

}
