package modules.command_flow_module;

import parser.YARLLexer;
import parser.YARLParser;
import parser.YARLYParserException;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: IsCom
 *
 * Module: Command Flow Module
 *
 * Description: Returns if the string is a valid command or not.
 *
 * Return: The boolean value representing if the string is a valid command or
 * not.
 *
 * Return Type: Boolean
 *
 * ---Attribute---
 *
 * Name: String to verify
 * Description: The string that will be verified
 * Type: String
 *
 * ---Attribute---
 *
 * Usage: < String to verify > IsCom -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class IsComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "IsCom";

    public IsComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public IsComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    String str = (String) parameteres[0];
                    YARLParser parser = new YARLParser();
                    try {
                        parser.parseCommand(new YARLLexer(), str);
                        interpreter.push(true);
                    } catch (YARLYParserException ex) {
                        interpreter.push(false);
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("String to verify", "The string that will be verified", "String")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns if the string is a valid command or not.", "The boolean value representing if the string is a valid command or not.", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new IsComCommand(this.getId(), this.getModule());
    }

}
