package modules.command_flow_module;

import parser.YARLLexer;
import parser.YARLParser;
import parser.YARLYParserException;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ParseCom
 *
 * Module: Command Flow Module
 *
 * Description: Parses the given string.
 *
 * Return: The parsed command
 *
 * Return Type: ParsedCommand
 *
 * ---Attribute---
 *
 * Name: CommandString
 * Description: The string that will be parsed
 * Type: String
 *
 * ---Attribute---
 *
 * Usage: < CommandString > ParseCom -> ParsedCommand
 *
 * @author Ricardo Cacheira
 */
public class ParseComCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ParseCom";

    public ParseComCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ParseComCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    String str = (String) parameteres[0];
                    YARLParser parser = new YARLParser();
                    try {
                        interpreter.push(parser.parseCommand(new YARLLexer(), str));
                    } catch (YARLYParserException ex) {
                        interpreter.throwError("Invalid Command:\n" + ex.getMessage());
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("CommandString", "The string that will be parsed", "String")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Parses the given string.", "The parsed command", "ParsedCommand", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ParseComCommand(this.getId(), this.getModule());
    }

}
