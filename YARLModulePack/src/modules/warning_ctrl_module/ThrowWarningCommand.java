package modules.warning_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ThrowWarning
 *
 * Module: Warning Control Module
 *
 * Description: Throws an warning with the given message.
 *
 * ---Attribute---
 *
 * Name: WarningMessage
 * Description: The string that will be outputted when the warning is shown.
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * This command will always generate an warning so depending on the Warning
 * Message Handler it might not be shown.
 *
 * ---Note---
 *
 * Usage: < WarningMessage > ThrowWarning
 *
 * @author Ricardo Cacheira
 */
public class ThrowWarningCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ThrowWarning";

    public ThrowWarningCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ThrowWarningCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.throwWarning((String) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("WarningMessage", "The string that will be outputted when the warning is shown.", "String")
        };
        String[] noteList = {
            "This command will always generate an warning so depending on the Warning Message Handler it might not be shown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Throws an warning with the given message.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ThrowWarningCommand(this.getId(), this.getModule());
    }

}
