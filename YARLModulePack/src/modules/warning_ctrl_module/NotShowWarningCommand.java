package modules.warning_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.warning.NotShowWarningMessage;

/**
 * Name: NotShowWarning
 *
 * Module: Warning Control Module
 *
 * Description: Hides the warning message from the user
 *
 * Return: NotShowWarning WarningMsgHandler
 *
 * Return Type: WarningMsgHandler
 *
 * Usage: NotShowWarning -> WarningMsgHandler
 *
 * @author Ricardo Cacheira
 */
public class NotShowWarningCommand extends ModuleCommand {

    public static String COMMAND_NAME = "NotShowWarning";

    public NotShowWarningCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public NotShowWarningCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new NotShowWarningMessage());
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Hides the warning message from the user", "NotShowWarning WarningMsgHandler", "WarningMsgHandler", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new NotShowWarningCommand(this.getId(), this.getModule());
    }

}
