package modules.warning_ctrl_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is responsible for defining how the warnings should be handled by the system.
 *
 * @author Ricardo Cacheira
 */
public class WarningCtrlModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new DefWarningMsgCommand(this), 
			new NotShowWarningCommand(this), 
			new ShowWarningCommand(this), 
			new ThrowWarningCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"DefWarningMsg",
			"ShowWarning",
			"NotShowWarning",
			"ThrowWarning"
		};
	}

	@Override
	public String moduleName() {
		return "Warning Control Module";
	}

	@Override
	public String help() {
		return "This module is responsible for defining how the warnings should be handled by the system.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new WarningCtrlModule();
	}

}