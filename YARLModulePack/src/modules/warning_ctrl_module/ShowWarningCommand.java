package modules.warning_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.warning.ShowWarningMessage;

/**
 * Name: ShowWarning
 *
 * Module: Warning Control Module
 *
 * Description: Shows the warning message to the user
 *
 * Return: ShowWarning WarningMsgHandler
 *
 * Return Type: WarningMsgHandler
 *
 * Usage: ShowWarning -> WarningMsgHandler
 *
 * @author Ricardo Cacheira
 */
public class ShowWarningCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ShowWarning";

    public ShowWarningCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ShowWarningCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new ShowWarningMessage(interpreter));
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Shows the warning message to the user", "ShowWarning WarningMsgHandler", "WarningMsgHandler", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ShowWarningCommand(this.getId(), this.getModule());
    }

}
