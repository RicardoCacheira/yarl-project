package modules.warning_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.warning.WarningMessageHandler;

/**
 * Name: DefWarningMsg
 *
 * Module: Warning Control Module
 *
 * Description: Defines how the warning message to be handled by the interpreter
 *
 * ---Attribute---
 *
 * Name: DesiredErrorMsgHandler
 * Description: The Warning message handler to be used by the interpreter
 * Type: WarningMsgHandler
 *
 * ---Attribute---
 *
 * Usage: < DesiredErrorMsgHandler > DefWarningMsg
 *
 * @author Ricardo Cacheira
 */
public class DefWarningMsgCommand extends ModuleCommand {

    public static String COMMAND_NAME = "DefWarningMsg";

    public DefWarningMsgCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public DefWarningMsgCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof WarningMessageHandler) {
                    interpreter.defineWarningMessageHandler((WarningMessageHandler) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("DesiredErrorMsgHandler", "The Warning message handler to be used by the interpreter", "WarningMsgHandler")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Defines how the warning message to be handled by the interpreter", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new DefWarningMsgCommand(this.getId(), this.getModule());
    }

}
