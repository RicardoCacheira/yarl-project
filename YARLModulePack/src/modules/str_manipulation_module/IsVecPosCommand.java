package modules.str_manipulation_module;

import parser.ParsedCommand;
import parser.YARLLexer;
import parser.YARLParser;
import parser.YARLYParserException;
import parser.parsedtoken.VecPosToken;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: IsVecPos
 *
 * Module: String Manipulation Module
 *
 * Description: Returns if the string is a valid Vector Position or not.
 *
 * Return: The boolean value representing if the string is a valid Vector
 * Position or not.
 *
 * Return Type: Boolean
 *
 * ---Attribute---
 *
 * Name: String to verify
 * Description: The that will be verified
 * Type: String
 *
 * ---Attribute---
 *
 * Usage: < String to verify > IsVecPos -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class IsVecPosCommand extends ModuleCommand {

    public static String COMMAND_NAME = "IsVecPos";

    public IsVecPosCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public IsVecPosCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    String str = (String) parameteres[0];
                    YARLParser parser = new YARLParser();
                    try {
                        ParsedCommand com = parser.parseCommand(new YARLLexer(), str);
                        interpreter.push(com.type() == ParsedCommand.ParsedCommandType.RUNNABLE && com.runnableSection().size() == 1 && com.runnableSection().get(0).type().equals(VecPosToken.VEC_POS_TYPE));
                    } catch (YARLYParserException ex) {
                        interpreter.push(false);
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("String to verify", "The that will be verified", "String")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns if the string is a valid Vector Position or not.", "The boolean value representing if the string is a valid Vector Position or not.", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new IsVecPosCommand(this.getId(), this.getModule());
    }

}
