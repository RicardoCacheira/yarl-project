package modules.str_manipulation_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Trim
 *
 * Module: String Manipulation Module
 *
 * Description: Returns the trimmed string.
 *
 * Return: The trimmed string.
 *
 * Return Type: String
 *
 * ---Attribute---
 *
 * Name: String to trimmed
 * Description: The string whoose trim will be returned
 * Type: String
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * The trim removes all spaces in the beginning and the end and also turns all
 * groups of spaces into one.
 *
 * ---Note---
 *
 * Usage: < String to trimmed > Trim -> String
 *
 * @author Ricardo Cacheira
 */
public class TrimCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Trim";

    public TrimCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public TrimCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.push(((String) parameteres[0]).trim());
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("String to trimmed", "The string whoose trim will be returned", "String")
        };
        String[] noteList = {
            "The trim removes all spaces in the beginning and the end and also turns all groups of spaces into one."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns the trimmed string.", "The trimmed string.", "String", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new TrimCommand(this.getId(), this.getModule());
    }

}
