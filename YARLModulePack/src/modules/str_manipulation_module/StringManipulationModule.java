package modules.str_manipulation_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is responsible for the manipulation of strings and tha acess of their properties.
 *
 * @author Ricardo Cacheira
 */
public class StringManipulationModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new CharAtCommand(this), 
			new ConcatCommand(this), 
			new IsEmptyCommand(this), 
			new IsFreeIDCommand(this), 
			new IsNumCommand(this), 
			new IsValidIDCommand(this), 
			new IsVecPosCommand(this), 
			new SplitCommand(this), 
			new StrSizeCommand(this), 
			new SubStrCommand(this), 
			new TrimCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"CharAt",
			"StrSize",
			"SubStr",
			"Concat",
			"Split",
			"Trim",
			"IsEmpty",
			"IsValidID",
			"IsNum",
			"IsVecPos",
			"IsCommand",
			"IsFreeID"
		};
	}

	@Override
	public String moduleName() {
		return "String Manipulation Module";
	}

	@Override
	public String help() {
		return "This module is responsible for the manipulation of strings and tha acess of their properties.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new StringManipulationModule();
	}

}