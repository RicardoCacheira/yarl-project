package modules.str_manipulation_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Split
 *
 * Module: String Manipulation Module
 *
 * Description: Splits the given string using the given token.
 *
 * ---Attributes---
 *
 * Name: String to split
 * Description: The string that will be split
 * Type: String
 *
 * Name: Split token
 * Description: The token that will be used to split the given String
 * Type: String
 *
 * Name: Array/Vector Id
 * Description: The id of the Array/Vector that will contain the result of the
 * split
 * Type: String
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * if it's an Array, then it will be cleared and then all the resulting
 * substrings will be added in order into the array.
 *
 * if it's an Vector, then all the resulting substrings will be added in order
 * into the vector until theres no more space, if all the substrings were added
 * and there are positions without any added substrings then an empty string
 * will be added instead in each.
 *
 * e.g. After running the following command ' "Test;123;xyz" ";" "result" Split'
 * the Array 'result' will be ["Test", "123", "xyz"].
 *
 * ---Notes---
 *
 * Usage: < String to split > < Split token > < Array/Vector Id > Split
 *
 * @author Ricardo Cacheira
 */
public class SplitCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Split";

    public SplitCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SplitCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(3, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String) && (parameteres[1] instanceof String) && (parameteres[2] instanceof String)) {
                    String str = (String) parameteres[0];
                    String split = (String) parameteres[1];
                    String id = (String) parameteres[2];
                    boolean hasArray = interpreter.hasArray(id);
                    boolean hasVec = interpreter.hasVector(id);
                    if (hasArray || hasVec) {
                        String[] splitted = str.split(split);
                        if (hasArray) {
                            interpreter.clearArray(id);
                            for (int i = 0; i < splitted.length; i++) {
                                interpreter.addObjToArray(id, splitted[i]);
                            }
                        } else {
                            int vecSize = interpreter.getArrayVecSize(id);
                            for (int i = 0; i < vecSize; i++) {
                                if (i < splitted.length) {
                                    interpreter.setArrayVectorPos(id, i, splitted[i]);
                                } else {
                                    interpreter.setArrayVectorPos(id, i, "");
                                }
                            }
                        }
                    } else {
                        interpreter.throwError(String.format("No Array/Vector found with the ID \"%s\".", id));
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("String to split", "The string that will be split", "String"),
            new CommandInfoAttribute("Split token", "The token that will be used to split the given String", "String"),
            new CommandInfoAttribute("Array/Vector Id", "The id of the Array/Vector that will contain the result of the split", "String")
        };
        String[] noteList = {
            "if it's an Array, then it will be cleared and then all the resulting substrings will be added in order into the array.",
            "if it's an Vector, then all the resulting substrings will be added in order into the vector until theres no more space, if all the substrings were added and there are positions without any added substrings then an empty string will be added instead in each.",
            "e.g. After running the following command ' \"Test;123;xyz\" \";\" \"result\" Split' the Array 'result' will be [\"Test\", \"123\", \"xyz\"]."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Splits the given string using the given token.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SplitCommand(this.getId(), this.getModule());
    }

}
