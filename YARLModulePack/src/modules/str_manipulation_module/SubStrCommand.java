package modules.str_manipulation_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: SubStr
 *
 * Module: String Manipulation Module
 *
 * Description: Returns the desired substring of the string.
 *
 * Return: The desired substring of the string.
 *
 * Return Type: String
 *
 * ---Attributes---
 *
 * Name: String to return the substring
 * Description: The string whoose substring will be returned
 * Type: String
 *
 * Name: StartPos
 * Description: The start of the desired substring
 * Type: Number
 *
 * Name: EndPos
 * Description: The end of the desired substring
 * Type: Number
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * If the one of the positions is invalid an error will be thrown.
 *
 * If the EndPos is smaller than the StartPos an error will be thrown.
 *
 * The substring ends in the character before the endPos.
 *
 * e.g. The command ' "Testing" 0 4 SubStr ' returns the String "Test"
 *
 * ---Notes---
 *
 * Usage: < String to return the substring > < StartPos > < EndPos > SubStr ->
 * String
 *
 * @author Ricardo Cacheira
 */
public class SubStrCommand extends ModuleCommand {

    public static String COMMAND_NAME = "SubStr";

    public SubStrCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SubStrCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(3, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String) && (parameteres[1] instanceof RationalNumber) && (parameteres[2] instanceof RationalNumber)) {
                    String str = (String) parameteres[0];
                    int startPos = interpreter.generateNonNegativeInt((RationalNumber) parameteres[1]);
                    int endPos = interpreter.generateNonNegativeInt((RationalNumber) parameteres[2]);
                    if (endPos <= str.length() && endPos >= 0) {
                        if (startPos >= 0 && startPos <= endPos) {
                            interpreter.push(str.substring(startPos, endPos));
                        } else {
                            interpreter.throwError("Invalid start position.");
                        }
                    } else {
                        interpreter.throwError("Invalid end position.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("String to return the substring", "The string whoose substring will be returned", "String"),
            new CommandInfoAttribute("StartPos", "The start of the desired substring", "Number"),
            new CommandInfoAttribute("EndPos", "The end of the desired substring", "Number")
        };
        String[] noteList = {
            "If the one of the positions is invalid an error will be thrown.",
            "If the EndPos is smaller than the StartPos an error will be thrown.",
            "The substring ends in the character before the endPos.",
            "e.g. The command ' \"Testing\" 0 4 SubStr ' returns the String \"Test\""
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns the desired substring of the string.", "The desired substring of the string.", "String", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SubStrCommand(this.getId(), this.getModule());
    }

}
