package modules.str_manipulation_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: StrSize
 *
 * Module: String Manipulation Module
 *
 * Description: Returns the size of the string.
 *
 * Return: The size of the string.
 *
 * Return Type: Number
 *
 * ---Attribute---
 *
 * Name: String to return the size
 * Description: The string whoose size will be returned
 * Type: String
 *
 * ---Attribute---
 *
 * Usage: < String to return the size > StrSize -> Number
 *
 * @author Ricardo Cacheira
 */
public class StrSizeCommand extends ModuleCommand {

    public static String COMMAND_NAME = "StrSize";

    public StrSizeCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public StrSizeCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.push(new RationalNumber(((String) parameteres[0]).length()));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("String to return the size", "The string whoose size will be returned", "String")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns the size of the string.", "The size of the string.", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new StrSizeCommand(this.getId(), this.getModule());
    }

}
