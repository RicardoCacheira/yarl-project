package modules.str_manipulation_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.variable.Array;
import yarli.variable.Vector;

/**
 * Name: Concat
 *
 * Module: String Manipulation Module
 *
 * Description: This command returns the concatnated string of the desired
 * values on the stack or the concatnated string of all the values of the
 * Array/Vector, depending on the given attribute
 *
 * Return: The concatnated string of the desired values
 *
 * Return Type: String
 *
 * ---Attribute---
 *
 * Name: attr
 * Description: The number of the values of the stack to concatnate, if it's a
 * Number, The Array/Vector containing the values to concatnate, if it's an
 * Array/Vector
 * Type: Number/Array/Vector
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * Any object that isn't a string will be concatnated as its representative
 * string.
 *
 * When attr is a Number it must be a positive integer Number, or else an error
 * will be thrown.
 *
 * If not enough objects exist in the stack an error will be thrown.
 *
 * The Objects will be concatnated from the bottom to the top of the stack.
 *
 * e.g. The command ' "Hello " "World" "!" 3 Concat ' returns the String "Hello
 * World!"
 *
 * Tip: The command ' StackSize Concat ' returns the concatnation of all the
 * objects on the stack
 *
 * ---Notes---
 *
 * Usage: < attr > Concat -> String
 *
 * @author Ricardo Cacheira
 */
public class ConcatCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Concat";

    public ConcatCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ConcatCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    private void concatStr(Object[] list, YARLI interpreter) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.length; i++) {
            Object aux = list[i];
            if (aux instanceof String) {
                sb.append((String) aux);
            } else {
                sb.append(interpreter.generateObjString(aux));
            }
        }
        interpreter.push(sb.toString());
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber)) {
                    int size = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                    if (size <= interpreter.stackSize() && size > 0) {
                        Object[] stack = interpreter.getParameters(size, COMMAND_NAME);
                        this.concatStr(stack, interpreter);
                    } else {
                        interpreter.throwError("The Received Number is not a valid stack size.");
                    }
                } else if (parameteres[0] instanceof Vector) {
                    Vector vec = (Vector) parameteres[0];
                    this.concatStr(vec.getCopy(), interpreter);
                } else if (parameteres[0] instanceof Array) {
                    Array array = (Array) parameteres[0];
                    if (array.size() != 0) {
                        this.concatStr(array.getCopy(), interpreter);
                    } else {
                        interpreter.throwError("The Array has a size of zero");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("attr", "The number of the values of the stack to concatnate, if it's a Number, The Array/Vector containing the values to concatnate, if it's an Array/Vector", "Number/Array/Vector")
        };
        String[] noteList = {
            "Any object that isn't a string will be concatnated as its representative string.",
            "When attr is a Number it must be a positive integer Number, or else an error will be thrown.",
            "If not enough objects exist in the stack an error will be thrown.",
            "The Objects will be concatnated from the bottom to the top of the stack.",
            "e.g. The command ' \"Hello \" \"World\" \"!\" 3 Concat ' returns the String \"Hello World!\"",
            "Tip: The command ' StackSize Concat ' returns the concatnation of all the objects on the stack"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the concatnated string of the desired values on the stack or the concatnated string of all the values of the Array/Vector, depending on the given attribute", "The concatnated string of the desired values", "String", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ConcatCommand(this.getId(), this.getModule());
    }

}
