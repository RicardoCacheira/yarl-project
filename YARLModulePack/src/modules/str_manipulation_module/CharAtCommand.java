package modules.str_manipulation_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: CharAt
 *
 * Module: String Manipulation Module
 *
 * Description: Returns the character of the string at the desired position.
 *
 * Return: The character of the string at the desired position.
 *
 * Return Type: Boolean
 *
 * ---Attributes---
 *
 * Name: String to return char
 * Description: The string whoose desired character will be returned
 * Type: String
 *
 * Name: CharPos
 * Description: The position of the desired character
 * Type: Number
 *
 * ---Attributes---
 *
 * ---Note---
 *
 * If the position is invalid an error will be thrown.
 *
 * ---Note---
 *
 * Usage: < String to return char > < CharPos > CharAt -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class CharAtCommand extends ModuleCommand {

    public static String COMMAND_NAME = "CharAt";

    public CharAtCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public CharAtCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String) && (parameteres[1] instanceof RationalNumber)) {
                    String str = (String) parameteres[0];
                    int pos = interpreter.generateNonNegativeInt((RationalNumber) parameteres[1]);
                    if (pos < str.length() && pos >= 0) {
                        interpreter.push("" + str.charAt(pos));
                    } else {
                        interpreter.throwError("The Received Number is not a valid character position.");
                    }

                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("String to return char", "The string whoose desired character will be returned", "String"),
            new CommandInfoAttribute("CharPos", "The position of the desired character", "Number")
        };
        String[] noteList = {
            "If the position is invalid an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns the character of the string at the desired position.", "The character of the string at the desired position.", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new CharAtCommand(this.getId(), this.getModule());
    }

}
