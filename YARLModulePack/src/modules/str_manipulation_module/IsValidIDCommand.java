package modules.str_manipulation_module;

import java.util.List;
import parser.Token;
import parser.YARLLexer;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: IsValidID
 *
 * Module: String Manipulation Module
 *
 * Description: Returns if the string is a valid id or not.
 *
 * Return: The boolean value representing if the string is a valid id or not.
 *
 * Return Type: Boolean
 *
 * ---Attribute---
 *
 * Name: String to verify
 * Description: The that will be verified
 * Type: String
 *
 * ---Attribute---
 *
 * Usage: < String to verify > IsValidID -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class IsValidIDCommand extends ModuleCommand {

    public static String COMMAND_NAME = "IsValidID";

    public IsValidIDCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public IsValidIDCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof String) {
                    interpreter.push(interpreter.isValidId((String) parameteres[0]));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("String to verify", "The that will be verified", "String")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns if the string is a valid id or not.", "The boolean value representing if the string is a valid id or not.", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new IsValidIDCommand(this.getId(), this.getModule());
    }

}
