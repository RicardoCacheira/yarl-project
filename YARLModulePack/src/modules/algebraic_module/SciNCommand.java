package modules.algebraic_module;

import rationalnumber.RationalNumber;
import rationalnumber.notationhandler.ScientificNotationHandler;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.obj_str_handler.NumberStringHandler;

/**
 * Name: SciN
 *
 * Module: Algebraic Module
 *
 * Description: This command returns a notation handler that shows the
 * Scientific notation
 *
 * Return: A notation handler that shows the Scientific notation
 *
 * Return Type: NotationHandler
 *
 * ---Attributes---
 *
 * Name: coefficient digit number
 * Description: The number of digits that will be shown in the coefficient
 * Type: Number
 *
 * Name: Shows decimal Comma
 * Description: Represents if an decimal comma should be used instead of the
 * decimal point.
 * Type: Boolean
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The coefficient digit number must be a positive integer Number, or else an
 * error will be thrown.
 *
 * e.g. if the coefficient digit number is 3 then, 23 will be shown as
 * 2.30x10^1, 1.5 will be shown as 1.50x10^0 and 0.1234 will be shown as
 * 1.23x10^-1
 *
 * While some fractional part might not be show it will remain in the number and
 * it will affect following calculations
 *
 * e.g. op1 = 1505 will be shown as 1.51x10^3, op2 = 2505 will be shown as
 * 2.51x10^3 but op1 + op2 = 4.01x10^3 and not op1 + op2 = 4.02x10^3
 *
 * Any rounding to the presented numbers will be using the Round half up system
 *
 * ---Notes---
 *
 * Usage: < coefficient digit number > < Shows decimal Comma > SciN ->
 * NotationHandler
 *
 * @author Ricardo Cacheira
 */
public class SciNCommand extends ModuleCommand {

    public static String COMMAND_NAME = "SciN";

    public SciNCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SciNCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof Boolean)) {
                    int coefficient = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                    if (coefficient > 0) {
                        interpreter.push(new NumberStringHandler(new ScientificNotationHandler(coefficient, (Boolean) parameteres[1])));
                    } else {
                        interpreter.throwError("Invalid coefficient Number.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("coefficient digit number", "The number of digits that will be shown in the coefficient", "Number"),
            new CommandInfoAttribute("Shows decimal Comma", "Represents if an decimal comma should be used instead of the decimal point.", "Boolean")
        };
        String[] noteList = {
            "The coefficient digit number must be a positive integer Number, or else an error will be thrown.",
            "e.g. if the coefficient digit number is 3 then, 23 will be shown as 2.30x10^1, 1.5 will be shown as 1.50x10^0 and 0.1234 will be shown as 1.23x10^-1",
            "While some fractional part might not be show it will remain in the number and it will affect following calculations",
            "e.g. op1 = 1505 will be shown as 1.51x10^3, op2 = 2505 will be shown as 2.51x10^3 but op1 + op2 = 4.01x10^3 and not op1 + op2 = 4.02x10^3",
            "Any rounding to the presented numbers will be using the Round half up system"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns a notation handler that shows the Scientific notation", "A notation handler that shows the Scientific notation", "NotationHandler", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SciNCommand(this.getId(), this.getModule());
    }

}
