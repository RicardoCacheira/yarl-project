package modules.algebraic_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import rationalnumber.RationalNumber;
import rationalnumber.RationalNumberException;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: /
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the quotient with the remainder between
 * operand1 and operand2
 *
 * Return: The quotient with the remainder of the division
 *
 * Return Type: Number
 *
 * ---Attributes---
 *
 * Name: operand1
 * Description: The dividend of the division
 * Type: Number
 *
 * Name: operand2
 * Description: The divisor of the division
 * Type: Number
 *
 * ---Attributes---
 *
 * ---Note---
 *
 * The meaning of returning the quotient with the remainder is 7 / 2 = 3.5 and
 * not 7 / 2 = 3.
 *
 * ---Note---
 *
 * Usage: < operand1 > < operand2 > / -> Number
 *
 * @author Ricardo Cacheira
 */
public class DivideCommand extends ModuleCommand {

    public static String COMMAND_NAME = "/";

    public DivideCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public DivideCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof RationalNumber)) {
                    try {
                        interpreter.push(((RationalNumber) parameteres[0]).divide((RationalNumber) parameteres[1]));
                    } catch (RationalNumberException ex) {
                        interpreter.throwError(ex.getMessage());
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("operand1", "The dividend of the division", "Number"),
            new CommandInfoAttribute("operand2", "The divisor of the division", "Number")
        };
        String[] noteList = {
            "The meaning of returning the quotient with the remainder is  7 / 2 = 3.5 and not 7 / 2 = 3."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the quotient with the remainder between operand1 and operand2", "The quotient with the remainder of the division", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new DivideCommand(this.getId(), this.getModule());
    }

}
