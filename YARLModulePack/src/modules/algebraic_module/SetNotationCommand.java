package modules.algebraic_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.obj_str_handler.NumberStringHandler;

/**
 * Name: SetNotation
 *
 * Module: Algebraic Module
 *
 * Description: This command defines the notation to be used when presenting
 * Numbers
 *
 * ---Attribute---
 *
 * Name: DesiredNotation
 * Description: The desired notation handler to be used when presenting Numbers
 * Type: NotationHandler
 *
 * ---Attribute---
 *
 * Usage: < DesiredNotation > SetNotation
 *
 * @author Ricardo Cacheira
 */
public class SetNotationCommand extends ModuleCommand {

    public static String COMMAND_NAME = "SetNotation";

    public SetNotationCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public SetNotationCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof NumberStringHandler)) {
                    interpreter.addObjStringHandler((NumberStringHandler) parameteres[0]);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("DesiredNotation", "The desired notation handler to be used when presenting Numbers", "NotationHandler")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command defines the notation to be used when presenting Numbers", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new SetNotationCommand(this.getId(), this.getModule());
    }

}
