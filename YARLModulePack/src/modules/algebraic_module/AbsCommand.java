package modules.algebraic_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Abs
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the absolute value of operand1
 *
 * Return: The absolute value of operand1
 *
 * Return Type: Number
 *
 * ---Attribute---
 *
 * Name: operand1
 * Description: The number whose absolute value will be return
 * Type: Number
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * e.g. The absolute value of -13 is 13
 *
 * e.g. The absolute value of 42 is 42
 *
 * ---Notes---
 *
 * Usage: < operand1 > Abs -> Number
 *
 * @author Ricardo Cacheira
 */
public class AbsCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Abs";

    public AbsCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public AbsCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber)) {
                    interpreter.push(((RationalNumber) parameteres[0]).absolute());
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("operand1", "The number whose absolute value will be return", "Number")
        };
        String[] noteList = {
            "e.g. The absolute value of -13 is 13",
            "e.g. The absolute value of 42 is 42"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the absolute value of operand1", "The absolute value of operand1", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new AbsCommand(this.getId(), this.getModule());
    }

}
