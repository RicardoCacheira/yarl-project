package modules.algebraic_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is responsible for performing algebraic operations and also defining the notation that numbers are shown by the interpreter.
			Warning - While there are commands where the operation is written in infix notaton, it's only for demonstration purpose, when writing in the interpreter it must be RNP.
 *
 * @author Ricardo Cacheira
 */
public class AlgebraicModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new AbsCommand(this), 
			new AddCommand(this), 
			new AvgCommand(this), 
			new DecNCommand(this), 
			new DecStdNCommand(this), 
			new DivideCommand(this), 
			new FDecNCommand(this), 
			new FractCommand(this), 
			new IntCommand(this), 
			new IntDivideCommand(this), 
			new InvCommand(this), 
			new IsIntCommand(this), 
			new MaxCommand(this), 
			new MinCommand(this), 
			new ModCommand(this), 
			new MultiplyCommand(this), 
			new OppCommand(this), 
			new PowerCommand(this), 
			new RoundCommand(this), 
			new SciNCommand(this), 
			new SetNotationCommand(this), 
			new StdNCommand(this), 
			new SubtractCommand(this), 
			new SumCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"+",
			"-",
			"*",
			"/",
			"Div",
			"Mod",
			"Power",
			"Opp",
			"Inv",
			"Abs",
			"Int",
			"Fract",
			"Round",
			"IsInt",
			"Sum",
			"Avg",
			"Max",
			"Min",
			"StdN",
			"DecN",
			"FDecN",
			"SciN",
			"DecStdN",
			"SetNotation"
		};
	}

	@Override
	public String moduleName() {
		return "Algebraic Module";
	}

	@Override
	public String help() {
		return "This module is responsible for performing algebraic operations and also defining the notation that numbers are shown by the interpreter.\nWarning - While there are commands where the operation is written in infix notaton, it's only for demonstration purpose, when writing in the interpreter it must be RNP.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new AlgebraicModule();
	}

}