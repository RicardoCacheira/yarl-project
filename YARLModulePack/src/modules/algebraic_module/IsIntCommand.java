package modules.algebraic_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: IsInt
 *
 * Module: Algebraic Module
 *
 * Description: Returns if the number is an integer or not.
 *
 * Return: The boolean value representing if the number is integer.
 *
 * Return Type: Boolean
 *
 * ---Attribute---
 *
 * Name: number
 * Description: The number to ckeck
 * Type: Number
 *
 * ---Attribute---
 *
 * Usage: < number > IsInt -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class IsIntCommand extends ModuleCommand {

    public static String COMMAND_NAME = "IsInt";

    public IsIntCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public IsIntCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber)) {
                    interpreter.push(((RationalNumber) parameteres[0]).isAnInteger());
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("number", "The number to ckeck", "Number")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns if the number is an integer or not.", "The boolean value representing if the number is integer.", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new IsIntCommand(this.getId(), this.getModule());
    }

}
