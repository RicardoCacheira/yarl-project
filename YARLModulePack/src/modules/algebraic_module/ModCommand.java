package modules.algebraic_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Mod
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the remainder between operand1 and operand2
 *
 * Return: The remainder of the division
 *
 * Return Type: Number
 *
 * ---Attributes---
 *
 * Name: operand1
 * Description: The dividend of the division
 * Type: Number
 *
 * Name: operand2
 * Description: The divisor of the division
 * Type: Number
 *
 * ---Attributes---
 *
 * ---Note---
 *
 * e.g. 7 MOD 2 = 1
 *
 * ---Note---
 *
 * Usage: < operand1 > < operand2 > Mod -> Number
 *
 * @author Ricardo Cacheira
 */
public class ModCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Mod";

    public ModCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ModCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof RationalNumber)) {
                    interpreter.push(((RationalNumber) parameteres[0]).modulo((RationalNumber) parameteres[1]));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("operand1", "The dividend of the division", "Number"),
            new CommandInfoAttribute("operand2", "The divisor of the division", "Number")
        };
        String[] noteList = {
            "e.g. 7 MOD 2 = 1 "
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the remainder between operand1 and operand2", "The remainder of the division", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ModCommand(this.getId(), this.getModule());
    }

}
