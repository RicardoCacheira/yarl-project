package modules.algebraic_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Opp
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the opposite(a.k.a. Additive inverse) of
 * operand1
 *
 * Return: The opposite of operand1
 *
 * Return Type: Number
 *
 * ---Attribute---
 *
 * Name: operand1
 * Description: The number whose opposite will be return
 * Type: Number
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * e.g. The opposite of 1/3 is -1/3
 *
 * ---Note---
 *
 * Usage: < operand1 > Opp -> Number
 *
 * @author Ricardo Cacheira
 */
public class OppCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Opp";

    public OppCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public OppCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber)) {
                    interpreter.push(((RationalNumber) parameteres[0]).opposite());
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("operand1", "The number whose opposite will be return", "Number")
        };
        String[] noteList = {
            "e.g. The opposite of 1/3 is -1/3"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the opposite(a.k.a. Additive inverse) of operand1", "The opposite of operand1", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new OppCommand(this.getId(), this.getModule());
    }

}
