package modules.algebraic_module;

import rationalnumber.RationalNumber;
import rationalnumber.RationalNumberException;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Power
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the base raised to the power of exponent
 *
 * Return: base^exponent
 *
 * Return Type: Number
 *
 * ---Attributes---
 *
 * Name: base
 * Description: The base of the power
 * Type: Number
 *
 * Name: exponent
 * Description: The exponent of the power
 * Type: Number
 *
 * ---Attributes---
 *
 * ---Note---
 *
 * The exponent must be a integer value
 *
 * ---Note---
 *
 * Usage: < base > < exponent > Power -> Number
 *
 * @author Ricardo Cacheira
 */
public class PowerCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Power";

    public PowerCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public PowerCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof RationalNumber)) {
                    try {
                        interpreter.push(((RationalNumber) parameteres[0]).power((RationalNumber) parameteres[1]));
                    } catch (RationalNumberException ex) {
                        interpreter.throwError(ex.getMessage());
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("base", "The base of the power", "Number"),
            new CommandInfoAttribute("exponent", "The exponent of the power", "Number")
        };
        String[] noteList = {
            "The exponent must be a integer value"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the base raised to the power of exponent", "base^exponent", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new PowerCommand(this.getId(), this.getModule());
    }

}
