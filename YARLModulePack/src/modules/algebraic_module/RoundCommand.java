package modules.algebraic_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Round
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the rounded value of operand1 with the
 * desired precision
 *
 * Return: The the rounded operand1
 *
 * Return Type: Number
 *
 * ---Attributes---
 *
 * Name: operand1
 * Description: The number whose rounded value will be return
 * Type: Number
 *
 * Name: DecimalsToRound
 * Description: The the amount of decimals to round on the operand1
 * Type: Number
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The precision must be a non-negative integer Number, or else an error will be
 * thrown.
 *
 * Any rounding to the presented numbers will be using the Round half up system
 *
 * e.g. The rounded value with a number of decimals to round of 3 of 27.5469 is
 * 27.547
 *
 * e.g. The rounded value with a number of decimals to round of 1 of 27.5469 is
 * 27.5
 *
 * e.g. The rounded value with a number of decimals to round of 0 of 27.5469 is
 * 28
 *
 * e.g. The rounded value with a number of decimals to round of 3 of -27.5469 is
 * -27.547
 *
 * e.g. The rounded value with a number of decimals to round of 1 of -27.5469 is
 * -27.5
 *
 * e.g. The rounded value with a number of decimals to round of 0 of -27.5469 is
 * -28
 *
 * ---Notes---
 *
 * Usage: < operand1 > < DecimalsToRound > Round -> Number
 *
 * @author Ricardo Cacheira
 */
public class RoundCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Round";

    public RoundCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public RoundCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof RationalNumber)) {
                    interpreter.push(((RationalNumber) parameteres[0]).rounded((RationalNumber) parameteres[1]));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("operand1", "The number whose rounded value will be return", "Number"),
            new CommandInfoAttribute("DecimalsToRound", "The the amount of decimals to round on the operand1", "Number")
        };
        String[] noteList = {
            "The precision must be a non-negative integer Number, or else an error will be thrown.",
            "Any rounding to the presented numbers will be using the Round half up system",
            "e.g. The rounded value with a number of decimals to round of 3 of 27.5469 is 27.547",
            "e.g. The rounded value with a number of decimals to round of 1 of 27.5469 is 27.5",
            "e.g. The rounded value with a number of decimals to round of 0 of 27.5469 is 28",
            "e.g. The rounded value with a number of decimals to round of 3 of -27.5469 is -27.547",
            "e.g. The rounded value with a number of decimals to round of 1 of -27.5469 is -27.5",
            "e.g. The rounded value with a number of decimals to round of 0 of -27.5469 is -28"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the rounded value of operand1 with the desired precision", "The the rounded operand1", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new RoundCommand(this.getId(), this.getModule());
    }

}
