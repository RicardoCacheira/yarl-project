package modules.algebraic_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Div
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the quotient between operand1 and operand2
 *
 * Return: The quotient of the division
 *
 * Return Type: Number
 *
 * ---Attributes---
 *
 * Name: operand1
 * Description: The dividend of the division
 * Type: Number
 *
 * Name: operand2
 * Description: The divisor of the division
 * Type: Number
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * In this funciton returning the quotient means that 7 / 2 = 3 and not 7 / 2 =
 * 3.5 .
 *
 * The operand1 and operand2 must be integer numbers.
 *
 * ---Notes---
 *
 * Usage: < operand1 > < operand2 > Div -> Number
 *
 * @author Ricardo Cacheira
 */
public class IntDivideCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Div";

    public IntDivideCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public IntDivideCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof RationalNumber)) {
                    interpreter.push(((RationalNumber) parameteres[0]).integerDivide((RationalNumber) parameteres[1]));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("operand1", "The dividend of the division", "Number"),
            new CommandInfoAttribute("operand2", "The divisor of the division", "Number")
        };
        String[] noteList = {
            "In this funciton returning the quotient means that 7 / 2 = 3 and not 7 / 2 = 3.5 .",
            "The operand1 and operand2 must be integer numbers."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the quotient between operand1 and operand2", "The quotient of the division", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new IntDivideCommand(this.getId(), this.getModule());
    }

}
