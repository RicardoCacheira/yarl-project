package modules.algebraic_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import rationalnumber.RationalNumber;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: +
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the addition of operand1 with operand2
 *
 * Return: operand1 + operand2
 *
 * Return Type: Number
 *
 * ---Attributes---
 *
 * Name: operand1
 * Description: The first operand of the addition
 * Type: Number
 *
 * Name: operand2
 * Description: The second operand of the addition
 * Type: Number
 *
 * ---Attributes---
 *
 * Usage: < operand1 > < operand2 > + -> Number
 *
 * @author Ricardo Cacheira
 */
public class AddCommand extends ModuleCommand {

    public static String COMMAND_NAME = "+";

    public AddCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public AddCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof RationalNumber)) {
                    interpreter.push(((RationalNumber) parameteres[0]).add((RationalNumber) parameteres[1]));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("operand1", "The first operand of the addition", "Number"),
            new CommandInfoAttribute("operand2", "The second operand of the addition", "Number")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the addition of operand1 with operand2", "operand1 + operand2", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new AddCommand(this.getId(), this.getModule());
    }

}
