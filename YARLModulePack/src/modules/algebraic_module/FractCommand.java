package modules.algebraic_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Fract
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the fractional part of operand1
 *
 * Return: The fractional part of operand1
 *
 * Return Type: Number
 *
 * ---Attribute---
 *
 * Name: operand1
 * Description: The number whose fractional part will be return
 * Type: Number
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * e.g. The fractional part of 27 is 0
 *
 * e.g. The fractional part of 86.75 is 0.75
 *
 * e.g. The fractional part of 0.125 is 0.125
 *
 * ---Notes---
 *
 * Usage: < operand1 > Fract -> Number
 *
 * @author Ricardo Cacheira
 */
public class FractCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Fract";

    public FractCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public FractCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber)) {
                    interpreter.push(((RationalNumber) parameteres[0]).decimalPart());
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("operand1", "The number whose fractional part will be return", "Number")
        };
        String[] noteList = {
            "e.g. The fractional part of 27 is 0",
            "e.g. The fractional part of 86.75 is 0.75",
            "e.g. The fractional part of 0.125 is 0.125"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the fractional part of operand1", "The fractional part of operand1", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new FractCommand(this.getId(), this.getModule());
    }

}
