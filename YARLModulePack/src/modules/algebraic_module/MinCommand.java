package modules.algebraic_module;

import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.variable.Array;
import yarli.variable.Matrix;
import yarli.variable.Vector;

/**
 * Name: Min
 *
 * Module: Algebraic Module
 *
 * Description: This command returns the minimum value of the desired values on
 * the stack or the minimum value of all the values of the array, depending on
 * the given attribute
 *
 * Return: The minimum value of the desired values
 *
 * Return Type: Number
 *
 * ---Attribute---
 *
 * Name: attr
 * Description: The number of the values of the stack to calculate the minimum
 * value, if it's a Number, The array containing the values to calculate the
 * minimum value, if it's as Array
 * Type: Number/Array
 *
 * ---Attribute---
 *
 * ---Notes---
 *
 * In the context of this command the concept of Array represents the Objects
 * Array, Vector and Matrix.
 *
 * If any of the chosen values is not an Number, then an error will be thrown.
 *
 * When attr is a Number it must be a positive integer Number, or else an error
 * will be thrown.
 *
 * When attr is an Array it must be a non-empty Array, or else an error will be
 * thrown.
 *
 * Tip: To calculate the minimum value of all values in the stack use the
 * command "StackSize Min"
 *
 * ---Notes---
 *
 * Usage: < attr > Min -> Number
 *
 * @author Ricardo Cacheira
 */
public class MinCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Min";

    public MinCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public MinCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    private void calculateMin(Object[] list, YARLI interpreter) {
        boolean hasBreak = false;
        RationalNumber min = RationalNumber.ZERO;
        if (list[0] instanceof RationalNumber) {
            min = (RationalNumber) list[0];
        } else {
            interpreter.throwError(String.format("Value \"%s\" is not a RationaNumber.", interpreter.generateObjString(list[0])));
            hasBreak = true;
        }
        if (!hasBreak) {
            for (int i = 1; i < list.length; i++) {
                if (list[i] instanceof RationalNumber) {
                    RationalNumber num = (RationalNumber) list[i];
                    if (min.compareTo(num) > 0) {
                        min = num;
                    }
                } else {
                    interpreter.throwError(String.format("Value \"%s\" is not a RationaNumber.", interpreter.generateObjString(list[i])));
                    hasBreak = true;
                    break;
                }
            }
            if (!hasBreak) {
                interpreter.push(min);
            }
        }
    }

    private void calculateMin(Object[][] matrix, YARLI interpreter) {
        boolean hasBreak = false;
        RationalNumber min = RationalNumber.ZERO;
        if (matrix[0][0] instanceof RationalNumber) {
            min = (RationalNumber) matrix[0][0];
        } else {
            interpreter.throwError(String.format("Value \"%s\" is not a RationaNumber.", interpreter.generateObjString(matrix[0][0])));
            hasBreak = true;
        }
        if (!hasBreak) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    Object aux = matrix[i][j];
                    if (aux instanceof RationalNumber) {
                        RationalNumber num = (RationalNumber) matrix[i][j];
                        if (min.compareTo(num) > 0) {
                            min = num;
                        }
                    } else {
                        interpreter.throwError(String.format("Value in position %d is not a RationaNumber.", i));
                        hasBreak = true;
                        break;
                    }
                }
                if (hasBreak) {
                    break;
                }
            }
            if (!hasBreak) {
                interpreter.push(min);
            }
        }
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber)) {
                    int size = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                    if (size <= interpreter.stackSize() && size > 0) {
                        Object[] stack = interpreter.getParameters(size, COMMAND_NAME);
                        this.calculateMin(stack, interpreter);
                    } else {
                        interpreter.throwError("The Received Number is not a valid stack size.");
                    }
                } else if (parameteres[0] instanceof Vector) {
                    Vector vec = (Vector) parameteres[0];
                    this.calculateMin(vec.getCopy(), interpreter);
                } else if (parameteres[0] instanceof Array) {
                    Array array = (Array) parameteres[0];
                    if (array.size() != 0) {
                        this.calculateMin(array.getCopy(), interpreter);
                    } else {
                        interpreter.throwError("The Array has a size of zero");
                    }
                } else if (parameteres[0] instanceof Matrix) {
                    Matrix matrix = (Matrix) parameteres[0];
                    this.calculateMin(matrix.getCopy(), interpreter);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("attr", "The number of the values of the stack to calculate the minimum value, if it's a Number, The array containing the values to calculate the minimum value, if it's as Array", "Number/Array")
        };
        String[] noteList = {
            "In the context of this command the concept of Array represents the Objects Array, Vector and Matrix.",
            "If any of the chosen values is not an Number, then an error will be thrown.",
            "When attr is a Number it must be a positive integer Number, or else an error will be thrown.",
            "When attr is an Array it must be a non-empty Array, or else an error will be thrown.",
            "Tip: To calculate the minimum value of all values in the stack use the command \"StackSize Min\""
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns the minimum value of the desired values on the stack or the minimum value of all the values of the array, depending on the given attribute", "The minimum value of the desired values", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new MinCommand(this.getId(), this.getModule());
    }

}
