package modules.algebraic_module;

import rationalnumber.notationhandler.StandartNotationHandler;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.obj_str_handler.NumberStringHandler;

/**
 * Name: StdN
 *
 * Module: Algebraic Module
 *
 * Description: This command returns a notation handler that shows the Standart
 * notation
 *
 * Return: A notation handler that shows the Standart notation
 *
 * Return Type: NotationHandler
 *
 * ---Notes---
 *
 * In standart notation all values that are interger will be shown as such, all
 * the others will be shown in form of a fraction.
 *
 * e.g. 23 will be shown as 23, 1.5 will be shown as 3/2
 *
 * ---Notes---
 *
 * Usage: StdN -> NotationHandler
 *
 * @author Ricardo Cacheira
 */
public class StdNCommand extends ModuleCommand {

    public static String COMMAND_NAME = "StdN";

    public StdNCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public StdNCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new NumberStringHandler(new StandartNotationHandler()));
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "In standart notation all values that are interger will be shown as such, all the others will be shown in form of a fraction.",
            "e.g. 23 will be shown as 23, 1.5 will be shown as 3/2"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns a notation handler that shows the Standart notation", "A notation handler that shows the Standart notation", "NotationHandler", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new StdNCommand(this.getId(), this.getModule());
    }

}
