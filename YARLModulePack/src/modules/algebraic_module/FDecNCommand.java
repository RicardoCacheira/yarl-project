package modules.algebraic_module;

import rationalnumber.RationalNumber;
import rationalnumber.notationhandler.FixedDecimalNotationHandler;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.obj_str_handler.NumberStringHandler;

/**
 * Name: FDecN
 *
 * Module: Algebraic Module
 *
 * Description: This command returns a notation handler that shows the Fixed
 * Decimal notation
 *
 * Return: A notation handler that shows the Fixed Decimal notation
 *
 * Return Type: NotationHandler
 *
 * ---Attributes---
 *
 * Name: fractional digit number
 * Description: The number of fractional digits will be shown
 * Type: Number
 *
 * Name: Shows decimal Comma
 * Description: Represents if an decimal comma should be used instead of the
 * decimal point.
 * Type: Boolean
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The fractional digit number must be a non-negative integer Number, or else an
 * error will be thrown.
 *
 * e.g. if the fractional digit number is 3 then, 23 will be shown as 23.000,
 * 1.5 will be shown as 1.500 and 0.1234 will be shown as 0.123
 *
 * While some fractional part might not be show it will remain in the number and
 * it will affect following calculations
 *
 * e.g. op1 = 1.5 will be shown as 1, op2 = 2.5 will be shown as 2 but op1 + op2
 * = 4 and not op1 + op2 = 3
 *
 * Any rounding to the presented numbers will be using the Round half up system
 *
 * ---Notes---
 *
 * Usage: < fractional digit number > < Shows decimal Comma > FDecN ->
 * NotationHandler
 *
 * @author Ricardo Cacheira
 */
public class FDecNCommand extends ModuleCommand {

    public static String COMMAND_NAME = "FDecN";

    public FDecNCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public FDecNCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof RationalNumber) && (parameteres[1] instanceof Boolean)) {
                    int precision = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                    if (precision != -1) {
                        interpreter.push(new NumberStringHandler(new FixedDecimalNotationHandler(precision, (Boolean) parameteres[1])));
                    } else {
                        interpreter.throwError("Invalid fractional digit Number.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("fractional digit number", "The number of fractional digits will be shown", "Number"),
            new CommandInfoAttribute("Shows decimal Comma", "Represents if an decimal comma should be used instead of the decimal point.", "Boolean")
        };
        String[] noteList = {
            "The fractional digit number must be a non-negative integer Number, or else an error will be thrown.",
            "e.g. if the fractional digit number is 3 then, 23 will be shown as 23.000, 1.5 will be shown as 1.500 and 0.1234 will be shown as 0.123",
            "While some fractional part might not be show it will remain in the number and it will affect following calculations",
            "e.g. op1 = 1.5 will be shown as 1, op2 = 2.5 will be shown as 2 but op1 + op2 = 4 and not op1 + op2 = 3",
            "Any rounding to the presented numbers will be using the Round half up system"
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "This command returns a notation handler that shows the Fixed Decimal notation", "A notation handler that shows the Fixed Decimal notation", "NotationHandler", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new FDecNCommand(this.getId(), this.getModule());
    }

}
