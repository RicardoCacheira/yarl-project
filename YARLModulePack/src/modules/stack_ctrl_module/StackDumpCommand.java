package modules.stack_ctrl_module;

import java.io.FileNotFoundException;
import java.util.Formatter;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: StackDump
 *
 * Module: Stack Control Module
 *
 * Description: Dumps all of the objects of the stack onto the desired file.
 *
 * ---Attributes---
 *
 * Name: filepath
 * Description: The the path of the file to be created with the list of the
 * variables
 * Type: String
 *
 * Name: ContainsVarType
 * Description: Defines if the type of variable will be outputted into the file
 * Type: Boolean
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * The values will be written line by line in the file and will contain the
 * string that identifies them and the type of variable(if chosen).
 *
 * If the interpreter is unable to create the file an error will be thrown.
 *
 * ---Notes---
 *
 * Usage: < filepath > < ContainsVarType > StackDump
 *
 * @author Ricardo Cacheira
 */
public class StackDumpCommand extends ModuleCommand {

    public static String COMMAND_NAME = "StackDump";

    public StackDumpCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public StackDumpCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String) && (parameteres[1] instanceof Boolean)) {
                    boolean containsVarType = (boolean) parameteres[1];
                    try {
                        Formatter out = new Formatter((String) parameteres[0]);
                        Object[] aux = interpreter.stackDump();
                        if (containsVarType) {
                            for (int i = 0; i < aux.length; i++) {
                                out.format("%d. %s - Type: %s\n", i + 1, interpreter.generateObjString(aux[i]), aux[i].getClass().getCanonicalName());
                            }
                        } else {
                            for (int i = 0; i < aux.length; i++) {
                                out.format("%d. %s\n", i + 1, interpreter.generateObjString(aux[i]));
                            }
                        }
                        out.close();
                    } catch (FileNotFoundException ex) {
                        interpreter.throwError("Error creating the File.");
                    }

                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("filepath", "The the path of the file to be created with the list of the variables", "String"),
            new CommandInfoAttribute("ContainsVarType", "Defines if the type of variable will be outputted into the file", "Boolean")
        };
        String[] noteList = {
            "The values will be written line by line in the file and will contain the string that identifies them and the type of variable(if chosen).",
            "If the interpreter is unable to create the file an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Dumps all of the objects of the stack onto the desired file.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new StackDumpCommand(this.getId(), this.getModule());
    }

}
