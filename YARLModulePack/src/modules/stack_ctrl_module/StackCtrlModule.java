package modules.stack_ctrl_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is used to control and manipulate the values and the operation of the stack.
 *
 * @author Ricardo Cacheira
 */
public class StackCtrlModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new FlipCommand(this), 
			new FlipStackCommand(this), 
			new FlushCommand(this), 
			new PeekCommand(this), 
			new PopCommand(this), 
			new ShowSimpleStackCommand(this), 
			new ShowStackCommand(this), 
			new StackDumpCommand(this), 
			new StackSizeCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"Pop",
			"Flush",
			"Flip",
			"FlipStack",
			"Peek",
			"StackSize",
			"StackDump",
			"ShowStack",
			"ShowSimpleStack"
		};
	}

	@Override
	public String moduleName() {
		return "Stack Control Module";
	}

	@Override
	public String help() {
		return "This module is used to control and manipulate the values and the operation of the stack.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new StackCtrlModule();
	}

}