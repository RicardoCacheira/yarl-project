package modules.stack_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Pop
 *
 * Module: Stack Control Module
 *
 * Description: Pops the top value from the stack.
 *
 * Usage: Pop
 *
 * @author Ricardo Cacheira
 */
public class PopCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Pop";

    public PopCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public PopCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.getParameters(1, COMMAND_NAME);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Pops the top value from the stack.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new PopCommand(this.getId(), this.getModule());
    }

}
