package modules.stack_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Flip
 *
 * Module: Stack Control Module
 *
 * Description: Flips the value on the top of the stack with the next value.
 *
 * ---Note---
 *
 * While the command doesn't have any attributes it still needs that two values
 * exist on the stack to flip them.
 *
 * ---Note---
 *
 * Usage: Flip
 *
 * @author Ricardo Cacheira
 */
public class FlipCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Flip";

    public FlipCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public FlipCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.flip();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "While the command doesn't have any attributes it still needs that two values exist on the stack to flip them."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Flips the value on the top of the stack with the next value.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new FlipCommand(this.getId(), this.getModule());
    }

}
