package modules.stack_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ShowSimpleStack
 *
 * Module: Stack Control Module
 *
 * Description: Prints all of the strings that identifies the objects of the
 * stack onto the shell.
 *
 * ---Note---
 *
 * The values will be printed line by line in the shell and will only contain
 * the string that identifies them.
 *
 * ---Note---
 *
 * Usage: ShowSimpleStack
 *
 * @author Ricardo Cacheira
 */
public class ShowSimpleStackCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ShowSimpleStack";

    public ShowSimpleStackCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ShowSimpleStackCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            StringBuilder sb = new StringBuilder("Stack: \n");
            Object[] aux = interpreter.stackDump();
            for (int i = aux.length - 1; i >= 0; i--) {
                sb.append(String.format(" %d. %s\n", i + 1, interpreter.generateObjString(aux[i])));
            }
            interpreter.showOutput(sb.toString(), YARLI.PLAIN_OUTPUT);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "The values will be printed line by line in the shell and will only contain the string that identifies them."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Prints all of the strings that identifies the objects of the stack onto the shell.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ShowSimpleStackCommand(this.getId(), this.getModule());
    }

}
