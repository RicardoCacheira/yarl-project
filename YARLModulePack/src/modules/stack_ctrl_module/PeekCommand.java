package modules.stack_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Peek
 *
 * Module: Stack Control Module
 *
 * Description: Duplicates and returns the object on the top of the stack.
 *
 * Return: The object on the top of the stack.
 *
 * Return Type: Object
 *
 * ---Attribute---
 *
 * Name: top
 * Description: The object on the top of the stack.
 * Type: Object
 *
 * ---Attribute---
 *
 * ---Note---
 *
 * While the command has the top value as an attribute it doesn't take it from
 * the stack.
 *
 * ---Note---
 *
 * Usage: < top > Peek -> Object
 *
 * @author Ricardo Cacheira
 */
public class PeekCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Peek";

    public PeekCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public PeekCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParametersCopy(1, COMMAND_NAME);
            if (parameteres != null) {
                interpreter.push(parameteres[0]);
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("top", "The object on the top of the stack.", "Object")
        };
        String[] noteList = {
            "While the command has the top value as an attribute it doesn't take it from the stack."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Duplicates and returns the object on the top of the stack.", "The object on the top of the stack.", "Object", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new PeekCommand(this.getId(), this.getModule());
    }

}
