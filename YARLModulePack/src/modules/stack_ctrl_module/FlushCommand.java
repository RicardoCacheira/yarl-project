package modules.stack_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Flush
 *
 * Module: Stack Control Module
 *
 * Description: Removes all values from the stack.
 *
 * Usage: Flush
 *
 * @author Ricardo Cacheira
 */
public class FlushCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Flush";

    public FlushCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public FlushCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.flush();
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Removes all values from the stack.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new FlushCommand(this.getId(), this.getModule());
    }

}
