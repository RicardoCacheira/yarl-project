package modules.stack_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ShowStack
 *
 * Module: Stack Control Module
 *
 * Description: Prints the identifing string and type of all the objects on the
 * stack onto the shell.
 *
 * ---Note---
 *
 * The values will be printed line by line in the shell and will contain the
 * string that identifies them and the respective type.
 *
 * ---Note---
 *
 * Usage: ShowStack
 *
 * @author Ricardo Cacheira
 */
public class ShowStackCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ShowStack";

    public ShowStackCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ShowStackCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            StringBuilder sb = new StringBuilder("Stack: \n");
            Object[] aux = interpreter.stackDump();
            for (int i = aux.length - 1; i >= 0; i--) {
                sb.append(String.format(" %d. %s - Type: %s\n", i + 1, interpreter.generateObjString(aux[i]), aux[i].getClass().getCanonicalName()));
            }
            interpreter.showOutput(sb.toString(), YARLI.PLAIN_OUTPUT);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {
            "The values will be printed line by line in the shell and will contain the string that identifies them and the respective type."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Prints the identifing string and type of all the objects on the stack onto the shell.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ShowStackCommand(this.getId(), this.getModule());
    }

}
