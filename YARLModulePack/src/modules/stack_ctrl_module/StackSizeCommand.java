package modules.stack_ctrl_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import rationalnumber.RationalNumber;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: StackSize
 *
 * Module: Stack Control Module
 *
 * Description: Returns the size of the stack.
 *
 * Return: The size of the stack.
 *
 * Return Type: Number
 *
 * Usage: StackSize -> Number
 *
 * @author Ricardo Cacheira
 */
public class StackSizeCommand extends ModuleCommand {

    public static String COMMAND_NAME = "StackSize";

    public StackSizeCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public StackSizeCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(new RationalNumber(interpreter.stackSize()));
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Returns the size of the stack.", "The size of the stack.", "Number", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new StackSizeCommand(this.getId(), this.getModule());
    }

}
