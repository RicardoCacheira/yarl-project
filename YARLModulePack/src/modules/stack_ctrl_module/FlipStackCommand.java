package modules.stack_ctrl_module;

import java.util.ArrayDeque;
import java.util.Deque;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: FlipStack
 *
 * Module: Stack Control Module
 *
 * Description: Flips the order of all the values on the stack.
 *
 * Usage: FlipStack
 *
 * @author Ricardo Cacheira
 */
public class FlipStackCommand extends ModuleCommand {
    
    public static String COMMAND_NAME = "FlipStack";
    
    public FlipStackCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }
    
    public FlipStackCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }
    
    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Deque aux = new ArrayDeque();
            int size = interpreter.stackSize();
            for (int i = 0; i < size; i++) {
                aux.add(interpreter.pop());
            }
            for (int i = 0; i < size; i++) {
                interpreter.push(aux.removeFirst());
            }
        }
    }
    
    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Flips the order of all the values on the stack.", attributeList, noteList);
    }
    
    @Override
    public IdDefinedCommand cloneObject() {
        return new FlipStackCommand(this.getId(), this.getModule());
    }
    
}
