package modules.logical_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: False
 *
 * Module: Logical Module
 *
 * Description: Pushes the boolean value False to the stack
 *
 * Return: The boolean value False
 *
 * Return Type: Boolean
 *
 * Usage: False -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class FalseCommand extends ModuleCommand {

    public static String COMMAND_NAME = "False";

    public FalseCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public FalseCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(false);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Pushes the boolean value False to the stack", "The boolean value False", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new FalseCommand(this.getId(), this.getModule());
    }

}
