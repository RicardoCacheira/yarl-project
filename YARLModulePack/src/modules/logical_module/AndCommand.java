package modules.logical_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: And
 *
 * Module: Logical Module
 *
 * Description: Performs the Logical operation AND to the two given attributes
 *
 * Return: The result of the logical operation
 *
 * Return Type: Boolean
 *
 * ---Attributes---
 *
 * Name: a
 * Description: The first attribute
 * Type: Boolean
 *
 * Name: b
 * Description: The second attribute
 * Type: Boolean
 *
 * ---Attributes---
 *
 * Usage: < a > < b > And -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class AndCommand extends ModuleCommand {

    public static String COMMAND_NAME = "And";

    public AndCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public AndCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof Boolean) && (parameteres[1] instanceof Boolean)) {
                    interpreter.push(((Boolean) parameteres[0]) && ((Boolean) parameteres[1]));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("a", "The first attribute", "Boolean"),
            new CommandInfoAttribute("b", "The second attribute", "Boolean")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Performs the Logical operation AND to the two given attributes", "The result of the logical operation", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new AndCommand(this.getId(), this.getModule());
    }

}
