package modules.logical_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ==
 *
 * Module: Logical Module
 *
 * Description: Checks if the two given attributes are equal
 *
 * Return: a.equals(b)
 *
 * Return Type: Boolean
 *
 * ---Attributes---
 *
 * Name: a
 * Description: The first attribute
 * Type: Object
 *
 * Name: b
 * Description: The second attribute
 * Type: Object
 *
 * ---Attributes---
 *
 * Usage: < a > < b > == -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class EqualsCommand extends ModuleCommand {

    public static String COMMAND_NAME = "==";

    public EqualsCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public EqualsCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                interpreter.push(parameteres[0].equals(parameteres[1]));
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("a", "The first attribute", "Object"),
            new CommandInfoAttribute("b", "The second attribute", "Object")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Checks if the two given attributes are equal", "a.equals(b)", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new EqualsCommand(this.getId(), this.getModule());
    }

}
