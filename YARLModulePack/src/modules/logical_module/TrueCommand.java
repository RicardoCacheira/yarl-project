package modules.logical_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: True
 *
 * Module: Logical Module
 *
 * Description: Pushes the boolean value True to the stack
 *
 * Return: The boolean value True
 *
 * Return Type: Boolean
 *
 * Usage: True -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class TrueCommand extends ModuleCommand {

    public static String COMMAND_NAME = "True";

    public TrueCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public TrueCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            interpreter.push(true);
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {};
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Pushes the boolean value True to the stack", "The boolean value True", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new TrueCommand(this.getId(), this.getModule());
    }

}
