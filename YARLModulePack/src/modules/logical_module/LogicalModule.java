package modules.logical_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module is reponsible for the logical operations.
 *
 * @author Ricardo Cacheira
 */
public class LogicalModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new AndCommand(this), 
			new EqualsCommand(this), 
			new FalseCommand(this), 
			new GreaterCommand(this), 
			new GreaterEqualCommand(this), 
			new LessCommand(this), 
			new LessEqualCommand(this), 
			new NotCommand(this), 
			new NotEqualsCommand(this), 
			new OrCommand(this), 
			new TrueCommand(this), 
			new XorCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"True",
			"False",
			"And",
			"Or",
			"Xor",
			"Not",
			"==",
			"!=",
			"<",
			"<=",
			">",
			">="
		};
	}

	@Override
	public String moduleName() {
		return "Logical Module";
	}

	@Override
	public String help() {
		return "This module is reponsible for the logical operations.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new LogicalModule();
	}

}