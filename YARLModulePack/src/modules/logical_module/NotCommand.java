package modules.logical_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: Not
 *
 * Module: Logical Module
 *
 * Description: Performs the Logical operation NOT to the given attribute
 *
 * Return: The result of the logical operation
 *
 * Return Type: Boolean
 *
 * ---Attribute---
 *
 * Name: a
 * Description: The attribute to invert
 * Type: Boolean
 *
 * ---Attribute---
 *
 * Usage: < a > Not -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class NotCommand extends ModuleCommand {

    public static String COMMAND_NAME = "Not";

    public NotCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public NotCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(1, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[0] instanceof Boolean) {
                    interpreter.push(!((Boolean) parameteres[0]));
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("a", "The attribute to invert", "Boolean")
        };
        String[] noteList = {};
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Performs the Logical operation NOT to the given attribute", "The result of the logical operation", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new NotCommand(this.getId(), this.getModule());
    }

}
