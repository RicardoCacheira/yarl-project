package modules.logical_module;

import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: <
 *
 * Module: Logical Module
 *
 * Description: Checks if the first attribute is less than the second
 *
 * Return: a.compare(b) < 0
 *
 * Return Type: Boolean
 *
 * ---Attributes---
 *
 * Name: a
 * Description: The first attribute
 * Type: Object
 *
 * Name: b
 * Description: The second attribute
 * Type: Object
 *
 * ---Attributes---
 *
 * ---Note---
 *
 * If the Objects are not of the same type or don't implement Comparable an
 * error will be thrown.
 *
 * ---Note---
 *
 * Usage: < a > < b >
 * < -> Boolean
 *
 * @author Ricardo Cacheira
 */
public class LessCommand extends ModuleCommand {

    public static String COMMAND_NAME = "<";

    public LessCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public LessCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof Comparable) && (parameteres[1].getClass().equals(parameteres[0].getClass()))) {
                    interpreter.push(((Comparable) parameteres[0]).compareTo(parameteres[1]) < 0);
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("a", "The first attribute", "Object"),
            new CommandInfoAttribute("b", "The second attribute", "Object")
        };
        String[] noteList = {
            "If the Objects are not of the same type or don't implement Comparable an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Checks if the first attribute is less than the second", "a.compare(b) < 0", "Boolean", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new LessCommand(this.getId(), this.getModule());
    }

}
