package modules.file_io_module;

import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.YARLI;

/**
 * This module contains the commands responsible for the interaction with the files in the system.
 *
 * @author Ricardo Cacheira
 */
public class FileIOModule implements YARLI_Module {

	@Override
	public void loadModule(YARLI interpreter) {
		interpreter.addModuleCommands(new ModuleCommand[]{
			new AppendToFileCommand(this), 
			new ReadFileCommand(this), 
			new WriteToFileCommand(this)
		});
	}

	@Override
	public void resetModule(YARLI interpreter) {
	}

	@Override
	public String[] commandList() {
		return new String[]{
			"ReadFile",
			"WriteToFile",
			"AppendToFile"
		};
	}

	@Override
	public String moduleName() {
		return "File I/O Module";
	}

	@Override
	public String help() {
		return "This module contains the commands responsible for the interaction with the files in the system.";
	}

	@Override
	public YARLI_Module cloneObject() {
		return new FileIOModule();
	}

}