package modules.file_io_module;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import rationalnumber.RationalNumber;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;
import yarli.variable.Array;
import yarli.variable.Vector;

/**
 * Name: WriteToFile
 *
 * Module: File I/O Module
 *
 * Description: Writes the contents of the given Array/Vector into the file with
 * the desired path.
 *
 * ---Attributes---
 *
 * Name: attr
 * Description: The number of the values of the stack to write into the file, if
 * it's a Number, the String to write into the file, if it's a String, the
 * Array/Vector containing the values to write into the file, if it's an
 * Array/Vector
 * Type: Number/Array/Vector/String
 *
 * Name: FilePath
 * Description: The path of the file to create/write
 * Type: String
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * If the interpreter is unable to create/write the file an error will be
 * thrown.
 *
 * When attr is a Number it must be a positive integer Number, or else an error
 * will be thrown.
 *
 * If not enough objects exist in the stack an error will be thrown.
 *
 * The Objects will be concatnated from the bottom to the top of the stack.
 *
 * Each position of the Array/Vector will be outputted as a line.
 *
 * Any object that isn't a string will be outputted as its representative
 * string.
 *
 * ---Notes---
 *
 * Usage: < attr > < FilePath > WriteToFile
 *
 * @author Ricardo Cacheira
 */
public class WriteToFileCommand extends ModuleCommand {

    public static String COMMAND_NAME = "WriteToFile";

    public WriteToFileCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public WriteToFileCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    private void writeToFile(String fileName, Object[] list, YARLI interpreter) {
        try {
            Formatter out = new Formatter(new File(fileName));
            for (int i = 0; i < list.length; i++) {
                Object aux = list[i];
                if (aux instanceof String) {
                    out.format("%s\n", (String) aux);
                } else {
                    out.format("%s\n", interpreter.generateObjString(aux));
                }
            }
            out.close();
        } catch (FileNotFoundException ex) {
            interpreter.throwError(String.format("Unable to create/write file \"%s\".", fileName));
        }
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if (parameteres[1] instanceof String) {
                    String fileName = (String) parameteres[1];
                    if (parameteres[0] instanceof RationalNumber) {
                        int size = interpreter.generateNonNegativeInt((RationalNumber) parameteres[0]);
                        if (size <= interpreter.stackSize() && size > 0) {
                            Object[] stack = interpreter.getParameters(size, COMMAND_NAME);
                            this.writeToFile(fileName, stack, interpreter);
                        } else {
                            interpreter.throwError("The Received Number is not a valid stack size.");
                        }
                    } else if (parameteres[0] instanceof Vector) {
                        Vector vec = (Vector) parameteres[0];
                        this.writeToFile(fileName, vec.getCopy(), interpreter);
                    } else if (parameteres[0] instanceof Array) {
                        Array array = (Array) parameteres[0];
                        this.writeToFile(fileName, array.getCopy(), interpreter);
                    } else if (parameteres[0] instanceof String) {
                        this.writeToFile(fileName, new Object[]{parameteres[0]}, interpreter);
                    } else {
                        interpreter.throwError("Invalid type of parameters.");
                    }
                } else {
                    interpreter.throwError("Invalid type of parameters.");
                }
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("attr", "The number of the values of the stack to write into the file, if it's a Number, the String to write into the file, if it's a String, the Array/Vector containing the values to write into the file, if it's an Array/Vector", "Number/Array/Vector/String"),
            new CommandInfoAttribute("FilePath", "The path of the file to create/write", "String")
        };
        String[] noteList = {
            "If the interpreter is unable to create/write the file an error will be thrown.",
            "When attr is a Number it must be a positive integer Number, or else an error will be thrown.",
            "If not enough objects exist in the stack an error will be thrown.",
            "The Objects will be concatnated from the bottom to the top of the stack.",
            "Each position of the Array/Vector will be outputted as a line.",
            "Any object that isn't a string will be outputted as its representative string."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Writes the contents of the given Array/Vector into the file with the desired path.", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new WriteToFileCommand(this.getId(), this.getModule());
    }

}
