package modules.file_io_module;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import yarli.command.IdDefinedCommand;
import yarli.command.IdDefinedCommandInfo;
import yarli.YARLI_Module;
import yarli.command.ModuleCommand;
import yarli.command.ModuleCommandInfo;
import yarli.command.CommandInfoAttribute;
import yarli.YARLI;

/**
 * Name: ReadFile
 *
 * Module: File I/O Module
 *
 * Description: Reads the contents of the file with the given path into the
 * desired Array/Vector
 *
 * ---Attributes---
 *
 * Name: FilePath
 * Description: The path of the file to that we desire to read
 * Type: String
 *
 * Name: Array/Vector Id
 * Description: The id of the Array/Vector that will contain the lines of the
 * loaded file
 * Type: String
 *
 * ---Attributes---
 *
 * ---Notes---
 *
 * If the interpreter is unable to load the file an error will be thrown.
 *
 * if it's an Array, then it will be cleared and then all the lines of the file
 * will be loaded in order into the array
 *
 * if it's an Vector, then all the lines of the file will be loaded in order
 * into the vector until theres no more space, if the file is fully loaded and
 * there are positions without any loaded lines then an emply string will be
 * loaded instead in each.
 *
 * If the variable doesn't exist, it will be created as an Array.
 *
 * If the id already exists and it's a variable that isn't an Array or a Vector
 * it will be overwritten as an Array, if it's a command an error will be
 * thrown.
 *
 * ---Notes---
 *
 * Usage: < FilePath > < Array/Vector Id > ReadFile
 *
 * @author Ricardo Cacheira
 */
public class ReadFileCommand extends ModuleCommand {

    public static String COMMAND_NAME = "ReadFile";

    public ReadFileCommand(String commandName, YARLI_Module module) {
        super(commandName, module);
    }

    public ReadFileCommand(YARLI_Module module) {
        this(COMMAND_NAME, module);
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter != null) {
            Object[] parameteres = interpreter.getParameters(2, COMMAND_NAME);
            if (parameteres != null) {
                if ((parameteres[0] instanceof String) && (parameteres[1] instanceof String)) {
                    String filename = (String) parameteres[0];
                    String id = (String) parameteres[1];
                    if (interpreter.isValidId(id)) {
                        if (!interpreter.hasCommand(id)) {
                            boolean hasArray = interpreter.hasArray(id);
                            boolean hasVec = interpreter.hasVector(id);
                            if (interpreter.hasVariable(id)) {
                                if (!hasArray && !hasVec) {
                                    interpreter.removeVariable(id);
                                    interpreter.newArray(id);
                                    hasArray = true;
                                }
                            } else {
                                interpreter.newArray(id);
                                hasArray = true;
                            }
                            try {
                                Scanner in = new Scanner(new File(filename));
                                if (hasArray) {
                                    interpreter.clearArray(id);
                                    while (in.hasNextLine()) {
                                        interpreter.addObjToArray(id, in.nextLine());
                                    }
                                    in.close();
                                } else {
                                    int vecSize = interpreter.getArrayVecSize(id);
                                    for (int i = 0; i < vecSize; i++) {
                                        if (in.hasNextLine()) {
                                            interpreter.setArrayVectorPos(id, i, in.nextLine());
                                        } else {
                                            interpreter.setArrayVectorPos(id, i, "");
                                        }
                                    }
                                    in.close();
                                }
                            } catch (FileNotFoundException ex) {
                                interpreter.throwError(String.format("Unable to open file \"%s\".", filename));
                            }
                        } else {
                            interpreter.throwError("The given id is already a defined command.");
                        }
                    } else {
                        interpreter.throwError("The given id is not a valid id.");
                    }
                }
            } else {
                interpreter.throwError("Invalid type of parameters.");
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        CommandInfoAttribute[] attributeList = {
            new CommandInfoAttribute("FilePath", "The path of the file to that we desire to read", "String"),
            new CommandInfoAttribute("Array/Vector Id", "The id of the Array/Vector that will contain the lines of the loaded file", "String")
        };
        String[] noteList = {
            "If the interpreter is unable to load the file an error will be thrown.",
            "if it's an Array, then it will be cleared and then all the lines of the file will be loaded in order into the array",
            "if it's an Vector, then all the lines of the file will be loaded in order into the vector until theres no more space, if the file is fully loaded and there are positions without any loaded lines then an emply string will be loaded instead in each.",
            "If the variable doesn't exist, it will be created as an Array.",
            "If the id already exists and it's a variable that isn't an Array or a Vector it will be overwritten as an Array, if it's a command an error will be thrown."
        };
        return new ModuleCommandInfo(this.getId(), this.getModule().moduleName(), "Reads the contents of the file with the given path into the desired Array/Vector", attributeList, noteList);
    }

    @Override
    public IdDefinedCommand cloneObject() {
        return new ReadFileCommand(this.getId(), this.getModule());
    }

}
