package loader;

import yarli.YARLI_Module;
import yarli.YARLI;
import modules.algebraic_module.AlgebraicModule;
import modules.command_ctrl_module.ComCtrlModule;
import modules.command_flow_module.CommandFlowModule;
import modules.error_ctrl_module.ErrorCtrlModule;
import modules.file_io_module.FileIOModule;
import modules.io_module.IOModule;
import modules.logical_module.LogicalModule;
import modules.stack_ctrl_module.StackCtrlModule;
import modules.str_manipulation_module.StringManipulationModule;
import modules.system_module.SystemModule;
import modules.type_conversion_module.TypeConversionModule;
import modules.var_ctrl_module.VarCtrlModule;
import modules.warning_ctrl_module.WarningCtrlModule;

/**
 * This module pack contains all the commands that define YARL.
 *
 * @author Ricardo Cacheira
 */
public class ModulePackLoader {

	public static YARLI_Module[] loadModules(YARLI interpreter) {
		return new YARLI_Module[]{
			new AlgebraicModule(), 
			new ComCtrlModule(), 
			new CommandFlowModule(), 
			new ErrorCtrlModule(), 
			new FileIOModule(), 
			new IOModule(), 
			new LogicalModule(), 
			new StackCtrlModule(), 
			new StringManipulationModule(), 
			new SystemModule(), 
			new TypeConversionModule(), 
			new VarCtrlModule(), 
			new WarningCtrlModule()
		};
	}
}