/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarlide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import utils.Utils;

/**
 *
 * @author Ricardo
 */
public class YARLIDE_Properties {

    public static enum DefaultTab {
        INTERPRETER, EDITOR
    }
    private static final String DEFAULT_FILE = "/resources/properties/defaultProperties.xml";

    private final DefaultTab defaultTab;

    private final String modulesFolder;

    private final String themesFolder;

    private final String theme;

    private final int[] mainWindowSize;

    private final int[] secondaryWindowSize;

    private final boolean showLogAtStart;

    private final boolean showIdPane;

    private final boolean closeTabOnExit;

    private final boolean exitAfterRun;

    private final boolean showHTMLInShell;

    public YARLIDE_Properties(DefaultTab defaultTab,
            String modulesFolder,
            String themesFolder,
            String theme,
            int[] mainWindowSize,
            int[] secondaryWindowSize,
            boolean showLogAtStart,
            boolean showIdPane,
            boolean closeTabOnExit,
            boolean exitAfterRun,
            boolean showHTMLInShell) throws IllegalArgumentException {
        this.defaultTab = defaultTab;
        this.modulesFolder = modulesFolder;
        this.themesFolder = themesFolder;
        this.theme = theme;
        this.mainWindowSize = mainWindowSize;
        this.secondaryWindowSize = secondaryWindowSize;
        this.showLogAtStart = showLogAtStart;
        this.showIdPane = showIdPane;
        this.closeTabOnExit = closeTabOnExit;
        this.exitAfterRun = exitAfterRun;
        this.showHTMLInShell = showHTMLInShell;
        check();
    }

    public YARLIDE_Properties(String defaultTab,
            String modulesFolder,
            String themesFolder,
            String theme,
            String mainWindowSize,
            String secondaryWindowSize,
            String showLogAtStart,
            String showIdPane,
            String closeTabOnExit,
            String exitAfterRun,
            String showHTMLInShell) throws IllegalArgumentException {
        this.modulesFolder = modulesFolder;
        this.themesFolder = themesFolder;
        this.theme = theme;
        this.showLogAtStart = Boolean.parseBoolean(showLogAtStart);
        this.showIdPane = Boolean.parseBoolean(showIdPane);
        this.closeTabOnExit = Boolean.parseBoolean(closeTabOnExit);
        this.exitAfterRun = Boolean.parseBoolean(exitAfterRun);
        this.showHTMLInShell = Boolean.parseBoolean(showHTMLInShell);

        switch (defaultTab.toLowerCase()) {
            case "interpreter":
                this.defaultTab = DefaultTab.INTERPRETER;
                break;
            case "editor":
                this.defaultTab = DefaultTab.EDITOR;
                break;
            default:
                throw new IllegalArgumentException("DefaultTab must be \"Interpreter\" or \"Editor\".");
        }

        String[] mainWindowSizeStr = mainWindowSize.split("x");
        this.mainWindowSize = new int[2];
        if (mainWindowSizeStr.length == 2) {
            try {
                this.mainWindowSize[0] = Integer.parseInt(mainWindowSizeStr[0]);
                this.mainWindowSize[1] = Integer.parseInt(mainWindowSizeStr[1]);
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Invalid MainWindowSize String.");
            }
        } else {
            throw new IllegalArgumentException("Invalid MainWindowSize String.");
        }

        String[] secondaryWindowSizeStr = secondaryWindowSize.split("x");
        this.secondaryWindowSize = new int[2];
        if (secondaryWindowSizeStr.length == 2) {
            try {
                this.secondaryWindowSize[0] = Integer.parseInt(secondaryWindowSizeStr[0]);
                this.secondaryWindowSize[1] = Integer.parseInt(secondaryWindowSizeStr[1]);
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Invalid SecondaryWindowSize String.");
            }
        } else {
            throw new IllegalArgumentException("Invalid SecondaryWindowSize String.");
        }
        check();
    }

    private void check() throws IllegalArgumentException {
        if (modulesFolder.isEmpty()) {
            throw new IllegalArgumentException("ModulesFolder value cannot be empty.");
        }
        if (themesFolder.isEmpty()) {
            throw new IllegalArgumentException("ThemesFolder value cannot be empty.");
        }
        if (theme.isEmpty()) {
            throw new IllegalArgumentException("Theme value cannot be empty.");
        }
        if (this.mainWindowSize[0] <= 0 || this.mainWindowSize[1] <= 0) {
            throw new IllegalArgumentException("The Main Window Size values must be positive numbers.");
        }
        if (this.secondaryWindowSize[0] <= 0 || this.secondaryWindowSize[1] <= 0) {
            throw new IllegalArgumentException("The Secondary Window Size values must be positive numbers.");
        }
    }

    /**
     * @return the defaultTab
     */
    public DefaultTab getDefaultTab() {
        return defaultTab;
    }

    /**
     * @return the modulesFolder
     */
    public String getModulesFolder() {
        return modulesFolder;
    }

    /**
     * @return the themesFolder
     */
    public String getThemesFolder() {
        return themesFolder;
    }

    /**
     * @return the theme
     */
    public String getTheme() {
        return theme;
    }

    /**
     * @return the mainWindowSize
     */
    public int[] getMainWindowSize() {
        return mainWindowSize;
    }

    /**
     * @return the secondaryWindowSize
     */
    public int[] getSecondaryWindowSize() {
        return secondaryWindowSize;
    }

    /**
     * @return the showLogAtStart
     */
    public boolean isShowLogAtStart() {
        return showLogAtStart;
    }

    /**
     * @return the showIdPane
     */
    public boolean isShowIdPane() {
        return showIdPane;
    }

    /**
     * @return the closeTabOnExit
     */
    public boolean isCloseTabOnExit() {
        return closeTabOnExit;
    }

    /**
     * @return the exitAfterRun
     */
    public boolean isExitAfterRun() {
        return exitAfterRun;
    }

    /**
     * @return the showHTMLInShell
     */
    public boolean isShowHTMLInShell() {
        return showHTMLInShell;
    }

    public static YARLIDE_Properties loadDefaultPropertiesFile() {
        try {
            return YARLIDE_Properties.loadPropertiesFile(new File(Utils.getResourceURL(DEFAULT_FILE).toURI()));
        } catch (URISyntaxException ex) {
            System.out.println("URISyntaxException loading the default properties.");
        } catch (ParserConfigurationException ex) {
            System.out.println("ParserConfigurationException loading the default properties.");
        } catch (org.xml.sax.SAXException ex) {
            System.out.println("SAXException loading the default properties.");
        } catch (IOException ex) {
            System.out.println("IOException loading the default properties.");
        }
        return null;
    }

    public static YARLIDE_Properties loadPropertiesFile(File propFile) throws IllegalArgumentException, ParserConfigurationException, org.xml.sax.SAXException, IOException {

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(propFile);
        doc.getDocumentElement().normalize();
        NodeList auxList = doc.getElementsByTagName("DefaultTab");
        if (auxList.getLength() == 1) {
            String defaultTab = auxList.item(0).getTextContent();
            auxList = doc.getElementsByTagName("ModulesFolder");
            if (auxList.getLength() == 1) {
                String modulesFolder = auxList.item(0).getTextContent();
                auxList = doc.getElementsByTagName("ThemesFolder");
                if (auxList.getLength() == 1) {
                    String themesFolder = auxList.item(0).getTextContent();
                    auxList = doc.getElementsByTagName("Theme");
                    if (auxList.getLength() == 1) {
                        String theme = auxList.item(0).getTextContent();
                        auxList = doc.getElementsByTagName("MainWindowSize");
                        if (auxList.getLength() == 1) {
                            String mainWindowSize = auxList.item(0).getTextContent();
                            auxList = doc.getElementsByTagName("SecondaryWindowSize");
                            if (auxList.getLength() == 1) {
                                String secondaryWindowSize = auxList.item(0).getTextContent();
                                auxList = doc.getElementsByTagName("ShowLogAtStart");
                                if (auxList.getLength() == 1) {
                                    String showLogAtStart = auxList.item(0).getTextContent();
                                    auxList = doc.getElementsByTagName("ShowIdPane");
                                    if (auxList.getLength() == 1) {
                                        String showIdPane = auxList.item(0).getTextContent();
                                        auxList = doc.getElementsByTagName("CloseTabOnExit");
                                        if (auxList.getLength() == 1) {
                                            String closeTabOnExit = auxList.item(0).getTextContent();
                                            auxList = doc.getElementsByTagName("ExitAfterRun");
                                            if (auxList.getLength() == 1) {
                                                String exitAfterRun = auxList.item(0).getTextContent();
                                                auxList = doc.getElementsByTagName("ShowHTMLInShell");
                                                if (auxList.getLength() == 1) {
                                                    String showHTMLInShell = auxList.item(0).getTextContent();
                                                    return new YARLIDE_Properties(defaultTab, modulesFolder, themesFolder, theme, mainWindowSize, secondaryWindowSize, showLogAtStart, showIdPane, closeTabOnExit, exitAfterRun, showHTMLInShell);
                                                } else {
                                                    throw new IllegalArgumentException("Invalid number of ShowHTMLInShell tags found.");
                                                }
                                            } else {
                                                throw new IllegalArgumentException("Invalid number of ExitAfterRun tags found.");
                                            }
                                        } else {
                                            throw new IllegalArgumentException("Invalid number of CloseTabOnExit tags found.");
                                        }
                                    } else {
                                        throw new IllegalArgumentException("Invalid number of ShowIdPane tags found.");
                                    }
                                } else {
                                    throw new IllegalArgumentException("Invalid number of ShowLogAtStart tags found.");
                                }
                            } else {
                                throw new IllegalArgumentException("Invalid number of SecondaryWindowSize tags found.");
                            }
                        } else {
                            throw new IllegalArgumentException("Invalid number of MainWindowSize tags found.");
                        }
                    } else {
                        throw new IllegalArgumentException("Invalid number of Theme tags found.");
                    }
                } else {
                    throw new IllegalArgumentException("Invalid number of ThemesFolder tags found.");
                }
            } else {
                throw new IllegalArgumentException("Invalid number of ModulesFolder tags found.");
            }
        } else {
            throw new IllegalArgumentException("Invalid number of DefaultTab tags found.");
        }

    }

    public boolean saveProperties(String filename) {
        try {

            FileOutputStream out = new FileOutputStream(filename);
            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter = xMLOutputFactory.createXMLStreamWriter(out, "UTF-8");

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeCharacters("\n");
            xMLStreamWriter.writeStartElement("Properties");
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("DefaultTab");
            if (this.getDefaultTab() == DefaultTab.INTERPRETER) {
                xMLStreamWriter.writeCharacters("Interpreter");
            } else {
                xMLStreamWriter.writeCharacters("Editor");
            }
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("ModulesFolder");
            xMLStreamWriter.writeCharacters(this.getModulesFolder());
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("ThemesFolder");
            xMLStreamWriter.writeCharacters(this.getThemesFolder());
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("Theme");
            xMLStreamWriter.writeCharacters(this.getTheme());
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("MainWindowSize");
            xMLStreamWriter.writeCharacters(this.mainWindowSize[0] + "x" + this.mainWindowSize[1]);
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("SecondaryWindowSize");
            xMLStreamWriter.writeCharacters(this.secondaryWindowSize[0] + "x" + this.secondaryWindowSize[1]);
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("ShowLogAtStart");
            xMLStreamWriter.writeCharacters("" + this.isShowLogAtStart());
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("ShowIdPane");
            xMLStreamWriter.writeCharacters("" + this.isShowIdPane());
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("CloseTabOnExit");
            xMLStreamWriter.writeCharacters("" + this.isCloseTabOnExit());
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("ExitAfterRun");
            xMLStreamWriter.writeCharacters("" + this.isExitAfterRun());
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n\t");

            xMLStreamWriter.writeStartElement("ShowHTMLInShell");
            xMLStreamWriter.writeCharacters("" + this.isShowHTMLInShell());
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeCharacters("\n");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndDocument();
            xMLStreamWriter.writeCharacters("\n");
            xMLStreamWriter.flush();
            xMLStreamWriter.close();

            out.close();
            return true;
        } catch (XMLStreamException e) {
            System.out.println("XMLStreamException saving the properties.");

        } catch (IOException e) {
            System.out.println("IOException saving the properties.");
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DefaultTab: ");
        sb.append(this.defaultTab);
        sb.append('\n');
        sb.append("ModulesFolder: ");
        sb.append(this.modulesFolder);
        sb.append('\n');
        sb.append("ThemesFolder: ");
        sb.append(this.themesFolder);
        sb.append('\n');
        sb.append("Theme: ");
        sb.append(this.theme);
        sb.append('\n');
        sb.append("MainWindowSize: ");
        sb.append(this.mainWindowSize[0]);
        sb.append("x");
        sb.append(this.mainWindowSize[1]);
        sb.append('\n');
        sb.append("SecondaryWindowSize: ");
        sb.append(this.secondaryWindowSize[0]);
        sb.append("x");
        sb.append(this.secondaryWindowSize[1]);
        sb.append('\n');
        sb.append("ShowLogAtStart: ");
        sb.append(this.showLogAtStart);
        sb.append('\n');
        sb.append("ShowIdPane: ");
        sb.append(this.showIdPane);
        sb.append('\n');
        sb.append("CloseTabOnExit: ");
        sb.append(this.closeTabOnExit);
        sb.append('\n');
        sb.append("ExitAfterRun: ");
        sb.append(this.exitAfterRun);
        sb.append('\n');
        sb.append("ShowHTMLInShell: ");
        sb.append(this.showHTMLInShell);
        sb.append('\n');
        return sb.toString();
    }
}
