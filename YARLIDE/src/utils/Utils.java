package utils;

import java.awt.Toolkit;
import java.net.URL;
import javax.swing.ImageIcon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ricardo
 */
public class Utils {

    public static URL getResourceURL(String resourcePath) {
        return Utils.class.getResource(resourcePath);
    }
    
    public static ImageIcon getImageResource(String imagePath) {
       return new ImageIcon(Toolkit.getDefaultToolkit().getImage(Utils.getResourceURL(imagePath)));
    }
}
