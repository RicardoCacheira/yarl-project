/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.swing.JPanel;

/**
 *
 * @author Ricardo
 */
public class TabPanel extends JPanel {

    private final long tabId;

    public TabPanel(long tabId) {
        super();
        this.tabId = tabId;
    }

    public long tabId() {
        return this.tabId;
    }

}
