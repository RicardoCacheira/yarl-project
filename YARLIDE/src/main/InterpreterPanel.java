/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Semaphore;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import yarli.io.YARLI_Input;
import yarli.io.YARLI_Output;
import yarli.YARLI;
import yarli.command.IdDefinedCommand;
import yarli.stop.StopHandler;
import yarli.variable.YARLI_Variable;

/**
 *
 * @author Ricardo
 */
public final class InterpreterPanel extends TabPanel implements YARLI_Input, YARLI_Output, StopHandler, Observer {

    public static final String INPUT_REQUEST = "Input:";
    public static final String NO_INPUT_REQUEST = "Command:";
    private JTextPane outputTextPane;
    private JTextField inputTextField;
    private YARLI interpreter;
    private Semaphore readSem;
    private boolean isReading;
    private JLabel ioRequest;
    private List<String> commandRegistry;
    private int registryPosition;
    private Font systemFont;
    private Color backgroundColor;
    private Style plainStyle;
    private Style infoStyle;
    private Style warningStyle;
    private Style errorStyle;
    private YARLIDE_Main mainInstance;

    public InterpreterPanel(YARLIDE_Main mainInstance, long tabId) {
        super(tabId);
        initVars(mainInstance);
        initComponents();
        initStyles();
        initActions();
        startUI();
        initInterpreter();
    }

    public void initVars(YARLIDE_Main newInstanceHandler) {
        this.mainInstance = newInstanceHandler;
        this.readSem = new Semaphore(0);
        this.isReading = false;
        this.commandRegistry = new ArrayList<>();
        this.registryPosition = 0;
    }

    public void initComponents() {
        this.systemFont = new Font("Courier New", Font.PLAIN, 13);
        backgroundColor = Color.WHITE;
        LayoutManager lm = new BorderLayout();
        setLayout(lm);
        outputTextPane = new JTextPane();
        outputTextPane.setContentType("text/html");
        outputTextPane.setBackground(backgroundColor);
        outputTextPane.setFont(this.systemFont);
        outputTextPane.setEditable(false);
        JScrollPane scroll = new JScrollPane(outputTextPane);
        JPanel inputPanel = new JPanel(new BorderLayout());
        ioRequest = new JLabel(NO_INPUT_REQUEST);
        ioRequest.setFont(systemFont);
        inputTextField = new JTextField();
        inputTextField.setFocusTraversalKeysEnabled(false);//So the tab works
        inputTextField.setBackground(backgroundColor);
        inputTextField.setFont(this.systemFont);
        inputPanel.add(ioRequest, BorderLayout.WEST);
        inputPanel.add(inputTextField, BorderLayout.CENTER);
        add(scroll, BorderLayout.CENTER);
        add(inputPanel, BorderLayout.SOUTH);

    }

    public void initStyles() {
        plainStyle = outputTextPane.addStyle("plainStyle", null);
        StyleConstants.setForeground(plainStyle, Color.BLACK);
        infoStyle = outputTextPane.addStyle("infoStyle", null);
        StyleConstants.setForeground(infoStyle, Color.BLUE);
        warningStyle = outputTextPane.addStyle("warningStyle", null);
        StyleConstants.setForeground(warningStyle, Color.ORANGE);
        errorStyle = outputTextPane.addStyle("errorStyle", null);
        StyleConstants.setForeground(errorStyle, Color.RED);
        StyleConstants.setBold(errorStyle, true);
    }

    public void initActions() {
        inputTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (isReading) {
                    InterpreterPanel.this.readSem.release();
                } else {
                    String buffer = inputTextField.getText();
                    InterpreterPanel.this.interpreter.addCommandToBuffer(buffer);
                    InterpreterPanel.this.addToRegistryList(buffer);
                    inputTextField.setText("");
                }
            }
        });
        Action upAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                InterpreterPanel.this.showPreviousCommand();
            }
        };
        String up = "UP";
        KeyStroke upKeyStroke = KeyStroke.getKeyStroke(up);
        inputTextField.getInputMap().put(upKeyStroke, up);
        inputTextField.getActionMap().put(up, upAction);

        Action downAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                InterpreterPanel.this.showNextCommand();
            }
        };
        String down = "DOWN";
        KeyStroke downKeyStroke = KeyStroke.getKeyStroke(down);
        inputTextField.getInputMap().put(downKeyStroke, down);
        inputTextField.getActionMap().put(down, downAction);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                InterpreterPanel.this.showPossibleIdsAndAutoComplete();
            }
        };
        String tab = "TAB";
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(tab);
        inputTextField.getInputMap().put(tabKeyStroke, tab);
        inputTextField.getActionMap().put(tab, tabAction);
    }

    private void startUI() {
        /*setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("YARLI");
        pack();
        setSize(640, 480);
        setLocationRelativeTo(null);
        setVisible(true);*/
    }

    private void initInterpreter() {
        this.interpreter = this.mainInstance.mainInterpreter().createNewInstance("YARLI", this, this, this, this.mainInstance);//new YARLI(, this, this, );//mudar o exit handler
        //interpreter.addObserver(this);
        new Thread(interpreter).start();
        showOutput("\nYARLI - Yet Another RPN Language Interpreter\n", YARLI.PLAIN_OUTPUT);
    }

    private void addToRegistryList(String str) {
        if (!str.trim().isEmpty()) {
            if (this.registryPosition < this.commandRegistry.size()) {
                if (str.equals(this.commandRegistry.get(this.registryPosition))) {
                    this.commandRegistry.add(this.commandRegistry.remove(this.registryPosition));
                } else {
                    this.commandRegistry.add(str);
                }
            } else {
                this.commandRegistry.add(str);
            }
        }
        this.registryPosition = this.commandRegistry.size();
    }

    private void showPreviousCommand() {
        //is in the beginning of the registry
        if (this.registryPosition > 0) {
            if (!(this.registryPosition < this.commandRegistry.size()) || (this.commandRegistry.get(this.registryPosition).equals(this.inputTextField.getText()))) {
                this.registryPosition--;
            }
            this.inputTextField.setText(this.commandRegistry.get(this.registryPosition));
        } else if (!this.commandRegistry.isEmpty() && !this.commandRegistry.get(0).equals(this.inputTextField.getText())) {
            this.inputTextField.setText(this.commandRegistry.get(0));
        }
    }

    private void showNextCommand() {
        //is not in the end
        if (this.registryPosition < this.commandRegistry.size()) {
            this.registryPosition++;
            if (this.registryPosition != this.commandRegistry.size()) {
                this.inputTextField.setText(this.commandRegistry.get(this.registryPosition));
            } else {
                this.inputTextField.setText("");
            }
        }
    }

    @Override
    public String readInput() {
        try {
            this.isReading = true;
            this.ioRequest.setText(INPUT_REQUEST);
            this.readSem.acquire();
            String buffer = this.inputTextField.getText();
            this.inputTextField.setText("");
            this.ioRequest.setText(NO_INPUT_REQUEST);
            this.isReading = false;
            return buffer;
        } catch (InterruptedException ex) {
            System.out.println("oops");
            return "";
        }

    }

    @Override
    public void showOutput(Object obj, int type) {
        StyledDocument styledDoc = outputTextPane.getStyledDocument();
        int size = styledDoc.getLength();
        try {
            switch (type) {
                case YARLI.PLAIN_OUTPUT:
                    styledDoc.insertString(size, obj.toString(), plainStyle);
                    break;
                case YARLI.INFO_OUTPUT:
                    styledDoc.insertString(size, obj.toString(), infoStyle);
                    break;
                case YARLI.WARNING_OUTPUT:
                    styledDoc.insertString(size, obj.toString(), warningStyle);
                    break;
                case YARLI.ERROR_OUTPUT:
                    styledDoc.insertString(size, obj.toString(), errorStyle);
                    break;
                case YARLI.HTML_OUTPUT:
                    HTMLDocument doc = (HTMLDocument) styledDoc;
                     {
                        try {
                            doc.insertAfterEnd(doc.getCharacterElement(doc.getLength()), obj.toString());
                        } catch (IOException ex) {
                            System.out.println("idk");
                        }
                    }
                    break;
            }
            outputTextPane.setCaretPosition(styledDoc.getLength());
        } catch (BadLocationException ex) {
            System.out.println("well ...");
        }

    }

    @Override
    public boolean supportsHTML() {
        return true;
    }

    @Override
    public void clearOutput() {
        this.outputTextPane.setText("");
    }

    @Override
    public void update(Observable o, Object o1) {
        System.out.println(o1 + "\n");
    }

    private boolean checkCharForPossibleId(char c) {
        return Character.isWhitespace(c) | c == '"' | c == '[' | c == ']' | c == '(' | c == ')' | c == ':';
    }

    private void showPossibleIdsAndAutoComplete() {
        String inputText = inputTextField.getText();
        int caretPos = inputTextField.getCaretPosition();
        String currentId = inputText.substring(0, caretPos);
        int idStart = 0;
        if (!(inputText.length() > caretPos && !checkCharForPossibleId(inputText.charAt(caretPos)))) {//checks if the next character is not a valid id character
            for (int i = currentId.length() - 1; i >= 0; i--) {
                char c = currentId.charAt(i);
                if (checkCharForPossibleId(c)) {
                    idStart = i + 1;
                    break;
                }
            }
            currentId = currentId.substring(idStart);
            if (!currentId.isEmpty()) {
                List<String> list = this.getPossibleIds(currentId);
                int size = list.size();
                if (size > 0) {
                    String inputStart = inputText.substring(0, idStart).trim();
                    String foundId;
                    int caretInNextSpace;
                    if (size > 1) {
                        this.showPossibleIds(list);
                        foundId = this.autoCompleteParcialId(currentId.length(), list);//Writes the common start to the found IDs
                        caretInNextSpace = 0;
                    } else {
                        foundId = list.get(0); //Writes the found IDs
                        caretInNextSpace = 1;
                    }
                    if (inputStart.length() != 0) {
                        this.inputTextField.setText(String.format("%s %s %s", inputStart, foundId, inputText.substring(caretPos).trim()));
                        this.inputTextField.setCaretPosition(inputStart.length() + foundId.length() + caretInNextSpace + 1);
                    } else {
                        this.inputTextField.setText(String.format("%s %s", foundId, inputText.substring(caretPos).trim()));
                        this.inputTextField.setCaretPosition(foundId.length() + caretInNextSpace);
                    }
                } else {
                    this.interpreter.showOutput(String.format("\nNo IDs found starting with \"%s\"\n", currentId), YARLI.INFO_OUTPUT);
                }
            }
        }
    }

    private List<String> getPossibleIds(String idStart) {
        List<String> list = new ArrayList<>();
        Map<String, IdDefinedCommand> comMap = this.interpreter.commandMap();
        Map<String, YARLI_Variable> varMap = this.interpreter.variableMap();
        for (Object id : comMap.keySet().toArray()) {
            String str = (String) id;
            if (str.startsWith(idStart.toUpperCase())) {
                list.add(comMap.get(str).getId());
            }
        }
        for (Object id : varMap.keySet().toArray()) {
            String str = (String) id;
            if (str.startsWith(idStart.toUpperCase())) {
                list.add(varMap.get(str).getId());
            }
        }
        return list;
    }

    private void showPossibleIds(List<String> idList) {
        StringBuilder sb = new StringBuilder("\nPossible Ids:\n\n");
        for (String id : idList) {
            sb.append(id);
            sb.append("\n");
        }
        this.interpreter.showOutput(sb.toString(), YARLI.INFO_OUTPUT);
    }

    private String autoCompleteParcialId(int currentIdLenght, List<String> idList) {
        String compareStr = idList.get(0);
        int compareStart = currentIdLenght;
        int compareEnd = compareStr.length();
        for (int i = 1; i < idList.size(); i++) {
            String currentStr = idList.get(i);
            if (currentStr.length() < compareEnd) {
                compareEnd = currentStr.length();
            }
            int aux = compareStart;
            for (int j = compareStart; j < compareEnd; j++) {
                if (compareStr.charAt(j) == currentStr.charAt(j)) {
                    aux++;
                } else {
                    break;
                }
            }
            if (aux < compareEnd) {
                compareEnd = aux;
            }
            if (compareEnd == compareStart) {
                break;
            }
        }
        return compareStr.substring(0, compareEnd);
    }

    @Override
    public void executeActionOnStop() {
        this.showOutput("\nThe interpreter " + this.tabId() + " has exited.", YARLI.INFO_OUTPUT);
        this.inputTextField.setEditable(false);
        if (this.mainInstance.properties().isCloseTabOnExit()) {
            this.mainInstance.removeTab(this.tabId());
        }

    }

    public void stopInterpreter() {
        this.interpreter.stop();
    }

    public YARLI interpreter() {
        return this.interpreter;
    }

}
