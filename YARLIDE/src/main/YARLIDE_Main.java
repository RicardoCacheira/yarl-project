/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import utils.Utils;
import yarli.io.YARLI_Output;
import yarli.YARLI;
import yarli.new_inst.NewInstanceHandler;
import yarli.stop.SystemExitOnStop;
import yarlide.YARLIDE_Properties;

/**
 *
 * @author Ricardo
 */
public final class YARLIDE_Main extends JFrame implements YARLI_Output, Observer, NewInstanceHandler {

    private static final String DEFAULT_PROPERTIES_FILE = "properties.xml";

    private YARLIDE_Properties properties;
    private JTextPane outputTextPane;

    private JMenuBar menuBar;
    private JTabbedPane mainTabPane;

    private YARLI interpreter;

    private Font systemFont;
    private Color backgroundColor;
    private Style plainStyle;
    private Style infoStyle;
    private Style warningStyle;
    private Style errorStyle;
    private LogFrame log;
    private long currentPanelId;
    private Map<Long, TabPanel> panelMap;

    public YARLIDE_Main() throws HeadlessException {
        super();
        this.currentPanelId = 0;
        this.panelMap = new HashMap();
        initComponents();
        // initStyles();
        initActions();
        startUI();
        initInterpreter();
        this.startNewInstance();
    }

    public void initComponents() {
        this.log = new LogFrame(this);
        try {
            this.log.showOutput("Loading Properties.\n", YARLI.INFO_OUTPUT);
            this.properties = YARLIDE_Properties.loadPropertiesFile(new File(DEFAULT_PROPERTIES_FILE));
            this.log.showOutput("Properties Loaded.\n", YARLI.INFO_OUTPUT);
        } catch (IllegalArgumentException | ParserConfigurationException | SAXException | IOException ex) {
            this.log.showOutput(ex.getMessage() + "\n", YARLI.WARNING_OUTPUT);
            this.log.showOutput("Loading Default Properties.\n", YARLI.INFO_OUTPUT);
            this.properties = YARLIDE_Properties.loadDefaultPropertiesFile();
            this.log.showOutput("Default Properties Loaded.\n", YARLI.INFO_OUTPUT);
            if (this.properties.saveProperties(DEFAULT_PROPERTIES_FILE)) {
                this.log.showOutput("Default Properties Saved.\n", YARLI.INFO_OUTPUT);
            } else {
                this.log.showOutput("Error saving Default Properties.\n", YARLI.INFO_OUTPUT);
            }

        }

        this.menuBar = new JMenuBar();
        this.menuBar.add(initFileMenu());
        this.menuBar.add(initRunMenu());
        this.menuBar.add(initOptionsMenu());
        this.menuBar.add(initHelpMenu());
        this.mainTabPane = new JTabbedPane();

        LayoutManager lm = new BorderLayout();
        this.setLayout(lm);

        this.setJMenuBar(this.menuBar);

        this.add(mainTabPane, BorderLayout.CENTER);

    }

    public JMenu initFileMenu() {
        JMenu fileMenu = new JMenu("File");
        JMenuItem newFileMenuItem = new JMenuItem("New File");
        newFileMenuItem.setAccelerator(KeyStroke.getKeyStroke("control N"));
        JMenuItem newInterpreterMenuItem = new JMenuItem("New Interpreter");
        newInterpreterMenuItem.setAccelerator(KeyStroke.getKeyStroke("control shift N"));
        newInterpreterMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                YARLIDE_Main.this.startNewInstance();
            }
        });
        JMenuItem openFileMenuItem = new JMenuItem("Open File");
        openFileMenuItem.setAccelerator(KeyStroke.getKeyStroke("control O"));
        JMenuItem saveFileMenuItem = new JMenuItem("Save File");
        saveFileMenuItem.setAccelerator(KeyStroke.getKeyStroke("control S"));
        JMenuItem saveFileAsMenuItem = new JMenuItem("Save File As...");
        saveFileAsMenuItem.setAccelerator(KeyStroke.getKeyStroke("control shift S"));
        JMenuItem closeTabMenuItem = new JMenuItem("Close Tab");
        closeTabMenuItem.setAccelerator(KeyStroke.getKeyStroke("control W"));
        closeTabMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                YARLIDE_Main.this.closeTab(((TabPanel) YARLIDE_Main.this.mainTabPane.getSelectedComponent()).tabId());
            }
        });
        JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.setAccelerator(KeyStroke.getKeyStroke("alt F4"));
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });
        fileMenu.add(newFileMenuItem);
        fileMenu.add(newInterpreterMenuItem);
        fileMenu.addSeparator();
        fileMenu.add(openFileMenuItem);
        fileMenu.addSeparator();
        fileMenu.add(saveFileMenuItem);
        fileMenu.add(saveFileAsMenuItem);
        fileMenu.add(closeTabMenuItem);
        fileMenu.addSeparator();
        fileMenu.add(exitMenuItem);
        return fileMenu;
    }

    public JMenu initRunMenu() {
        JMenu runMenu = new JMenu("Run");
        JMenuItem loadFileMenuItem = new JMenuItem("Load File");
        loadFileMenuItem.setAccelerator(KeyStroke.getKeyStroke("F5"));
        JMenuItem runFileMenuItem = new JMenuItem("Run File");
        runFileMenuItem.setAccelerator(KeyStroke.getKeyStroke("F6"));
        JMenuItem runNewFileMenuItem = new JMenuItem("Run New File");
        runNewFileMenuItem.setAccelerator(KeyStroke.getKeyStroke("shift F6"));
        runNewFileMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileFilter(new FileNameExtensionFilter("YARL Files", "yarl"));
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fileChooser.setFileHidingEnabled(true);
                fileChooser.setDialogTitle("Run New File");
                int returnVal = fileChooser.showOpenDialog(YARLIDE_Main.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    YARLIDE_Main.this.runFileInNewInstance(fileChooser.getSelectedFile());
                }
            }
        });
        runMenu.add(loadFileMenuItem);
        runMenu.add(runFileMenuItem);
        runMenu.add(runNewFileMenuItem);
        return runMenu;
    }

    public JMenu initOptionsMenu() {
        JMenu optionsMenu = new JMenu("Options");
        JMenuItem styleMenuItem = new JMenuItem("Style");
        JMenuItem propertiesMenuItem = new JMenuItem("Properties");
        propertiesMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JFrame propertiesFrame = new PropertiesFrame(YARLIDE_Main.this);

            }
        });
        optionsMenu.add(styleMenuItem);
        optionsMenu.add(propertiesMenuItem);
        return optionsMenu;
    }

    public JMenu initHelpMenu() {
        JMenu helpMenu = new JMenu("Help");
        JMenuItem logMenuItem = new JMenuItem("Log");
        logMenuItem.setAccelerator(KeyStroke.getKeyStroke("control L"));
        logMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                YARLIDE_Main.this.showLog();
            }
        });
        JMenuItem helpMenuItem = new JMenuItem("Help");
        helpMenuItem.setAccelerator(KeyStroke.getKeyStroke("F1"));
        JMenuItem aboutMenuItem = new JMenuItem("About");
        helpMenu.add(logMenuItem);
        helpMenu.add(helpMenuItem);
        helpMenu.add(aboutMenuItem);
        return helpMenu;
    }

    public void initStyles() {
        this.plainStyle = outputTextPane.addStyle("plainStyle", null);
        StyleConstants.setForeground(plainStyle, Color.BLACK);
        this.infoStyle = outputTextPane.addStyle("infoStyle", null);
        StyleConstants.setForeground(infoStyle, Color.BLUE);
        this.warningStyle = outputTextPane.addStyle("warningStyle", null);
        StyleConstants.setForeground(warningStyle, Color.ORANGE);
        this.errorStyle = outputTextPane.addStyle("errorStyle", null);
        StyleConstants.setForeground(errorStyle, Color.RED);
        StyleConstants.setBold(errorStyle, true);
    }

    public void initActions() {

    }

    private void startUI() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("YARLI");
        pack();
        int[] size = this.properties.getMainWindowSize();
        setSize(size[0], size[1]);
        setLocationRelativeTo(null);
        setVisible(true);
        if (this.properties.isShowLogAtStart()) {
            this.showLog();
        }
    }

    private void initInterpreter() {
        interpreter = new YARLI("YARLI", null, this, new SystemExitOnStop());
        interpreter.addObserver(this);
        interpreter.loadModulePackFolder(this.properties.getModulesFolder());
    }

    @Override
    public void showOutput(Object obj, int type) {
        this.log.showOutput(obj, type);

    }

    @Override
    public void update(Observable o, Object o1) {
        //   System.out.println(o1 + "\n");
    }

    @Override
    public boolean supportsHTML() {
        return true;
    }

    @Override
    public void clearOutput() {
    }

    public static void main(String[] args) {
        new YARLIDE_Main();
    }

    public InterpreterPanel createNewInstancePanel() {
        long id = this.currentPanelId;
        this.currentPanelId++;
        InterpreterPanel panel = new InterpreterPanel(this, id);
        this.panelMap.put(id, panel);
        this.mainTabPane.add(panel);
        int index = this.mainTabPane.indexOfComponent(panel);
        this.mainTabPane.setTabComponentAt(index, createInterpreterTabPanel("Interpreter " + id, id));
        this.mainTabPane.setSelectedIndex(index);
        return panel;
    }

    @Override
    public void startNewInstance() {
        this.createNewInstancePanel();
    }

    private JPanel createInterpreterTabPanel(String name, long id) {
        JPanel panel = new JPanel();
        BorderLayout layout = new BorderLayout();
        layout.setHgap(10);

        panel.setLayout(layout);

        panel.setOpaque(false);
        JLabel label = new JLabel(name);
        JButton closeButton = new JButton(Utils.getImageResource("/resources/icons/close.png"));
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                YARLIDE_Main.this.closeTab(id);
            }
        });
        closeButton.setBorderPainted(false);
        closeButton.setBorder(null);
        closeButton.setFocusable(false);
        closeButton.setMargin(new Insets(0, 0, 0, 0));
        closeButton.setContentAreaFilled(false);
        ImageIcon selectedIcon = Utils.getImageResource("/resources/icons/close_selected.png");
        closeButton.setRolloverIcon(selectedIcon);
        closeButton.setPressedIcon(selectedIcon);
        panel.add(label, BorderLayout.CENTER);
        panel.add(closeButton, BorderLayout.EAST);

        return panel;
    }

    @Override
    public void runFileInNewInstance(String filename) {
        this.runFileInNewInstance(new File(filename));
    }

    public void runFileInNewInstance(File file) {
        ArrayList<String> commandList = new ArrayList<>();
        try {
            Scanner in = new Scanner(file);
            while (in.hasNextLine()) {
                commandList.add(in.nextLine());
            }
            in.close();
            InterpreterPanel panel = this.createNewInstancePanel();
            panel.interpreter().addCommandListToBuffer(commandList.toArray(new String[commandList.size()]));
            if (this.properties.isExitAfterRun()) {
                panel.interpreter().addCommandToBuffer(YARLI.STOP_COMMAND);
            }
        } catch (FileNotFoundException ex) {
            interpreter.throwError("File not found.");
        }
    }

    public YARLI mainInterpreter() {
        return this.interpreter;
    }

    public void removeTab(long paneId) {
        this.mainTabPane.remove(this.panelMap.remove(paneId));
    }

    public YARLIDE_Properties properties() {
        return this.properties;
    }

    public void showLog() {
        int[] size = YARLIDE_Main.this.properties().getSecondaryWindowSize();
        YARLIDE_Main.this.log.setSize(size[0], size[1]);
        YARLIDE_Main.this.log.setLocationRelativeTo(YARLIDE_Main.this);
        YARLIDE_Main.this.log.setVisible(true);
    }

    public void closeTab(long tabId) {
        TabPanel tab = YARLIDE_Main.this.panelMap.get(tabId);
        if (tab instanceof InterpreterPanel) {
            ((InterpreterPanel) tab).stopInterpreter();
            if (!YARLIDE_Main.this.properties.isCloseTabOnExit()) {
                YARLIDE_Main.this.removeTab(tabId);
            }
        } else {
            //editor Tab
        }

    }

    public void setProperties(YARLIDE_Properties properties) {
        this.properties = properties;
        this.properties.saveProperties(DEFAULT_PROPERTIES_FILE);
    }

}
