/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.Format;
import java.text.NumberFormat;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import yarli.YARLI;
import yarlide.YARLIDE_Properties;
import yarlide.YARLIDE_Properties.DefaultTab;

/**
 *
 * @author Ricardo
 */
public class PropertiesFrame extends JFrame {

    private JTextPane outputTextPane;

    private YARLIDE_Main mainWindow;

    private JComboBox<YARLIDE_Properties.DefaultTab> defaultOpeningTab;

    private JTextField modulesFolder;

    private JTextField themesFolder;

    private JTextField mainWindowSizeX;

    private JTextField mainWindowSizeY;

    private JTextField secondaryWindowSizeX;

    private JTextField secondaryWindowSizeY;

    private JCheckBox showLogAtStart;

    private JCheckBox showIdPane;

    private JCheckBox closeInterpreterTabOnExit;

    private JCheckBox exitAfterRun;

    private JCheckBox showHTMLInShell;

    public PropertiesFrame(YARLIDE_Main mainWindow) {
        super();
        this.mainWindow = mainWindow;
        initComponents();
        loadValues();
        //initActions();
        startUI();

    }

    public void initComponents() {
        this.setLayout(new BorderLayout(10, 10));
        JPanel labelPanel = new JPanel(new GridLayout(10, 1, 0, 10));
        labelPanel.add(new JLabel("Default Opening Tab: "));
        labelPanel.add(new JLabel("Modules Folder: "));
        labelPanel.add(new JLabel("Themes Folder: "));
        labelPanel.add(new JLabel("Main Window Size: "));
        labelPanel.add(new JLabel("Secondary Window Size: "));
        labelPanel.add(new JLabel("Show Log At Start: "));
        labelPanel.add(new JLabel("Show Id Pane: "));
        labelPanel.add(new JLabel("Close Interpreter Tab on Exit: "));
        labelPanel.add(new JLabel("Exit After Run: "));
        labelPanel.add(new JLabel("Show HTML in Shell: "));

        JPanel optionsPanel = new JPanel(new GridLayout(10, 1, 0, 10));
        this.defaultOpeningTab = new JComboBox<>(new DefaultTab[]{DefaultTab.INTERPRETER, DefaultTab.EDITOR});
        this.modulesFolder = new JTextField();
        this.themesFolder = new JTextField();
        this.mainWindowSizeX = new JFormattedTextField(NumberFormat.getNumberInstance());
        this.mainWindowSizeY = new JTextField();
        this.secondaryWindowSizeX = new JTextField();
        this.secondaryWindowSizeY = new JTextField();
        this.showLogAtStart = new JCheckBox();
        this.showIdPane = new JCheckBox();
        this.closeInterpreterTabOnExit = new JCheckBox();
        this.exitAfterRun = new JCheckBox();
        this.showHTMLInShell = new JCheckBox();
        JPanel mainWindowSizePanel = new JPanel(new GridLayout(1, 2, 10, 0));
        mainWindowSizePanel.add(this.mainWindowSizeX);
        mainWindowSizePanel.add(this.mainWindowSizeY);
        JPanel secondaryWindowSizePanel = new JPanel(new GridLayout(1, 2, 10, 0));
        secondaryWindowSizePanel.add(this.secondaryWindowSizeX);
        secondaryWindowSizePanel.add(this.secondaryWindowSizeY);
        optionsPanel.add(this.defaultOpeningTab);
        optionsPanel.add(this.modulesFolder);
        optionsPanel.add(this.themesFolder);
        optionsPanel.add(mainWindowSizePanel);
        optionsPanel.add(secondaryWindowSizePanel);
        optionsPanel.add(this.showLogAtStart);
        optionsPanel.add(this.showIdPane);
        optionsPanel.add(this.closeInterpreterTabOnExit);
        optionsPanel.add(this.exitAfterRun);
        optionsPanel.add(this.showHTMLInShell);

        JPanel buttonPanel = new JPanel(new GridLayout(1, 0, 10, 0));
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                PropertiesFrame.this.dispose();
            }
        });
        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (JOptionPane.showConfirmDialog(PropertiesFrame.this, "Do you really want to reset the properties?", "Reset", JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
                    PropertiesFrame.this.mainWindow.setProperties(YARLIDE_Properties.loadDefaultPropertiesFile());
                    PropertiesFrame.this.dispose();
                }
            }
        });
        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (PropertiesFrame.this.saveProperties()) {
                    PropertiesFrame.this.dispose();
                }
            }
        });
        buttonPanel.add(cancelButton);
        buttonPanel.add(resetButton);
        buttonPanel.add(saveButton);
        this.add(labelPanel, BorderLayout.WEST);
        this.add(optionsPanel, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);
        PropertiesFrame.this.setVisible(false);
    }

    private void loadValues() {
        YARLIDE_Properties props = this.mainWindow.properties();
        this.defaultOpeningTab.setSelectedItem(props.getDefaultTab());
        this.modulesFolder.setText(props.getModulesFolder());
        this.themesFolder.setText(props.getThemesFolder());
        this.mainWindowSizeX.setText("" + props.getMainWindowSize()[0]);
        this.mainWindowSizeY.setText("" + props.getMainWindowSize()[1]);
        this.secondaryWindowSizeX.setText("" + props.getSecondaryWindowSize()[0]);
        this.secondaryWindowSizeY.setText("" + props.getSecondaryWindowSize()[1]);
        this.showLogAtStart.setSelected(props.isShowLogAtStart());
        this.showIdPane.setSelected(props.isShowIdPane());
        this.closeInterpreterTabOnExit.setSelected(props.isCloseTabOnExit());
        this.exitAfterRun.setSelected(props.isExitAfterRun());
        this.showHTMLInShell.setSelected(props.isShowHTMLInShell());
    }

    private boolean saveProperties() {
        try {
            int[] mainVals = new int[]{Integer.parseInt(this.mainWindowSizeX.getText()), Integer.parseInt(this.mainWindowSizeY.getText())};
            try {
                int[] secondaryVals = new int[]{Integer.parseInt(this.secondaryWindowSizeX.getText()), Integer.parseInt(this.secondaryWindowSizeY.getText())};
                try {
                    this.mainWindow.setProperties(new YARLIDE_Properties((DefaultTab) this.defaultOpeningTab.getSelectedItem(), modulesFolder.getText(), themesFolder.getText(), this.mainWindow.properties().getTheme(), mainVals, secondaryVals, this.showLogAtStart.isSelected(), this.showIdPane.isSelected(), this.closeInterpreterTabOnExit.isSelected(), this.exitAfterRun.isSelected(), this.showHTMLInShell.isSelected()));
                    return true;
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "The Secondary Window sizes must be numbers.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "The Main Window sizes must be numbers.", "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    private void startUI() {
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("Properties");
        pack();
        setSize(this.mainWindow.properties().getSecondaryWindowSize()[0], this.mainWindow.properties().getSecondaryWindowSize()[1]);
        setLocationRelativeTo(this.mainWindow);
        setVisible(true);
    }

}
