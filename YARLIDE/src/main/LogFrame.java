/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class LogFrame extends JFrame {

    private JTextPane outputTextPane;

    private YARLIDE_Main mainWindow;

    public LogFrame(YARLIDE_Main mainWindow) {
        super();
        this.mainWindow = mainWindow;
        initComponents();
        //initStyles();
        //initActions();
        startUI();

    }

    public void initComponents() {
        Font systemFont = new Font("Courier New", Font.PLAIN, 13);
        Color backgroundColor = Color.WHITE;
        LayoutManager lm = new BorderLayout();
        this.setLayout(lm);
        this.outputTextPane = new JTextPane();
        this.outputTextPane.setContentType("text/html");
        this.outputTextPane.setBackground(backgroundColor);
        this.outputTextPane.setFont(systemFont);
        this.outputTextPane.setEditable(false);
        JScrollPane scroll = new JScrollPane(outputTextPane);
        JPanel buttonPanel = new JPanel(new BorderLayout());
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                LogFrame.this.setVisible(false);
            }
        });
        buttonPanel.add(closeButton);
        this.add(scroll, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);
        LogFrame.this.setVisible(false);
    }

    private void startUI() {
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle("Log");
        pack();
    }

    public void showOutput(Object obj, int type) {
        Style plainStyle = outputTextPane.addStyle("plainStyle", null);
        StyleConstants.setForeground(plainStyle, Color.BLACK);
        Style infoStyle = outputTextPane.addStyle("infoStyle", null);
        StyleConstants.setForeground(infoStyle, Color.BLUE);
        Style warningStyle = outputTextPane.addStyle("warningStyle", null);
        StyleConstants.setForeground(warningStyle, Color.ORANGE);
        Style errorStyle = outputTextPane.addStyle("errorStyle", null);
        StyleConstants.setForeground(errorStyle, Color.RED);
        StyleConstants.setBold(errorStyle, true);
        StyledDocument styledDoc = outputTextPane.getStyledDocument();
        int size = styledDoc.getLength();
        try {
            switch (type) {
                case YARLI.PLAIN_OUTPUT:
                    styledDoc.insertString(size, obj.toString(), plainStyle);
                    break;
                case YARLI.INFO_OUTPUT:
                    styledDoc.insertString(size, obj.toString(), infoStyle);
                    break;
                case YARLI.WARNING_OUTPUT:
                    styledDoc.insertString(size, obj.toString(), warningStyle);
                    break;
                case YARLI.ERROR_OUTPUT:
                    styledDoc.insertString(size, obj.toString(), errorStyle);
                    break;
                case YARLI.HTML_OUTPUT:
                    HTMLDocument doc = (HTMLDocument) styledDoc;
                     {
                        try {
                            doc.insertAfterEnd(doc.getCharacterElement(doc.getLength()), obj.toString());
                        } catch (IOException ex) {
                            System.out.println("idk");
                        }
                    }
                    break;
            }
            outputTextPane.setCaretPosition(styledDoc.getLength());
        } catch (BadLocationException ex) {
            System.out.println("well ...");
        }

    }

}
