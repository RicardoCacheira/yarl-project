/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.notationhandler;

/**
 *
 * @author Ricardo
 */
public class RationalNumberNotationHandlerExeption extends IllegalArgumentException {

    public RationalNumberNotationHandlerExeption(String msn) {
        super(msn);
    }
}
