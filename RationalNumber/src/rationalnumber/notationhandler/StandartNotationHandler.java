/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.notationhandler;

import rationalnumber.RationalNumber;

/**
 *
 * @author Ricardo
 */
public class StandartNotationHandler implements RationalNumberNotationHandler {

    @Override
    public String generateNumberStringInNotation(RationalNumber number) {
        if (number.isAnInteger()) {
            return number.numerator().toString();
        }
        return number.toFraction();
    }

    @Override
    public String toString() {
        return "Standart Notation";
    }
}
