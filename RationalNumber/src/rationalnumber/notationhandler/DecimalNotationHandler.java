/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.notationhandler;

import rationalnumber.RationalNumber;

/**
 *
 * @author Ricardo
 */
public class DecimalNotationHandler implements RationalNumberNotationHandler {

    private int maxFractionalDigits;
    private boolean usesDecimalComma;

    public DecimalNotationHandler(int maxFractionalDigits, boolean usesDecimalComma) {
        if (maxFractionalDigits < 0) {
            throw new RationalNumberNotationHandlerExeption("The maximum fractional digit number cannot be negative.");
        }
        this.maxFractionalDigits = maxFractionalDigits;
        this.usesDecimalComma = usesDecimalComma;
    }

    @Override
    public String generateNumberStringInNotation(RationalNumber number) {
        boolean isNegative = number.isNegative();
        if (isNegative) {
            number = number.opposite();
        }
        String integerPart = number.integerPart().numerator().toString();
        String str;
        if (maxFractionalDigits == 0 || number.isAnInteger()) {
            str = integerPart;
        } else {
            String decimalPart = number.decimalPart().toBigDecimal(maxFractionalDigits).toString().substring(2);
            if (usesDecimalComma) {
                str = String.format("%s,%s", integerPart, decimalPart);
            } else {
                str = String.format("%s.%s", integerPart, decimalPart);
            }
        }
        if (isNegative) {
            str = "-" + str;
        }
        return str;
    }

    @Override
    public String toString() {
        return String.format("Decimal Notation, Max Fractional Digits: %d, Uses Decimal Comma: %b", this.maxFractionalDigits, this.usesDecimalComma);
    }
}
