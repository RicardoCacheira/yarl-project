/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.notationhandler;

import rationalnumber.RationalNumber;

/**
 *
 * @author Ricardo
 */
public class DecimalStandartNotationHandler implements RationalNumberNotationHandler {

    private int maxFractionalDigits;
    private boolean usesDecimalComma;

    public DecimalStandartNotationHandler(int maxFractionalDigits, boolean usesDecimalComma) {
        if (maxFractionalDigits < 0) {
            throw new RationalNumberNotationHandlerExeption("The maximum fractional digit number cannot be negative.");
        }
        this.maxFractionalDigits = maxFractionalDigits;
        this.usesDecimalComma = usesDecimalComma;
    }

    @Override
    public String generateNumberStringInNotation(RationalNumber number) {
        RationalNumber num;
        boolean isNegative = number.isNegative();
        if (isNegative) {
            num = number.opposite();
        } else {
            num = number;
        }
        String integerPart = num.integerPart().numerator().toString();
        String str;
        if (maxFractionalDigits == 0 || number.isAnInteger()) {
            if (isNegative) {
                return "-" + integerPart;
            } else {
                return integerPart;
            }
        } else {
            String decimalPart = num.decimalPart().toBigDecimal(maxFractionalDigits + 1).toString().substring(2);
            if (decimalPart.length() <= maxFractionalDigits) {
                if (usesDecimalComma) {
                    str = String.format("%s,%s", integerPart, decimalPart);
                } else {
                    str = String.format("%s.%s", integerPart, decimalPart);
                }
                if (isNegative) {
                    str = "-" + str;
                }
                return str;
            } else {
                return number.toFraction();
            }
        }
    }

    @Override
    public String toString() {
        return String.format("Decimal Notation, Max Fractional Digits: %d, Uses Decimal Comma: %b", this.maxFractionalDigits, this.usesDecimalComma);
    }
}
