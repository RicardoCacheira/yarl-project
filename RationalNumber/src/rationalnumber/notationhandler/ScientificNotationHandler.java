/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.notationhandler;

import rationalnumber.RationalNumber;

/**
 *
 * @author Ricardo
 */
public class ScientificNotationHandler implements RationalNumberNotationHandler {

    private FixedDecimalNotationHandler fDec;
    private int coefficient;
    private boolean usesDecimalComma;

    public ScientificNotationHandler(int coefficient, boolean usesDecimalComma) {
        if (coefficient < 1) {
            throw new RationalNumberNotationHandlerExeption("The coefficient must be a positive number.");
        }
        this.coefficient = coefficient;
        this.usesDecimalComma = usesDecimalComma;
        this.fDec = new FixedDecimalNotationHandler(coefficient-1, usesDecimalComma);
    }

    @Override
    public String generateNumberStringInNotation(RationalNumber number) {
        boolean isNegative = number.isNegative();
        if (isNegative) {
            number = number.opposite();
        }
        RationalNumber power = RationalNumber.ZERO;
        while (number.compareTo(RationalNumber.TEN) >= 0) {
            power = power.add(RationalNumber.ONE);
            number = number.divide(RationalNumber.TEN);
        }
        while (number.compareTo(RationalNumber.ONE) < 0) {
            power = power.subtract(RationalNumber.ONE);
            number = number.multiply(RationalNumber.TEN);
        }
        String str;
        str = String.format("%sx10^%s", this.fDec.generateNumberStringInNotation(number), power.numerator().toString());

        if (isNegative) {
            str = "-" + str;
        }
        return str;
    }

    @Override
    public String toString() {
        return String.format("Scientific Notation, Coefficient: %d, Uses Decimal Comma: %b", this.coefficient, this.usesDecimalComma);
    }
}
