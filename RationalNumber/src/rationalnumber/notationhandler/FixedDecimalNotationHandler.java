/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.notationhandler;

import rationalnumber.RationalNumber;

/**
 *
 * @author Ricardo
 */
public class FixedDecimalNotationHandler implements RationalNumberNotationHandler {

    private int fractionalDigits;
    private boolean usesDecimalComma;

    public FixedDecimalNotationHandler(int fractionalDigits, boolean usesDecimalComma) {
        if (fractionalDigits < 0) {
            throw new RationalNumberNotationHandlerExeption("The fractional digit number cannot be negative.");
        }
        this.fractionalDigits = fractionalDigits;
        this.usesDecimalComma = usesDecimalComma;
    }

    @Override
    public String generateNumberStringInNotation(RationalNumber number) {
        boolean isNegative = number.isNegative();
        if (isNegative) {
            number = number.opposite();
        }
        String integerPart = number.integerPart().numerator().toString();
        String str;
        if (fractionalDigits == 0) {
            str = integerPart;
        } else {
            StringBuilder decimalPart = new StringBuilder();
            if (!number.isAnInteger()) {//Only adds a decimal part if its not an integer
                decimalPart.append(number.decimalPart().toBigDecimal(fractionalDigits).toString().substring(2));
            }
            for (int i = decimalPart.toString().length(); i < this.fractionalDigits; i++) {
                decimalPart.append('0');
            }
            if (usesDecimalComma) {
                str = String.format("%s,%s", integerPart, decimalPart.toString());
            } else {
                str = String.format("%s.%s", integerPart, decimalPart.toString());
            }
        }
        if (isNegative) {
            str = "-" + str;
        }
        return str;
    }

    @Override
    public String toString() {
        return String.format("Fixed Decimal Notation, Fractional Digits: %d, Uses Decimal Comma: %b", this.fractionalDigits, this.usesDecimalComma);
    }
}
