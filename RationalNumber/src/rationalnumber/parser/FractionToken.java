/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.parser;

import java.math.BigInteger;

/**
 *
 * @author Ricardo
 */
public class FractionToken implements NumberToken {

    private BigInteger numerator;
    private BigInteger denominator;
    private boolean isNegative;

    public FractionToken(String numerator, String denominator, boolean isNegative) throws IllegalArgumentException {
        this.numerator = new BigInteger(numerator);
        this.denominator = new BigInteger(denominator);
        this.isNegative = isNegative;
        if (this.denominator.equals(BigInteger.ZERO)) {
            throw new IllegalArgumentException("0 in the denominator.");
        }
    }

    @Override
    public BigInteger[] generateNumeratorDenominator() {
        return new BigInteger[]{this.numerator, this.denominator};
    }

    @Override
    public boolean isNegative() {
        return this.isNegative;
    }
}
