/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.parser;

/**
 *
 * @author Ricardo
 */
public class RationalNumberParser {

    public static NumberToken parseNumber(String numberStr) throws RationalNumberParserExeption {
        boolean decimalPoint = false, seperator = false, isNegative = false;
        StringBuilder num1 = new StringBuilder();
        StringBuilder num2 = new StringBuilder();
        int start = 0;
        if (numberStr.length() > 0 && numberStr.charAt(0) == '-') {
            start = 1;
            isNegative = true;
        }
        for (int i = start; i < numberStr.length(); i++) {
            char c = numberStr.charAt(i);
            if (Character.isDigit(c)) {
                if (!(decimalPoint || seperator)) {
                    num1.append(c);
                } else {
                    num2.append(c);
                }
            } else if (c == ',' || c == '.') {
                if (decimalPoint || seperator) {
                    throw new RationalNumberParserExeption("Invalid decimal point", numberStr, i);
                } else {
                    if (num1.toString().isEmpty()) {
                        num1.append('0');
                    }
                    decimalPoint = true;
                }
            } else if (c == '/') {
                if (decimalPoint || seperator) {
                    throw new RationalNumberParserExeption("Invalid seperator", numberStr, i);
                } else if (num1.toString().isEmpty()) {
                    throw new RationalNumberParserExeption("No numerator found", numberStr, i);
                } else {
                    seperator = true;
                }
            } else {
                throw new RationalNumberParserExeption("Invalid character", numberStr, i);
            }

        }
        if (decimalPoint) {
            if (num2.toString().isEmpty()) {
                throw new RationalNumberParserExeption("Invalid decimal point", numberStr, numberStr.length() - 1);
            }
            return new DecimalNumberToken(num1.toString(), num2.toString(), isNegative);
        } else if (seperator) {
            if (num2.toString().isEmpty()) {
                throw new RationalNumberParserExeption("No denominator found", numberStr, numberStr.length() - 1);
            }
            try {
                return new FractionToken(num1.toString(), num2.toString(), isNegative);
            } catch (IllegalArgumentException ex) {
                throw new RationalNumberParserExeption(ex.getMessage(), numberStr, numberStr.length() - 1);//rever a posicao do erro
            }
        } else {
            return new IntegerNumberToken(num1.toString(), isNegative);
        }
    }

    public static boolean isNumberValid(String numStr) {
        try {
            RationalNumberParser.parseNumber(numStr);
            return true;
        } catch (RationalNumberParserExeption ex) {
            return false;
        }
    }
}
