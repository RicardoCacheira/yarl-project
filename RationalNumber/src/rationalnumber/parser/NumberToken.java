/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.parser;

import java.math.BigInteger;

/**
 *
 * @author Ricardo
 */
public interface NumberToken {

    public BigInteger[] generateNumeratorDenominator();

    public boolean isNegative();
}
