/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.parser;

import java.math.BigInteger;

/**
 *
 * @author Ricardo
 */
public class DecimalNumberToken implements NumberToken {

    private String num1;
    private String num2;
    private boolean isNegative;

    public DecimalNumberToken(String num1, String num2, boolean isNegative) {
        this.num1 = num1;
        this.num2 = num2;
        this.isNegative = isNegative;
    }

    @Override
    public BigInteger[] generateNumeratorDenominator() {
        BigInteger[] result = new BigInteger[2];
        long decimal = num2.length();

        result[0] = new BigInteger(num1 + num2);

        result[1] = BigInteger.ONE;
        for (long i = 0; i < decimal; i++) {
            result[1] = result[1].multiply(BigInteger.TEN);
        }
        return result;
    }

    @Override
    public boolean isNegative() {
        return this.isNegative;
    }

}
