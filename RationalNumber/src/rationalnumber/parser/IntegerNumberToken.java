/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber.parser;

import java.math.BigInteger;

/**
 *
 * @author Ricardo
 */
public class IntegerNumberToken implements NumberToken {

    private String numStr;
    private boolean isNegative;

    public IntegerNumberToken(String numStr, boolean isNegative) {
        this.numStr = numStr;
        this.isNegative = isNegative;
    }

    @Override
    public BigInteger[] generateNumeratorDenominator() {
        BigInteger[] result = {new BigInteger(numStr), BigInteger.ONE};
        return result;
    }

    @Override
    public boolean isNegative() {
        return this.isNegative;
    }
}
