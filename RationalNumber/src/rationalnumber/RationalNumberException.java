/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber;

/**
 *
 * @author Ricardo
 */
public class RationalNumberException extends ArithmeticException {

    public RationalNumberException() {
    }

    public RationalNumberException(String string) {
        super(string);
    }

}
