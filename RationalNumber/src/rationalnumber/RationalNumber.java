/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rationalnumber;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import rationalnumber.parser.NumberToken;
import rationalnumber.parser.RationalNumberParser;
import rationalnumber.parser.RationalNumberParserExeption;

/**
 *
 * @author Ricardo
 */
public class RationalNumber implements Comparable<RationalNumber> {

    public static RationalNumber ZERO = new RationalNumber(BigInteger.ZERO);

    public static RationalNumber ONE = new RationalNumber(BigInteger.ONE);

    public static RationalNumber TEN = new RationalNumber(BigInteger.TEN);

    public static RationalNumber INT_MAX = new RationalNumber(Integer.MAX_VALUE);

    public static RationalNumber INT_MIN = new RationalNumber(Integer.MIN_VALUE);

    public static RationalNumber LONG_MAX = new RationalNumber(Long.MAX_VALUE);

    public static RationalNumber LONG_MIN = new RationalNumber(Long.MIN_VALUE);

    private BigInteger numerator;

    private BigInteger denominator;

    public RationalNumber(int integer) {
        this.numerator = new BigInteger(Integer.toString(integer));
        this.denominator = BigInteger.ONE;
    }

    public RationalNumber(long longValue) {
        this.numerator = new BigInteger(Long.toString(longValue));
        this.denominator = BigInteger.ONE;
    }

    public RationalNumber(BigInteger integer) {
        this.numerator = integer;
        this.denominator = BigInteger.ONE;
    }

    public RationalNumber(BigInteger numerator, BigInteger denominator) throws RationalNumberException {
        if (denominator.equals(BigInteger.ZERO)) {
            throw new RationalNumberException("0 in the denominator");
        }
        this.numerator = numerator;
        this.denominator = denominator;
        normalizeNumber();
    }

    /**
     * Supports 15/6, 24.54 and 13
     *
     * @param number
     */
    public RationalNumber(String number) throws RationalNumberParserExeption {
        NumberToken token = RationalNumberParser.parseNumber(number);
        BigInteger[] result = token.generateNumeratorDenominator();
        if (token.isNegative()) {
            this.numerator = result[0].multiply(new BigInteger("-1"));
        } else {
            this.numerator = result[0];
        }
        this.denominator = result[1];
        normalizeNumber();
    }

    private void normalizeNumber() {
        if (this.denominator.compareTo(BigInteger.ZERO) < 0) {
            this.numerator = numerator.multiply(new BigInteger("-1"));
            this.denominator = denominator.multiply(new BigInteger("-1"));
        }
        reduceFraction();
    }

    private void reduceFraction() {
        if (this.numerator.compareTo(BigInteger.ZERO) == 0) {
            this.denominator = BigInteger.ONE;
        } else if (this.denominator.compareTo(BigInteger.ONE) != 0) {
            BigInteger gcd = this.numerator.gcd(this.denominator);
            this.numerator = this.numerator.divide(gcd);
            this.denominator = this.denominator.divide(gcd);
        }
    }

    /**
     * least common multiple
     *
     * @return
     */
    private BigInteger calculateLCM(BigInteger a, BigInteger b) {
        return a.multiply(b).divide(a.gcd(b));
    }

    public RationalNumber add(RationalNumber number) {
        if (this.denominator == number.denominator) {
            return new RationalNumber(this.numerator.add(number.numerator), this.denominator);
        } else {
            BigInteger lcm = calculateLCM(this.denominator, number.denominator);
            return new RationalNumber(this.numerator.multiply(lcm.divide(this.denominator)).add(number.numerator.multiply(lcm.divide(number.denominator))), lcm);
        }
    }

    public RationalNumber subtract(RationalNumber number) {
        return add(number.opposite());
    }

    public RationalNumber multiply(RationalNumber secondOperator) {
        return new RationalNumber(this.numerator.multiply(secondOperator.numerator), denominator.multiply(secondOperator.denominator));
    }

    public RationalNumber divide(RationalNumber secondOperator) throws RationalNumberException {
        if (secondOperator.numerator.equals(BigInteger.ZERO)) {
            throw new RationalNumberException("Divide by 0");
        } else if (secondOperator.equals(RationalNumber.ONE)) {
            return this;
        }
        return new RationalNumber(this.numerator.multiply(secondOperator.denominator), denominator.multiply(secondOperator.numerator));
    }

    public RationalNumber integerDivide(RationalNumber secondOperator) throws RationalNumberException {
        if (secondOperator.numerator.equals(BigInteger.ZERO)) {
            throw new RationalNumberException("Divide by 0");
        } else if (secondOperator.equals(RationalNumber.ONE)) {
            return this;
        }
        return new RationalNumber(this.numerator.multiply(secondOperator.denominator), denominator.multiply(secondOperator.numerator)).integerPart();
    }

    public RationalNumber modulo(RationalNumber secondOperator) throws RationalNumberException {
        return this.subtract(this.integerDivide(secondOperator).multiply(secondOperator));
    }

    /**
     * 0^0 is undefined
     *
     * @param exponent must be and integer
     * @return
     * @throws RationalNumberException
     */
    public RationalNumber power(RationalNumber exponent) throws RationalNumberException {
        if (exponent.isAnInteger()) {
            if (this.equals(ZERO)) {
                if (exponent.equals(ZERO)) {
                    throw new RationalNumberException("Undefined power 0^0.");
                } else if (exponent.isNegative()) {
                    throw new RationalNumberException("Undefined power 0^negative.");
                } else {
                    return ZERO;
                }
            } else if (this.equals(ONE)) {
                return ONE;
            } else if (exponent.equals(ZERO)) {
                return ONE;

            } else if (exponent.equals(ONE)) {
                return this;
            } else if (exponent.isNegative()) {
                return this.power(exponent.opposite()).multiplicativeInverse();
            } else {
                RationalNumber result = this;
                exponent = exponent.subtract(ONE);
                while (!exponent.equals(ZERO)) {
                    result = result.multiply(this);
                    exponent = exponent.subtract(ONE);
                }
                return result;
            }
        } else {
            throw new RationalNumberException("The given exponent is not an integer.");
        }

    }

    public RationalNumber opposite() {
        return new RationalNumber(this.numerator.multiply(new BigInteger("-1")), this.denominator);
    }

    public RationalNumber multiplicativeInverse() throws RationalNumberException {
        if (this.numerator.equals(BigInteger.ZERO)) {
            throw new RationalNumberException("0 has no defined multiplicative inverse.");
        }
        return new RationalNumber(this.denominator, this.numerator);

    }

    public RationalNumber absolute() {
        if (this.numerator.compareTo(BigInteger.ZERO) < 0) {
            return this.opposite();
        }
        return this;
    }

    public RationalNumber rounded(RationalNumber decimalsToRound) throws RationalNumberException {
        if (!decimalsToRound.isAnInteger()) {
            throw new RationalNumberException("The number of decimals to round must be an integer number.");
        }
        if (decimalsToRound.isNegative()) {
            throw new RationalNumberException("The number of decimals to round must be a non-negative number.");
        }
        RationalNumber multiplier = RationalNumber.TEN.power(decimalsToRound);
        RationalNumber aux = this.multiply(multiplier);
        RationalNumber result = aux.integerPart();
        aux = aux.decimalPart().absolute();
        if (aux.compareTo(new RationalNumber("1/2")) >= 0) {
            if (result.isNegative()) {
                result = result.subtract(RationalNumber.ONE);
            } else {
                result = result.add(RationalNumber.ONE);
            }
        }
        return result.divide(multiplier);
    }

    public boolean isAnInteger() {
        return this.denominator.equals(BigInteger.ONE);
    }

    public boolean isNegative() {
        return this.numerator.compareTo(BigInteger.ZERO) < 0;
    }

    public String toFraction() {
        return String.format("%s/%s", this.numerator, this.denominator);
    }

    public BigDecimal toBigDecimal() throws ArithmeticException {
        return new BigDecimal(this.numerator).divide(new BigDecimal(this.denominator));
    }

    public BigDecimal toBigDecimal(int precision) {//Rever o 0 funcionar como o sem parametro
        return new BigDecimal(this.numerator).divide(new BigDecimal(this.denominator), new MathContext(precision));
    }

    /**
     * Tries to fully convert to BigDecimal, in not possible returns a
     * BigDecimal with the desired precision.
     *
     * @param precision
     * @return
     */
    public BigDecimal toBigDecimal2(int precision) {
        try {
            return toBigDecimal();
        } catch (ArithmeticException e) {
            return toBigDecimal(precision);
        }
    }

    public RationalNumber integerPart() {
        return new RationalNumber(this.numerator.divide(this.denominator));
    }

    public RationalNumber decimalPart() {
        return this.subtract(this.integerPart());
    }

    public BigInteger numerator() {
        return this.numerator;
    }

    public BigInteger denominator() {
        return this.denominator;
    }

    public int toInt() throws RationalNumberException {
        try {
            return this.integerPart().numerator.intValueExact();
        } catch (ArithmeticException ex) {
            throw new RationalNumberException("This value is not a valid integer.");
        }
    }

    public long toLong() throws RationalNumberException {
        try {
            return this.integerPart().numerator.longValueExact();
        } catch (ArithmeticException ex) {
            throw new RationalNumberException("This value is not a valid integer.");
        }
    }

    @Override
    public String toString() {
        return String.format("Numerator: %s\nDenominator: %s", this.numerator.toString(), this.denominator.toString());
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof RationalNumber)) {
            return false;
        }
        RationalNumber rn = (RationalNumber) o;
        return this.numerator.equals(rn.numerator) && this.denominator.equals(rn.denominator);
    }

    @Override
    public int compareTo(RationalNumber number) {
        if (this.denominator.equals(number.denominator)) {
            return this.numerator.compareTo(number.numerator);
        } else {
            return this.numerator.multiply(number.denominator).compareTo(number.numerator.multiply(this.denominator));
        }
    }

}
