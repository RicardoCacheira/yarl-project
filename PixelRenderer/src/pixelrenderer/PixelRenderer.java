/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pixelrenderer;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;

/**
 *
 * @author Ricardo
 */
public class PixelRenderer extends JFrame {

    private PixelComponent pixelComponent;

    public PixelRenderer(int pixelSize, int xPixels, int yPixels, int layers, Color background) throws HeadlessException {
        super();
        this.pixelComponent = new PixelComponent(pixelSize, xPixels, yPixels, layers, background);

        add(pixelComponent);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        setVisible(true);
    }

    public void drawPixel(int xPos, int yPos, int layer, Color color) {
        this.pixelComponent.drawPixel(xPos, yPos, layer, color);
    }

    public void drawSimpleLine(int xStart, int yStart, int lenght, int lineType, int layer, Color color) {
        this.pixelComponent.drawSimpleLine(xStart, yStart, lenght, lineType, layer, color);
    }

    public void drawLine(int xStart, int yStart, int xEnd, int yEnd, int layer, Color color) {
        this.pixelComponent.drawLine(xStart, yStart, xEnd, yEnd, layer, color);
    }

    public void drawRectangle(int xStart, int yStart, int xLenght, int yLenght, int layer, Color color) {
        this.pixelComponent.drawRectangle(xStart, yStart, xLenght, yLenght, layer, color);
    }

    public void drawFilledRectangle(int xStart, int yStart, int xLenght, int yLenght, int layer, Color color) {
        this.pixelComponent.drawFilledRectangle(xStart, yStart, xLenght, yLenght, layer, color);
    }

    public void disableLayer(int layer) {
        this.pixelComponent.disableLayer(layer);
    }

    public void enableLayer(int layer) {
        this.pixelComponent.enableLayer(layer);
    }

    public void addMouseListenerToPixelComponent(MouseListener ml) {
        this.pixelComponent.addMouseListener(ml);
    }

    public void addKeyEventDispatcher(KeyEventDispatcher dispatcher) {
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(dispatcher);
    }

    public int[] getCorrespondentPixel(int xPos, int yPos) {
        return this.pixelComponent.getCorrespondentPixel(xPos, yPos);
    }

    public void clearLayer(int layer) {
        this.pixelComponent.clearLayer(layer);
    }

    public void clear() {
        this.pixelComponent.clear();
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.pixelComponent.setBackgroundColor(backgroundColor);
    }

    public void saveImage(String imageFile) {
        this.pixelComponent.save(imageFile);
    }

    public static void main(String[] args) {
        PixelRenderer pr = new PixelRenderer(20, 20, 10, 1, Color.BLACK);
        class BagOData {

            boolean p1Set = false;
            boolean p2Set = false;
            int[] p1Pos;
            int[] p2Pos;
        }
        /*   boolean p1Set = false;
        boolean p2Set = false;
        int[] p1Pos;
        int[] p2Pos;*/
        BagOData bod = new BagOData();
        pr.addKeyEventDispatcher(new KeyEventDispatcher() {
            @Override
            public boolean dispatchKeyEvent(KeyEvent ke) {
                if (ke.getID() == KeyEvent.KEY_PRESSED) {
                    if (ke.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                        pr.clear();
                        bod.p1Set = false;
                        bod.p2Set = false;
                    } else if (ke.getKeyCode() == KeyEvent.VK_S) {
                        System.out.println("saved");
                        pr.saveImage("capture.png");
                    } else if (bod.p1Set && bod.p2Set) {
                        pr.drawLine(bod.p1Pos[0], bod.p1Pos[1], bod.p2Pos[0], bod.p2Pos[1], 0, Color.WHITE);
                        bod.p1Set = false;
                        bod.p2Set = false;
                    }
                }
                return false;
            }
        });
        pr.addMouseListenerToPixelComponent(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                int[] pos = pr.getCorrespondentPixel(me.getX(), me.getY());
                switch (me.getButton()) {
                    case MouseEvent.BUTTON1:
                        if (bod.p1Set) {
                            pr.drawPixel(bod.p1Pos[0], bod.p1Pos[1], 0, Color.BLACK);
                        }
                        pr.drawPixel(pos[0], pos[1], 0, Color.BLUE);
                        bod.p1Set = true;
                        bod.p1Pos = pos;
                        break;
                    case MouseEvent.BUTTON2:
                        if (bod.p1Set) {
                            pr.drawPixel(bod.p1Pos[0], bod.p1Pos[1], 0, Color.BLACK);
                            bod.p1Set = false;
                        }
                        if (bod.p2Set) {
                            pr.drawPixel(bod.p2Pos[0], bod.p2Pos[1], 0, Color.BLACK);
                            bod.p2Set = false;
                        }

                        break;
                    case MouseEvent.BUTTON3:
                        if (bod.p2Set) {
                            pr.drawPixel(bod.p2Pos[0], bod.p2Pos[1], 0, Color.BLACK);
                        }
                        pr.drawPixel(pos[0], pos[1], 0, Color.RED);
                        bod.p2Set = true;
                        bod.p2Pos = pos;
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }
        });
    }

}
