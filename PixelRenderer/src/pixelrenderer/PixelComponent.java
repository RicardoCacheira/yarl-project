/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pixelrenderer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 *
 * @author Ricardo
 */
public class PixelComponent extends JComponent {

    public static final int VERTICAL_LINE = 0;

    public static final int HORIZONTAL_LINE = 1;

    private int pixelSize;

    private Color backgroundColor;

    private Color[][][] pixelBuffer;

    boolean[] activeLayers;

    private int xPixels;
    private int yPixels;
    private int layerNum;

    public PixelComponent(int pixelSize, int xPixels, int yPixels, int layers, Color background) {
        super();
        this.pixelSize = pixelSize;
        this.pixelBuffer = new Color[layers][xPixels][yPixels];
        this.backgroundColor = background;
        this.xPixels = xPixels;
        this.yPixels = yPixels;
        this.layerNum = layers;
        this.activeLayers = new boolean[layers];
        for (int i = 0; i < layers; i++) {
            this.activeLayers[i] = true;
            for (int j = 0; j < xPixels; j++) {
                for (int k = 0; k < yPixels; k++) {
                    this.pixelBuffer[i][j][k] = new Color(0, 0, 0, 0);
                }
            }
        }
        setPreferredSize(new Dimension(xPixels * pixelSize, yPixels * pixelSize));
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D g = (Graphics2D) graphics;
        g.setPaint(backgroundColor);
        g.fill(new Rectangle2D.Double(0, 0, xPixels * pixelSize, yPixels * pixelSize));
        for (int i = 0; i < layerNum; i++) {
            if (activeLayers[i]) {
                for (int j = 0; j < xPixels; j++) {
                    for (int k = 0; k < yPixels; k++) {
                        g.setPaint(pixelBuffer[i][j][k]);
                        g.fill(new Rectangle2D.Double(j * pixelSize, k * pixelSize, pixelSize, pixelSize));
                    }
                }
            }
        }
        for (int i = 0; i < xPixels; i++) {
            for (int j = 0; j < yPixels; j++) {

                g.setPaint(Color.BLACK);
                g.draw(new Rectangle2D.Double(i * pixelSize, j * pixelSize, pixelSize, pixelSize));

            }
        }
    }

    private void drawPixel(int xPos, int yPos, int layer, Color color, boolean repaint) throws IllegalArgumentException {
        if (xPos < 0 || yPos < 0 || layer < 0 || color == null) {
            throw new IllegalArgumentException("Negative ints or null color.");
        }
        if (layer < this.layerNum && xPos < this.xPixels && yPos < this.yPixels) {
            this.pixelBuffer[layer][xPos][yPos] = color;
            if (repaint) {
                revalidate();
                repaint();
            }
        }
    }

    public void drawPixel(int xPos, int yPos, int layer, Color color) throws IllegalArgumentException {
        this.drawPixel(xPos, yPos, layer, color, true);
    }

    private void drawSimpleLine(int xStart, int yStart, int lenght, int lineType, int layer, Color color, boolean repaint) throws IllegalArgumentException {
        if (xStart < 0 || yStart < 0 || lenght < 0 || lineType < 0 || layer < 0 || color == null) {
            throw new IllegalArgumentException("Negative ints or null color.");
        }
        switch (lineType) {
            case PixelComponent.VERTICAL_LINE:
                for (int i = 0; i < lenght; i++) {
                    this.drawPixel(xStart, yStart + i, layer, color, false);
                }
                break;
            case PixelComponent.HORIZONTAL_LINE:
                for (int i = 0; i < lenght; i++) {
                    this.drawPixel(xStart + i, yStart, layer, color, false);
                }
                break;
        }
        if (repaint) {
            revalidate();
            repaint();
        }
    }

    public void drawSimpleLine(int xStart, int yStart, int lenght, int lineType, int layer, Color color) throws IllegalArgumentException {
        this.drawSimpleLine(xStart, yStart, lenght, lineType, layer, color, true);
    }

    private void drawLine(int xStart, int yStart, int xEnd, int yEnd, int layer, Color color, boolean repaint) throws IllegalArgumentException {
        if (xStart < 0 || yStart < 0 || xEnd < 0 || yEnd < 0 || layer < 0 || color == null) {
            throw new IllegalArgumentException("Negative ints or null color.");
        }
        if (xStart == xEnd) {
            if (yStart > yEnd) {
                int aux = yStart;
                yStart = yEnd;
                yEnd = aux;
            }
            this.drawSimpleLine(xStart, yStart, yEnd - yStart + 1, VERTICAL_LINE, layer, color, false);
        } else if (yStart == yEnd) {
            if (xStart > xEnd) {
                int aux = xStart;
                xStart = xEnd;
                xEnd = aux;
            }
            this.drawSimpleLine(xStart, yStart, xEnd - xStart + 1, HORIZONTAL_LINE, layer, color, false);
        } else if (Math.abs(yEnd - yStart) > Math.abs(xEnd - xStart)) {//More values of y than values of x
            if (yStart > yEnd) {
                int aux = xStart;
                xStart = xEnd;
                xEnd = aux;
                aux = yStart;
                yStart = yEnd;
                yEnd = aux;
            }
            //x=ay+b
            double a = (double) (xEnd - xStart) / (yEnd - yStart);
            double b = (double) xStart - yStart * a;
            for (int i = yStart; i <= yEnd; i++) {
                this.drawPixel((int) Math.round(a * i + b), i, layer, color, false);
            }
        } else {//More values of x than values of y or the same amount
            if (xStart > xEnd) {
                int aux = xStart;
                xStart = xEnd;
                xEnd = aux;
                aux = yStart;
                yStart = yEnd;
                yEnd = aux;
            }
            //y=ax+b
            double a = (double) (yEnd - yStart) / (xEnd - xStart);
            double b = (double) yStart - xStart * a;
            for (int i = xStart; i <= xEnd; i++) {
                this.drawPixel(i, (int) Math.round(a * i + b), layer, color, false);
            }
        }

        if (repaint) {
            revalidate();
            repaint();
        }
    }

    public void drawLine(int xStart, int yStart, int xEnd, int yEnd, int layer, Color color) throws IllegalArgumentException {
        this.drawLine(xStart, yStart, xEnd, yEnd, layer, color, true);
    }

    private void drawRectangle(int xStart, int yStart, int xLenght, int yLenght, int layer, Color color, boolean repaint) throws IllegalArgumentException {
        if (xStart < 0 || yStart < 0 || xLenght < 0 || yLenght < 0 || layer < 0 || color == null) {
            throw new IllegalArgumentException("Negative ints or null color.");
        }
        this.drawSimpleLine(xStart, yStart, xLenght, PixelComponent.VERTICAL_LINE, layer, color, false);
        this.drawSimpleLine(xStart, yStart, yLenght, PixelComponent.HORIZONTAL_LINE, layer, color, false);
        this.drawSimpleLine(xStart, yStart + yLenght - 1, xLenght, PixelComponent.VERTICAL_LINE, layer, color, false);
        this.drawSimpleLine(xStart + xLenght - 1, yStart, yLenght, PixelComponent.HORIZONTAL_LINE, layer, color, false);
        if (repaint) {
            revalidate();
            repaint();
        }
    }

    public void drawRectangle(int xStart, int yStart, int xLenght, int yLenght, int layer, Color color) throws IllegalArgumentException {
        this.drawRectangle(xStart, yStart, xLenght, yLenght, layer, color, true);
    }

    private void drawFilledRectangle(int xStart, int yStart, int xLenght, int yLenght, int layer, Color color, boolean repaint) throws IllegalArgumentException {
        if (xStart < 0 || yStart < 0 || xLenght < 0 || yLenght < 0 || layer < 0 || color == null) {
            throw new IllegalArgumentException("Negative ints or null color.");
        }
        for (int i = 0; i < yLenght; i++) {
            this.drawSimpleLine(xStart, yStart + i, xLenght, PixelComponent.VERTICAL_LINE, layer, color, false);
        }
        if (repaint) {
            revalidate();
            repaint();
        }
    }

    public void drawFilledRectangle(int xStart, int yStart, int xLenght, int yLenght, int layer, Color color) throws IllegalArgumentException {
        this.drawFilledRectangle(xStart, yStart, xLenght, yLenght, layer, color, true);
    }

    public void disableLayer(int layer) {
        this.activeLayers[layer] = false;
    }

    public void enableLayer(int layer) {
        this.activeLayers[layer] = true;
    }

    public int[] getCorrespondentPixel(int xPos, int yPos) {
        return new int[]{xPos / this.pixelSize, yPos / this.pixelSize};
    }

    private void clearLayer(int layer, boolean repaint) {
        for (int i = 0; i < xPixels; i++) {
            for (int j = 0; j < yPixels; j++) {
                this.pixelBuffer[layer][i][j] = new Color(0, 0, 0, 0);
            }
        }
        if (repaint) {
            revalidate();
            repaint();
        }
    }

    public void clearLayer(int layer) {
        this.clearLayer(layer, true);
    }

    public void clear() {
        for (int i = 0; i < this.layerNum; i++) {
            this.clearLayer(i, false);
        }
        revalidate();
        repaint();
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void save(String imageFile) {
        Rectangle r = getBounds();
        try {
            BufferedImage i = new BufferedImage(r.width, r.height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics g = i.getGraphics();
            paint(g);
            ImageIO.write(i, "png", new File(imageFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
