/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module_loader;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import yarli.YARLI;
import yarli.YARLI_Module;

/**
 *
 * @author Ricardo
 */
public class YARLI_ModuleLoader extends URLClassLoader {

    public YARLI_ModuleLoader(URL[] urls) {
        super(urls);
    }

    public void addFile(File jarPack) throws MalformedURLException {
        URL fileURL = jarPack.toURI().toURL();
        String urlPath = "jar:" + fileURL.toString() + "!/";
        System.out.println(urlPath);
        addURL(new URL(urlPath));
    }

    public void loadYARLIModuleLibs(File libFolder, YARLI interpreter) {
        for (File lib : libFolder.listFiles()) {
            try {
                this.addFile(lib);
                interpreter.showOutput("Library loaded: \"" + lib.getName() + "\"\n", YARLI.INFO_OUTPUT);
            } catch (MalformedURLException ex) {
                interpreter.showOutput("Error 2\n", YARLI.ERROR_OUTPUT);
            }
        }
    }

    public YARLI_Module[] loadJarModulePack(File modulePack, YARLI interpreter, boolean showCommands) {//<---------------
        try {
            this.addFile(modulePack);
            Class c = this.loadClass("loader.ModulePackLoader");
            YARLI_Module[] modules = (YARLI_Module[]) c.getMethod("loadModules", new Class[]{Class.forName("yarli.YARLI")}).invoke(null, new Object[]{interpreter});//-------------Rever
            if (modules == null || modules.length == 0) {
                interpreter.throwWarning("No modules loaded from pack \"" + modulePack.getName() + "\".");
            } else {
                interpreter.showOutput("Loading modules from pack \"" + modulePack.getName() + "\":\n", YARLI.INFO_OUTPUT);
                for (YARLI_Module module : modules) {
                    interpreter.showOutput("-> " + module.moduleName() + " loaded.\n", YARLI.INFO_OUTPUT);
                    if (showCommands) {
                        interpreter.showOutput("     Possible commands:\n", YARLI.INFO_OUTPUT);
                        for (String command : module.commandList()) {
                            interpreter.showOutput("       " + command + "\n", YARLI.INFO_OUTPUT);
                        }
                        interpreter.showOutput("\n", YARLI.INFO_OUTPUT);
                    }
                }
                return modules;
            }
        } catch (MalformedURLException ex) {
            interpreter.showOutput("Error 1\n", YARLI.ERROR_OUTPUT);
        } catch (ClassNotFoundException ex) {
            interpreter.showOutput("ModulePackLoader class not found in the \"" + modulePack.getName() + "\" pack.\n", YARLI.ERROR_OUTPUT);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            interpreter.showOutput("Error running \"loadModules\" in the \"" + modulePack.getName() + "\" pack.\n", YARLI.ERROR_OUTPUT);
        }
        return null;
    }

}
