/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.ArrayList;
import java.util.List;
import rationalnumber.parser.RationalNumberParser;

/**
 *
 * @author Ricardo
 */
public class YARLLexer {

    private StringBuilder tokenBuffer;
    private List<Token> tokenList;
    private boolean isReadingToken;

    private void throwHighlightedExeption(String msn, String source, String token) throws YARLYParserException {
        int startPos = source.indexOf(token);
        int endPos = startPos + token.length() - 1;
        throw new YARLYParserException(msn, source, startPos, endPos);
    }

    public static boolean isValidNumber(String num) {
        return RationalNumberParser.isNumberValid(num);
    }

    private void closeBuffer() {
        isReadingToken = false;
        String str = tokenBuffer.toString();
        if (isValidNumber(str)) {
            tokenList.add(new Token(str, Token.TokenType.NUMBER));
        } else {
            tokenList.add(new Token(str, Token.TokenType.ID));
        }
    }

    private void closeBufferAsVecPos(String source) {
        isReadingToken = false;
        String str = tokenBuffer.toString();
        if (isValidNumber(str)) {
            throwHighlightedExeption("Invalid vector name", source, str);
        } else {
            tokenList.add(new Token(str, Token.TokenType.VEC_POS));
        }
    }

    public List<Token> analizeCommand(String source) {
        tokenList = new ArrayList<>();
        isReadingToken = false;
        char[] sourceChars = source.toCharArray();
        for (int i = 0; i < sourceChars.length; i++) {
            if (!Character.isWhitespace(sourceChars[i])) {//Ignores whitespaces
                switch (sourceChars[i]) {
                    case ':':
                        if (isReadingToken) {
                            closeBuffer();
                        }
                        this.tokenList.add(new Token(":", Token.TokenType.SEPERATOR));
                        break;

                    case '[':
                        if (!isReadingToken) {
                            if (i != 0 && source.charAt(i - 1) != ']') {
                                throw new YARLYParserException("Invalid '['", source, i);
                            }
                        }
                        if (source.charAt(i - 1) != ']') {
                            closeBufferAsVecPos(source);
                        }
                        this.tokenList.add(new Token("[", Token.TokenType.VEC_POS_START));
                        break;
                    case ']':
                        if (isReadingToken) {
                            closeBuffer();
                        }
                        this.tokenList.add(new Token("]", Token.TokenType.VEC_POS_END));
                        break;
                    case '(':
                        if (isReadingToken) {
                            throw new YARLYParserException("Invalid Command Start", source, i);
                        }
                        this.tokenList.add(new Token("(", Token.TokenType.COMMAND_START));
                        break;
                    case ')':
                        if (isReadingToken) {
                            closeBuffer();
                        }
                        this.tokenList.add(new Token(")", Token.TokenType.COMMAND_END));
                        break;
                    case '"':
                        if (isReadingToken) {
                            throw new YARLYParserException("Invalid quotation mark", source, i);
                        }
                        i++;
                        if (i == sourceChars.length) {
                            throw new YARLYParserException("Line end with '\"'");
                        }
                        StringBuilder strBuffer = new StringBuilder();
                        while (sourceChars[i] != '"') {
                            if (sourceChars[i] == '\\') {
                                i++;
                                if (i == sourceChars.length) {
                                    throw new YARLYParserException("Line end with '\\'");
                                }
                                switch (sourceChars[i]) {
                                    case 'n':
                                        strBuffer.append('\n');
                                        break;
                                    case 't':
                                        strBuffer.append('\t');
                                        break;
                                    case 'r':
                                        strBuffer.append('\r');
                                        break;
                                    case 'b':
                                        strBuffer.append('\b');
                                        break;
                                    case '"':
                                        strBuffer.append('"');
                                        break;
                                    case '\\':
                                        strBuffer.append('\\');
                                        break;
                                    default:
                                        throwHighlightedExeption("Invalid escape sequence", source, "\\" + sourceChars[i]);
                                }
                            } else {
                                strBuffer.append(sourceChars[i]);
                            }
                            i++;
                            if (i == sourceChars.length) {
                                throw new YARLYParserException("Open String definition\n");
                            }
                        }
                        tokenList.add(new Token(strBuffer.toString(), Token.TokenType.STRING));
                        break;
                    case '/'://Checks of this is a comment
                        if (i < sourceChars.length - 1) {
                            if (sourceChars[i + 1] == '/') {
                                if (isReadingToken) {
                                    closeBuffer();
                                }
                                this.tokenList.add(new Token(source.substring(i), Token.TokenType.COMMENT));
                                i = sourceChars.length;
                                break;
                            }
                        }
                    default:
                        if (!isReadingToken) {
                            isReadingToken = true;
                            tokenBuffer = new StringBuilder();
                        }
                        tokenBuffer.append(sourceChars[i]);
                        break;

                }
            } else if (isReadingToken) {//if it was previously reading a command
                closeBuffer();
            }
        }
        if (isReadingToken) {//Writes the last command
            closeBuffer();
        }

        return this.tokenList;
    }

}
