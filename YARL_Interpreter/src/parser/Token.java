/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

/**
 *
 * @author Ricardo
 */
public class Token {

    public static enum TokenType {
        ID, SEPERATOR, STRING, NUMBER, COMMENT, VEC_POS_START, VEC_POS_END, VEC_POS, COMMAND_START, COMMAND_END
    }

    private final String content;

    private final TokenType type;

    public Token(String content, TokenType type) {
        this.content = content;
        this.type = type;
    }

    public String content() {
        return this.content;
    }

    public TokenType type() {
        return this.type;
    }

    @Override
    public String toString() {
        return "Content: " + this.content + "\nType: " + this.type + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Token)) {
            return false;
        }
        Token t = (Token) o;
        return this.type == t.type && this.content.equals(t.content);
    }
}
