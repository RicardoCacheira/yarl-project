/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import simplelib.HighlightedExeption;

/**
 *
 * @author Ricardo
 */
public class YARLYParserException extends HighlightedExeption {
    
    public YARLYParserException(String msn){
        super(msn);
    }
    
    public YARLYParserException(String msn, String source, int pos) {
        super(msn, source, pos);
    }

    public YARLYParserException(String msn, String source, int startPos, int endPos) {
        super(msn, source, startPos, endPos);
    }

}
