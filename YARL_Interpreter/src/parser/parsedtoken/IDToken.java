/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.parsedtoken;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class IDToken implements ParsedToken {

    public static final String ID_TYPE = "Id";

    private String id;

    public IDToken(String id) {
        this.id = id;
    }

    @Override
    public void runTokenAction(YARLI interpreter) {
        if (interpreter.hasVariable(id)) {
            interpreter.push(interpreter.getVariableValue(id));
        } else if (interpreter.hasCommand(id)) {
            interpreter.runIDCommand(id);
        } else {
            interpreter.throwError(String.format("No Command/Variable found with ID \"%s\"", id));
        }
    }

    @Override
    public String content() {
        return id;
    }

    @Override
    public String type() {
        return ID_TYPE;
    }

    @Override
    public String toString() {
        return "Type: " + ID_TYPE + "\nContent: " + this.id + "\n";
    }

}
