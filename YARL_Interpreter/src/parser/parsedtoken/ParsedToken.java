/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.parsedtoken;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public interface ParsedToken {

    public void runTokenAction(YARLI interpreter);

    public String content();

    public String type();

}
