/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.parsedtoken;

import rationalnumber.RationalNumber;
import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class NumberToken implements ParsedToken {

    public static final String NUMBER_TYPE = "Number";

    private String number;

    public NumberToken(String number) {
        this.number = number;
    }

    @Override
    public void runTokenAction(YARLI interpreter) {
        interpreter.push(new RationalNumber(this.number));
    }

    @Override
    public String content() {
        return number;
    }

    @Override
    public String type() {
        return NUMBER_TYPE;
    }

    @Override
    public String toString() {
        return "Type: " + NUMBER_TYPE + "\nContent: " + this.number + "\n";
    }

}
