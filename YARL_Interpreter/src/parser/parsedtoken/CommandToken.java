/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.parsedtoken;

import java.util.List;
import parser.ParsedCommand;
import parser.Token;
import parser.YARLParser;
import yarli.YARLI;
import yarli.obj_str_handler.ParsedComStrHandler;

/**
 *
 * @author Ricardo
 */
public class CommandToken implements ParsedToken {

    public static final String COMMAND_TYPE = "Command";

    private final ParsedCommand command;

    public CommandToken(YARLParser parser, List<Token> commandTokenList) {
        this.command = parser.parseCommand(commandTokenList);
    }

    @Override
    public void runTokenAction(YARLI interpreter) {
        interpreter.push(this.command);
    }

    @Override
    public String content() {
        return new ParsedComStrHandler().generateString(this.command);
    }

    @Override
    public String type() {
        return COMMAND_TYPE;
    }

    @Override
    public String toString() {
        return "Type: " + COMMAND_TYPE + "\nContent:\n" + this.command;
    }
}
