/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.parsedtoken;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class StringToken implements ParsedToken {

    public static final String STRING_TYPE = "String";

    private String str;

    public StringToken(String str) {
        this.str = str;
    }

    @Override
    public void runTokenAction(YARLI interpreter) {
        interpreter.push(this.str);
    }

    @Override
    public String content() {
        return this.str;
    }

    @Override
    public String type() {
        return STRING_TYPE;
    }

    @Override
    public String toString() {
        return "Type: " + STRING_TYPE + "\nContent: " + this.str + "\n";
    }
}
