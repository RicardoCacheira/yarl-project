/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.parsedtoken;

import java.util.List;
import parser.ParsedCommand;
import parser.Token;
import parser.YARLParser;
import rationalnumber.RationalNumber;
import yarli.YARLI;
import yarli.obj_str_handler.StringStrHandler;

/**
 *
 * @author Ricardo
 */
public class VecPosToken implements ParsedToken {

    public static final String VEC_POS_TYPE = "VecPos";

    private String vecName;

    private ParsedCommand firstPosCommand;

    private ParsedCommand secondPosCommand;

    private boolean isTwoDimension;

    private VecPosToken(YARLParser parser, String vecName, List<Token> firstPosCommandTokenList, boolean isTwoDimansion) throws IllegalArgumentException {
        this.vecName = vecName;
        this.isTwoDimension = isTwoDimansion;
        this.firstPosCommand = parser.parseCommand(firstPosCommandTokenList);
        if (this.firstPosCommand.type() == ParsedCommand.ParsedCommandType.DEFINITION || this.firstPosCommand.type() == ParsedCommand.ParsedCommandType.DEFINITION_WITH_VAR) {
            throw new IllegalArgumentException("The first position must be a valid runnable command");
        }
    }

    public VecPosToken(YARLParser parser, String vecName, List<Token> firstPosCommandTokenList) throws IllegalArgumentException {
        this(parser, vecName, firstPosCommandTokenList, false);
    }

    public VecPosToken(YARLParser parser, String vecName, List<Token> firstPosCommandTokenList, List<Token> secondPosCommandTokenList) throws IllegalArgumentException {
        this(parser, vecName, firstPosCommandTokenList, true);
        this.secondPosCommand = parser.parseCommand(secondPosCommandTokenList);
        if (this.secondPosCommand.type() == ParsedCommand.ParsedCommandType.DEFINITION || this.secondPosCommand.type() == ParsedCommand.ParsedCommandType.DEFINITION_WITH_VAR) {
            throw new IllegalArgumentException("The second position must be a valid runnable command");
        }
    }

    public String name() {
        return this.vecName;
    }

    public boolean isTwoDimension() {
        return this.isTwoDimension;
    }

    /**
     *
     * @param interpreter
     * @return Returns a empty vector if one of the position is invalid
     */
    public int[] getPositions(YARLI interpreter) {
        if (interpreter.runParsedCommand(this.firstPosCommand)) {
            Object firstPos = interpreter.pop();
            if (firstPos instanceof RationalNumber) {
                int firstPosInt = interpreter.generateNonNegativeInt((RationalNumber) firstPos);
                if (firstPosInt != -1) {
                    if (this.isTwoDimension) {
                        if (interpreter.runParsedCommand(this.secondPosCommand)) {
                            Object secondPos = interpreter.pop();
                            if (secondPos instanceof RationalNumber) {
                                int secondPosInt = interpreter.generateNonNegativeInt((RationalNumber) secondPos);
                                if (secondPosInt != -1) {
                                    return new int[]{firstPosInt, secondPosInt};
                                } else {
                                    interpreter.throwError("Second position number isn't a valid position");
                                }
                            } else {
                                interpreter.throwError("Second position didn't return a Number");
                            }
                        } else {
                            interpreter.throwError("Second position broke execution");
                        }
                    } else {//If its one dimension vector/array
                        return new int[]{firstPosInt};
                    }
                } else {
                    interpreter.throwError("First position number isn't a valid position");
                }
            } else {
                interpreter.throwError("First position didn't return a Number");
            }
        } else {
            interpreter.throwError("First position broke execution");
        }
        return new int[]{};
    }

    @Override
    public void runTokenAction(YARLI interpreter) {
        boolean hasArray = interpreter.hasArray(vecName);
        boolean hasVector = interpreter.hasVector(vecName);
        boolean hasMatrix = interpreter.hasMatrix(vecName);
        if ((hasArray || hasVector) && !this.isTwoDimension) {
            int[] pos = this.getPositions(interpreter);
            if (pos.length == 1) {
                try {
                    interpreter.push(interpreter.getArrayVectorPos(vecName, pos[0]));
                } catch (IndexOutOfBoundsException ex) {
                    interpreter.throwError("Invalid position");
                }
            }
        } else if (hasMatrix && this.isTwoDimension) {
            int[] pos = this.getPositions(interpreter);
            if (pos.length == 2) {
                try {
                    interpreter.push(interpreter.getMatrixPos(vecName, pos[0], pos[1]));
                } catch (IndexOutOfBoundsException ex) {
                    interpreter.throwError(ex.getMessage());
                }
            }
        } else {
            interpreter.throwError("Invalid VecPos");
        }

    }

    @Override
    public String content() {
        StringStrHandler stringHandler = new StringStrHandler();
        StringBuilder sb = new StringBuilder(this.vecName);
        sb.append('[');
        for (ParsedToken token : firstPosCommand.runnableSection()) {
            sb.append(' ');
            if (token.type().equals(StringToken.STRING_TYPE)) {
                sb.append(stringHandler.generateString(token.content()));
            } else {
                sb.append(token.content());
            }
        }
        sb.append(' ');
        sb.append(']');
        if (this.isTwoDimension) {
            sb.append('[');
            for (ParsedToken token : secondPosCommand.runnableSection()) {
                sb.append(' ');
                if (token.type().equals(StringToken.STRING_TYPE)) {
                    sb.append(stringHandler.generateString(token.content()));
                } else {
                    sb.append(token.content());
                }
            }
            sb.append(' ');
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public String type() {
        return VEC_POS_TYPE;
    }

    @Override
    public String toString() {
        if (this.isTwoDimension) {
            return "Type: " + VEC_POS_TYPE + "\nContent: \n\nVecName: " + this.vecName + "\nFirstPos:\n" + this.firstPosCommand.toString() + "\nSecondPos:\n" + this.secondPosCommand.toString();
        }
        return "Type: " + VEC_POS_TYPE + "\nContent: \n\nVecName: " + this.vecName + "\nFirstPos:\n" + this.firstPosCommand.toString() + "\n";
    }
}
