/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.ArrayList;
import java.util.List;
import parser.parsedtoken.CommandToken;
import parser.parsedtoken.IDToken;
import parser.parsedtoken.NumberToken;
import parser.parsedtoken.ParsedToken;
import parser.parsedtoken.StringToken;
import parser.parsedtoken.VecPosToken;

/**
 *
 * @author Ricardo
 */
public class YARLParser {

    public static int MAX_SEPERATOR_NUM = 2;

    private void throwHighlightedExeption(String msn, String source, String token) throws YARLYParserException {
        int startPos = source.indexOf(token);
        int endPos = startPos + token.length() - 1;
        throw new YARLYParserException(msn, source, startPos, endPos);
    }

    public static String highlightChar(String source, int charPos) {
        StringBuilder sb = new StringBuilder(source);
        sb.append('\n');
        for (int i = 0; i < charPos; i++) {
            sb.append(' ');
        }
        sb.append("^\n");
        return sb.toString();
    }

    public ParsedCommand parseCommand(List<Token> tokenList) throws YARLYParserException {
        ArrayList<ArrayList<ParsedToken>> sectionList = new ArrayList<>();
        int foundSep = 0;
        ArrayList<ParsedToken> currentSection = new ArrayList<>();
        for (int i = 0; i < tokenList.size(); i++) {
            Token currentToken = tokenList.get(i);
            switch (currentToken.type()) {
                case ID:
                    currentSection.add(new IDToken(currentToken.content()));
                    break;
                case NUMBER:
                    currentSection.add(new NumberToken(currentToken.content()));
                    break;
                case STRING:
                    currentSection.add(new StringToken(currentToken.content()));
                    break;
                case SEPERATOR:
                    foundSep++;
                    if (foundSep > MAX_SEPERATOR_NUM) {
                        throw new YARLYParserException("Too many seperators");
                    }
                    sectionList.add(currentSection);
                    currentSection = new ArrayList<>();
                    break;
                case VEC_POS_START:
                case COMMAND_END:
                case VEC_POS_END:
                    throw new YARLYParserException("ERROR END");
                case COMMAND_START:
                    int currentOpenCommands = 1;
                    ArrayList<Token> auxList = new ArrayList<>();
                    boolean commandEndFound = false;
                    while (i < tokenList.size() - 1 && !commandEndFound) {
                        i++;
                        currentToken = tokenList.get(i);
                        switch (currentToken.type()) {
                            case COMMAND_START:
                                currentOpenCommands++;
                                auxList.add(currentToken);
                                break;
                            case ID:
                            case NUMBER:
                            case STRING:
                            case SEPERATOR:
                            case VEC_POS_START:
                            case VEC_POS_END:
                            case VEC_POS:
                                auxList.add(currentToken);
                                break;
                            case COMMAND_END:
                                currentOpenCommands--;
                                if (currentOpenCommands == 0) {//All parenthesis pairs were found
                                    commandEndFound = true;
                                    currentSection.add(new CommandToken(this, auxList));
                                } else {
                                    auxList.add(currentToken);
                                    break;
                                }
                            case COMMENT:
                                break;
                        }
                    }
                    if (!commandEndFound) {
                        throw new YARLYParserException(String.format("Open parenthesis found."));
                    }
                    break;
                case VEC_POS:
                    String vecName = currentToken.content();
                    int currentOpenBrakets = 1;
                    ArrayList<Token> firstPosList = new ArrayList<>();
                    boolean firstBraketEndFound = false;
                    i++;//Ignores the VecPos first VEC_POS_START
                    while (i < tokenList.size() - 1 && !firstBraketEndFound) {
                        i++;
                        currentToken = tokenList.get(i);
                        switch (currentToken.type()) {
                            case VEC_POS_START:
                                currentOpenBrakets++;
                                firstPosList.add(currentToken);
                                break;
                            case VEC_POS_END:
                                currentOpenBrakets--;
                                if (currentOpenBrakets == 0) {//All parenthesis pairs were found
                                    firstBraketEndFound = true;
                                    break;
                                } else {
                                    firstPosList.add(currentToken);
                                    break;
                                }
                            case COMMAND_START:
                            case ID:
                            case NUMBER:
                            case STRING:
                            case SEPERATOR:
                            case VEC_POS:
                            case COMMAND_END:
                                firstPosList.add(currentToken);
                            case COMMENT:
                                break;
                        }
                    }
                    if (!firstBraketEndFound) {
                        throw new YARLYParserException(String.format("Open first brakets found."));
                    }

                    if (i < tokenList.size() - 1 && tokenList.get(i + 1).type() == Token.TokenType.VEC_POS_START) {//Checks if the vec pos is from a matrix
                        currentOpenBrakets = 1;
                        ArrayList<Token> secondPosList = new ArrayList<>();
                        boolean secondBraketEndFound = false;
                        i++;
                        while (i < tokenList.size() - 1 && !secondBraketEndFound) {
                            i++;
                            currentToken = tokenList.get(i);
                            switch (currentToken.type()) {
                                case VEC_POS_START:
                                    currentOpenBrakets++;
                                    secondPosList.add(currentToken);
                                    break;
                                case VEC_POS_END:
                                    currentOpenBrakets--;
                                    if (currentOpenBrakets == 0) {//All parenthesis pairs were found
                                        secondBraketEndFound = true;
                                        if (i < tokenList.size() - 1 && tokenList.get(i + 1).type() == Token.TokenType.VEC_POS_START) {//Checks if the vec pos is three-dimension(which is invalid)
                                            throw new YARLYParserException(String.format("Invalid three-dimension vector position."));
                                        }
                                        currentSection.add(new VecPosToken(this, vecName, firstPosList, secondPosList));//adds a two-dimension vector
                                        break;
                                    } else {
                                        secondPosList.add(currentToken);
                                        break;
                                    }
                                case COMMAND_START:
                                case ID:
                                case NUMBER:
                                case STRING:
                                case SEPERATOR:
                                case VEC_POS:
                                case COMMAND_END:
                                    secondPosList.add(currentToken);
                                case COMMENT:
                                    break;
                            }
                        }
                        if (!secondBraketEndFound) {
                            throw new YARLYParserException(String.format("Open second brakets found."));
                        }
                    } else {//adds a one-dimension vec pos
                        currentSection.add(new VecPosToken(this, vecName, firstPosList));
                    }
                    break;
                case COMMENT:
                    break;
            }
        }
        sectionList.add(currentSection);
        ParsedToken possibleID;
        switch (sectionList.size()) {
            case 1://runnable command
                return new ParsedCommand(sectionList.get(0));
            case 2://possible definition command
                if (sectionList.get(0).size() != 1) {
                    throw new YARLYParserException(String.format("Invalid invalid number of tokens on first section (found %d, valid 1)", sectionList.get(0).size()));
                }
                possibleID = sectionList.get(0).get(0);
                if (!(possibleID instanceof IDToken)) {
                    throwHighlightedExeption(String.format("Invalid token type on first section (type: %s, valid type: ID)", possibleID.type()), IDToken.ID_TYPE, possibleID.content());
                }
                return new ParsedCommand(possibleID.content(), sectionList.get(1), true);
            case 3://possible runnable with var command or definition with var command
                if (sectionList.get(1).size() != 1) {
                    throw new YARLYParserException(String.format("Invalid invalid number of tokens on second section (found %d, valid 1)", sectionList.get(1).size()));
                }

                ParsedToken possibleVar;
                switch (sectionList.get(0).size()) {
                    case 0://runnable with var command
                        possibleVar = sectionList.get(1).get(0);
                        if (!(possibleVar instanceof IDToken)) {
                            throwHighlightedExeption(String.format("Invalid token type on second section (type: %s, valid type: ID)", possibleVar.type()), "", possibleVar.content());
                        }
                        return new ParsedCommand(possibleVar.content(), sectionList.get(2), false);
                    case 1://definition with var command
                        possibleVar = sectionList.get(1).get(0);
                        if (!(possibleVar instanceof IDToken)) {
                            throwHighlightedExeption(String.format("Invalid token type on second section (type: %s, valid type: ID)", possibleVar.type()), "", possibleVar.content());
                        }
                        possibleID = sectionList.get(0).get(0);
                        if (!(possibleID instanceof IDToken)) {
                            throwHighlightedExeption(String.format("Invalid token type on first section (type: %s, valid type: ID)", possibleID.type()), "", possibleID.content());
                        }
                        return new ParsedCommand(possibleID.content(), possibleVar.content(), sectionList.get(2));
                    default:
                        throw new YARLYParserException(String.format("Invalid invalid number of tokens on first section (found %d, valid 0 or 1)", 0/* sectionNumList.get(0)*/));
                }
            default://Will never happen
                return null;
        }
    }

    public ParsedCommand parseCommand(YARLLexer lexer, String source) throws YARLYParserException {
        List<Token> tokenList = lexer.analizeCommand(source);
        return this.parseCommand(tokenList);
    }
}
