/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.List;
import parser.parsedtoken.ParsedToken;

/**
 *
 * @author Ricardo
 */
public class ParsedCommand {

    public static enum ParsedCommandType {
        DEFINITION_WITH_VAR, DEFINITION, RUNNABLE_WITH_VAR, RUNNABLE
    }

    private static int printingCommands = 0;
    private ParsedCommandType type;
    private String id;
    private String var;
    private List<ParsedToken> runnableSection;

    private ParsedCommand(ParsedCommandType type, List<ParsedToken> runnableSection) {
        this.type = type;
        this.runnableSection = runnableSection;
        this.id = "";
        this.var = "";
    }

    public ParsedCommand(String id, String var, List<ParsedToken> runnableSection) {
        this(ParsedCommandType.DEFINITION_WITH_VAR, runnableSection);
        this.id = id;
        this.var = var;
    }

    public ParsedCommand(String str, List<ParsedToken> runnableSection, boolean isDefenition) {
        this(ParsedCommandType.DEFINITION, runnableSection);
        if (isDefenition) {
            this.id = str;
        } else {
            this.type = ParsedCommandType.RUNNABLE_WITH_VAR;
            this.var = str;
        }

    }

    public ParsedCommand(List<ParsedToken> runnableSection) {
        this(ParsedCommandType.RUNNABLE, runnableSection);

    }

    /**
     * @return the type
     */
    public ParsedCommandType type() {
        return type;
    }

    /**
     * @return the id
     */
    public String id() {
        return id;
    }

    /**
     * @return the var
     */
    public String var() {
        return var;
    }

    /**
     * @return the runnableSection
     */
    public List<ParsedToken> runnableSection() {
        return runnableSection;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int currentCommand = ParsedCommand.printingCommands;
        ParsedCommand.printingCommands++;
        if (currentCommand == 0) {
            sb.append("------Root-Command-Start\n");
        } else {
            for (int i = 0; i < currentCommand; i++) {
                sb.append("------");
            }
            sb.append(String.format("------Command-%d-Start\n", currentCommand));
        }

        sb.append(String.format("Type: %s\n\n", this.type));
        if (!this.id.isEmpty()) {
            sb.append(String.format("Id: %s\n\n", this.id));
        }
        if (!this.var.isEmpty()) {
            sb.append(String.format("Var: %s\n\n", this.var));
        }
        for (ParsedToken token : this.runnableSection) {
            sb.append(token.toString());
            sb.append('\n');
        }
        if (currentCommand == 0) {
            sb.append("------Root-Command-End\n");
        } else {
            for (int i = 0; i < currentCommand; i++) {
                sb.append("------");
            }
            sb.append(String.format("------Command-%d-End\n", currentCommand));
        }
        ParsedCommand.printingCommands--;
        return sb.toString();
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof ParsedCommand)) {
            return false;
        }
        ParsedCommand pc = (ParsedCommand) o;
        return this.type == pc.type && this.id.equals(pc.id) && this.var.equals(pc.var) && this.runnableSection.equals(pc.runnableSection);
    }
}
