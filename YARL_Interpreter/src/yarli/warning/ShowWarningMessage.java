/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.warning;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class ShowWarningMessage implements WarningMessageHandler {

    private YARLI interpreter;

    public ShowWarningMessage(YARLI interpreter) {
        this.interpreter = interpreter;
    }

    @Override
    public void handleWarningMessage(String message) {
        this.interpreter.showOutput(message + "\n", YARLI.WARNING_OUTPUT);
    }

    @Override
    public String toString() {
        return "ShowErrorMessage";
    }

}
