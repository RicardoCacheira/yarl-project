/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.obj_str_handler;

import yarli.YARLI;
import yarli.variable.Vector;

/**
 *
 * @author Ricardo
 */
public class VectorStrHandler implements ObjStringHandler<Vector> {

    private final YARLI interpreter;

    public VectorStrHandler(YARLI interpreter) {
        this.interpreter = interpreter;
    }

    @Override
    public String generateString(Vector vector) {
        StringBuilder sb = new StringBuilder("[");
        int lastVal = vector.size() - 1;
        for (int i = 0; i < lastVal; i++) {
            sb.append(interpreter.generateObjString(vector.get(i)));
            sb.append(", ");
        }
        if (lastVal != -1) {
            sb.append(interpreter.generateObjString(vector.get(lastVal)));
        }
        sb.append(']');
        return sb.toString();
    }

    @Override
    public Class handledObjectClass() {
        return Vector.class;
    }

}
