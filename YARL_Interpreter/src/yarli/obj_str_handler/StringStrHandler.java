/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.obj_str_handler;

/**
 *
 * @author Ricardo
 */
public class StringStrHandler implements ObjStringHandler<String> {

    @Override
    public String generateString(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case '\n':
                    sb.append("\\n");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '"':
                    sb.append("\\\"");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                default:
                    sb.append(str.charAt(i));
            }
        }
        return String.format("\"%s\"", sb.toString());
    }

    @Override
    public Class handledObjectClass() {
        return String.class;
    }

}
