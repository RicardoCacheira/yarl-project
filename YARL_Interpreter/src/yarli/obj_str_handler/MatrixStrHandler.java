/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.obj_str_handler;

import yarli.YARLI;
import yarli.variable.Matrix;

/**
 *
 * @author Ricardo
 */
public class MatrixStrHandler implements ObjStringHandler<Matrix> {

    private final YARLI interpreter;

    public MatrixStrHandler(YARLI interpreter) {
        this.interpreter = interpreter;
    }

    @Override
    public String generateString(Matrix matrix) {
        StringBuilder sb = new StringBuilder("\n");
        int xLastVal = matrix.xSize() - 1;
        int yLastVal = matrix.ySize() - 1;
        for (int i = 0; i < xLastVal; i++) {
            sb.append('[');
            for (int j = 0; j < yLastVal; j++) {
                sb.append(interpreter.generateObjString(matrix.get(i, j)));
                sb.append(", ");
            }
            if (yLastVal != -1) {
                sb.append(interpreter.generateObjString(matrix.get(i, yLastVal)));
            }

            sb.append("]\n");

        }
        sb.append('[');
        if (xLastVal != -1) {
            for (int j = 0; j < yLastVal; j++) {
                sb.append(interpreter.generateObjString(matrix.get(xLastVal, j)));
                sb.append(", ");
            }
            if (yLastVal != -1) {
                sb.append(interpreter.generateObjString(matrix.get(xLastVal, yLastVal)));
            }
        }
        sb.append(']');
        return sb.toString();
    }

    @Override
    public Class handledObjectClass() {
        return Matrix.class;
    }

}
