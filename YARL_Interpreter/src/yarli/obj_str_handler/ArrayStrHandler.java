/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.obj_str_handler;

import yarli.YARLI;
import yarli.variable.Array;

/**
 *
 * @author Ricardo
 */
public class ArrayStrHandler implements ObjStringHandler<Array> {

    private final YARLI interpreter;

    public ArrayStrHandler(YARLI interpreter) {
        this.interpreter = interpreter;
    }

    @Override
    public String generateString(Array array) {
        StringBuilder sb = new StringBuilder("[");
        int lastVal = array.size() - 1;
        for (int i = 0; i < lastVal; i++) {
            sb.append(interpreter.generateObjString(array.get(i)));
            sb.append(", ");
        }
        if (lastVal != -1) {
            sb.append(interpreter.generateObjString(array.get(lastVal)));
        }
        sb.append(']');
        return sb.toString();
    }

    @Override
    public Class handledObjectClass() {
        return Array.class;
    }

}
