/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.obj_str_handler;

/**
 *
 * @author Ricardo
 * @param <T>
 */
public interface ObjStringHandler<T> {

    public String generateString(T obj);

    public Class handledObjectClass();
}
