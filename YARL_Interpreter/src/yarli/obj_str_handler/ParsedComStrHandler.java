/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.obj_str_handler;

import java.util.List;
import parser.ParsedCommand;
import parser.parsedtoken.ParsedToken;
import parser.parsedtoken.StringToken;

/**
 *
 * @author Ricardo
 */
public class ParsedComStrHandler implements ObjStringHandler<ParsedCommand> {

    private String generateRunnableSectionStr(ParsedCommand com) {
        StringBuilder sb = new StringBuilder();
        StringStrHandler stringHandler = new StringStrHandler();
        for (ParsedToken token : com.runnableSection()) {
            sb.append(' ');
            if (token.type().equals(StringToken.STRING_TYPE)) {
                sb.append(stringHandler.generateString(token.content()));
            } else {
                sb.append(token.content());
            }
        }
        return sb.toString();
    }

    @Override
    public String generateString(ParsedCommand com) {
        switch (com.type()) {
            case RUNNABLE:
                return String.format("(%s )", this.generateRunnableSectionStr(com));
            case RUNNABLE_WITH_VAR:
                return String.format("( : %s :%s )", com.var(), this.generateRunnableSectionStr(com));
            case DEFINITION:
                return String.format("( %s :%s )", com.id(), this.generateRunnableSectionStr(com));
            case DEFINITION_WITH_VAR:
                return String.format("( %s : %s :%s )", com.id(), com.var(), this.generateRunnableSectionStr(com));
            default://Not Happening
                return null;
        }

    }

    @Override
    public Class handledObjectClass() {
        return ParsedCommand.class;
    }

}
