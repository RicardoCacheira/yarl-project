/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.obj_str_handler;

import rationalnumber.RationalNumber;
import rationalnumber.notationhandler.RationalNumberNotationHandler;

/**
 *
 * @author Ricardo
 */
public class NumberStringHandler implements ObjStringHandler<RationalNumber> {

    private final RationalNumberNotationHandler handler;

    public NumberStringHandler(RationalNumberNotationHandler handler) {
        this.handler = handler;
    }

    @Override
    public String generateString(RationalNumber number) {
        return this.handler.generateNumberStringInNotation(number);
    }

    @Override
    public Class handledObjectClass() {
        return RationalNumber.class;
    }
    
    @Override
    public String toString(){
        return this.handler.toString();
    }

}
