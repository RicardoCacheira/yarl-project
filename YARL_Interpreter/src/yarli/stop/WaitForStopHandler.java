/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.stop;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricardo
 */
public class WaitForStopHandler implements StopHandler {

    Semaphore waitSem;

    public WaitForStopHandler() {
        this.waitSem = new Semaphore(0);
    }

    public void waitForStop() {
        try {
            this.waitSem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(WaitForStopHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void executeActionOnStop() {
        this.waitSem.release();
    }

}
