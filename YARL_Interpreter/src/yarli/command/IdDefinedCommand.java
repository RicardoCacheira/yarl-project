/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.command;

import simplelib.ClonableObject;
import yarli.YARLI;
import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public abstract class IdDefinedCommand implements ClonableObject<IdDefinedCommand> {

    private String id;

    public IdDefinedCommand(String id) {
        this.id = id;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof IdDefinedCommand)) {
            return false;
        }
        IdDefinedCommand com = (IdDefinedCommand) o;
        return this.id.equals(com.id);
    }

    public abstract void run(YARLI interpreter);//Possible containers

    public String getId() {
        return this.id;
    }

    public void setId(String newName) {
        this.id = newName;
    }

    public abstract IdDefinedCommandInfo getInfo();
}
