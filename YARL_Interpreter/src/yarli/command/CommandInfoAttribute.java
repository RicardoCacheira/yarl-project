/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.command;

/**
 *
 * @author Ricardo
 */
public class CommandInfoAttribute {

    private String name;
    private String description;
    private String type;

    public CommandInfoAttribute(String name, String description, String type) {
        this.name = name;
        this.description = description;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(String.format("Name: %s\n", this.getName()));
        sb.append(String.format("Description: %s\n", this.getDescription()));
        sb.append(String.format("Type: %s\n", this.getType()));
        return sb.toString();
    }

}
