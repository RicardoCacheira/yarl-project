/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.command;

/**
 *
 * @author Ricardo
 */
public class UserDefinedCommandInfo implements IdDefinedCommandInfo {

    private String name;
    private String command;
    private boolean hasVar;
    private String var;

    private UserDefinedCommandInfo(String name, String command, boolean hasVar) {
        this.name = name;
        this.command = command;
        this.hasVar = hasVar;
    }

    public UserDefinedCommandInfo(String name, String command) {
        this(name, command, false);
    }

    public UserDefinedCommandInfo(String name, String command, String var) {
        this(name, command, true);
        this.var = var;
    }

    @Override
    public String generateInfoHTML() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        if (this.hasVar) {
            return String.format("%s : %s : %s", this.name, this.var, this.command);
        } else {
            return String.format("%s : %s", this.name, this.command);
        }
    }

}
