/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.command;

/**
 *
 * @author Ricardo
 */
public class ModuleCommandInfo implements IdDefinedCommandInfo {

    private String name;
    private String moduleName;
    private String description;
    private boolean hasReturn;
    private String returnDescription;
    private String returnType;
    private CommandInfoAttribute[] attributeList;
    private String[] noteList;

    private ModuleCommandInfo(String name, String moduleName, String description, boolean hasReturn, CommandInfoAttribute[] attributeList, String[] noteList) {
        this.name = name;
        this.moduleName = moduleName;
        this.description = description;
        this.hasReturn = hasReturn;
        this.attributeList = attributeList;
        this.noteList = noteList;
    }

    public ModuleCommandInfo(String name, String moduleName, String description, CommandInfoAttribute[] attributeList, String[] noteList) {
        this(name, moduleName, description, false, attributeList, noteList);
    }

    public ModuleCommandInfo(String name, String moduleName, String description, String returnDescription, String returnType, CommandInfoAttribute[] attributeList, String[] noteList) {
        this(name, moduleName, description, true, attributeList, noteList);
        this.returnDescription = returnDescription;
        this.returnType = returnType;
    }

    public String showFullUsage() {
        StringBuilder sb = new StringBuilder();
        if (this.attributeList.length > 0) {
            for (int i = 0; i < this.attributeList.length; i++) {
                CommandInfoAttribute aux = this.attributeList[i];
                sb.append(String.format("< %s : %s > ", aux.getName(), aux.getType()));
            }
        }
        sb.append(this.name);
        if (this.hasReturn) {
            sb.append(String.format(" -> %s", this.returnType));
        }
        return sb.toString();
    }

    public String showSimpleUsage() {
        StringBuilder sb = new StringBuilder();
        if (this.attributeList.length > 0) {
            for (int i = 0; i < this.attributeList.length; i++) {
                CommandInfoAttribute aux = this.attributeList[i];
                sb.append(String.format("< %s > ", aux.getName()));
            }
        }
        sb.append(this.name);
        if (this.hasReturn) {
            sb.append(String.format(" -> %s", this.returnType));
        }
        return sb.toString();
    }

    @Override
    public String generateInfoHTML() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(String.format("Name: %s\n", this.name));
        sb.append(String.format("Module: %s\n", this.moduleName));
        sb.append(String.format("Description: %s\n", this.description));
        if (this.hasReturn) {
            sb.append(String.format("Return: %s\n", this.returnDescription));
            sb.append(String.format("Return Type: %s\n", this.returnType));
        } else {
            sb.append("Return: None\n");
        }
        if (this.attributeList.length > 0) {
            if (this.attributeList.length > 1) {
                int last = this.attributeList.length - 1;
                sb.append("---Attributes---\n");
                for (int i = 0; i < last; i++) {
                    sb.append(this.attributeList[i].toString());
                    sb.append("------\n");
                }
                sb.append(this.attributeList[last].toString());
                sb.append("---Attributes---\n");
            } else {
                sb.append("---Attribute---\n");
                sb.append(this.attributeList[0].toString());
                sb.append("---Attribute---\n");
            }
        } else {
            sb.append("Attributes: None\n");
        }
        if (this.noteList.length > 0) {
            if (this.noteList.length > 1) {
                int last = this.noteList.length - 1;
                sb.append("---Notes---\n");
                for (int i = 0; i < last; i++) {
                    sb.append(this.noteList[i]);
                    sb.append("\n------\n");
                }
                sb.append(this.noteList[last]);
                sb.append("\n---Notes---\n");
            } else {
                sb.append("---Note---\n");
                sb.append(this.noteList[0]);
                sb.append("\n---Note---\n");
            }
        }
        sb.append(String.format("Usage: %s\n", this.showSimpleUsage()));
        return sb.toString();
    }
}
