/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.command;

import yarli.YARLI_Module;

/**
 *
 * @author Ricardo
 */
public abstract class ModuleCommand extends IdDefinedCommand {

    private YARLI_Module module;

    public ModuleCommand(String name, YARLI_Module module) {
        super(name);
        this.module = module;
    }

    public YARLI_Module getModule() {
        return this.module;
    }

}
