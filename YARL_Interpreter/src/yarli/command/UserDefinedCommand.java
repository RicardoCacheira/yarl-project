/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.command;

import java.util.List;
import parser.parsedtoken.ParsedToken;
import parser.parsedtoken.StringToken;
import yarli.YARLI;
import yarli.obj_str_handler.StringStrHandler;

/**
 *
 * @author Ricardo
 */
public class UserDefinedCommand extends IdDefinedCommand {

    private List<ParsedToken> tokenList;

    private String var;

    public UserDefinedCommand(String name, List<ParsedToken> tokenList) {
        super(name);
        this.tokenList = tokenList;
        this.var = null;
    }

    public UserDefinedCommand(String name, List<ParsedToken> tokenList, String var) {
        this(name, tokenList);
        this.var = var;
    }

    @Override
    public void run(YARLI interpreter) {
        if (interpreter.runTokenList(tokenList)) {
            if (var != null) {
                interpreter.setVariableValue(var, interpreter.pop());
            }
        }
    }

    @Override
    public IdDefinedCommandInfo getInfo() {
        StringStrHandler stringHandler=new StringStrHandler();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.tokenList.size() - 1; i++) {
            ParsedToken token = this.tokenList.get(i);
            if (token.type().equals(StringToken.STRING_TYPE)) {
                sb.append(stringHandler.generateString(token.content()));
            } else {
                sb.append(token.content());
            }
            sb.append(' ');
        }
        ParsedToken token = this.tokenList.get(this.tokenList.size() - 1);
        if (token.type().equals(StringToken.STRING_TYPE)) {
            sb.append(stringHandler.generateString(token.content()));
        } else {
            sb.append(token.content());
        }
        if (var != null) {
            return new UserDefinedCommandInfo(this.getId(), sb.toString(), var);
        }
        return new UserDefinedCommandInfo(this.getId(), sb.toString());
    }

    @Override
    public IdDefinedCommand cloneObject() {
        if (var != null) {
            return new UserDefinedCommand(this.getId(), this.tokenList, this.var);
        } else {
            return new UserDefinedCommand(this.getId(), this.tokenList);
        }
    }

}
