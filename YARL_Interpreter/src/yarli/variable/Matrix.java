/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.variable;

import rationalnumber.RationalNumber;
import simplelib.ClonableObject;

/**
 *
 * @author Ricardo
 */
public class Matrix implements ClonableObject<Matrix> {

    private int xSize;
    private int ySize;
    private Object[][] matrix;

    public Matrix(int xSize, int ySize) throws IllegalArgumentException {
        if (xSize < 1) {
            throw new IllegalArgumentException("The xSize must be a positive integer.");
        }
        if (ySize < 1) {
            throw new IllegalArgumentException("The ySize must be a positive integer.");
        }
        this.xSize = xSize;
        this.ySize = ySize;
        this.matrix = new Object[xSize][ySize];
        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < ySize; j++) {
                this.matrix[i][j] = RationalNumber.ZERO;
            }
        }
    }

    public Object get(int xPos, int yPos) throws IndexOutOfBoundsException {
        if (xPos >= 0 && xPos < this.xSize) {
            if (yPos >= 0 && yPos < this.ySize) {
                Object obj = this.matrix[xPos][yPos];
                if (obj instanceof ClonableObject) {
                    return ((ClonableObject) obj).cloneObject();
                }
                return obj;
            } else {
                throw new IndexOutOfBoundsException("Invalid Y position.");
            }
        } else {
            throw new IndexOutOfBoundsException("Invalid X position.");
        }
    }

    public void set(int xPos, int yPos, Object obj) throws IndexOutOfBoundsException {
        if (xPos >= 0 && xPos < this.xSize) {
            if (yPos >= 0 && yPos < this.ySize) {
                if (obj instanceof ClonableObject) {
                    this.matrix[xPos][yPos] = ((ClonableObject) obj).cloneObject();
                } else {
                    this.matrix[xPos][yPos] = obj;
                }
            } else {
                throw new IndexOutOfBoundsException("Invalid Y position.");
            }
        } else {
            throw new IndexOutOfBoundsException("Invalid X position.");
        }
    }

    public int xSize() {
        return this.xSize;
    }

    public int ySize() {
        return this.ySize;
    }

    public Object[][] getCopy() {
        Object[][] copy = new Object[this.xSize()][this.ySize];
        for (int i = 0; i < this.xSize(); i++) {
            for (int j = 0; j < this.ySize; j++) {
                copy[i][j] = this.get(i, j);
            }
        }
        return copy;
    }

    @Override
    public Matrix cloneObject() {
        Matrix newMatrix = new Matrix(this.xSize, this.ySize);
        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < ySize; j++) {
                newMatrix.set(i, j, this.matrix[i][j]);
            }
        }
        return newMatrix;
    }

}
