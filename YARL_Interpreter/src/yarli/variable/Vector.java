/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.variable;

import rationalnumber.RationalNumber;
import simplelib.ClonableObject;

/**
 *
 * @author Ricardo
 */
public class Vector implements ClonableObject<Vector> {

    private int size;
    private Object[] vector;

    public Vector(int size) throws IllegalArgumentException {
        if (size < 1) {
            throw new IllegalArgumentException("The size must be a positive integer.");
        }
        this.size = size;
        this.vector = new Object[size];
        for (int i = 0; i < size; i++) {
            this.vector[i] = RationalNumber.ZERO;
        }
    }

    public Object get(int pos) throws IndexOutOfBoundsException {
        if (pos >= 0 && pos < this.size) {
            Object obj = this.vector[pos];
            if (obj instanceof ClonableObject) {
                return ((ClonableObject) obj).cloneObject();
            }
            return obj;
        } else {
            throw new IndexOutOfBoundsException("Invalid position.");
        }
    }

    public void set(int pos, Object obj) throws IndexOutOfBoundsException {
        if (pos >= 0 && pos < this.size) {
            if (obj instanceof ClonableObject) {
                this.vector[pos] = ((ClonableObject) obj).cloneObject();
            } else {
                this.vector[pos] = obj;
            }
        } else {
            throw new IndexOutOfBoundsException("Invalid position.");
        }
    }

    public int size() {
        return this.size;
    }

    public Object[] getCopy() {
        Object[] copy = new Object[this.size()];
        for (int i = 0; i < copy.length; i++) {
            copy[i] = get(i);
        }
        return copy;
    }

    @Override
    public Vector cloneObject() {
        Vector newVector = new Vector(this.size);
        for (int i = 0; i < this.size; i++) {
            newVector.set(i, this.vector[i]);

        }
        return newVector;
    }

}
