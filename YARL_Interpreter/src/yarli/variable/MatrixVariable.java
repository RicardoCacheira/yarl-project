/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.variable;

/**
 *
 * @author Ricardo
 */
public class MatrixVariable extends YARLI_Variable {

    private Matrix matrix;

    public MatrixVariable(String id, Matrix matrix) {
        super(id, null, YARLI_Variable.CONSTANT);
        this.matrix = matrix;
    }

    public MatrixVariable(String id, int xSize, int ySize) throws IllegalArgumentException{
        this(id, new Matrix(xSize, ySize));
    }

    public Object getPos(int xPos, int yPos) throws IndexOutOfBoundsException {
        return this.matrix.get(xPos, yPos);
    }

    public void setPos(int xPos, int yPos, Object obj) throws IndexOutOfBoundsException {
        this.matrix.set(xPos, yPos, obj);
    }

    public int xSize() {
        return this.matrix.xSize();
    }

    public int ySize() {
        return this.matrix.ySize();
    }

    /**
     * @return the val
     */
    @Override
    public Object getValue() {
        return matrix.cloneObject();
    }

    @Override
    public YARLI_Variable cloneObject() {
        return new MatrixVariable(this.getId(), (Matrix) this.getValue());
    }
}
