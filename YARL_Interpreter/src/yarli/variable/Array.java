/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.variable;

import java.util.ArrayList;
import java.util.List;
import simplelib.ClonableObject;

/**
 *
 * @author Ricardo
 */
public class Array implements ClonableObject<Array> {

    private List<Object> array;

    public Array() {
        this.array = new ArrayList<>();
    }

    public Object get(int pos) throws IndexOutOfBoundsException {
        Object obj = this.array.get(pos);
        if (obj instanceof ClonableObject) {
            return ((ClonableObject) obj).cloneObject();
        }
        return obj;
    }

    public void set(int pos, Object obj) throws IndexOutOfBoundsException {
        if (obj instanceof ClonableObject) {
            this.array.set(pos, ((ClonableObject) obj).cloneObject());
        } else {
            this.array.set(pos, obj);
        }
    }

    public void add(Object obj) {
        if (obj instanceof ClonableObject) {
            this.array.add(((ClonableObject) obj).cloneObject());
        } else {
            this.array.add(obj);
        }
    }

    public void addAt(int pos, Object obj) throws IndexOutOfBoundsException {
        if (obj instanceof ClonableObject) {
            this.array.add(pos, ((ClonableObject) obj).cloneObject());
        } else {
            this.array.add(pos, obj);
        }
    }

    public Object remove(int pos) throws IndexOutOfBoundsException {
        return this.array.remove(pos);
    }

    public void clear() {
        this.array.clear();
    }

    public int size() {
        return this.array.size();
    }

    public Object[] getCopy() {
        Object[] copy = new Object[this.size()];
        for (int i = 0; i < copy.length; i++) {
            copy[i] = get(i);
        }
        return copy;
    }

    @Override
    public Array cloneObject() {
        Array newArray = new Array();
        for (Object obj : this.array) {
            newArray.add(obj);
        }
        return newArray;
    }

}
