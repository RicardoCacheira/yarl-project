/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.variable;

import simplelib.ClonableObject;

/**
 *
 * @author Ricardo
 */
public class YARLI_Variable implements ClonableObject<YARLI_Variable> {

    public static final boolean VARIABLE = false;
    public static final boolean CONSTANT = true;

    private String id;
    private Object value;
    private boolean isConstant;

    public YARLI_Variable(String id, Object var, boolean isConstant) {
        this.id = id;
        this.value = var;
        this.isConstant = isConstant;
    }

    /**
     * @return the name
     */
    public String getId() {
        return id;
    }

    /**
     * @return the val
     */
    public Object getValue() {
        if (value instanceof ClonableObject) {
            return ((ClonableObject) value).cloneObject();
        }
        return value;
    }

    /**
     * @param val the val to set
     */
    public void setValue(Object val) {
        if (!this.isConstant) {
            this.value = val;
        }
    }

    /**
     * @return the constant
     */
    public boolean isConstant() {
        return this.isConstant;
    }

    /**
     */
    public void setAsConstant() {
        this.isConstant = true;
    }

    @Override
    public YARLI_Variable cloneObject() {
        Object newValue;
        if (this.value instanceof ClonableObject) {
            newValue = ((ClonableObject) this.value).cloneObject();
        } else {
            newValue = this.value;
        }
        return new YARLI_Variable(this.id, newValue, this.isConstant);
    }

}
