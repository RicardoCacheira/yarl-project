/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.variable;

/**
 *
 * @author Ricardo
 */
public class NoVariableFoundException extends IllegalArgumentException {

    public NoVariableFoundException(String msn) {
        super(msn);
    }

}
