/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.variable;

/**
 *
 * @author Ricardo
 */
public class ArrayVariable extends YARLI_Variable {

    private Array array;

    public ArrayVariable(String id, Array array) {
        super(id, null, YARLI_Variable.CONSTANT);
        this.array = array;
    }

    public ArrayVariable(String id) {
        this(id, new Array());
    }

    public Object getPos(int pos) throws IndexOutOfBoundsException {
        return this.array.get(pos);
    }

    public void setPos(int pos, Object obj) throws IndexOutOfBoundsException {
        this.array.set(pos, obj);
    }

    public void add(Object obj) {
        this.array.add(obj);
    }

    public void addAt(int pos, Object obj) throws IndexOutOfBoundsException {
        this.array.addAt(pos, obj);
    }

    public Object remove(int pos) throws IndexOutOfBoundsException {
        return this.array.remove(pos);
    }

    public void clear() {
        this.array.clear();
    }

    public int size() {
        return this.array.size();
    }

    /**
     * @return the val
     */
    @Override
    public Object getValue() {
        return array.cloneObject();
    }

    @Override
    public YARLI_Variable cloneObject() {
        return new ArrayVariable(this.getId(), (Array) this.getValue());
    }
}
