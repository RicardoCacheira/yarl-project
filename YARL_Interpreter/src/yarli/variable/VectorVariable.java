/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.variable;

/**
 *
 * @author Ricardo
 */
public class VectorVariable extends YARLI_Variable {

    private Vector vector;

    public VectorVariable(String id, Vector vector) {
        super(id, null, YARLI_Variable.CONSTANT);
        this.vector = vector;
    }

    public VectorVariable(String id, int size) throws IllegalArgumentException {
        this(id, new Vector(size));
    }

    public Object getPos(int pos) throws IndexOutOfBoundsException {
        return this.vector.get(pos);
    }

    public void setPos(int pos, Object obj) throws IndexOutOfBoundsException {
        this.vector.set(pos, obj);
    }

    public int size() {
        return this.vector.size();
    }

    /**
     * @return the val
     */
    @Override
    public Object getValue() {
        return vector.cloneObject();
    }

    @Override
    public YARLI_Variable cloneObject() {
        return new VectorVariable(this.getId(), (Vector) this.getValue());
    }
}
