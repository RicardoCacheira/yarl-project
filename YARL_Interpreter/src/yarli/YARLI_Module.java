/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli;

import simplelib.ClonableObject;

/**
 * YARLI Module
 *
 * @author Ricardo
 */
public interface YARLI_Module extends ClonableObject<YARLI_Module> {

    public void loadModule(YARLI interpreter);

    public void resetModule(YARLI interpreter);
    
    public String[] commandList();

    public String moduleName();

    public String help();
}
