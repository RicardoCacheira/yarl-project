/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.error;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class CurrentBreakRevertPolicy implements ErrorPolicy {

    private final YARLI interpreter;

    public CurrentBreakRevertPolicy(YARLI interpreter) {
        this.interpreter = interpreter;
        interpreter.setAutomaticRevertPoint(YARLI.AutomaticRevertPoint.CURRENT_COMMAND);
    }

    @Override
    public void handleError() {
        interpreter.breakRevertCurrentCommand();
    }

    @Override
    public String toString() {
        return "CurrentBreakRevertPolicy";
    }

}
