/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.error;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class ContinuePolicy implements ErrorPolicy {
    
    public ContinuePolicy(YARLI interpreter) {
        interpreter.setAutomaticRevertPoint(YARLI.AutomaticRevertPoint.NO_POINT);
    }
    
    @Override
    public void handleError() {
    }
    
    @Override
    public String toString() {
        return "ContinuePolicy";
    }
    
}
