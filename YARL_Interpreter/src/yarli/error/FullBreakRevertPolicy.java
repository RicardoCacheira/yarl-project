/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.error;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class FullBreakRevertPolicy implements ErrorPolicy {

    private final YARLI interpreter;

    public FullBreakRevertPolicy(YARLI interpreter) {
        this.interpreter = interpreter;
        interpreter.setAutomaticRevertPoint(YARLI.AutomaticRevertPoint.FULL_COMMAND);
    }

    @Override
    public void handleError() {
        interpreter.breakRevertFullCommand();
    }

    @Override
    public String toString() {
        return "FullBreakRevertPolicy";
    }
}
