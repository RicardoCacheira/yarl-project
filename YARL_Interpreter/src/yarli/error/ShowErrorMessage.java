/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.error;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class ShowErrorMessage implements ErrorMessageHandler {

    private YARLI interpreter;

    public ShowErrorMessage(YARLI interpreter) {
        this.interpreter = interpreter;
    }

    @Override
    public void handleErrorMessage(String message) {
        this.interpreter.showOutput(message + "\n", YARLI.ERROR_OUTPUT);
    }

    @Override
    public String toString() {
        return "ShowErrorMessage";
    }

}
