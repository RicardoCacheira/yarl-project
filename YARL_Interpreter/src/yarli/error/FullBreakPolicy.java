/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.error;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class FullBreakPolicy implements ErrorPolicy {

    private final YARLI interpreter;

    public FullBreakPolicy(YARLI interpreter) {
        this.interpreter = interpreter;
        interpreter.setAutomaticRevertPoint(YARLI.AutomaticRevertPoint.NO_POINT);
    }

    @Override
    public void handleError() {
        interpreter.breakFullCommand();
    }

    @Override
    public String toString() {
        return "FullBreakPolicy";
    }

}
