/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.new_inst;

/**
 *
 * @author Ricardo
 */
public interface NewInstanceHandler {

    public void startNewInstance();

    public void runFileInNewInstance(String filename);
}
