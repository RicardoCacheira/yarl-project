/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.io;

/**
 *
 * @author Ricardo
 */
public interface YARLI_Output {

    public void showOutput(Object obj, int type);

    public boolean supportsHTML();
    
    public void clearOutput();

}
