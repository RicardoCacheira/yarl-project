/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.io;

import yarli.YARLI;

/**
 *
 * @author Ricardo
 */
public class CLI_Output implements YARLI_Output {

    @Override
    public void showOutput(Object obj, int type) {
        if (type != YARLI.HTML_OUTPUT) {
            System.out.print(obj);
        } else {
            System.out.print("The current output system doesn't support HTML Output.");
        }
    }

    @Override
    public boolean supportsHTML() {
        return false;
    }

    @Override
    public void clearOutput() {
    }

}
