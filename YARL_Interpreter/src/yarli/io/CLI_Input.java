/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli.io;

import java.util.Scanner;

/**
 *
 * @author Ricardo
 */
public class CLI_Input implements YARLI_Input {

    private Scanner in;

    public CLI_Input(Scanner in) {
        this.in = in;
    }

    @Override
    public String readInput() {
        System.out.print("Input: ");
        String aux = in.nextLine();
        return aux;

    }

}
