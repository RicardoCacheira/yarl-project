package yarli;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ricardo
 */
public class YARLI_Log {

    public enum LogType {
        START_INTERPRETER,
        START_RUN_INTERPRETER,
        STOP_INTERPRETER,
        STOP_INTERPRETER_COMMAND,
        START_RUN_COM,
        END_RUN_COM,
        RUN_PARSED_COM,
        RUN_ID_COM,
        START_RUN_TOKEN_LIST,
        RUN_TOKEN_ACTION,
        END_RUN_TOKEN_LIST,
        DEFINE_VAR,
        REM_VAR,
        GET_VAR,
        GET_VAR_VALUE,
        SET_VAR_VALUE,
        SET_VAR_AS_CONSTANT,
        NEW_ARRAY,
        NEW_VECTOR,
        NEW_MATRIX,
        GET_ARRAY_VEC_POS,
        GET_MATRIX_POS,
        SET_ARRAY_VEC_POS,
        SET_MATRIX_POS,
        ADD_TO_ARRAY,
        ADD_TO_ARRAY_AT,
        REM_FROM_ARRAY,
        CLEAR_ARRAY,
        ERROR,
        WARNING,
        OUTPUT,
        INPUT,
        ADD_ID_COM,
        REM_ID_COM,
        GET_ID_COM,
        SET_NEW_COM_ID,
        ADD_COM_TO_BUFFER,
        ADD_PRIORITY_COM_TO_BUFFER,
        ADD_COM_LIST_TO_BUFFER,
        ADD_PRIORITY_COM_LIST_TO_BUFFER,
        ADD_COM_TO_NEW_INST,
        ADD_PRIORITY_COM_TO_NEW_INST,
        ADD_COM_LIST_TO_NEW_INST,
        ADD_PRIORITY_COM_LIST_TO_NEW_INST,
        CLEAR_COM_BUFFER,
        ENABLE_AUTOCOM,
        DISABLE_AUTOCOM,
        REVERT_TO_LAST_POINT,
        SET_REVERT_POINT,
        LOAD_DEFAULT_VALUES,
        START_RUN_FILE_W_RETURN,
        END_RUN_FILE_W_RETURN,
        RUN_FILE_W_RETURN_ERROR,
        RUN_FILE,
        NEW_INSTANCE,
        GET_PARAMETERS,
        GET_PARAMETERS_COPY,
        FLIP,
        FLUSH,
        PEEK,
        POP,
        PUSH,
        BREAK_CURRENT_COM,
        BREAK_REVERT_CURRENT_COM,
        BREAK_FULL_COM,
        BREAK_REVERT_FULL_COM,
        ADD_MODULE,
        REM_MODULE,
        ADD_MODULE_PACK,
        LOAD_ADDED_MODULES,
        RESET_ADDED_MODULES,
        GET_MODULE
    }

    private LogType type;

    private String message;

    public YARLI_Log(LogType type, String message) {
        this.type = type;
        this.message = message;
    }

    public LogType type() {
        return this.type;
    }

    public String message() {
        return this.message;
    }

    @Override
    public String toString() {
        if (!this.message.isEmpty()) {
            return String.format("Type: %s\nMessage: %s", this.type, this.message);
        }
        return String.format("Type: %s", this.type);
    }

}
