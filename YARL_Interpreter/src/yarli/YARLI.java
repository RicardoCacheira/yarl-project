/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yarli;

import yarli.command.UserDefinedCommand;
import yarli.command.ModuleCommand;
import yarli.command.IdDefinedCommand;
import yarli.variable.NoVariableFoundException;
import yarli.variable.YARLI_Variable;
import yarli.stop.StopHandler;
import yarli.io.YARLI_Output;
import yarli.io.YARLI_Input;
import module_loader.YARLI_ModuleLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Scanner;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import parser.ParsedCommand;
import parser.Token;
import parser.YARLParser;
import parser.YARLYParserException;
import parser.YARLLexer;
import parser.parsedtoken.ParsedToken;
import rationalnumber.RationalNumber;
import rationalnumber.RationalNumberException;
import rationalnumber.notationhandler.StandartNotationHandler;
import simplelib.ClonableObject;
import yarli.YARLI_Log.LogType;
import yarli.error.ErrorMessageHandler;
import yarli.error.ErrorPolicy;
import yarli.error.FullBreakRevertPolicy;
import yarli.error.ShowErrorMessage;
import yarli.new_inst.NewInstanceHandler;
import yarli.obj_str_handler.ArrayStrHandler;
import yarli.obj_str_handler.MatrixStrHandler;
import yarli.obj_str_handler.NumberStringHandler;
import yarli.obj_str_handler.ObjStringHandler;
import yarli.obj_str_handler.ParsedComStrHandler;
import yarli.obj_str_handler.StringStrHandler;
import yarli.obj_str_handler.VectorStrHandler;
import yarli.stop.WaitForStopHandler;
import yarli.variable.Array;
import yarli.variable.ArrayVariable;
import yarli.variable.Matrix;
import yarli.variable.MatrixVariable;
import yarli.variable.Vector;
import yarli.variable.VectorVariable;
import yarli.warning.ShowWarningMessage;
import yarli.warning.WarningMessageHandler;

/**
 * Yet Another RPN Language Interpreter
 *
 * @author Ricardo
 */
public class YARLI extends Observable implements Runnable {

    public enum AutomaticRevertPoint {
        NO_POINT, CURRENT_COMMAND, FULL_COMMAND
    };

    private class RevertPoint {

        Deque<Object> stack;
        Map<String, IdDefinedCommand> commandMap;
        Map<String, YARLI_Variable> varMap;
        Map<Class, ObjStringHandler> objStringHandlerMap;
        ErrorPolicy errorPolicy;
        ErrorMessageHandler errorMsgHandler;
        WarningMessageHandler warningMsgHandler;
        AutomaticRevertPoint automaticRevertPoint;

        public RevertPoint(Deque<Object> stack,
                Map<String, IdDefinedCommand> commandMap,
                Map<String, YARLI_Variable> varMap,
                Map<Class, ObjStringHandler> ObjStringHandlerMap,
                ErrorPolicy errorPolicy,
                ErrorMessageHandler errorMsgHandler,
                WarningMessageHandler warningMsgHandler,
                AutomaticRevertPoint automaticRevertPoint) {

            this.stack = new ArrayDeque<>();
            this.commandMap = new HashMap<>();
            this.varMap = new HashMap<>();
            this.objStringHandlerMap = new HashMap<>();
            this.errorPolicy = errorPolicy;
            this.errorMsgHandler = errorMsgHandler;
            this.warningMsgHandler = warningMsgHandler;
            this.automaticRevertPoint = automaticRevertPoint;

            for (Object object : stack) {
                if (object instanceof ClonableObject) {
                    this.stack.add(((ClonableObject) object).cloneObject());
                } else {
                    this.stack.add(object);
                }
            }

            for (Entry<String, IdDefinedCommand> entry : commandMap.entrySet()) {
                this.commandMap.put(entry.getKey(), entry.getValue().cloneObject());
            }

            for (Entry<String, YARLI_Variable> entry : varMap.entrySet()) {
                this.varMap.put(entry.getKey(), entry.getValue().cloneObject());
            }

            for (Entry<Class, ObjStringHandler> entry : ObjStringHandlerMap.entrySet()) {
                this.objStringHandlerMap.put(entry.getKey(), entry.getValue());
            }

        }
    }

    public static final String STOP_COMMAND = ":::";
    public static final int PLAIN_OUTPUT = 0;
    public static final int INFO_OUTPUT = 1;
    public static final int WARNING_OUTPUT = 2;
    public static final int ERROR_OUTPUT = 3;
    public static final int HTML_OUTPUT = 4;

    private String name;
    private Semaphore runSem;
    private Semaphore bufferSem;
    private Deque<Object> stack;
    private Map<String, IdDefinedCommand> commandMap;
    private Map<String, YARLI_Variable> variableMap;
    private Map<String, YARLI_Module> addedModuleMap;
    private Map<Class, ObjStringHandler> objStringHandlerMap;
    private boolean showCommands;
    private YARLI_Input input;
    private YARLI_Output output;
    private Deque<String> commandBuffer;
    private ErrorPolicy errorPolicy;
    private ErrorMessageHandler errorMsgHandler;
    private WarningMessageHandler warningMsgHandler;
    private boolean breakCurrentCommand;
    private boolean breakFullCommand;
    private AutomaticRevertPoint automaticRevertPoint;
    private RevertPoint lastRevertPoint;
    private StopHandler stopHandler;
    private boolean hasAutoCommand;
    private ParsedCommand autoCommand;
    private NewInstanceHandler newInstHandler;
    private boolean hasRunningInstance;
    private YARLI runningInstance;

    public YARLI(String name, YARLI_Input input, YARLI_Output output, StopHandler stopHandler, NewInstanceHandler newInstHandler) {
        this.name = name;
        this.newInstHandler = newInstHandler;
        this.hasRunningInstance = false;
        this.runningInstance = null;
        this.stack = new ArrayDeque<>();
        this.commandMap = new HashMap<>();
        this.variableMap = new HashMap<>();
        this.addedModuleMap = new HashMap<>();
        this.objStringHandlerMap = new HashMap<>();
        this.input = input;
        this.output = output;
        this.runSem = new Semaphore(0);
        this.bufferSem = new Semaphore(1);
        this.commandBuffer = new ArrayDeque();
        this.breakCurrentCommand = false;
        this.breakFullCommand = false;
        this.automaticRevertPoint = AutomaticRevertPoint.NO_POINT;
        this.lastRevertPoint = null;
        this.stopHandler = stopHandler;
        this.hasAutoCommand = false;
        this.autoCommand = null;
        this.defineErrorPolicy(new FullBreakRevertPolicy(this));
        this.defineErrorMessageHandler(new ShowErrorMessage(this));
        this.defineWarningMessageHandler(new ShowWarningMessage(this));
    }

    public YARLI(String name, YARLI_Input input, YARLI_Output output, StopHandler stopHandler) {
        this(name, input, output, stopHandler, null);
    }

    /*------Modules-----------------------------------------------------------------------*/
    public void addModule(YARLI_Module module) {
        this.sendLog(LogType.ADD_MODULE, module.moduleName());
        if (this.addedModuleMap.containsKey(module.moduleName())) {
            this.throwError(String.format("Module \"%s\" already added.", module.moduleName()));
        } else {
            this.addedModuleMap.put(module.moduleName(), module);
        }
    }

    public void removeModule(String moduleName) {
        this.sendLog(LogType.REM_MODULE, moduleName);
        if (this.addedModuleMap.remove(moduleName) == null) {
            this.throwWarning(String.format("Module \"%s\" not found.", moduleName));
        }
        this.loadDefaultValues();
    }

    public void addModuleCommands(ModuleCommand[] moduleCommandList) {
        for (ModuleCommand moduleCommand : moduleCommandList) {
            this.addCommand(moduleCommand);
        }
    }

    public void addModuleList(YARLI_Module[] moduleList) {
        if (moduleList != null) {
            for (YARLI_Module module : moduleList) {
                this.addModule(module);
            }
        }
    }

    /**
     * Loads th modules (also loads libraries if receives a folder with the name
     * "lib")
     *
     * @param moduleFile
     */
    public void addModulePack(File moduleFile) {
        this.sendLog(LogType.ADD_MODULE_PACK, String.format("Module File: %s", moduleFile));
        YARLI_ModuleLoader loader = new YARLI_ModuleLoader(new URL[]{});
        if (moduleFile.isFile()) {
            YARLI_Module[] moduleList = loader.loadJarModulePack(moduleFile, this, this.showCommands);
            this.addModuleList(moduleList);
        } else if (moduleFile.isDirectory() && moduleFile.getName().equals("lib")) {
            loader.loadYARLIModuleLibs(moduleFile, this);
        }
    }

    public void loadAddedModules() {
        this.sendLog(LogType.LOAD_ADDED_MODULES);
        for (YARLI_Module module : this.addedModuleMap.values()) {
            module.loadModule(this);
        }
    }

    public void resetAddedModules() {
        this.sendLog(LogType.RESET_ADDED_MODULES);
        for (YARLI_Module module : this.addedModuleMap.values()) {
            module.resetModule(this);
        }
    }

    public void loadModulePackFolder(String moduleFolder) {
        this.showOutput("Loading Module Packs from \"" + moduleFolder + "\" folder.\n", YARLI.INFO_OUTPUT);
        File folder = new File(moduleFolder);
        if (folder.isDirectory()) {
            for (File modulePack : folder.listFiles()) {
                this.addModulePack(modulePack);
            }
        }
        this.showOutput("Loaded Module Packs from \"" + moduleFolder + "\" folder.\n", YARLI.INFO_OUTPUT);
    }

    public YARLI_Module getModule(String moduleName) {
        this.sendLog(LogType.GET_MODULE, moduleName);
        return this.addedModuleMap.get(moduleName);
    }

    /*------Commands----------------------------------------------------------------------*/
    public boolean addCommand(IdDefinedCommand command) {
        this.sendLog(LogType.ADD_ID_COM, command.getId());
        String key = command.getId().toUpperCase();
        if (!this.isIdInUse(key)) {
            this.commandMap.put(key, command);
            return true;
        }
        this.throwError(String.format("ID \"%s\" already defined for another Command/Variable", key));
        return false;
    }

    public void removeCommand(String commandId) {
        this.sendLog(LogType.REM_ID_COM, commandId);
        if (this.commandMap.remove(commandId.toUpperCase()) == null) {
            this.throwError(String.format("Command \"%s\" not defined", commandId));
        }
    }

    public boolean hasCommand(String commandId) {
        return this.commandMap.containsKey(commandId.toUpperCase());
    }

    public IdDefinedCommand getCommand(String commandId) {
        this.sendLog(LogType.GET_ID_COM, commandId);
        if (this.hasCommand(commandId)) {
            return this.commandMap.get(commandId.toUpperCase());
        } else {
            this.throwError(String.format("Variable \"%s\" not defined", commandId));
            return null;
        }
    }

    public void setNewCommandId(String currentCommandId, String newCommandId) {
        this.sendLog(LogType.SET_NEW_COM_ID, String.format("Current Id: %s, New Id: %s", currentCommandId, newCommandId));
        if (!this.hasCommand(currentCommandId)) {
            this.throwError(String.format("Command \"%s\" not defined", currentCommandId));
        } else if (this.isIdInUse(newCommandId)) {
            this.throwError(String.format("ID \"%s\" already defined for another Command/Variable", newCommandId));
        } else {
            IdDefinedCommand command = this.commandMap.remove(currentCommandId.toUpperCase());
            command.setId(newCommandId);
            this.commandMap.put(newCommandId.toUpperCase(), command);
        }
    }

    public Map<String, IdDefinedCommand> commandMap() {
        return this.commandMap;
    }

    public void resetCommandMap() {
        this.commandMap.clear();
        this.loadAddedModules();
    }

    /*------Variables---------------------------------------------------------------------*/
    private boolean defineVariable(String varId, Object value, boolean isConstant) {
        this.sendLog(LogType.DEFINE_VAR, String.format("Id: %s, Value: %s, IsConstant: %b", varId, this.generateObjString(value), isConstant));
        if (!this.isIdInUse(varId)) {
            if (this.isValidId(varId)) {
                if (isConstant) {
                    this.variableMap.put(varId.toUpperCase(), new YARLI_Variable(varId, value, YARLI_Variable.CONSTANT));
                    return true;
                } else {
                    this.variableMap.put(varId.toUpperCase(), new YARLI_Variable(varId, value, YARLI_Variable.VARIABLE));
                    return true;
                }
            } else {
                this.throwError(String.format("Invalid ID", varId));
                return false;
            }
        } else {
            this.throwError(String.format("ID \"%s\" already defined for another Command/Variable", varId));
            return false;
        }
    }

    public boolean defineVariable(String varId, Object value) {
        return this.defineVariable(varId, value, false);
    }

    public boolean defineConstant(String constId, Object value) {
        return this.defineVariable(constId, value, true);
    }

    public boolean newVariable(String varId) {
        return this.defineVariable(varId, RationalNumber.ZERO);
    }

    public boolean removeVariable(String varId) {
        this.sendLog(LogType.REM_VAR, varId);
        if (this.variableMap.remove(varId.toUpperCase()) == null) {
            this.throwWarning(String.format("Variable \"%s\" not defined", varId));
            return false;
        }
        return true;
    }

    public boolean hasVariable(String varId) {
        return this.variableMap.containsKey(varId.toUpperCase());
    }

    public YARLI_Variable getVariable(String varId) {
        this.sendLog(LogType.GET_VAR, varId);
        if (this.hasVariable(varId)) {
            return this.variableMap.get(varId.toUpperCase());
        } else {
            this.throwError(String.format("Variable \"%s\" not defined", varId));
            return null;
        }
    }

    public Object getVariableValue(String varId) throws NoVariableFoundException {
        this.sendLog(LogType.GET_VAR_VALUE, varId);
        YARLI_Variable var = this.getVariable(varId);
        if (var != null) {
            return var.getValue();
        } else {
            throw new NoVariableFoundException(String.format("Variable \"%s\" not defined", varId));
        }
    }

    public boolean setVariableValue(String varId, Object value) {
        this.sendLog(LogType.SET_VAR_VALUE, String.format("Id: %s, Value: %s", varId, this.generateObjString(value)));
        YARLI_Variable var = this.getVariable(varId);
        if (var != null) {
            if (var.isConstant()) {
                this.throwWarning(String.format("Cannot change the value of a constant", varId));
                return false;
            } else if (value instanceof Array) {
                this.removeVariable(varId);
                this.variableMap.put(varId.toUpperCase(), new ArrayVariable(varId, (Array) value));
            } else if (value instanceof Vector) {
                this.removeVariable(varId);
                this.variableMap.put(varId.toUpperCase(), new VectorVariable(varId, (Vector) value));
            } else if (value instanceof Matrix) {
                this.removeVariable(varId);
                this.variableMap.put(varId.toUpperCase(), new MatrixVariable(varId, (Matrix) value));
            } else {
                var.setValue(value);
            }
            return true;
        }
        return false;
    }

    public boolean setVariableAsConstant(String varId) {
        this.sendLog(LogType.SET_VAR_AS_CONSTANT, varId);
        YARLI_Variable var = this.getVariable(varId);
        if (var != null) {
            var.setAsConstant();
            return true;
        }
        return false;
    }

    public Map<String, YARLI_Variable> variableMap() {
        return this.variableMap;
    }

    public void resetVariableMap() {
        this.variableMap.clear();
    }

    /*------Array/Vector/Matrix-----------------------------------------------------------*/
    public void newArray(String arrayId) {
        this.sendLog(LogType.NEW_ARRAY, arrayId);
        if (!this.isIdInUse(arrayId)) {
            if (this.isValidId(arrayId)) {
                this.variableMap.put(arrayId.toUpperCase(), new ArrayVariable(arrayId));
            } else {
                this.throwError(String.format("Invalid ID", arrayId));
            }
        } else {
            this.throwError(String.format("ID \"%s\" already defined for another Command/Variable", arrayId));
        }
    }

    public void newVector(String vectorId, int size) {
        this.sendLog(LogType.NEW_VECTOR, String.format("Id: %s, Size: %s", vectorId, size));
        if (!this.isIdInUse(vectorId)) {
            if (this.isValidId(vectorId)) {
                try {
                    this.variableMap.put(vectorId.toUpperCase(), new VectorVariable(vectorId, size));
                } catch (IllegalArgumentException ex) {
                    this.throwError(ex.getMessage());
                }
            } else {
                this.throwError("Invalid ID");
            }
        } else {
            this.throwError(String.format("ID \"%s\" already defined for another Command/Variable", vectorId));
        }
    }

    public void newMatrix(String matrixId, int xSize, int ySize) {
        this.sendLog(LogType.NEW_VECTOR, String.format("Id: %s, xSize: %s, ySize: %s", matrixId, xSize, ySize));
        if (!this.isIdInUse(matrixId)) {
            if (this.isValidId(matrixId)) {
                try {
                    this.variableMap.put(matrixId.toUpperCase(), new MatrixVariable(matrixId, xSize, ySize));
                } catch (IllegalArgumentException ex) {
                    this.throwError(ex.getMessage());
                }
            } else {
                this.throwError(String.format("Invalid ID", matrixId));
            }

        } else {
            this.throwError(String.format("ID \"%s\" already defined for another Command/Variable", matrixId));
        }
    }

    public boolean hasArray(String arrayId) {
        if (this.hasVariable(arrayId)) {
            return this.variableMap.get(arrayId.toUpperCase()) instanceof ArrayVariable;
        }
        return false;
    }

    public boolean hasVector(String vectorId) {
        if (this.hasVariable(vectorId)) {
            return this.variableMap.get(vectorId.toUpperCase()) instanceof VectorVariable;
        }
        return false;
    }

    public boolean hasMatrix(String matrixId) {
        if (this.hasVariable(matrixId)) {
            return this.variableMap.get(matrixId.toUpperCase()) instanceof MatrixVariable;
        }
        return false;
    }

    public Object getArrayVectorPos(String arrayVecId, int pos) throws IndexOutOfBoundsException, NoVariableFoundException {
        this.sendLog(LogType.GET_ARRAY_VEC_POS, String.format("Id: %s, Position: %s", arrayVecId, pos));
        if (this.hasArray(arrayVecId)) {
            return ((ArrayVariable) this.getVariable(arrayVecId)).getPos(pos);
        } else if (this.hasVector(arrayVecId)) {
            return ((VectorVariable) this.getVariable(arrayVecId)).getPos(pos);
        }
        throw new NoVariableFoundException(String.format("Array/Vector \"%s\" not defined", arrayVecId));
    }

    public Object getMatrixPos(String matrixId, int xPos, int yPos) throws IndexOutOfBoundsException, NoVariableFoundException {
        this.sendLog(LogType.GET_MATRIX_POS, String.format("Id: %s, xPosition: %s, yPosition: %s", matrixId, xPos, yPos));
        if (this.hasMatrix(matrixId)) {
            return ((MatrixVariable) this.getVariable(matrixId)).getPos(xPos, yPos);
        }
        throw new NoVariableFoundException(String.format("Matrix \"%s\" not defined", matrixId));
    }

    public void setArrayVectorPos(String arrayVecId, int pos, Object obj) {
        this.sendLog(LogType.SET_ARRAY_VEC_POS, String.format("Id: %s, Position: %s, Value: %s", arrayVecId, pos, this.generateObjString(obj)));
        try {
            if (this.hasArray(arrayVecId)) {
                ((ArrayVariable) this.getVariable(arrayVecId)).setPos(pos, obj);
            } else if (this.hasVector(arrayVecId)) {
                ((VectorVariable) this.getVariable(arrayVecId)).setPos(pos, obj);
            } else {
                this.throwError(String.format("Array/Vector \"%s\" not defined", arrayVecId));
            }
        } catch (IndexOutOfBoundsException ex) {
            this.throwError("Invalid position");
        }
    }

    public void setMatrixPos(String matrixId, int xPos, int yPos, Object obj) {
        this.sendLog(LogType.SET_MATRIX_POS, String.format("Id: %s, xPosition: %s, yPosition: %s, Value: %s", matrixId, xPos, yPos, this.generateObjString(obj)));
        if (this.hasMatrix(matrixId)) {
            try {
                ((MatrixVariable) this.getVariable(matrixId)).setPos(xPos, yPos, obj);
            } catch (IndexOutOfBoundsException ex) {
                this.throwError("Invalid position");
            }
        } else {
            this.throwError(String.format("Matrix \"%s\" not defined", matrixId));
        }
    }

    public void addObjToArray(String arrayId, Object obj) {
        this.sendLog(LogType.ADD_TO_ARRAY, String.format("Id: %s, Value: %s", arrayId, this.generateObjString(obj)));
        if (this.hasArray(arrayId)) {
            ((ArrayVariable) this.getVariable(arrayId)).add(obj);
        } else {
            this.throwError(String.format("Array \"%s\" not defined", arrayId));
        }
    }

    public void addObjToArrayAt(String arrayId, int pos, Object obj) {
        this.sendLog(LogType.ADD_TO_ARRAY_AT, String.format("Id: %s, Position: %s, Value: %s", arrayId, pos, this.generateObjString(obj)));
        if (this.hasArray(arrayId)) {
            try {
                ((ArrayVariable) this.getVariable(arrayId)).addAt(pos, obj);
            } catch (IndexOutOfBoundsException ex) {
                this.throwError("Invalid position");
            }
        } else {
            this.throwError(String.format("Array \"%s\" not defined", arrayId));
        }
    }

    public Object removeObjFromArrayAt(String arrayId, int pos) throws IndexOutOfBoundsException, NoVariableFoundException {
        this.sendLog(LogType.REM_FROM_ARRAY, String.format("Id: %s, Position: %s", arrayId, pos));
        if (this.hasArray(arrayId)) {
            return ((ArrayVariable) this.getVariable(arrayId)).remove(pos);
        } else {
            throw new NoVariableFoundException(String.format("Array \"%s\" not defined", arrayId));
        }
    }

    public void clearArray(String arrayId) {
        this.sendLog(LogType.CLEAR_ARRAY, arrayId);
        if (this.hasArray(arrayId)) {
            ((ArrayVariable) this.getVariable(arrayId)).clear();
        } else {
            this.throwError(String.format("Array \"%s\" not defined", arrayId));
        }
    }

    public int getArrayVecSize(String arrayVecId) throws NoVariableFoundException {
        if (this.hasArray(arrayVecId)) {
            return ((ArrayVariable) this.getVariable(arrayVecId)).size();
        } else if (this.hasVector(arrayVecId)) {
            return ((VectorVariable) this.getVariable(arrayVecId)).size();
        } else {
            throw new NoVariableFoundException(String.format("Array/Vector \"%s\" not defined", arrayVecId));
        }
    }

    public int[] getMatrixSizes(String matrixId) throws NoVariableFoundException {
        if (this.hasMatrix(matrixId)) {
            MatrixVariable matrix = (MatrixVariable) this.getVariable(matrixId);
            return new int[]{matrix.xSize(), matrix.ySize()};
        } else {
            throw new NoVariableFoundException(String.format("Matrix \"%s\" not defined", matrixId));
        }
    }

    /*------Run---------------------------------------------------------------------------*/
    public void runIDCommand(String idCommand) {
        this.sendLog(LogType.RUN_ID_COM, idCommand);
        if (this.hasCommand(idCommand)) {
            this.commandMap.get(idCommand.toUpperCase()).run(this);
        } else {
            this.throwError(String.format("Command %s not found.", idCommand));
        }
    }

    public boolean runTokenList(List<ParsedToken> tokenList) {//Secalhar por isnto no Command
        this.sendLog(LogType.START_RUN_TOKEN_LIST);
        if (this.automaticRevertPoint == AutomaticRevertPoint.CURRENT_COMMAND) {
            this.setRevertPoint();
        }
        for (ParsedToken token : tokenList) {
            this.sendLog(LogType.RUN_TOKEN_ACTION, String.format("Type: %s, Content: %s", token.type(), token.content()));
            token.runTokenAction(this);
            if (this.breakCurrentCommand || this.breakFullCommand) {
                if (this.breakCurrentCommand) {
                    this.breakCurrentCommand = false;
                }
                this.sendLog(LogType.END_RUN_TOKEN_LIST);
                return false;
            }
        }
        this.sendLog(LogType.END_RUN_TOKEN_LIST);
        return true;
    }

    public boolean runParsedCommand(ParsedCommand parsedCommand) {
        this.sendLog(LogType.RUN_PARSED_COM, this.generateObjString(parsedCommand));
        switch (parsedCommand.type()) {
            case RUNNABLE:
                return this.runTokenList(parsedCommand.runnableSection());
            case RUNNABLE_WITH_VAR:
                if (this.runTokenList(parsedCommand.runnableSection())) {
                    if (this.hasVariable(parsedCommand.var())) {
                        Object value = this.pop();
                        if (value != null) {
                            this.variableMap.get(parsedCommand.var()).setValue(value);
                            return true;
                        } else {
                            this.throwError(String.format("Empty Stack.", parsedCommand.var()));
                            return false;
                        }
                    } else {
                        this.throwError(String.format("No variable with the id \"%s\" found.", parsedCommand.var()));
                        return false;
                    }
                } else {
                    return false;
                }
            case DEFINITION:
                return this.addCommand(new UserDefinedCommand(parsedCommand.id(), parsedCommand.runnableSection()));
            case DEFINITION_WITH_VAR:
                return this.addCommand(new UserDefinedCommand(parsedCommand.id(), parsedCommand.runnableSection(), parsedCommand.var()));
            default:
                return false;
        }
    }

    public synchronized void addCommandToBuffer(String command) {
        try {
            this.bufferSem.acquire();
            if (!this.hasRunningInstance) {
                this.sendLog(LogType.ADD_COM_TO_BUFFER, command);
                this.commandBuffer.add(command);
                this.runSem.release();
            } else {
                this.sendLog(LogType.ADD_COM_TO_NEW_INST, command);
                this.runningInstance.addCommandToBuffer(command);
            }
            this.bufferSem.release();

        } catch (InterruptedException ex) {
            System.out.println("addCommandToBuffer Error");
        }
    }

    public synchronized void addPriorityCommandToBuffer(String command) {
        try {
            this.bufferSem.acquire();
            if (!this.hasRunningInstance) {
                this.sendLog(LogType.ADD_PRIORITY_COM_TO_BUFFER, command);
                this.commandBuffer.addFirst(command);
                this.runSem.release();
            } else {
                this.sendLog(LogType.ADD_PRIORITY_COM_TO_NEW_INST, command);
                this.runningInstance.addPriorityCommandToBuffer(command);
            }
            this.bufferSem.release();
        } catch (InterruptedException ex) {
            System.out.println("addPriorityCommandToBuffer Error");
        }
    }

    public synchronized void addCommandListToBuffer(String[] commandList) {
        try {
            this.bufferSem.acquire();
            if (!this.hasRunningInstance) {
                this.sendLog(LogType.ADD_COM_LIST_TO_BUFFER);
                for (String command : commandList) {
                    this.sendLog(LogType.ADD_COM_TO_BUFFER, command);
                    this.commandBuffer.add(command);
                    this.runSem.release();
                }
            } else {
                this.sendLog(LogType.ADD_COM_LIST_TO_NEW_INST);
                this.runningInstance.addCommandListToBuffer(commandList);
            }
            this.bufferSem.release();

        } catch (InterruptedException ex) {
            System.out.println("addCommandToBuffer Error");
        }
    }

    public synchronized void addPriorityCommandListToBuffer(String[] commandList) {
        try {
            this.bufferSem.acquire();
            if (!this.hasRunningInstance) {
                this.sendLog(LogType.ADD_PRIORITY_COM_LIST_TO_BUFFER);
                for (int i = commandList.length - 1; i >= 0; i++) {
                    this.sendLog(LogType.ADD_PRIORITY_COM_TO_BUFFER, commandList[i]);
                    this.commandBuffer.addFirst(commandList[i]);
                    this.runSem.release();
                }

            } else {
                this.sendLog(LogType.ADD_PRIORITY_COM_LIST_TO_NEW_INST);
                this.runningInstance.addPriorityCommandListToBuffer(commandList);
            }
            this.bufferSem.release();
        } catch (InterruptedException ex) {
            System.out.println("addPriorityCommandToBuffer Error");
        }
    }

    public void breakCurrentCommand() {
        this.sendLog(LogType.BREAK_CURRENT_COM);
        this.breakCurrentCommand = true;
    }

    public void breakRevertCurrentCommand() {
        this.sendLog(LogType.BREAK_REVERT_CURRENT_COM);
        this.breakCurrentCommand = true;
        this.revertToLastPoint();
    }

    public void breakFullCommand() {
        this.sendLog(LogType.BREAK_FULL_COM);
        this.breakFullCommand = true;
    }

    public void breakRevertFullCommand() {
        this.sendLog(LogType.BREAK_REVERT_FULL_COM);
        this.breakFullCommand = true;
        this.revertToLastPoint();
    }

    /**
     * Stops the running of the interpreter even if the buffer still has
     * commands left Sends the stop command to the top of the command buffer
     */
    public void stop() {
        this.sendLog(LogType.STOP_INTERPRETER_COMMAND);
        this.breakFullCommand();
        this.addPriorityCommandToBuffer(STOP_COMMAND);
    }

    public void run() {
        this.sendLog(LogType.START_INTERPRETER);
        this.loadDefaultValues();
        YARLLexer lexer = new YARLLexer();
        YARLParser parser = new YARLParser();
        this.breakFullCommand = false;
        boolean isRunning = true;
        this.sendLog(LogType.START_RUN_INTERPRETER);
        while (isRunning) {
            try {
                this.runSem.acquire();
                this.bufferSem.acquire();
                String command = this.commandBuffer.removeFirst();
                this.bufferSem.release();
                if (!command.equals(YARLI.STOP_COMMAND)) {
                    this.sendLog(LogType.START_RUN_COM, command);
                    ParsedCommand parsedCommand = parser.parseCommand(lexer, command);
                    if (this.automaticRevertPoint == AutomaticRevertPoint.FULL_COMMAND) {
                        this.setRevertPoint();
                    }
                    this.runParsedCommand(parsedCommand);
                    if (this.hasAutoCommand) {
                        //If the auto command breaks, then it will be disabled.
                        if (!this.runParsedCommand(this.autoCommand)) {
                            this.disableAutoCommand();
                            this.throwWarning("The Auto Command was disabled due to a break.");
                        }
                    }
                    this.sendLog(LogType.END_RUN_COM, command);
                    if (this.breakFullCommand) {
                        this.breakFullCommand = false;
                    }
                } else {
                    isRunning = false;
                }
            } catch (YARLYParserException ex) {
                this.showOutput(ex.getMessage(), YARLI.ERROR_OUTPUT);
                this.bufferSem.release();
            } catch (InterruptedException ex) {
                Logger.getLogger(YARLI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.sendLog(LogType.STOP_INTERPRETER);
        this.stopHandler.executeActionOnStop();
    }

    /*------Stack-------------------------------------------------------------------------*/
    public void push(Object o) {
        this.sendLog(LogType.PUSH, this.generateObjString(o));
        this.stack.push(o);
    }

    public Object pop() {
        this.sendLog(LogType.POP);
        if (!this.stack.isEmpty()) {
            return this.stack.pop();
        }
        this.throwError("Empty Stack.");
        return null;
    }

    public Object peek() {
        this.sendLog(LogType.PEEK);
        if (!this.stack.isEmpty()) {
            return this.stack.peek();
        }
        this.throwError("Empty Stack.");
        return null;
    }

    public void flush() {
        this.sendLog(LogType.FLUSH);
        this.stack.clear();
    }

    public void flip() {
        this.sendLog(LogType.FLIP);

        if (this.stack.size() >= 2) {
            Object a = pop();
            Object b = pop();
            this.push(a);
            this.push(b);
        } else {
            this.throwError("Not enough values to flip");
        }
    }

    public int stackSize() {
        return this.stack.size();
    }

    public Object[] stackDump() {
        Object[] stackDump = this.stack.toArray();
        return stackDump;
    }

    /**
     * Pops the needed parameters from the stack
     *
     * @param paramterNum
     * @param commandName
     * @return
     */
    public Object[] getParameters(int paramterNum, String commandName) {
        this.sendLog(LogType.GET_PARAMETERS, String.format("Command Name: %s, ParameterNum: %s", commandName, paramterNum));
        if (this.stack.size() < paramterNum) {
            this.throwError(String.format("Not enough parameters for command \"%s\"", commandName));
            return null;
        } else {
            Object[] parameters = new Object[paramterNum];
            for (int i = paramterNum - 1; i >= 0; i--) {
                parameters[i] = this.pop();
            }
            return parameters;
        }
    }

    /**
     * Copies the needed parameters from the stack
     *
     * @param paramterNum
     * @param commandName
     * @return
     */
    public Object[] getParametersCopy(int paramterNum, String commandName) {
        this.sendLog(LogType.GET_PARAMETERS_COPY, String.format("Command Name: %s, ParameterNum: %s", commandName, paramterNum));
        if (this.stack.size() < paramterNum) {
            this.throwError(String.format("Not enough parameters for command \"%s\"", commandName));
            return null;
        } else {
            Object[] parameters = new Object[paramterNum];
            for (int i = paramterNum - 1; i >= 0; i--) {
                parameters[i] = this.pop();
            }
            for (Object parameter : parameters) {
                if (parameter instanceof ClonableObject) {
                    this.stack.push(((ClonableObject) parameter).cloneObject());
                } else {
                    this.stack.push(parameter);
                }
            }
            return parameters;
        }
    }

    /*------I/O---------------------------------------------------------------------------*/
    public void registerInput(YARLI_Input input) {
        this.input = input;
    }

    public void registerOutput(YARLI_Output output) {
        this.output = output;
    }

    public String readInput() {//Rever este metodo
        String aux = this.input.readInput();
        this.sendLog(LogType.INPUT, aux);
        return aux;
    }

    public void showOutput(Object obj, int type) {
        String typeStr;
        switch (type) {
            case PLAIN_OUTPUT:
                typeStr = "Plain Output";
                break;
            case INFO_OUTPUT:
                typeStr = "Info Output";
                break;
            case WARNING_OUTPUT:
                typeStr = "Warning Output";
                break;
            case ERROR_OUTPUT:
                typeStr = "Error Output";
                break;
            case HTML_OUTPUT:
                typeStr = "Html Output";
                break;
            default:
                typeStr = "" + type;
        }
        this.sendLog(LogType.OUTPUT, String.format("Output: %s, Type: %s", obj, typeStr));
        this.output.showOutput(obj, type);
    }

    public void clearOutput() {
        this.output.clearOutput();
    }

    public void addObjStringHandler(ObjStringHandler handler) {
        if (this.objStringHandlerMap.containsKey(handler.handledObjectClass())) {
            this.objStringHandlerMap.remove(handler.handledObjectClass());
        }
        this.objStringHandlerMap.put(handler.handledObjectClass(), handler);
    }

    public String generateObjString(Object obj) {
        if (this.objStringHandlerMap.containsKey(obj.getClass())) {
            return this.objStringHandlerMap.get(obj.getClass()).generateString(obj);
        } else {
            return obj.toString();
        }
    }

    private void loadDefaultObjStringHandlers() {
        this.addObjStringHandler(new StringStrHandler());
        this.addObjStringHandler(new NumberStringHandler(new StandartNotationHandler()));
        this.addObjStringHandler(new ArrayStrHandler(this));
        this.addObjStringHandler(new VectorStrHandler(this));
        this.addObjStringHandler(new MatrixStrHandler(this));
        this.addObjStringHandler(new ParsedComStrHandler());
    }

    /*------New_Instances-----------------------------------------------------------------*/
    public YARLI createNewInstance(String name, YARLI_Input input, YARLI_Output output, StopHandler stopHandler, NewInstanceHandler newInstHandler) {
        YARLI newInst = new YARLI(name, input, output, stopHandler, newInstHandler);
        for (Entry<String, YARLI_Module> entry : this.addedModuleMap.entrySet()) {
            newInst.addModule(entry.getValue().cloneObject());
        }
        return newInst;
    }

    public void runFile(String filename) {
        this.sendLog(LogType.RUN_FILE);
        if (this.newInstHandler != null) {
            this.newInstHandler.runFileInNewInstance(filename);
        } else {
            this.runFileWithReturn(filename);
            this.pop();
        }
    }

    public void runFileWithReturn(String filename) {
        try {
            this.sendLog(LogType.START_RUN_FILE_W_RETURN, String.format("Filemame: %s", filename));
            WaitForStopHandler newInstStopHandler = new WaitForStopHandler();
            YARLI newInst = this.createNewInstance(this.name + " - " + filename, this.input, this.output, newInstStopHandler, this.newInstHandler);
            new Thread(newInst).start();
            Scanner in = new Scanner(new File(filename));
            while (in.hasNextLine()) {
                newInst.addCommandToBuffer(in.nextLine());
            }
            in.close();
            //Sends the stop command to the end of the command buffer
            newInst.addCommandToBuffer(YARLI.STOP_COMMAND);
            newInstStopHandler.waitForStop();
            if (newInst.stack.size() > 0) {
                this.push(newInst.pop());
            } else {
                this.push("Empty Stack");
            }
            this.sendLog(LogType.END_RUN_FILE_W_RETURN, String.format("Filemame: %s", filename));
        } catch (FileNotFoundException ex) {
            this.sendLog(LogType.RUN_FILE_W_RETURN_ERROR, String.format("Filemame: %s", filename));
            this.throwError(String.format("File \"%s\" not found.", filename));
        }
    }

    public void startNewInstance() {
        this.sendLog(LogType.NEW_INSTANCE);
        if (this.newInstHandler != null) {
            this.newInstHandler.startNewInstance();
        } else if (!this.hasRunningInstance) {
            this.hasRunningInstance = true;
            WaitForStopHandler newInstStopHandler = new WaitForStopHandler();
            this.runningInstance = this.createNewInstance(this.name + " - New Instance", this.input, this.output, newInstStopHandler, this.newInstHandler);
            new Thread(this.runningInstance).start();
            newInstStopHandler.waitForStop();
            this.hasRunningInstance = false;
        }
    }

    /*------Interpreter_Properties--------------------------------------------------------*/
    public boolean isIdInUse(String newId) {
        return this.hasCommand(newId) || this.hasVariable(newId);
    }

    public boolean isValidId(String id) {
        YARLLexer lexer = new YARLLexer();
        if (id.equals(id.trim())) {
            List<Token> result = lexer.analizeCommand(id);
            return (result.size() == 1) && (result.get(0).type() == Token.TokenType.ID);
        }
        return false;
    }

    public synchronized void clearCommandBuffer() {
        try {
            this.bufferSem.acquire();
            this.commandBuffer.clear();
            this.sendLog(LogType.CLEAR_COM_BUFFER);
            this.bufferSem.release();
        } catch (InterruptedException ex) {
            System.out.println("clearCommandBuffer Error");
        }
    }

    public void loadDefaultValues() {
        this.sendLog(LogType.LOAD_DEFAULT_VALUES);
        this.breakFullCommand = true;//Breaks all the current commands
        this.hasAutoCommand = false;
        this.autoCommand = null;
        this.objStringHandlerMap.clear();
        this.resetCommandMap();
        this.resetVariableMap();
        this.flush();
        this.defineErrorPolicy(new FullBreakRevertPolicy(this));
        this.defineErrorMessageHandler(new ShowErrorMessage(this));
        this.defineWarningMessageHandler(new ShowWarningMessage(this));
        this.loadDefaultObjStringHandlers();
        this.setRevertPoint();
    }

    public void setAutomaticRevertPoint(AutomaticRevertPoint automaticRevertPoint) {
        this.automaticRevertPoint = automaticRevertPoint;
    }

    public void setRevertPoint() {
        this.sendLog(LogType.SET_REVERT_POINT);
        this.lastRevertPoint = new RevertPoint(this.stack, this.commandMap, this.variableMap, this.objStringHandlerMap, this.errorPolicy, this.errorMsgHandler, this.warningMsgHandler, this.automaticRevertPoint);

    }

    public void revertToLastPoint() {
        if (this.lastRevertPoint != null) {
            this.sendLog(LogType.REVERT_TO_LAST_POINT);
            this.stack = this.lastRevertPoint.stack;
            this.commandMap = this.lastRevertPoint.commandMap;
            this.variableMap = this.lastRevertPoint.varMap;
            this.objStringHandlerMap = this.lastRevertPoint.objStringHandlerMap;
            this.errorPolicy = this.lastRevertPoint.errorPolicy;
            this.errorMsgHandler = this.lastRevertPoint.errorMsgHandler;
            this.warningMsgHandler = this.lastRevertPoint.warningMsgHandler;
            this.automaticRevertPoint = this.lastRevertPoint.automaticRevertPoint;
        }
    }

    public void enableAutoCommand(ParsedCommand autoCommand) {
        this.sendLog(LogType.ENABLE_AUTOCOM, this.generateObjString(autoCommand));
        if (autoCommand.type() == ParsedCommand.ParsedCommandType.RUNNABLE || autoCommand.type() == ParsedCommand.ParsedCommandType.RUNNABLE_WITH_VAR) {
            this.hasAutoCommand = true;
            this.autoCommand = autoCommand;
        } else {
            this.throwError("The Auto Command must be of the type Runnable/Runnable With Var.");
        }
    }

    public void disableAutoCommand() {
        this.sendLog(LogType.DISABLE_AUTOCOM, this.generateObjString(this.autoCommand));
        this.hasAutoCommand = false;
        this.autoCommand = null;
    }

    public boolean supportsHTML() {
        return this.output.supportsHTML();
    }

    /*------Error_Handling----------------------------------------------------------------*/
    public void throwError(String message) {
        this.sendLog(LogType.ERROR, message);
        this.errorMsgHandler.handleErrorMessage(message);
        this.errorPolicy.handleError();
    }

    public void defineErrorPolicy(ErrorPolicy errorPolicy) {
        this.errorPolicy = errorPolicy;
    }

    public void defineErrorMessageHandler(ErrorMessageHandler errorMsgHandler) {
        this.errorMsgHandler = errorMsgHandler;
    }

    /*------Warning_Handling--------------------------------------------------------------*/
    public void throwWarning(String message) {
        this.sendLog(LogType.WARNING, message);
        this.warningMsgHandler.handleWarningMessage(message);

    }

    public void defineWarningMessageHandler(WarningMessageHandler warningMsgHandler) {
        this.warningMsgHandler = warningMsgHandler;
    }

    /*------xxxxxx------------------------------------------------------------------------*/
    /**
     *
     * @param number
     * @return If the number in not a valid position or size returns -1 else
     * returns the correspondent int
     */
    public int generateNonNegativeInt(RationalNumber number) {
        if (number.isAnInteger()) {
            try {
                int num = number.toInt();
                if (num >= 0) {
                    return num;
                }
            } catch (RationalNumberException ex) {
            }
        }
        return -1;
    }

    public boolean isInBreak() {
        return (this.breakCurrentCommand || this.breakFullCommand);
    }

    private void sendLog(LogType type) {
        this.sendLog(type, "");
    }

    private void sendLog(LogType type, String msn) {
        super.setChanged();
        super.notifyObservers(new YARLI_Log(type, msn));
    }

    public String getName() {
        return this.name;
    }

}
