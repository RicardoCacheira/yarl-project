/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;
import java.util.concurrent.Semaphore;
import javax.swing.JOptionPane;
import yarli.io.CLI_Input;
import yarli.io.CLI_Output;
import yarli.io.YARLI_Input;
import yarli.io.YARLI_Output;
import yarli.YARLI;
import yarli.YARLI_Log;
import yarli.stop.SystemExitOnStop;

/**
 * Yet Another RPN Language
 *
 * @author Ricardo
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner in = new Scanner(System.in);
        Semaphore sem = new Semaphore(0);
        YARLI_Input input = new CLI_Input(in);
        YARLI_Output output = new CLI_Output();
        YARLI interpreter = new YARLI("YARLI",input, output, new SystemExitOnStop());
        interpreter.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object o1) {
                if (((YARLI_Log) o1).type() == YARLI_Log.LogType.END_RUN_COM) {
                    sem.release();
                }
            }
        });
        interpreter.loadModulePackFolder("modules");
        Thread t = new Thread(interpreter);
        t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable thrwbl) {
                JOptionPane.showMessageDialog(null, thrwbl.getMessage(), "ERROR!", JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
        });
        t.start();
        String opc;
        System.out.println("YARLI");
        do {
            System.out.print("Command: ");
            opc = in.nextLine();
            interpreter.addCommandToBuffer(opc);
            sem.acquire();
        } while (true);
        //in.close();
    }
}
